import React from 'react';
import AuthLogin from "../../Utils/AuthLogin";
import Link from "next/link";
const auth = new AuthLogin();
const dataFields = [
    {
        persona_id: 0,
        activo: 0,
        apellidos: "",
        comision: "",
        persona_id: 0,
        corporacion_id: 0,
        cuatrienio_id: 0,
        nombres: "",
        partido: "",
        partido_imagen: "",
        persona_imagen: "",
    }
];
const SquareCards = ({ data = Object.assign({}, dataFields), handler, pathImgOrigen = "", defaultImage = "", handlerForLoadModal, disableBGPhoto=false, esModal = false, targetModal = "" ,pageSize = 8, bootstrapClasses = "col-lg-3 col-md-4 col-sm-6", pageExtends = 1, pageSizeExtends = 5, totalRows = 0, onClickLinks = null }) => {
    const handleFocus = (event) => event.target.select();
    return (
        <>
            <div className="SquareCardsContainer">
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select className="form-control"
                        value={pageSize}
                        onChange={async (e) => {
                            let pageSizeT = Number(e.target.value);
                            pageExtends = 1;
                            await handler(pageExtends, pageSizeT);
                        }}
                    >
                        {[8, 24, 64, 112, 500].map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
                <div className="SquareCards integrantesContainer four-columns">
                    {
                        data.map((x, i) => {
                            let params = auth.filterStringForURL(`${x.nombres} ${x.apellidos}`)
                            return (
                                <div key={i} className={``}>
                                    {
                                        esModal ? 
                                        <a href={`/`} data-bs-toggle="modal" data-bs-target={targetModal} onClick={()=>{handlerForLoadModal(x)}}>
                                            <div className="integrante">
                                                <div className="topInfo" style={{ backgroundImage: `url('${x.partido_imagen !== "" && !disableBGPhoto ? pathImgOrigen + x.partido_imagen : defaultImage}')` }}></div>
                                                <div className="photo">
                                                    <img src={x.persona_imagen !== "" ? pathImgOrigen + x.persona_imagen : defaultImage} alt={x.nombres} />
                                                </div>
                                                <div className="bottomInfo">
                                                    <div className="tagDescriptions no-icons text-center">
                                                        <ul>
                                                            <li><strong>{x.nombres || ''} {x.apellidos || ''}</strong></li>
                                                            <li>{x.partido || ''}</li>
                                                            <li>Comisión: {x.comision || ''}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        :
                                        
                                            <a href={`/congresistas/perfil/${params}/${x.persona_id}`} onClick={()=>{ if(onClickLinks !== null) onClickLinks(); }}>
                                                <div className="integrante">
                                                    <div className="topInfo" style={{ backgroundImage: `url('${x.partido_imagen !== "" && !disableBGPhoto ? pathImgOrigen + x.partido_imagen : defaultImage}')` }}>
                                                    {
                                                        x.activo === 0 ?
                                                        <div className="icon danger">
                                                            <i className="fas fa-arrow-alt-circle-down"></i>
                                                        </div>
                                                        : ""
                                                    }
                                                    </div>
                                                    <div className="photo">
                                                        <img src={x.persona_imagen !== "" ? pathImgOrigen + x.persona_imagen : defaultImage} alt={`${x.nombres} ${x.apellidos}`} />
                                                    </div>
                                                
                                                    
                                                    <div className="bottomInfo">
                                                        <div className="tagDescriptions no-icons text-center">
                                                            <ul>
                                                                <li><h4><strong>{x.apellidos || ''} {x.nombres || ''}</strong></h4></li>
                                                                <li>{x.partido || 'Sin partido'}</li>
                                                                <li>Comisión: {x.comision || 'Sin comisión'}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                    }
                                    
                                </div>
                            );
                        })
                    }
                </div>
                <div className="pagination">
                    <div>
                        <span>
                            Página{" "}
                            <strong>
                                {pageExtends} de {Math.ceil(totalRows / pageSize)}
                            </strong>{" (Mostrando "}{data.length}{" registros)"}
                        </span>
                    </div>
                    {/* <div>
                        <button onClick={async (e) => await handler(1, pageSize)} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends > 1) {
                                pageExtends--;
                                await handler(pageExtends, pageSize);
                            }

                        }} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <span>
                            <input
                                type="number"
                                value={pageExtends || ''}
                                className="form-control"
                                onFocus={handleFocus}
                                onClick={handleFocus}
                                onChange={async (e) => {
                                    if (e.target.value > 0) {
                                        pageExtends = Number(e.target.value);
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                    else  if (e.target.value === 0){
                                        e.target.value = 1;
                                        pageExtends = 1;
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                }}
                                style={{ width: "150px", textAlign: "center", display: "inline-block" }}
                            />
                            {" / " + Math.ceil(totalRows / pageSize)}
                        </span>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends < Math.ceil(totalRows / pageSize)) {
                                pageExtends++;
                                await handler(pageExtends, pageSize);
                            }
                        }} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                        <button onClick={async (e) => await handler(Math.ceil(totalRows / pageSize), pageSize)} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                    </div> */}
                </div>
            </div>
        </>
    );
}

export default SquareCards;

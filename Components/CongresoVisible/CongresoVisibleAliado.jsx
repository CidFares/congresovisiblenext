import React from 'react';
import { useCvAliados } from "../../Hooks/UseCvAliados.js";
import Link from 'next/link'
import { URLBase } from "../../Constants/Constantes.js";
import AuthLogin from "../../Utils/AuthLogin";

const auth = new AuthLogin();

let pos = { top: 0, left: 0, x: 0, y: 0 };
const CongresoVisibleAliado = () => {

    const { state } = useCvAliados();
    const mouseDownHandler = function (e) {

        pos = {
            left: e.currentTarget.scrollLeft,
            top: e.currentTarget.scrollTop,
            // Get the current mouse position
            x: e.clientX,
            y: e.clientY,
        };

        e.currentTarget.addEventListener('mousemove', mouseMoveHandler);
        e.currentTarget.addEventListener('mouseup', mouseUpHandler);
    };
    const mouseMoveHandler = function (e) {
        // How far the mouse has been moved
        const dx = e.clientX - pos.x;
        const dy = e.clientY - pos.y;

        // Scroll the element
        e.currentTarget.scrollTop = pos.top - dy;
        e.currentTarget.scrollLeft = pos.left - dx;
    };

    const mouseUpHandler = function (e, curul) {
        e.currentTarget.removeEventListener('mousemove', mouseMoveHandler);
        e.currentTarget.removeEventListener('mouseup', mouseUpHandler);
    };

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="branding">
                            {
                                state !== null && state !== "" ?
                                    state.map(x =>
                                        <figure key={x.id}>
                                            <a href={x.urlexterna} rel="noreferrer" target="_blank">
                                                <img draggable="false" src={auth.pathApi() + x.aliado_imagen[0].imagen} alt={x.nombre} />
                                            </a>
                                        </figure>
                                    ) : ''
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default CongresoVisibleAliado

import React from 'react';
import AuthLogin from "../../Utils/AuthLogin";

// Const
const auth = new AuthLogin();

const ContenidoMultimediaList = ({ data, propiedades, params = [], link = "#", className= "", handlerPagination, handlerForLoadModal, esModal = false, targetModal = "", pathImgOrigen = "", defaultImage = "", pageSize = 8, pageExtends = 1, totalRows = 0 }) => {
    const handleFocus = (event) => event.target.select();
    return (
        <>
        <style jsx>
        {`
            .CMList .CMItem{
                margin: 10px 0;
                border-radius: 10px;
                padding: 10px 15px;
                padding-right: 95px;
                position: relative;
                min-height: 95px;
            }

            .CMList .CMItem:nth-child(odd){
                background-color: var(--bg-color-odd-evento);
            }
            .CMList .CMItem:nth-child(even){
                background-color: var(--bg-color-even-evento);
            }
            .CMList .CMItem .title strong{
                display: inline-block;
            }
            .CMList .CMItem .action{
                width: 75px;
                height: 75px;
                background-color: #fff;
                text-align: center;
                border-radius: 5px;
                margin: 7px;
                color: #fff;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                box-shadow: 1px 2px 2px 0px rgba(0,0,0,.3);
                border: 1px solid var(--bg-header);
                color: var(--bg-header);
                position: absolute;
                right: 0;
                align-self: center;
                bottom: 0;
            }
            .CMList .CMItem .action p{
                margin: 0;
                font-size: .7em
            }
            .CMList .CMItem .action :first-child{
                font-size: 1.7em;
            }
            .CMList .CMItem .action :first-child{
                margin-bottom: 10px;
            }
            .CMList .CMItem .action p{
                margin-top: -10px
            }
            .CMList .CMItem > :first-child{
                min-width: 280px;
            }
            .CMList .CMItem > *:not(a){
                width: calc(100%);
                margin: 0 15px;
                font-size: .9em;
            }
            /* End contenido multimedia */

            .CMList .CMItem{
                padding-bottom: 30px;
                margin: 20px 10px !important;
                border: 1px solid #ccc;
            }

            .CMList .CMItem .title.fixImg {
                position: absolute;
                left: 0;
                bottom: -30px;
                width: auto;
                display: flex;
                align-items: center;
                z-index: 5;
            }

            .CMList .CMItem .title.fixImg .text-center {
                background-color: #fff;
                border: 1px solid #ccc;
                padding: 3px 9px;
                border-radius: 5px;
                position: relative;
                padding-left: 16px;
                left: -10px;
                z-index: -1;
                bottom: -12px;
                max-width: 250px;
                font-family: var(--font-condensed)
            }

            @media only screen and (max-width: 1280px) and (min-width: 300px){
                .CMList .CMItem{
                    padding-right: 15px;
                }
                .CMList .CMItem .action{
                    bottom: -20px;
                }
            }
        `}
        </style>
            <div className={`ContenidoMultimediaContainer ${className}`}>
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select className="form-control"
                        value={pageSize}
                        onChange={async (e) => {
                            let pageSizeT = Number(e.target.value);
                            pageExtends = 1;
                            await handlerPagination(pageExtends, pageSizeT);
                        }}
                    >
                        {[18, 36, 60, 100, 200, 500].map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
                <div className="ContenidoMultimedia CMList two-columns">
                {
                    data.map((item, i) => {
                        // Ejemplo de propiedades.description
                        /*
                        {
                            title: "", text: "", esImg: false, img: "ruta de imagen", putOnFirstElement: false (pone el atributo text como parte del texto del primero elemento)
                        }

                        */
                        let elemento = {
                            description: propiedades.description,
                            generalIcon: propiedades.generalIcon,
                            eachIcon: getObjectValueByString(propiedades.eachIcon, item),
                            generalActionTitle: propiedades.generalActionTitle,
                            eachActionTitle: getObjectValueByString(propiedades.eachActionTitle, item),
                            data: item
                        }
                        let putOnFirstElementDescriptions = elemento.description.filter((h)=>{return h.putOnFirstElement === true});
                        let putOnFirstElementString = ``;
                        putOnFirstElementDescriptions.forEach((x,k)=> {
                            putOnFirstElementString += ` - ${getObjectValueByString(x.text, elemento.data)}`;
                        })
                        let str = "";
                        if (params.length > 0) {
                            params.forEach(p => {
                                if(p.includes("titulo")){
                                    let param = auth.filterStringForURL(item[p])
                                    str += `/${param}`;
                                }else{
                                    str += `/${item[p] === null ? "" : item[p]}`;
                                }
                            });
                        }
                        let href = link + str;
                        if (link != "#") {
                            return (
                                <div key={i} className="CMItem">
                                    {elemento.description.map((val, j) => {
                                        return (
                                            <div key={j} className={`title ${val.putOnFirstElement ? "none" : ""} ${val.esImg ? "fixImg" : ""}`}>
                                                {
                                                    val.esImg ?
                                                    <>
                                                        <div className="photo avatar center-block">
                                                            <img src={val.img !== "" ? (getObjectValueByString(val.img, elemento.data) !== "Na" ? pathImgOrigen + getObjectValueByString(val.img, elemento.data) : defaultImage) : defaultImage} alt=""/>
                                                        </div>
                                                        <p className={val.esImg ? "text-center" : ""}>{getObjectValueByString(val.text, elemento.data) === null
                                                                || getObjectValueByString(val.text, elemento.data) === ""
                                                                || getObjectValueByString(val.text, elemento.data) === undefined
                                                                ? "No disponible" : getObjectValueByString(val.text, elemento.data)} {" "}
                                                            {
                                                                j === 0 ? // Para poner los elementos sobre el texto del primero
                                                                <strong className="font-condensed-it">{putOnFirstElementString}</strong>
                                                                :
                                                                ""
                                                            }
                                                        </p>
                                                    </>
                                                     :
                                                    <strong>{val.title}:</strong>
                                                }
                                                {
                                                    !val.esImg ?
                                                    <p className={val.esImg ? "text-center" : ""}>{getObjectValueByString(val.text, elemento.data) === null
                                                            || getObjectValueByString(val.text, elemento.data) === ""
                                                            || getObjectValueByString(val.text, elemento.data) === undefined
                                                            ? "No disponible" : getObjectValueByString(val.text, elemento.data)} {" "}
                                                        {
                                                            j === 0 ? // Para poner los elementos sobre el texto del primero
                                                            <strong className="font-condensed-it">{putOnFirstElementString}</strong>
                                                            :
                                                            ""
                                                        }
                                                    </p>
                                                    : <div className="none"></div>
                                                }
                                            </div>
                                        )
                                    })}
                                    {
                                        esModal ? 
                                        <a href={href} data-bs-toggle="modal" data-bs-target={targetModal} onClick={(e)=>{
                                            e.stopPropagation()
                                            handlerForLoadModal(elemento)}} className="action">
                                            <i className={
                                                    elemento.generalIcon !== null ? elemento.generalIcon :
                                                    elemento.eachIcon
                                                }></i>
                                            <p>
                                                {
                                                    elemento.generalActionTitle !== null ? elemento.generalActionTitle :
                                                    elemento.eachActionTitle
                                                }
                                            </p>
                                        </a>
                                        :
                                        <a href={href} className="action">
                                            <i className={
                                                    elemento.generalIcon !== null ? elemento.generalIcon :
                                                    elemento.eachIcon
                                                }></i>
                                            <p>
                                                {
                                                    elemento.generalActionTitle !== null ? elemento.generalActionTitle :
                                                    elemento.eachActionTitle
                                                }
                                            </p>
                                        </a>
                                    }
                                </div>
                            );
                        }
                    })
                }
                </div>
                <div className="pagination">
                    <div>
                        <span>
                            Página{" "}
                            <strong>
                                {pageExtends} de {Math.ceil(totalRows / pageSize)}
                            </strong>{" (Mostrando "}{data.length}{" registros)"}
                        </span>
                    </div>
                    {/* <div>
                        <button onClick={async (e) => await handlerPagination(1, pageSize)} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends > 1) {
                                pageExtends--;
                                await handlerPagination(pageExtends, pageSize);
                            }

                        }} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <span>
                            <input
                                type="number"
                                value={pageExtends || ''}
                                className="form-control"
                                onFocus={handleFocus}
                                onClick={handleFocus}
                                onChange={async (e) => {
                                    if (e.target.value > 0) {
                                        pageExtends = Number(e.target.value);
                                        await handlerPagination(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                    else  if (e.target.value === 0){
                                        e.target.value = 1;
                                        pageExtends = 1;
                                        await handlerPagination(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                }}
                                style={{ width: "150px", textAlign: "center", display: "inline-block" }}
                            />
                            {" / " + Math.ceil(totalRows / pageSize)}
                        </span>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends < Math.ceil(totalRows / pageSize)) {
                                pageExtends++;
                                await handlerPagination(pageExtends, pageSize);
                            }
                        }} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                        <button onClick={async (e) => await handlerPagination(Math.ceil(totalRows / pageSize), pageSize)} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                    </div> */}
                </div>
            </div>
        </>
    );
}

function getObjectValueByString(s, obj) {
    if (s === null || s === undefined)
        return null
    let properties;
    if (s.includes("."))
        properties = s.split(".");
    else
        properties = [s];
    
    properties.forEach(i => {
        obj = obj !== null && typeof obj[i] !== "undefined" ? obj[i] : "Na"
        // if(Array.isArray(obj))
    })
    return obj
}

export default ContenidoMultimediaList;
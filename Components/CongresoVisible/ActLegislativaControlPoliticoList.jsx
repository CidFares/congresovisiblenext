import React from 'react';
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
const auth = new AuthLogin();
const ActLegislativaControlPoliticoList = ({ data, handler, pageSize = 8, pageExtends = 1, totalRows = 0 , origen = "", imgDefault = ""}) => {
    const handleFocus = (event) => event.target.select();
    return (
        <>
            <div className="ActLegislativaControlPoliticoListContainer">
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select className="form-control"
                        value={pageSize}
                        onChange={async (e) => {
                            let pageSizeT = Number(e.target.value);
                            pageExtends = 1;
                            await handler(pageExtends, pageSizeT);
                        }}
                    >
                        {[8, 24, 64, 112, 500].map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
                <div className="ActLegislativaControlPoliticoList">
                    {
                        data.map((control, i) => {
                            let href = auth.filterStringForURL(`${control.titulo}`);
                            return (
                                <div key={i} className="listadoItem type-2">
                                    <div className="itemHeader">
                                        <h3><a href={`/citaciones/${href}/${control.id}`}>{control.titulo || 'No disponible'}</a></h3>
                                    </div>
                                     <div className="itemBody">
                                        <div className="itemSection">
                                            <p className="title">Citantes</p>
                                            <div className="list-avatar">
                                                <ul>
                                                    {
                                                        control.control_politico_citantes?.map((citante, j) => {
                                                            if(citante.congresista_partido !== null){
                                                                if(j === 6){ // máximo 6
                                                                    return(
                                                                        <li key={j}>+{control.control_politico_citantes.length - 6 < 0 ? (control.control_politico_citantes.length - 6) * -1 : control.control_politico_citantes.length - 6}...</li>
                                                                    );
                                                                }else if(j < 6){
                                                                    return(
                                                                        <li key={j}><figure><img src={typeof citante.congresista_partido.persona?.imagenes[0] !== "undefined" ? (origen + citante.congresista_partido.persona?.imagenes[0].imagen) : imgDefault} alt={citante.congresista_partido.persona?.nombres} /></figure> {citante.congresista_partido.persona?.nombres || ''} {citante.congresista_partido.persona?.apellidos || ''}</li>
                                                                    );
                                                                }
                                                            }
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="itemSection">
                                            <p className="title">Citados</p>
                                            <div className="list-avatar">
                                                <ul>
                                                    {
                                                        control.control_politico_citados?.map((citante, j) => {
                                                            if(j === 6){ // máximo 6
                                                                return(
                                                                    <li key={j}>+{control.control_politico_citados.length - 6 < 0 ? (control.control_politico_citados.length -6) * -1 : control.control_politico_citados.length -6}...</li>
                                                                );
                                                            }else if(j < 6){
                                                                return(
                                                                    <li key={j}><figure><img src={typeof citante.persona?.imagenes[0] !== "undefined" ? (origen + citante.persona?.imagenes[0].imagen) : imgDefault} alt={citante.persona?.nombres} /></figure> {citante.persona?.nombres || ''}</li>
                                                                );
                                                            }
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="itemFooter">
                                        <div><p>{new Date(control.fecha).toLocaleDateString()}</p></div>
                                        <div className={`${control.estado_control_politico !== null && typeof control.estado_control_politico !== 'undefined' ? "" : "none"}`}><p>{control.estado_control_politico !== null && typeof control.estado_control_politico !== 'undefined'  ? control.estado_control_politico.nombre : "" || ""}</p></div>
                                        {
                                            control.comision !== null ?
                                            <div className="with-pdb">
                                                <p>{control.comision !== null ? control.comision.nombre : "S/C" || "S/C" }</p>
                                                <div className="sub">
                                                    <p>Comisión</p>
                                                </div>
                                            </div>
                                            : <div className="none"></div>
                                        }
                                        <div className="with-pdb">
                                            <p>{control.tema_principal_control_politico !== null && typeof control.tema_principal_control_politico !== 'undefined'  ? control.tema_principal_control_politico.nombre : "" || ""}</p>
                                            <div className="sub">
                                                <p>Tema</p>
                                            </div>
                                        </div>
                                        <div><p>Cuatrienio {control.cuatrienio !== null ? control.cuatrienio.nombre : "" || ''}</p></div>
                                        <div><p>Legislatura {control.legislatura !== null ? control.legislatura.nombre : "" || ''}</p></div>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
                <div className="pagination">
                    <div>
                        <span>
                            Página{" "}
                            <strong>
                                {pageExtends} de {Math.ceil(totalRows / pageSize)}
                            </strong>{" (Mostrando "}{data.length}{" registros)"}
                        </span>
                    </div>
                    {/* <div>
                        <button onClick={async (e) => await handler(1, pageSize)} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends > 1) {
                                pageExtends--;
                                await handler(pageExtends, pageSize);
                            }

                        }} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <span>
                            <input
                                type="number"
                                value={pageExtends || ''}
                                className="form-control"
                                onFocus={handleFocus}
                                onClick={handleFocus}
                                onChange={async (e) => {
                                    if (e.target.value > 0) {
                                        pageExtends = Number(e.target.value);
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                    else  if (e.target.value === 0){
                                        e.target.value = 1;
                                        pageExtends = 1;
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                }}
                                style={{ width: "150px", textAlign: "center", display: "inline-block" }}
                            />
                            {" / " + Math.ceil(totalRows / pageSize)}
                        </span>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends < Math.ceil(totalRows / pageSize)) {
                                pageExtends++;
                                await handler(pageExtends, pageSize);
                            }
                        }} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                        <button onClick={async (e) => await handler(Math.ceil(totalRows / pageSize), pageSize)} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                    </div> */}
                </div>
            </div>
        </>
    );
}

export default ActLegislativaControlPoliticoList;

import React from 'react';
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
const auth = new AuthLogin();
const ActLegislativaVotacionesList = ({ data, tiposRespuesta = [], origen = "", handler, pageSize = 8, pageExtends = 1, totalRows = 0 }) => {
    const handleFocus = (event) => event.target.select();
    return (
        <>
            <div className="ActLegislativaVotacionesListContainer">
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select className="form-control"
                        value={pageSize}
                        onChange={async (e) => {
                            let pageSizeT = Number(e.target.value);
                            pageExtends = 1;
                            await handler(pageExtends, pageSizeT);
                        }}
                    >
                        {[8, 24, 64, 112, 500].map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
                <div className="ActLegislativaVotacionesList">
                    {
                        data.map((votacion, i) => {
                            return (
                                <div key={i} className="listadoItem type-2">
                                    <div className="itemHeader">
                                        <h3><a href={`/votaciones/${votacion.id}`}>{votacion.proyecto_de_ley !== null && typeof votacion.proyecto_de_ley !== 'undefined' ? votacion.proyecto_de_ley.titulo : "" || 'No disponible'}</a></h3>
                                    </div>
                                    {/* <div className="itemBody">
                                        <div className="itemSection"></div>
                                    </div> */}
                                    <div className="itemFooter">
                                        <div><p>{auth.shortDate(votacion.fecha) || ''}</p></div>
                                        
                                            {
                                                votacion.esPlenaria === 1 ?
                                                <div><p>Plenaria</p></div> : <p className="none"></p>
                                            }
                                        
                                        <div><p>Cuatrienio {votacion.cuatrienio !== null ? votacion.cuatrienio.nombre : "" || ''}</p></div>
                                        <div><p>Legislatura {votacion.legislatura !== null ? votacion.legislatura.nombre : ""  || ''}</p></div>
                                        {
                                                votacion.esComision === 1 ?
                                                (<div>
                                                    <p>Comisión {votacion.votacion_comision.comision.nombre || ''}</p>
                                                </div>)
                                                : <p className="none"></p>
                                        }
                                        <div className={`iconsForVotacion ${votacion.voto_general === 1 ? "none" : ""} with-pdb`}>
                                        {
                                            votacion.voto_general === 0 && (tiposRespuesta !== null || tiposRespuesta.length !== 0 || typeof tiposRespuesta !== 'undefined') ?
                                            tiposRespuesta.map((res, y) => {
                                                let conteo = votacion.votacion_congresista.filter((v)=>{ return v.tipo_respuesta_votacion_id === res.id}).length;
                                                let clase = y === 0 ? "bg-green" : (y === 1 ? "bg-red" : "bg-gray");
                                                return (
                                                    <div key={y} className={clase}>
                                                        <p>{conteo}</p>
                                                    </div>
                                                )
                                            })
                                            : <div className="none"></div>

                                        }

                                            {
                                                votacion.urlGaceta !== null ?
                                                <a rel="noreferrer" href={votacion.urlGaceta !== "" ? origen + votacion.urlGaceta : "#"} className="bg-blue-icon" target="_blank">
                                                    <div>
                                                        <p><i className="fa fa-download"></i></p>
                                                    </div>
                                                </a>
                                                :
                                                <div className="bg-blue-icon">
                                                    <p><i className="fas fa-circle"></i></p>
                                                </div>
                                            }
                                            <div className="sub">
                                                <p>VOTACIONES</p>
                                            </div>
                                        </div>
                                        <div className={`iconsForVotacion ${votacion.voto_general === 0 ? "none" : ""} with-pdb`}>
                                            <div className="bg-green">
                                                <p>{votacion.votosFavor}</p>
                                            </div>
                                            <div className="bg-red">
                                                <p>{votacion.votosContra}</p>
                                            </div>
                                            <div className="bg-gray">
                                                <p>{votacion.numero_no_asistencias}</p>
                                            </div>
                                            {
                                                votacion.urlGaceta !== null ?
                                                    <a rel="noreferrer" href={votacion.urlGaceta !== "" ? origen + votacion.urlGaceta : "#"} className="bg-blue-icon" target="_blank">
                                                        <div>
                                                            <p><i className="fa fa-download"></i></p>
                                                        </div>
                                                    </a>
                                                 : <div className="none"></div>
                                            }
                                            <div className="sub">
                                                <p>VOTACIONES</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
                <div className="pagination">
                    <div>
                        <span>
                            Página{" "}
                            <strong>
                                {pageExtends} de {Math.ceil(totalRows / pageSize)}
                            </strong>{" (Mostrando "}{data.length}{" registros)"}
                        </span>
                    </div>
                    {/* <div>
                        <button onClick={async (e) => await handler(1, pageSize)} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends > 1) {
                                pageExtends--;
                                await handler(pageExtends, pageSize);
                            }

                        }} disabled={!(pageExtends > 1)}>
                            <i className="fa fa-caret-left"></i>
                        </button>{" "}
                        <span>
                            <input
                                type="number"
                                value={pageExtends || ''}
                                className="form-control"
                                onFocus={handleFocus}
                                onClick={handleFocus}
                                onChange={async (e) => {
                                    if (e.target.value > 0) {
                                        pageExtends = Number(e.target.value);
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                    else  if (e.target.value === 0){
                                        e.target.value = 1;
                                        pageExtends = 1;
                                        await handler(pageExtends > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : pageExtends, pageSize);
                                    }
                                }}
                                style={{ width: "150px", textAlign: "center", display: "inline-block" }}
                            />
                            {" / " + Math.ceil(totalRows / pageSize)}
                        </span>{" "}
                        <button onClick={async (e) => {
                            if (pageExtends < Math.ceil(totalRows / pageSize)) {
                                pageExtends++;
                                await handler(pageExtends, pageSize);
                            }
                        }} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                        <button onClick={async (e) => await handler(Math.ceil(totalRows / pageSize), pageSize)} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                            <i className="fa fa-caret-right"></i>
                            <i className="fa fa-caret-right"></i>
                        </button>{" "}
                    </div> */}
                </div>
            </div>
        </>
    );
}

export default ActLegislativaVotacionesList;

import React from 'react';
import Link from "next/link"
import styles from '../styles/CIDPagination.module.css'
const CIDPagination = ({ totalPages = 1, totalRows = 8, maxPageForChangePaginationView = 30, hrefQuery = { page: 1, rows: 18 }, hrefPath = "/", currentPage = 1, searchBy = "", maxPagesAtMiddle = 10, maxPagesAtBegin = 8, maxPagesAtEnd = 8}) => {
    let PaginasTotales = Math.ceil(totalPages / totalRows);

    // Insertaremos los parámetros que sean diferentes de -1, vacios o nulos
    let RealHrefQuery = "";
    Object.keys(hrefQuery).forEach((x, i) => {
        if(Number(hrefQuery[x]) !== -1 && hrefQuery[x] !== "" && hrefQuery[x] !== null){
            if(i === Object.keys(hrefQuery).length - 1)
                RealHrefQuery += `${x}=${hrefQuery[x]}`;
            else
                RealHrefQuery += `${x}=${hrefQuery[x]}&`;
        }
    })

    // Si no hay muchas páginas, se devuelve una paginación sencilla
    if(maxPageForChangePaginationView >= PaginasTotales){
        return (
            <div className={styles.CIDPaginationContainer}>
                <div className={styles.PaginationBar}>
                    {
                        // Iniciamos ciclando n veces las páginas 
                        Array(PaginasTotales).fill().map((no_importa, i) => {
                            let page = i + 1;
                            if(page === Number(currentPage))
                                return (<p key={page} className={styles.Current}>{page}</p>)

                            return (
                                <a key={page} href={`${hrefPath}/?page=${page}&rows=${totalRows}&search=${searchBy}&${RealHrefQuery}`}>{page}</a>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
    else{
        return (
            <div className={styles.CIDPaginationContainer}>
                <div className={styles.PaginationBar}>
                    {
                        // Iniciamos ciclando n veces las páginas 
                        Array(maxPagesAtBegin).fill().map((no_importa, i) => {
                            let page = i + 1;
                            if(page === Number(currentPage))
                                return (<p key={page} className={styles.Current}>{page}</p>)

                            return (
                                <a key={page} href={`${hrefPath}/?page=${page}&rows=${totalRows}&search=${searchBy}&${RealHrefQuery}`}>{page}</a>
                            )
                        })
                    }
                    <p className={styles.Separator}>...</p>
                    {/* Carousel de páginas */}
                    {
                        // Iniciamos ciclando n veces las páginas 
                        Array(maxPagesAtMiddle).fill().map((no_importa, i) => {
                            let page = i + 1;
                            page = maxPagesAtBegin + page;

                            if(Number(currentPage) >= (maxPagesAtBegin + 7))
                                page = Number(currentPage) + (i - 2);

                            if((Number(currentPage) >= (PaginasTotales - maxPagesAtEnd) - 7)){
                                page = i;
                                page = (PaginasTotales - maxPagesAtEnd) + (i - 9) 
                            }

                            if(page === Number(currentPage))
                                return (<p key={page} className={styles.Current}>{page}</p>)

                            return (
                                <a key={page} href={`${hrefPath}/?page=${page}&rows=${totalRows}&search=${searchBy}&${RealHrefQuery}`}>{page}</a>
                            )
                        })
                    }
                    {/* End carousel de páginas */}
                    <p className={styles.Separator}>...</p>
                    {
                        // Iniciamos ciclando n veces las páginas 
                        Array(maxPagesAtEnd).fill().map((no_importa, i) => {
                            let page = i + 1;
                            page = (PaginasTotales - maxPagesAtEnd) + page
                            if(page === Number(currentPage))
                                return (<p key={page} className={styles.Current}>{page}</p>)

                            return (
                                <a key={page} href={`${hrefPath}/?page=${page}&rows=${totalRows}&search=${searchBy}&${RealHrefQuery}`}>{page}</a>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}
export default CIDPagination;
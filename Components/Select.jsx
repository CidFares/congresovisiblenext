﻿import React from "react";
import ReactSelect from "react-select";

const Select = ({
    divClass = "",
    noOptionsMessage = "Ningún elemento disponible",
    selectplaceholder,
    selectValue,
    selectOnchange,
    selectoptions,
    selectIsSearchable,
    selectclassNamePrefix,
    spanClass,
    spanError,
    divClassSpan = "",
    divClassSpanI = "",
    disabled = false,
    handlerOnClick = null
}) => {
    if (
        typeof selectValue === "function" &&
        typeof selectOnchange === "function"
    ) {
        return (
            <div className={divClass}>
                <ReactSelect
                    onMenuOpen={()=>{ if(handlerOnClick !== null) handlerOnClick();}}
                    isDisabled={disabled}
                    placeholder={selectplaceholder}
                    value={selectValue()}
                    onChange={(e) => {
                        selectOnchange(e);
                    }}
                    options={selectoptions}
                    isSearchable={selectIsSearchable}
                    classNamePrefix={selectclassNamePrefix}
                ></ReactSelect>
                <span className={spanClass}>{spanError}</span>
            </div>
        );
    } else if (divClassSpan === "")
        return (
            <div className={divClass}>
                <ReactSelect
                    noOptionsMessage={() => {
                        return noOptionsMessage;
                    }}
                    onMenuOpen={()=>{ if(handlerOnClick !== null) handlerOnClick();}}
                    isDisabled={disabled}
                    placeholder={selectplaceholder}
                    value={selectValue}
                    onChange={selectOnchange}
                    options={selectoptions}
                    isSearchable={selectIsSearchable}
                    classNamePrefix={selectclassNamePrefix}
                ></ReactSelect>
                <span className={spanClass}>{spanError}</span>
            </div>
        );
    else
        return (
            <div>
                <div className={divClass}>
                    <span className={divClassSpan}>
                        <i className={divClassSpanI}></i>
                    </span>
                    <ReactSelect
                        onMenuOpen={()=>{ if(handlerOnClick !== null) handlerOnClick();}}
                        noOptionsMessage={() => {
                            return noOptionsMessage;
                        }}
                        isDisabled={disabled}
                        placeholder={selectplaceholder}
                        value={selectValue}
                        onChange={selectOnchange}
                        options={selectoptions}
                        classNamePrefix={selectclassNamePrefix}
                    ></ReactSelect>
                </div>
                <span className={spanClass}>{spanError}</span>
            </div>
        );
};
export default Select;

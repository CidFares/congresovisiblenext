import React from 'react';
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar } from "react-modern-calendar-datepicker";


const myCustomLocale = {
    // months list by order
    months: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
    ],
  
    // week days by order
    weekDays: [
      {
        name: 'Domingo', // used for accessibility 
        short: 'D', // displayed at the top of days' rows
        isWeekend: true, // is it a formal weekend or not?
      },
      {
        name: 'Lunes',
        short: 'L',
      },
      {
        name: 'Martes',
        short: 'M',
      },
      {
        name: 'Miércoles',
        short: 'X',
      },
      {
        name: 'Jueves',
        short: 'J',
      },
      {
        name: 'Viernes',
        short: 'V',
      },
      {
        name: 'Sábado',
        short: 'S',
        isWeekend: true,
      },
    ],
  
    // just play around with this number between 0 and 6
    weekStartingIndex: 0,
  
    // return a { year: number, month: number, day: number } object
    getToday(gregorainTodayObject) {
      return gregorainTodayObject;
    },
  
    // return a native JavaScript date here
    toNativeDate(date) {
      return new Date(date.year, date.month - 1, date.day);
    },
  
    // return a number for date's month length
    getMonthLength(date) {
      return new Date(date.year, date.month, 0).getDate();
    },
  
    // return a transformed digit to your locale
    transformDigit(digit) {
      return digit;
    },
  
    // texts in the date picker
    nextMonth: 'Siguiente mes',
    previousMonth: 'Mes anterior',
    openMonthSelector: 'Abrir selector de mes',
    openYearSelector: 'Abrir selector de año',
    closeMonthSelector: 'Cerrar selector de mes',
    closeYearSelector: 'Cerrar selector de año',
    defaultPlaceholder: 'Seleccione...',
  
    // for input range value
    from: 'Desde',
    to: 'hasta',
  
  
    // used for input value when multi dates are selected
    digitSeparator: ',',
  
    // if your provide -2 for example, year will be 2 digited
    yearLetterSkip: 0,
  
    // is your language rtl or ltr?
    isRtl: false,
  }

const LittleCalendar = ({ value = "", onChange = null ,customDaysClassName=[]}) => {
    if(onChange === null){
        console.error("Falta definir onChange y value")
        return false;
    }
    return (
        <div>
          <style>
            {`
            .Calendar {
              background-color: transparent;
              width: 100%;
              height: 100%;
              box-shadow: none;
          }
          .Calendar__weekDays{
              color: var(--bg-header);
              font-weight: bold;
              text-decoration: none
          }
          .Calendar__day{
              border-radius: 7px !important;
          }
          
          .Calendar__day.-weekend:not(.-selected):not(.-blank):not(.-selectedStart):not(.-selectedEnd):not(.-selectedBetween){
              color: var(--bg-header);
              font-weight: bold;
          }
          .Calendar__day:not(.-blank):not(.-selectedStart):not(.-selectedEnd):not(.-selectedBetween):not(.-selected):hover{
              background: var(--bg-color-odd-evento);
              color: #000;
              border-color: transparent;
          }
          .Calendar__day.-today:not(.-selectedStart):not(.-selectedEnd):not(.-selectedBetween){
              color: var(--blue-primary) !important;
              border: 1px solid var(--bg-header);
          }
          .Calendar__monthSelector, .Calendar__yearSelector{
              background-color: var(--bg-header);
          }
          .Calendar > :not(.Calendar__footer) button{
              color: var(--blue-secondary);
          }
          .Calendar__monthYear.-shown button{
              color: #000;
              font-weight: bold;
              font-family: var(--font-condensed);
              font-size: 1.5em;
          }
          .Calendar__monthYear.-shown > *:hover, .Calendar:not(.-noFocusOutline) .Calendar__monthYear.-shown > *:focus, .Calendar__monthYear > *.-activeBackground{
              background: transparent;
          }
          
          .Calendar__monthSelectorItem:not(.-active) .Calendar__monthSelectorItemText:not(:disabled):hover, .Calendar__yearSelectorItem:not(.-active) .Calendar__yearSelectorText:not(:disabled):hover{
              background: var(--blue-secondary);
              color: var(--bg-header);
          }
          .Calendar__monthSelectorItem.-active .Calendar__monthSelectorItemText, .Calendar__yearSelectorItem.-active .Calendar__yearSelectorText{
              background-color: transparent;
              color: var(--blue-secondary);
              border: 1px solid var(--blue-secondary);
          }
          .Calendar__yearSelectorWrapper::before {
              background-image: linear-gradient(to top, var(--bg-header) var(--bg-header) 10%, rgba(245, 245, 245, 0));
              bottom: 0;
          }
          .Calendar__yearSelectorWrapper::after {
              background-image: linear-gradient(to bottom, var(--bg-header) var(--bg-header) 10%, rgba(245, 245, 245, 0));
              top: -0.1em;
          }
          /* .calendarContainer .dayWrapper button{
              color: var(--bg-header);!important;
          }
          .today button{
              border: 3px solid var(--bg-blue-op7) !important;
          }
          .calendarContainer .selected button, .calendarContainer .selected button:active, .calendarContainer .selected button:focus, .calendarContainer .selected button:hover :not([disabled]){
              background-color: var(--bg-header);!important;
              color: var(--blue-secondary) !important;
          }
          .calendarContainer .dayWrapper:not(.selected) button:hover{
              --bg-blue-op7: rgba(38, 61, 95, .1) !important;
              background-color:  var(--bg-blue-op7) !important;   
          
          }
          .calendarContainer .heading .title{
              border: none !important;
          }
          .calendarContainer .heading .title:hover{
              background-color: transparent !important;
          }
          .calendarContainer .heading .title:focus{
              outline: none !important;
          }
          .calendarButton{
              background-color: var(--bg-header);!important;
              width: 250px !important;
              display: block !important;
              margin: auto !important;
              margin-top: 22px !important;
              border: 1px solid transparent !important;
          }
          .calendarButton:focus{
              outline: none !important;
          }
          .calendarButton:hover{
              background-color: var(--blue-secondary) !important;
              color: var(--bg-header);!important;
              border: 1px solid var(--bg-header);!important;
          } */
          .Calendar__day.tieneEventos{
              position: relative;
          }
          .Calendar__day.tieneEventos::before{
              content: '';
              width: 10px;
              height: 10px;
              background-color: #00b963;
              border-radius: 50%;
              position: absolute;
              left: calc(10%);
              top: 5px;
          }
          .Calendar__day.-selected, .Calendar__day.-selectedStart, .Calendar__day.-selectedEnd{
              background-color: var(--blue-primary);
          }
          
            `}
          </style>
            <Calendar                
                value={value}
                onChange={(e)=>{onChange(e)}}
                locale={myCustomLocale} // custom locale object
                shouldHighlightWeekends
                customDaysClassName = {customDaysClassName}
                />
        </div>
    );
}
export default LittleCalendar;
import React, { useState } from "react";
import dynamic from "next/dynamic";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File
import AuthLogin from "../Utils/AuthLogin";
import Link from "next/link";
import Image from "next/image";
import { Constantes } from "../Constants/Constantes.js";

const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});

const auth = new AuthLogin();

const ListComisiones = ({
    data,
    tipoComisionSelected = 0,
    propiedades,
    handler,
    pageExtends = 1,
    pageSizeExtends = 5,
    totalRows = 0,
    pageSizeOptions = [7, 14, 28, 63, 100],
    search = "",
    bootstrapClasses = "col-lg-3 col-md-4 col-sm-6",
}) => {
    const [pageSize, setPageSize] = useState(pageSizeExtends);
    let description = getDesc(tipoComisionSelected);
    return (
        <>
            <div>
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select
                        className="form-control"
                        value={pageSize}
                        onChange={(e) => {
                            pageExtends = 1;
                            setPageSize(Number(e.target.value));
                            handler(pageExtends, e.target.value, search, false);
                        }}
                    >
                        {pageSizeOptions.map((pageSize) => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
            </div>
            <>
                <div className="comisionDescription">
                    <h3>¿Qué son?</h3>
                    <p
                        style={{
                            whiteSpace: "pre-line",
                            textAlign: "justify",
                            fontSize: ".9em",
                        }}
                    >
                        {description}
                    </p>
                </div>
                {data.map((item, i) => {
                    let path = typeof item.comision_imagen[1] !== "undefined" ? auth.pathApi() + item.comision_imagen[1].imagen : Constantes.NoImagenPicture ;
                    return (
                        <div className="listadoItem type-2" key={i}>
                            <div className="itemHeader">
                                <h3>
                                    <a href={`/comisiones/${item.id}`}>{item.nombre}</a>
                                </h3>
                            </div>
                            <div className="itemBody">
                                <div className="itemSection with-photo">
                                    <div className="headerPhoto" style={{position: "relative"}}>
                                        <img src={`${path}`} alt={item.nombre} />
                                    </div>
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: item.descripcion || "Sin descripción" }}></div>
                                </div>
                            </div>
                            <div className="itemFooter">
                                <div className="with-pdb">
                                    <p>{item.tipo_comision.nombre}</p>
                                    <div className="sub">
                                        <p>TIPO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </>
            <div className="pagination">
                <div style={{ float: "left" }}>
                    <span>
                        Página{" "}
                        <strong>
                            {pageExtends} de {Math.ceil(totalRows / pageSize)}
                        </strong>
                        {" (Mostrando "}
                        {data.length}
                        {" registros)"}
                    </span>
                </div>
                {/* <div style={{ float: "right" }}>
                    <button
                        onClick={() => handler(1, pageSize, search, false)}
                        disabled={!(pageExtends > 1)}
                    >
                        <i className="fa fa-caret-left"></i>
                        <i className="fa fa-caret-left"></i>
                    </button>{" "}
                    <button
                        onClick={() => {
                            if (pageExtends > 1) {
                                pageExtends--;
                                handler(pageExtends, pageSize, search, false);
                            }
                        }}
                        disabled={!(pageExtends > 1)}
                    >
                        <i className="fa fa-caret-left"></i>
                    </button>{" "}
                    <span>
                        <input
                            type="number"
                            value={pageExtends}
                            className="form-control"
                            onChange={(e) => {
                                if (e.target.value > 0)
                                    handler(
                                        e.target.value >
                                            Math.ceil(totalRows / pageSize)
                                            ? Math.ceil(totalRows / pageSize)
                                            : e.target.value,
                                        pageSize,
                                        search,
                                        true
                                    );
                                else e.target.value = 1;
                            }}
                            style={{
                                width: "150px",
                                textAlign: "center",
                                display: "inline-block",
                            }}
                        />
                        {" / " + Math.ceil(totalRows / pageSize)}
                    </span>{" "}
                    <button
                        onClick={(e) => {
                            if (pageExtends < Math.ceil(totalRows / pageSize)) {
                                pageExtends++;
                                handler(pageExtends, pageSize, search, false);
                            }
                        }}
                        disabled={
                            !(pageExtends < Math.ceil(totalRows / pageSize))
                        }
                    >
                        <i className="fa fa-caret-right"></i>
                    </button>{" "}
                    <button
                        onClick={() =>
                            handler(
                                Math.ceil(totalRows / pageSize),
                                pageSize,
                                search,
                                false
                            )
                        }
                        disabled={
                            !(pageExtends < Math.ceil(totalRows / pageSize))
                        }
                    >
                        <i className="fa fa-caret-right"></i>
                        <i className="fa fa-caret-right"></i>
                    </button>{" "}
                </div> */}
            </div>
        </>
    );
};

function getImgComision(item) {
    if (item.comision_imagen.length > 0) {
        let img = item.comision_imagen[5];
        if (img != undefined) return img.imagen;
    }
    return null;
}

function getDesc(tipo) {
    switch (tipo) {
        case 0:
            return "Seleccione un tipo de comisión en los filtros para obtener una descripción más detallada.";
            break;
        case 1:
            return `Las comisiones constitucionales son creadas por mandato constitucional y son de carácter común para Senado y Cámara. Tienen como función tramitar a primer debate los proyectos de ley de acuerdo con los asuntos que son de competencia de cada comisión. Existen siete comisiones constitucionales en senado y en cámara sumando en total 14 comisiones.
            Las comisiones permanentes tienen una mayor relevancia dadas sus responsabilidades, las cuales son estudiar, discutir y aprobar en primer debate proyectos de acto legislativo y proyectos de ley.
            -	Comisión primera Constitucional permanente.
            -	Comisión segunda (Relaciones Internacionales)
            -	Comisión tercera (Hacienda y crédito público)
            -	Comisión cuarta (Presupuesto)
            -	Comisión quinta Constitucional permanente
            -	Comisión sexta (transportes y comunicaciones)
            -	Comisión séptima constitucional permanente
            `;
            break;
        case 2:
            return `Las Comisiones Legales son creadas por ley, a diferencia de las Constitucionales, y son encargadas de asuntos específicos diferentes a los correspondiente a las Comisiones Constitucionales Permanentes.  Existen comisiones para ambas Cámaras y comisiones conjuntas.
            -	Comisión de Acusación e Investigación
            -	Comisión Legal de Cuentas
            -	Comisión Afrocolombiana
            -	Comisión Legal de Seguimiento a las Actividades de Inteligencia y Contrainteligencia
            -	Comisión de Administración Senado
            -	Comisión Legal de Crédito Público
            -	Comisión Asesora de Relaciones Exteriores
            Comisiones Legales establecidas para ambas Cámaras.
            •	La Comisión de Derechos Humanos y Audiencia
            •	La Comisión de Ética y Estatuto del Congresista
            •	La Comisión de Acreditación Documental
            Comisiones Legales Conjuntas
            •	La Comisión para la Equidad de la Mujer
            `;
            break;
        case 3:
            return `Son de creación legal con participación de Senadores o Representantes y sus funciones están determinadas por la disposición legal que las crea.
            -	Las Comisiones Especiales de Seguimiento
            •	Comisión de Vigilancia de los Organismos de Control Público.
            •	Comisión de Ordenamiento Territorial
            -	Comisión de Crédito Público
            -	Comisión Especial de Modernización
            -	Parlamento Andino (Senado)
            -	Comisión Especial de Vigilancia y Seguimiento al Organismo Electoral (Cámara)
            `;
            break;
        case 4:
            return `Estas Comisiones pueden ser designadas por los presidentes y las mesas directivas de las cámaras, así como también por sus comisiones permanentes con el objetivo de encargarles tareas, misiones o asuntos específicos, con el fin de tener un mejor desarrollo de la labor Legislativa y Administrativa.
            -	Accidental de paz.
            -	Afrodescendiente e Indígena
            -	Costa Caribe
            `;
            break;
        case 5:
            return ``;
            break;
        default:
            break;
    }
}
export default ListComisiones;

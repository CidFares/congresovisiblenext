import React from 'react';

const AccordionCheckbox = ({ children, open = false, handlerCheckboxSelected = null, label, errorMessage = "", errorClass = "error" }) => {
    if(handlerCheckboxSelected === null){
        console.error("Debe asignar un manejador para el checkbox seleccionado (handlerCheckboxSelected)");
        return false;
    }
    
    return (
        <div className="accordionCheckboxContainer">
            <style>
                {`
                .accordionCheckboxContainer{
                    position: relative;
                    border-bottom: 1px solid var(--gray-primary);
                    padding: 7px 0;
                }
                
                .accordionCheckboxContainer .topCheckbox{
                    display: inline-flex;
                    justify-content: center;
                    flex-wrap: nowrap;
                    align-items: center;
                    width: 100%;
                }
                .accordionCheckboxContainer .topCheckbox > *{
                    margin: 7px 12px;
                    padding: 0;
                    /* font-size: 1.2em; */
                }
                .accordionCheckboxContainer .topCheckbox input{
                    width: 25px;
                    height: 25px;
                }
                .accordionCheckboxContainer .topCheckbox label{
                    color: var(--blue-primary);
                    font-family: var(--font-condensed);
                    text-transform: uppercase;
                    font-size: 1em;
                    width: calc(100% - 45px);
                    user-select: none;
                }
                .accordionCheckboxContainer .topCheckbox label:hover{
                    cursor: pointer;
                }
                .accordionCheckboxContainer .bodyContent{
                    max-height: 0;
                    transition: linear all .3s;
                    overflow: hidden;
                }
                .accordionCheckboxContainer .bodyContent.active{
                    max-height: none;
                    overflow: inherit;
                }
                
                .accordionCheckboxContainer .bodyContent::-webkit-scrollbar{
                    opacity: 0;
                }
                /* checkbox */
                
                .accordionCheckboxContainer .accordionCheckbox{
                    position: relative;
                    width: 35px;
                    height: 35px;
                    border-radius: 50%;
                    background-color: #fff;
                    border: 1px solid #ccc;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    transition: ease all .3s;
                }
                .accordionCheckboxContainer .accordionCheckbox:hover{
                    cursor: pointer;
                    background-color: rgb(228, 228, 228);
                }
                .accordionCheckboxContainer .accordionCheckbox::before{
                    transition: ease all .3s;
                    transform: scale(0);
                }
                .accordionCheckboxContainer .accordionCheckbox::before{
                    content: '';
                    position: absolute;
                    width: 100%; height: 100%;
                    border-radius: 50%;
                    transform-origin: center;
                }
                .accordionCheckboxContainer .accordionCheckbox i{
                    font-size: 0.9em;
                    transition-delay: .2s;
                    color: var(--blue-primary);
                    transition: ease all .3s;
                    position: relative;
                    z-index: 1;
                }
                
                .accordionCheckboxContainer .accordionCheckbox.checked::before{
                    transform: scale(1);
                    background-color: var(--blue-primary);
                }
                .accordionCheckboxContainer .accordionCheckbox.checked i{
                    color: #fff;
                    transform: rotateZ(45deg);
                    left: -1px;
                }
                /* end checkbox */
                
                .accordionCheckboxContainer .bodyContent .cfItemDescription label{
                    color: var(--blue-primary);
                }
                
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2{
                    border: 1px solid #eaeaea;
                }
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemHeader{
                    border-radius: 5px
                }
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemHeader h4{
                    font-size: 1.2em;
                }
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemBody *{
                    white-space: pre-line;
                    font-size: 14px;
                    text-align: justify;
                }
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemFooter > .with-pdb{
                    margin-bottom: 30px
                }
                
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemFooter, 
                .accordionCheckboxContainer .bodyContent .listadoItem.type-2 .itemFooter > *{
                    justify-content: center;
                }
                `}
            </style>
            <div className={`topCheckbox`}>
                {/* <input type="checkbox" name={label} checked={open ? "checked" : ""} onChange={(e)=>{handlerCheckboxSelected(e.currentTarget.checked)}}/> */}
                <div className={`accordionCheckbox ${open ? "checked" : ""}`} name={label} onClick={(e)=>{
                    handlerCheckboxSelected(!e.currentTarget.classList.contains('checked'))
                    }}>
                        <i className="fas fa-plus"></i>
                </div>
                <label onClick={(e)=>{
                    handlerCheckboxSelected(!e.currentTarget.parentNode.querySelector('.accordionCheckbox').classList.contains('checked'))
                    }}>{label}</label>
            </div>
            <div className={`bodyContent ${open ? "active" : ""}`}>
                {children}
            </div>
            <div className="footerContent">
                {
                    errorMessage !== "" ? <span className={errorClass}>{errorMessage}</span> : ""
                }
            </div>
        </div>
    );
}
export default AccordionCheckbox;
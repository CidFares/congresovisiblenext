import React, { useState } from 'react';
import AuthLogin from "../Utils/AuthLogin";
import Link from "next/link"
const auth = new AuthLogin();

const ListProyectosLey = ({ data, handler, pageExtends = 1, pageSizeExtends = 5, totalRows = 0, pageSizeOptions = [15, 30, 100, 200, 500], search = "", bootstrapClasses = "col-lg-3 col-md-4 col-sm-6" }) => {
    const [pageSize, setPageSize] = useState(pageSizeExtends);
    return (
        <>
            <div >
                <div className="RowNumberContainer">
                    <label htmlFor="">Mostrar</label>
                    <select className="form-control"
                        value={pageSize}
                        onChange={e => {
                            pageExtends = 1;
                            setPageSize(Number(e.target.value));
                            handler(pageExtends, e.target.value, search, false);
                        }}
                    >
                        {pageSizeOptions.map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        ))}
                    </select>
                    <label htmlFor="">registros por página</label>
                </div>
            </div>
            <>
                {
                    data.map((item, i) => {
                        let autores = '';
                        if(item.proyecto_ley_autor_legislativos.length > 0){
                            if(item.iniciativa_id === 1){
                                item.proyecto_ley_autor_legislativos.map(item2 =>{
                                    if(item2.hasOwnProperty('congresista') && item2.congresista){
                                        if(item2.congresista.hasOwnProperty('persona') && item2.congresista.persona){
                                            autores += `${item2.congresista.persona.nombres || ''} ${item2.congresista.persona.apellidos || ''}, `;
                                        }
                                    }
                                })
                            }
                         }
                        if(item.proyecto_ley_autor_personas.length > 0)
                        {
                            if((item.iniciativa_id === 2 || item.iniciativa_id === 3)){
                                item.proyecto_ley_autor_personas.map(item2 =>{
                                    if(item2.hasOwnProperty('persona') && item2.persona){
                                        autores += `${item2.persona.nombres || ''} ${item2.persona.apellidos || ''}, `;
                                    }
                                });
                            }
                        }
                        autores = autores.replace(/,\s*$/,"");
                        let href = auth.filterStringForURL(`p${item.titulo}`)
                        return (
                            <div className="listadoItem type-2" key={i}>
                                <div className="itemHeader">
                                    <h2>
                                        <a href={`${href}/${item.id}`}>{item.titulo}</a>
                                    </h2>
                                </div>
                                <div className="itemBody">
                                    <div className="itemSection">
                                        <p className="title">Información</p>
                                        <div className="list-avatar block-list">
                                            <ul>
                                                <li><strong>Cuatrienio: </strong> {' '} {item.cuatrienio?.nombre || ''}</li>
                                                <li><strong>Legislatura: </strong> {' '} {item.legislatura?.nombre || ''}</li>
                                                <li><strong>Iniciativa: </strong> {' '} {item.iniciativa?.nombre || ''}</li>
                                                <li><strong>Tipo de proyecto: </strong> {' '} {item.tipo_proyecto_ley?.nombre || ''}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="itemSection">
                                        <p className="title">Autores</p>
                                        <p>{autores}</p>
                                    </div>
                                </div>
                                <div className="itemFooter">
                                    <div className="with-pdb">
                                        <p>{item.estado_proyecto_ley?.nombre || ''}</p>
                                        <div className="sub">
                                            <p>ESTADO</p>
                                        </div>
                                    </div>
                                    <div className="with-pdb">
                                        <p>{item.numero_camara || ''}</p>
                                        <div className="sub">
                                            <p>No. Cámara</p>
                                        </div>
                                    </div>
                                    <div className="with-pdb">
                                        <p>{item.numero_senado || ''}</p>
                                        <div className="sub">
                                            <p>No. Senado</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </>
            <div className="pagination">
                <div style={{ float: "left" }}>
                    <span>
                        Página{" "}
                        <strong>
                            {pageExtends} de {Math.ceil(totalRows / pageSize)}
                        </strong>{" (Mostrando "}{data.length}{" registros)"}
                    </span>
                </div>
                {/* <div style={{ float: "right" }}>
                    <button onClick={() => handler(1, pageSize, search, false)} disabled={!(pageExtends > 1)}>
                        <i className="fa fa-caret-left"></i>
                        <i className="fa fa-caret-left"></i>
                    </button>{" "}
                    <button onClick={() => {
                        if (pageExtends > 1) {
                            pageExtends--;
                            handler(pageExtends, pageSize, search, false);
                        }

                    }} disabled={!(pageExtends > 1)}>
                        <i className="fa fa-caret-left"></i>
                    </button>{" "}
                    <span>
                        <input
                            type="number"
                            value={pageExtends}
                            className="form-control"
                            onChange={e => {
                                if (e.target.value > 0)
                                    handler(e.target.value > Math.ceil(totalRows / pageSize) ? Math.ceil(totalRows / pageSize) : e.target.value, pageSize, search, true);
                                else
                                    e.target.value = 1;
                            }}
                            style={{ width: "150px", textAlign: "center", display: "inline-block" }}
                        />
                        {" / " + Math.ceil(totalRows / pageSize)}
                    </span>{" "}
                    <button onClick={(e) => {
                        if (pageExtends < Math.ceil(totalRows / pageSize)) {
                            pageExtends++;
                            handler(pageExtends, pageSize, search, false);
                        }
                    }} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                        <i className="fa fa-caret-right"></i>
                    </button>{" "}
                    <button onClick={() => handler(Math.ceil(totalRows / pageSize), pageSize, search, false)} disabled={!(pageExtends < Math.ceil(totalRows / pageSize))}>
                        <i className="fa fa-caret-right"></i>
                        <i className="fa fa-caret-right"></i>
                    </button>{" "}
                </div> */}
            </div>
        </>);
}

function getImgComision(item) {
    if (item.comision_imagen.length > 0) {
        let img = item.comision_imagen[5];
        if (img != undefined)
            return img.imagen;
    }
    return null;
}
export default ListProyectosLey;

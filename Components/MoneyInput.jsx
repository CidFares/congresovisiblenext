import React from 'react';

const MoneyInput = ({ value = 0, index = 0, propertyToSetValue = "", showAddon = true, readOnly = false, justTextForReadOnly = false, icon = "fas fa-dollar-sign", countryLocale = 'es-CO', styleCurrency = 'currency', currency = 'COP', showDecimal = true, numberDecimalDigits = 2, handlerChangeMoney = null }) => {
    let formatter = new Intl.NumberFormat(countryLocale, {
        style: styleCurrency,
        currency: currency,
        minimumFractionDigits: showDecimal ? numberDecimalDigits : 0
    })
    return (
        <div className="MoneyInputContainer">
            <style jsx>
                {`
                .MoneyInputContainer {
                    width: 100%;
                    max-width: 100%;
                    position: relative;
                    overflow: hidden;
                }
                .MoneyInputContainer .addon{
                    position: absolute;
                    width: 36px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    height: 100%;
                    top: 0;
                }
                .MoneyInputContainer .addon{
                    background-color: var(--blue-primary);
                    color: #fff;
                    border-top-left-radius: 5px;
                    border-bottom-left-radius: 5px;
                }
                .MoneyInputContainer .Input input{
                    background-color: #fff;
                    padding: 10px 12px;
                    border: 1px solid #eaeaea;
                    white-space: nowrap;
                    text-overflow: ellipsis;
                    overflow: hidden;
                    width: 100%;
                    border-radius: 3px;
                    padding-left: 50px;
                    padding-right: 160px;
                }
                
                .MoneyInputContainer .Input p{
                    margin: 0;
                }
                `}
            </style>
            {
                showAddon ?
                <div className="addon"><i className={icon}></i></div>
                :
                ""
            }
            <div className="Input">
                <input type="number"
                onBlur={(e)=>{
                    e.currentTarget.parentNode.querySelector('input[type="text"]').style.display = "block";
                    e.currentTarget.style.display = "none"
                }}
                onKeyUp={(e)=>{
                    if (e.key === "Enter") {
                        e.currentTarget.parentNode.querySelector('input[type="text"]').style.display = "block";
                        e.currentTarget.style.display = "none"
                    }
                }}
                style={{display: "none"}} value={value} onChange={(e) => { if(handlerChangeMoney !== null) handlerChangeMoney(e.currentTarget.value, propertyToSetValue, index) }} />
                {
                    justTextForReadOnly ?
                    <p>{formatter.format(value)}</p>
                    :
                    <input type="text"
                    onFocus={(e)=>{
                        if(!readOnly){
                            let elementNumber = e.currentTarget.parentNode.querySelector('input[type="number"]');
                            elementNumber.style.display = "block";
                            elementNumber.focus();
                            e.currentTarget.style.display = "none"
                        }
                    }}
                    value={formatter.format(value)} />
                }
            </div>
        </div>
    );
}
export default MoneyInput;
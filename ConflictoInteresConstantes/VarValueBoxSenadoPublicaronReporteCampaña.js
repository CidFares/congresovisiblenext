// VarValueBoxSenadoPublicaronReporteCampaña

class VarValueBoxSenadoPublicaronReporteCampaña {
    valueBox() {
        return {
            default_value_box: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        backgroundColor: "#E18335",
                        style: {
                            fontFamily: "Helvetica"
                        }
                    },
                    title: {
                        text: "Senadores que publicaron el registro de los aportes que recibieron sus campañas",
                        align: "left",
                        style: {
                            color: "#FFF"
                        }
                    },
                    yAxis: {
                        title: {
                            text: "n",
                            enabled: false
                        },
                        lineWidth: 0,
                        type: "linear",
                        gridLineWidth: 0,
                        labels: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            label: {
                                enabled: false
                            },
                            turboThreshold: 0,
                            showInLegend: false,
                            lineWidth: 8
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        }
                    },
                    series: [],
                    xAxis: {
                        lineWidth: 0,
                        labels: {
                            enabled: false
                        },
                        tickWidth: 0
                    },
                    subtitle: {
                        text: "",
                        style: {
                            fontSize: "150px",
                            color: "#FFF"
                        },
                        align: "left"
                    },
                    colors: ["#FFF"],
                    tooltip: {
                        formatter: function() {
                            return  this.point.n + ' congresistas con conflictos de interés.';
                        }
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarValueBoxSenadoPublicaronReporteCampaña();

//  End VarValueBoxSenadoPublicaronReporteCampaña
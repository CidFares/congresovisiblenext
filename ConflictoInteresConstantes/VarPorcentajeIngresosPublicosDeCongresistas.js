// VarPorcentajeIngresosPublicosDeCongresistas

class VarPorcentajeIngresosPublicosDeCongresistas {
    total() {
        return {
            porcentaje_ingresos_publicos_privados: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Seleccione una corporación",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'Porcentaje de ingresos públicos y privados de los congresistas',
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            shadow: false,
                            center: ['50%', '50%']
                        },
                        series: {
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                        }
                    },
                    tooltip: {
                        valueSuffix: '%',
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    series: [{
                        name: 'Totales',
                        data: [],
                        size: '60%',
                        dataLabels: {
                            formatter: function () {
                                return this.y > 5 ? this.point.name : null;
                            },
                            color: '#ffffff',
                            distance: -30
                        }
                    }, {
                        name: 'Ingreso',
                        data: [],
                        size: '80%',
                        innerSize: '60%',
                        dataLabels: {
                            formatter: function () {
                                // display only if larger than 1
                                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                                    this.y + '%' : null;
                            }
                        },
                        id: 'versions'
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 400
                            },
                            chartOptions: {
                                series: [{
                                }, {
                                    id: 'versions',
                                    dataLabels: {
                                        enabled: false
                                    }
                                }]
                            }
                        }]
                    }
                },
            },
        };
    };
}

export default new VarPorcentajeIngresosPublicosDeCongresistas();

//  End VarPorcentajeIngresosPublicosDeCongresistas
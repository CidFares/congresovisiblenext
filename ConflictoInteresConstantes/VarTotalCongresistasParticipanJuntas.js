// VarTotalCongresistasParticipanJuntas

class VarTotalCongresistasParticipanJuntas {
    total() {
        return {
            total_congresistas_participan_juntas: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Seleccione una corporación",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true
                    },
                    title: {
                        text: "Total de congresistas que participan en Juntas Directivas, Corporaciones y Sociedades",
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    yAxis: {
                        title: {
                            text: "1",
                            enabled: false
                        },
                        type: "linear",
                        lineWidth: 0,
                        labels: {
                            enabled: false
                        },
                        gridLineWidth: 0
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            label: {
                                enabled: false
                            },
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            turboThreshold: 0,
                            showInLegend: true
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        },
                        bubble: {
                            colorByPoint: false,
                            dataLabels: {
                                enabled: true,
                                style: {
                                    fontSize: "20px"
                                }
                            },
                            maxSize: "50%",
                            minSize: "20%",
                            marker: {
                                fillOpacity: 0.91,
                                lineWidth: 0
                            }
                        }
                    },
                    series: [],
                    xAxis: {
                        type: "category",
                        title: {
                            text: "genero",
                            enabled: false
                        },
                        lineWidth: 0,
                        labels: {
                            enabled: false
                        }
                    },
                    tooltip: {
                        pointFormat: "{point.pct}%",
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarTotalCongresistasParticipanJuntas();

//  End VarTotalCongresistasParticipanJuntas
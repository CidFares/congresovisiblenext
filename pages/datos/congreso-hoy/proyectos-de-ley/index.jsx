import { useState, useEffect } from "react";
import Head from "next/head";
import DatosSenadoCongresoHoyDataService from "../../../../Services/Datos/Datos.Service";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarCongresoHoyTop10TemasProyectosLey from "../../../../GraficasConstantes/VarCongresoHoyTop10TemasProyectosLey";
import VarCongresoHoyEstadoProyectosLey from "../../../../GraficasConstantes/VarCongresoHoyEstadoProyectosLey";
import VarCamaraOrigenIniciativas from "../../../../GraficasConstantes/VarCamaraOrigenIniciativas";
import VarValueBoxObjecionesEmitidas from "../../../../GraficasConstantes/VarValueBoxObjecionesEmitidas";
import VarValueBoxProyectoDeLey from "../../../../GraficasConstantes/VarValueBoxProyectoDeLey";
import VarValueBoxProyectosRadicados from "../../../../GraficasConstantes/VarValueBoxProyectosRadicados";
import VarValueBoxSentenciasEmitidas from "../../../../GraficasConstantes/VarValueBoxSentenciasEmitidas";
import VarValueBoxDebatesDeControlPolitico from "../../../../GraficasConstantes/VarValueBoxDebatesDeControlPolitico";
import ValueBoxAudienciasPublicasCitadas from "../../../../GraficasConstantes/ValueBoxAudienciasPublicasCitadas";

if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    ValueBoxAudienciasPublicasCitadas: true,
    ValueBoxDebatesDeControlPolitico: true,
    ValueBoxProyectosRadicados: true,
    ValueBoxProyectoDeLey: true,
    ValueBoxObjecionesEmitidas: true,
    ValueBoxSentenciasEmitidas: true,
    CamaraOrigenIniciativas: true,
    CongresoHoyTop10TemasProyectosLey: true,
    CongresoHoyEstadoProyectosLey: true
}
const ConstCharts = {
    ValueBoxObjecionesEmitidas: {
        value_box: Object.assign({}, VarValueBoxObjecionesEmitidas.value_box()),
    },
    ValueBoxProyectoDeLey: {
        value_box: Object.assign({}, VarValueBoxProyectoDeLey.value_box()),
    },
    ValueBoxProyectosRadicados: {
        value_box: Object.assign({}, VarValueBoxProyectosRadicados.value_box()),
    },
    ValueBoxSentenciasEmitidas: {
        value_box: Object.assign({}, VarValueBoxSentenciasEmitidas.value_box()),
    },
    ValueBoxAudienciasPublicasCitadas: {
        value_box: Object.assign({}, ValueBoxAudienciasPublicasCitadas.value_box()),
    },
    ValueBoxDebatesDeControlPolitico: {
        value_box: Object.assign({}, VarValueBoxDebatesDeControlPolitico.value_box()),
    },
    CamaraOrigenIniciativas: {
        camara: Object.assign({}, VarCamaraOrigenIniciativas.default_camara()),
    },
    CongresoHoyTop10TemasProyectosLey: {
        congreso_hoy: Object.assign({}, VarCongresoHoyTop10TemasProyectosLey.default_congreso_hoy()),
    },
    CongresoHoyEstadoProyectosLey: {
        congreso_hoy: Object.assign({}, VarCongresoHoyEstadoProyectosLey.default_congreso_hoy()),
    },
    ComboCongresoHoy: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Ver todos",
                },
                data: [],
                error: "",
            },
            legislatura: {
                item: {
                    value: "",
                    label: "Ver todas",
                },
                data: [],
                error: "",
            },
            corporacion: {
                item: {
                    value: "",
                    label: "Ver ambas",
                },
                data: [],
                error: "",
            },
        },
    },
}

// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();
    let { DSLegislatura, SelectedLegislatura } = await getLegislatura(SelectedCuatrienio.value);
    let { DSCorporacion, SelectedCorporacion } = await getCorporacion()

    // let { ValueBoxAudienciasPublicasCitadasSeries, ValueBoxAudienciasPublicasCitadasTotal } = await ValueBoxAudienciasPublicasCitadas_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxAudienciasPublicasCitadas.value_box.audiencias_publicas_citadas.grafica.series = ValueBoxAudienciasPublicasCitadasSeries;
    // graficas.ValueBoxAudienciasPublicasCitadas.value_box.audiencias_publicas_citadas.grafica.subtitle.text = ValueBoxAudienciasPublicasCitadasTotal;

    // let { ValueBoxDebatesDeControlPoliticoSeries, ValueBoxDebatesDeControlPoliticoTotal } = await ValueBoxDebatesDeControlPolitico_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxDebatesDeControlPolitico.value_box.debates_de_control_politicos.grafica.series = ValueBoxDebatesDeControlPoliticoSeries;
    // graficas.ValueBoxDebatesDeControlPolitico.value_box.debates_de_control_politicos.grafica.subtitle.text = ValueBoxDebatesDeControlPoliticoTotal;

    // let { ValueBoxObjecionesEmitidasSeries, ValueBoxObjecionesEmitidasTotal } = await ValueBoxObjecionesEmitidas_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxObjecionesEmitidas.value_box.objeciones_emitidas.grafica.series = ValueBoxObjecionesEmitidasSeries;
    // graficas.ValueBoxObjecionesEmitidas.value_box.objeciones_emitidas.grafica.subtitle.text = ValueBoxObjecionesEmitidasTotal;

    // let { ValueBoxProyectoDeLeySeries, ValueBoxProyectoDeLeyTotal } = await ValueBoxProyectoDeLey_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxProyectoDeLey.value_box.proyecto_de_ley.grafica.series = ValueBoxProyectoDeLeySeries;
    // graficas.ValueBoxProyectoDeLey.value_box.proyecto_de_ley.grafica.subtitle.text = ValueBoxProyectoDeLeyTotal;

    // let { ValueBoxProyectosRadicadosSeries, ValueBoxProyectosRadicadosTotal } = await ValueBoxProyectosRadicados_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxProyectosRadicados.value_box.proyectos_radicados.grafica.series = ValueBoxProyectosRadicadosSeries;
    // graficas.ValueBoxProyectosRadicados.value_box.proyectos_radicados.grafica.subtitle.text = ValueBoxProyectosRadicadosTotal;

    // let { ValueBoxSentenciasEmitidasSeries, ValueBoxSentenciasEmitidasTotal } = await ValueBoxSentenciasEmitidas_handlerValueBox(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.ValueBoxSentenciasEmitidas.value_box.sentencias_emitidas.grafica.series = ValueBoxSentenciasEmitidasSeries;
    // graficas.ValueBoxSentenciasEmitidas.value_box.sentencias_emitidas.grafica.subtitle.text = ValueBoxSentenciasEmitidasTotal;

    // let { CamaraOrigenIniciativasData } = await CamaraOrigenIniciativas_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.CamaraOrigenIniciativas.camara.origen_iniciativa.grafica.series[0].data = CamaraOrigenIniciativasData;
    // graficas.CamaraOrigenIniciativas.camara.origen_iniciativa.grafica.title.text = `Origen de las iniciativas - ${SelectedCorporacion.value === "" ? "Cámara y Senado" : SelectedCorporacion.label}`
    // graficas.CamaraOrigenIniciativas.camara.origen_iniciativa.grafica.subtitle.text = `${SelectedCuatrienio.value === "" ? "Todas las legislaturas" : "Legislatura " + SelectedCuatrienio.label}`

    // let { CongresoHoyTop10TemasProyectosLeySeries, CongresoHoyTop10TemasProyectosLeyTotal } = await CongresoHoyTop10TemasProyectosLey_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.CongresoHoyTop10TemasProyectosLey.congreso_hoy.top_10_temas_proyectos_ley.grafica.series = CongresoHoyTop10TemasProyectosLeySeries;
    // graficas.CongresoHoyTop10TemasProyectosLey.congreso_hoy.top_10_temas_proyectos_ley.grafica.title.text = `Top 10: Temas de los proyectos de ley - ${SelectedCorporacion.value === "" ? "Cámara y Senado" : SelectedCorporacion.label}`
    // graficas.CongresoHoyTop10TemasProyectosLey.congreso_hoy.top_10_temas_proyectos_ley.grafica.subtitle.text = `${SelectedCuatrienio.value === "" ? "Todas las legislaturas" : "Legislatura " + SelectedCuatrienio.label}`

    // let { CongresoHoyEstadoProyectosLeySeries, CongresoHoyEstadoProyectosLeyTotal } = await CongresoHoyEstadoProyectosLey_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.CongresoHoyEstadoProyectosLey.congreso_hoy.estado_proyectos_ley.grafica.series = CongresoHoyEstadoProyectosLeySeries;
    // graficas.CongresoHoyEstadoProyectosLey.congreso_hoy.estado_proyectos_ley.grafica.title.text = `Estado de los proyectos de ley - ${SelectedCorporacion.value === "" ? "Cámara y Senado" : SelectedCorporacion.label}`
    // graficas.CongresoHoyEstadoProyectosLey.congreso_hoy.estado_proyectos_ley.grafica.subtitle.text = `${SelectedCuatrienio.value === "" ? "Todas las legislaturas" : "Legislatura " + SelectedCuatrienio.label}`

    return {
        props: { charts: JSON.stringify(graficas), DSCorporacion, SelectedCorporacion, DSCuatrienio, SelectedCuatrienio, DSLegislatura, SelectedLegislatura }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let getLegislatura = false;
        let year = new Date().getFullYear();

        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
                getLegislatura = true;
            }
        });

        if (!getLegislatura)
            selected = { value: response.data[0].id, label: response.data[0].nombre }

        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const getLegislatura = async (cuatrienio_id) => { // método general para llenar todos los obj de legislatura
    let combo = [];
    let selected = Object.assign({}, ConstCharts.ComboCongresoHoy.combo.legislatura.item);
    await UtilsDataService.getComboLegislatura(cuatrienio_id).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.legislatura.item));
    });


    return { DSLegislatura: combo, SelectedLegislatura: selected };
};
const getCorporacion = async () => {
    let combo = [];
    let selected = Object.assign({}, ConstCharts.ComboCongresoHoy.combo.corporacion.item);
    await UtilsDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.corporacion.item));
    });

    return { DSCorporacion: combo, SelectedCorporacion: selected }
};
const ValueBoxAudienciasPublicasCitadas_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await DatosDataService.getValueBoxAudiendiasPublicasCitadas(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { ValueBoxAudienciasPublicasCitadasSeries: series, ValueBoxAudienciasPublicasCitadasTotal: total }
};
const ValueBoxDebatesDeControlPolitico_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await DatosDataService.getValueBoxDebatesDeControlPolitico(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { ValueBoxDebatesDeControlPoliticoSeries: series, ValueBoxDebatesDeControlPoliticoTotal: total }
};
const ValueBoxProyectosRadicados_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await DatosDataService.getValueBoxProyectosRadicados(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { ValueBoxProyectosRadicadosSeries: series, ValueBoxProyectosRadicadosTotal: total }
};
const ValueBoxProyectoDeLey_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;

    await DatosDataService.getValueBoxProyectosLey(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { ValueBoxProyectoDeLeySeries: series, ValueBoxProyectoDeLeyTotal: total }
};
const ValueBoxObjecionesEmitidas_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;

    await DatosDataService.getValueBoxObjecionesEmitidas(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { ValueBoxObjecionesEmitidasSeries: series, ValueBoxObjecionesEmitidasTotal: total }
};
const ValueBoxSentenciasEmitidas_handlerValueBox = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;

    await DatosDataService.getValueBoxSentenciasEmitidas(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push({
                "mes": currentValue.mes,
                "n": currentValue.n,
                "y": currentValue.n,
            });
        });

        if (response.data.length === 0) {
            total += 0;
            series[0].data.push({
                "mes": "01/01/2021",
                "n": 0,
                "y": 0,
            });
        }
    })
        .catch((error) => {
            console.log(error);
        });
    return { ValueBoxSentenciasEmitidasSeries: series, ValueBoxSentenciasEmitidasTotal: total }
};
const CamaraOrigenIniciativas_handlerGetChart = async (cuatrienio, legislatura, corporacion) => {
    let data = [];
    await DatosDataService.getCamaraOrigenIniciativas(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            data.push({
                "tipo_proyecto": currentValue.tipo_proyecto_ley,
                "iniciativa": currentValue.iniciativa,
                "n": currentValue.n,
                "y": currentValue.n,
                "name": currentValue.iniciativa
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CamaraOrigenIniciativasData: data }
};
const CongresoHoyTop10TemasProyectosLey_handlerGetChart = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "treemap" }];
    let total = 0;
    await DatosDataService.getTop10TemasProyectosLey(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push(
                {
                    "fecha_inicio_cuatri": currentValue.cuatrienio,
                    "tema": currentValue.tema,
                    "n": currentValue.n,
                    "totales": currentValue.n,
                    "name": currentValue.tema,
                });
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.porc = parseFloat(((currentValueData.n * 100) / total).toFixed(2));
                currentValueData.value = currentValueData.porc;
                currentValueData.totales = total;
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyTop10TemasProyectosLeySeries: series, CongresoHoyTop10TemasProyectosLeyTotal: total }
};
const CongresoHoyEstadoProyectosLey_handlerGetChart = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "data": [], "type": "treemap" }];
    let total = 0;
    await DatosDataService.getEstadoProyectosLey(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            series[0].data.push(
                {
                    "fecha_inicio_cuatri": currentValue.cuatrienio,
                    "tema": currentValue.estado_proyecto,
                    "n": currentValue.n,
                    "totales": currentValue.n,
                    "name": currentValue.estado_proyecto,
                });
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.porc = parseFloat(((currentValueData.n * 100) / total).toFixed(2));
                currentValueData.value = currentValueData.porc;
                currentValueData.totales = total;
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyEstadoProyectosLeySeries: series, CongresoHoyEstadoProyectosLeyTotal: total }
};
// EXPORT
const CongresoHoyProyectosDeLey = ({ charts = ConstCharts, DSCorporacion = ConstCharts.ComboCongresoHoy.combo.corporacion.data, SelectedCorporacion = ConstCharts.ComboCongresoHoy.combo.corporacion.item, DSCuatrienio = ConstCharts.ComboCongresoHoy.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboCongresoHoy.combo.cuatrienio.item, DSLegislatura = ConstCharts.ComboCongresoHoy.combo.legislatura.data, SelectedLegislatura = ConstCharts.ComboCongresoHoy.combo.legislatura.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCorporacion, setDSCorporacion] = useState(DSCorporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [dataSelectLegislatura, setDSLegislatura] = useState(DSLegislatura);
    const [filterCorporacion, setSelectedCorporacion] = useState(SelectedCorporacion);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [filterLegislatura, setSelectedLegislatura] = useState(SelectedLegislatura);
    const [C_ValueBoxAudienciasPublicasCitadasGrafica, setValueBoxAudienciasPublicasCitadasGrafica] = useState(JSON.parse(charts).ValueBoxAudienciasPublicasCitadas.value_box.audiencias_publicas_citadas.grafica);
    const [C_ValueBoxDebatesDeControlPoliticoGrafica, setValueBoxDebatesDeControlPoliticoGrafica] = useState(JSON.parse(charts).ValueBoxDebatesDeControlPolitico.value_box.debates_de_control_politicos.grafica);
    const [C_ValueBoxProyectosRadicadosGrafica, setValueBoxProyectosRadicadosGrafica] = useState(JSON.parse(charts).ValueBoxProyectosRadicados.value_box.proyectos_radicados.grafica);
    const [C_ValueBoxProyectoDeLeyGrafica, setValueBoxProyectoDeLeyGrafica] = useState(JSON.parse(charts).ValueBoxProyectoDeLey.value_box.proyecto_de_ley.grafica);
    const [C_ValueBoxObjecionesEmitidasGrafica, setValueBoxObjecionesEmitidasGrafica] = useState(JSON.parse(charts).ValueBoxObjecionesEmitidas.value_box.objeciones_emitidas.grafica);
    const [C_ValueBoxSentenciasEmitidasGrafica, setValueBoxSentenciasEmitidasGrafica] = useState(JSON.parse(charts).ValueBoxSentenciasEmitidas.value_box.sentencias_emitidas.grafica);
    const [C_CamaraOrigenIniciativasGrafica, setCamaraOrigenIniciativasGrafica] = useState(JSON.parse(charts).CamaraOrigenIniciativas.camara.origen_iniciativa.grafica);
    const [C_CongresoHoyTop10TemasProyectosLeyGrafica, setCongresoHoyTop10TemasProyectosLeyGrafica] = useState(JSON.parse(charts).CongresoHoyTop10TemasProyectosLey.congreso_hoy.top_10_temas_proyectos_ley.grafica);
    const [C_CongresoHoyEstadoProyectosLeyGrafica, setCongresoHoyEstadoProyectosLeyGrafica] = useState(JSON.parse(charts).CongresoHoyEstadoProyectosLey.congreso_hoy.estado_proyectos_ley.grafica);

    useEffect(async () => {
        let { ValueBoxAudienciasPublicasCitadasSeries, ValueBoxAudienciasPublicasCitadasTotal } = await ValueBoxAudienciasPublicasCitadas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxAudienciasPublicasCitadasGrafica({ series: ValueBoxAudienciasPublicasCitadasSeries, subtitle: { text: ValueBoxAudienciasPublicasCitadasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxAudienciasPublicasCitadas: false })

        let { ValueBoxDebatesDeControlPoliticoSeries, ValueBoxDebatesDeControlPoliticoTotal } = await ValueBoxDebatesDeControlPolitico_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxDebatesDeControlPoliticoGrafica({ series: ValueBoxDebatesDeControlPoliticoSeries, subtitle: { text: ValueBoxDebatesDeControlPoliticoTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxDebatesDeControlPolitico: false })

        let { ValueBoxObjecionesEmitidasSeries, ValueBoxObjecionesEmitidasTotal } = await ValueBoxObjecionesEmitidas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxObjecionesEmitidasGrafica({ series: ValueBoxObjecionesEmitidasSeries, subtitle: { text: ValueBoxObjecionesEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxObjecionesEmitidas: false })

        let { ValueBoxProyectoDeLeySeries, ValueBoxProyectoDeLeyTotal } = await ValueBoxProyectoDeLey_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxProyectoDeLeyGrafica({ series: ValueBoxProyectoDeLeySeries, title: { text: `Proyectos de ley en ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: ValueBoxProyectoDeLeyTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectoDeLey: false })

        let { ValueBoxProyectosRadicadosSeries, ValueBoxProyectosRadicadosTotal } = await ValueBoxProyectosRadicados_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxProyectosRadicadosGrafica({ series: ValueBoxProyectosRadicadosSeries, subtitle: { text: ValueBoxProyectosRadicadosTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectosRadicados: false })

        let { ValueBoxSentenciasEmitidasSeries, ValueBoxSentenciasEmitidasTotal } = await ValueBoxSentenciasEmitidas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setValueBoxSentenciasEmitidasGrafica({ series: ValueBoxSentenciasEmitidasSeries, subtitle: { text: ValueBoxSentenciasEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxSentenciasEmitidas: false })

        let { CamaraOrigenIniciativasData } = await CamaraOrigenIniciativas_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setCamaraOrigenIniciativasGrafica({ series: { data: CamaraOrigenIniciativasData }, title: { text: `Origen de las iniciativas - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CamaraOrigenIniciativas: false })

        let { CongresoHoyTop10TemasProyectosLeySeries, CongresoHoyTop10TemasProyectosLeyTotal } = await CongresoHoyTop10TemasProyectosLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setCongresoHoyTop10TemasProyectosLeyGrafica({ series: CongresoHoyTop10TemasProyectosLeySeries, title: { text: `Top 10: Temas de los proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyTop10TemasProyectosLey: false })

        let { CongresoHoyEstadoProyectosLeySeries, CongresoHoyEstadoProyectosLeyTotal } = await CongresoHoyEstadoProyectosLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setCongresoHoyEstadoProyectosLeyGrafica({ series: CongresoHoyEstadoProyectosLeySeries, title: { text: `Estado de los proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyEstadoProyectosLey: false })

        setSubloaders(false)
    }, []);

    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }

    const ComboCongresoHoy_handlerSelectComboLegislatura = async (selectLegislatura) => {
        setSubloaders(true)
        setSelectedLegislatura(selectLegislatura);

        let { ValueBoxAudienciasPublicasCitadasSeries, ValueBoxAudienciasPublicasCitadasTotal } = await ValueBoxAudienciasPublicasCitadas_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxAudienciasPublicasCitadasGrafica({ series: ValueBoxAudienciasPublicasCitadasSeries, subtitle: { text: ValueBoxAudienciasPublicasCitadasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxAudienciasPublicasCitadas: false })

        let { ValueBoxDebatesDeControlPoliticoSeries, ValueBoxDebatesDeControlPoliticoTotal } = await ValueBoxDebatesDeControlPolitico_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxDebatesDeControlPoliticoGrafica({ series: ValueBoxDebatesDeControlPoliticoSeries, subtitle: { text: ValueBoxDebatesDeControlPoliticoTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxDebatesDeControlPolitico: false })

        let { ValueBoxObjecionesEmitidasSeries, ValueBoxObjecionesEmitidasTotal } = await ValueBoxObjecionesEmitidas_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxObjecionesEmitidasGrafica({ series: ValueBoxObjecionesEmitidasSeries, subtitle: { text: ValueBoxObjecionesEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxObjecionesEmitidas: false })

        let { ValueBoxProyectoDeLeySeries, ValueBoxProyectoDeLeyTotal } = await ValueBoxProyectoDeLey_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxProyectoDeLeyGrafica({ series: ValueBoxProyectoDeLeySeries, title: { text: `Proyectos de ley en ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: ValueBoxProyectoDeLeyTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectoDeLey: false })

        let { ValueBoxProyectosRadicadosSeries, ValueBoxProyectosRadicadosTotal } = await ValueBoxProyectosRadicados_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxProyectosRadicadosGrafica({ series: ValueBoxProyectosRadicadosSeries, subtitle: { text: ValueBoxProyectosRadicadosTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectosRadicados: false })

        let { ValueBoxSentenciasEmitidasSeries, ValueBoxSentenciasEmitidasTotal } = await ValueBoxSentenciasEmitidas_handlerValueBox(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setValueBoxSentenciasEmitidasGrafica({ series: ValueBoxSentenciasEmitidasSeries, subtitle: { text: ValueBoxSentenciasEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxSentenciasEmitidas: false })

        let { CamaraOrigenIniciativasData } = await CamaraOrigenIniciativas_handlerGetChart(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setCamaraOrigenIniciativasGrafica({ series: { data: CamaraOrigenIniciativasData }, title: { text: `Origen de las iniciativas - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${selectLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + selectLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CamaraOrigenIniciativas: false })

        let { CongresoHoyTop10TemasProyectosLeySeries, CongresoHoyTop10TemasProyectosLeyTotal } = await CongresoHoyTop10TemasProyectosLey_handlerGetChart(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setCongresoHoyTop10TemasProyectosLeyGrafica({ series: CongresoHoyTop10TemasProyectosLeySeries, title: { text: `Top 10: Temas de los proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${selectLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + selectLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyTop10TemasProyectosLey: false })

        let { CongresoHoyEstadoProyectosLeySeries, CongresoHoyEstadoProyectosLeyTotal } = await CongresoHoyEstadoProyectosLey_handlerGetChart(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setCongresoHoyEstadoProyectosLeyGrafica({ series: CongresoHoyEstadoProyectosLeySeries, title: { text: `Estado de los proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${selectLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + selectLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyEstadoProyectosLey: false })
        setSubloaders(false)
    }
    const ComboCongresoHoy_handlerSelectComboCorporacion = async (selectCorporacion) => {
        setSubloaders(true)
        setSelectedCorporacion(selectCorporacion);

        let { ValueBoxAudienciasPublicasCitadasSeries, ValueBoxAudienciasPublicasCitadasTotal } = await ValueBoxAudienciasPublicasCitadas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxAudienciasPublicasCitadasGrafica({ series: ValueBoxAudienciasPublicasCitadasSeries, subtitle: { text: ValueBoxAudienciasPublicasCitadasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxAudienciasPublicasCitadas: false })

        let { ValueBoxDebatesDeControlPoliticoSeries, ValueBoxDebatesDeControlPoliticoTotal } = await ValueBoxDebatesDeControlPolitico_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxDebatesDeControlPoliticoGrafica({ series: ValueBoxDebatesDeControlPoliticoSeries, subtitle: { text: ValueBoxDebatesDeControlPoliticoTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxDebatesDeControlPolitico: false })

        let { ValueBoxObjecionesEmitidasSeries, ValueBoxObjecionesEmitidasTotal } = await ValueBoxObjecionesEmitidas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxObjecionesEmitidasGrafica({ series: ValueBoxObjecionesEmitidasSeries, subtitle: { text: ValueBoxObjecionesEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxObjecionesEmitidas: false })

        let { ValueBoxProyectoDeLeySeries, ValueBoxProyectoDeLeyTotal } = await ValueBoxProyectoDeLey_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxProyectoDeLeyGrafica({ series: ValueBoxProyectoDeLeySeries, title: { text: `Proyectos de ley en ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: ValueBoxProyectoDeLeyTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectoDeLey: false })

        let { ValueBoxProyectosRadicadosSeries, ValueBoxProyectosRadicadosTotal } = await ValueBoxProyectosRadicados_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxProyectosRadicadosGrafica({ series: ValueBoxProyectosRadicadosSeries, subtitle: { text: ValueBoxProyectosRadicadosTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxProyectosRadicados: false })

        let { ValueBoxSentenciasEmitidasSeries, ValueBoxSentenciasEmitidasTotal } = await ValueBoxSentenciasEmitidas_handlerValueBox(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setValueBoxSentenciasEmitidasGrafica({ series: ValueBoxSentenciasEmitidasSeries, subtitle: { text: ValueBoxSentenciasEmitidasTotal } });
        // setAllSubloaders({ ...AllSubloaders, ValueBoxSentenciasEmitidas: false })

        let { CamaraOrigenIniciativasData } = await CamaraOrigenIniciativas_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setCamaraOrigenIniciativasGrafica({ series: { data: CamaraOrigenIniciativasData }, title: { text: `Origen de las iniciativas - ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CamaraOrigenIniciativas: false })

        let { CongresoHoyTop10TemasProyectosLeySeries, CongresoHoyTop10TemasProyectosLeyTotal } = await CongresoHoyTop10TemasProyectosLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setCongresoHoyTop10TemasProyectosLeyGrafica({ series: CongresoHoyTop10TemasProyectosLeySeries, title: { text: `Top 10: Temas de los proyectos de ley - ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyTop10TemasProyectosLey: false })

        let { CongresoHoyEstadoProyectosLeySeries, CongresoHoyEstadoProyectosLeyTotal } = await CongresoHoyEstadoProyectosLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setCongresoHoyEstadoProyectosLeyGrafica({ series: CongresoHoyEstadoProyectosLeySeries, title: { text: `Estado de los proyectos de ley - ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyEstadoProyectosLey: false })
        setSubloaders(false)
    }
    return (
        <>
            <Head>
                <title>Congreso Hoy | Proyectos de ley</title>
                <meta name="description" content="Reportes actuales sobre proyectos de ley" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li className="active">
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li>
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active" >
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li className="active">

                                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Proyectos de ley</p></div>
                                                </a>

                                            </li>
                                            <li>

                                                <a href={"/datos/congreso-hoy/congresistas-activos"}>
                                                    <div className="icon"><i className="fas fa-users"></i></div>
                                                    <div className="desc"><p>Congresistas más activos</p></div>
                                                </a>

                                            </li>
                                            <li>

                                                <a href={"/datos/congreso-hoy/actividades-partidos-politicos"}>
                                                    <div className="icon"><i className="fa fa-vote-yea"></i></div>
                                                    <div className="desc"><p>Actividades por partido político</p></div>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        {/* <div className="item none">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={this.state.ComboCongresoHoy.combo.cuatrienio.data}
                                                                selectOnchange={this.ComboCongresohoy_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                disabled={true}
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div> */}
                                                        <div className="item">
                                                            <label htmlFor="">Legislatura</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterLegislatura}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectLegislatura}
                                                                selectOnchange={ComboCongresoHoy_handlerSelectComboLegislatura}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                        <div className="item">
                                                            <label htmlFor="">Corporación</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCorporacion}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCorporacion}
                                                                selectOnchange={ComboCongresoHoy_handlerSelectComboCorporacion}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxProyectosRadicados ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxProyectosRadicadosGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxProyectoDeLey ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxProyectoDeLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxDebatesDeControlPolitico ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxDebatesDeControlPoliticoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxAudienciasPublicasCitadas ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxAudienciasPublicasCitadasGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxObjecionesEmitidas ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxObjecionesEmitidasGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox sm">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.ValueBoxSentenciasEmitidas ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_ValueBoxSentenciasEmitidasGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div className="principalDataContainer">
                                                    {/* <div className="dataValueBox md">
                                                        <p className="dataTempText">Número de PL en trámite en cámara</p>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <p className="dataTempText">Número de PL en trámite en senado</p>
                                                    </div> */}
                                                    <div className="dataValueBox md long">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CamaraOrigenIniciativas ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CamaraOrigenIniciativasGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md long">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyTop10TemasProyectosLey ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyTop10TemasProyectosLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyEstadoProyectosLey ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyEstadoProyectosLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default CongresoHoyProyectosDeLey;
import { useState, useEffect } from "react";
import Head from "next/head";
import Link from "next/link"
import DatosSenadoCongresoHoyDataService from "../../../../Services/Datos/Datos.Service";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarCamaraRepresentantesMayorNumeroAutoriasProyectoLey from "../../../../GraficasConstantes/VarCamaraRepresentantesMayorNumeroAutoriasProyectoLey";
import VarCongresoHoyCongresistasMasCitaciones from "../../../../GraficasConstantes/VarCongresoHoyCongresistasMasCitaciones";

if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    CamaraRepresentantesMayorNumeroAutoriasProyectoLey: true,
    CongresoHoyCongresistasMasCitaciones: true
}
const ConstCharts = {
    CamaraRepresentantesMayorNumeroAutoriasProyectoLey: {
        camara: Object.assign({}, VarCamaraRepresentantesMayorNumeroAutoriasProyectoLey.default_camara()),
    },
    CongresoHoyCongresistasMasCitaciones: {
        control_politico: Object.assign({}, VarCongresoHoyCongresistasMasCitaciones.control_politico())
    },
    ComboCongresoHoy: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Ver todos",
                },
                data: [],
                error: "",
            },
            legislatura: {
                item: {
                    value: "",
                    label: "Ver todas",
                },
                data: [],
                error: "",
            },
            corporacion: {
                item: {
                    value: "",
                    label: "Ver ambas",
                },
                data: [],
                error: "",
            },
        },
    },
}
// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();
    let { DSLegislatura, SelectedLegislatura } = await getLegislatura(SelectedCuatrienio.value);
    let { DSCorporacion, SelectedCorporacion } = await getCorporacion()

    // let { CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries } = await CamaraRepresentantesMayorNumeroAutoriasProyectoLey_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.CamaraRepresentantesMayorNumeroAutoriasProyectoLey.camara.representantes_mayor_numero_autorias_proyecto_ley.grafica.series = CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries;
    // graficas.CamaraRepresentantesMayorNumeroAutoriasProyectoLey.camara.representantes_mayor_numero_autorias_proyecto_ley.grafica.title.text = `Representantes con mayor número de autorías de proyectos de ley - ${SelectedCorporacion.value === "" ? "Cámara y Senado" : SelectedCorporacion.label}`
    // graficas.CamaraRepresentantesMayorNumeroAutoriasProyectoLey.camara.representantes_mayor_numero_autorias_proyecto_ley.grafica.subtitle.text = `${SelectedLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + SelectedLegislatura.label}`;


    // let { CongresoHoyCongresistasMasCitacionesData } = await CongresoHoyCongresistasMasCitaciones_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value, SelectedCorporacion.value)
    // graficas.CongresoHoyCongresistasMasCitaciones.control_politico.congresistas_mas_citaciones.grafica.series[0].data = CongresoHoyCongresistasMasCitacionesData;
    // graficas.CongresoHoyCongresistasMasCitaciones.control_politico.congresistas_mas_citaciones.grafica.title.text = `Congresistas con más citaciones - ${SelectedCorporacion.value === "" ? "Cámara y Senado" : SelectedCorporacion.label}`
    // graficas.CongresoHoyCongresistasMasCitaciones.control_politico.congresistas_mas_citaciones.grafica.subtitle.text = `${SelectedLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + SelectedLegislatura.label}`

    return {
        props: { charts: JSON.stringify(graficas), DSCorporacion, SelectedCorporacion, DSCuatrienio, SelectedCuatrienio, DSLegislatura, SelectedLegislatura }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let getLegislatura = false;
        let year = new Date().getFullYear();

        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
                getLegislatura = true;
            }
        });

        if (!getLegislatura)
            selected = { value: response.data[0].id, label: response.data[0].nombre }

        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const getLegislatura = async (cuatrienio_id) => { // método general para llenar todos los obj de legislatura
    let combo = [];
    let selected = Object.assign({}, ConstCharts.ComboCongresoHoy.combo.legislatura.item);
    await UtilsDataService.getComboLegislatura(cuatrienio_id).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.legislatura.item));
    });


    return { DSLegislatura: combo, SelectedLegislatura: selected };
};
const getCorporacion = async () => {
    let combo = [];
    let selected = Object.assign({}, ConstCharts.ComboCongresoHoy.combo.corporacion.item);
    await UtilsDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboCongresoHoy.combo.corporacion.item));
    });

    return { DSCorporacion: combo, SelectedCorporacion: selected }
};
const CamaraRepresentantesMayorNumeroAutoriasProyectoLey_handlerGetChart = async (cuatrienio, legislatura, corporacion) => {
    let series = [{ "group": "group", "type": "lollipop", data: [] }];
    await DatosDataService.getCamaraRepresentantesMayorNumeroAutoriasProyectoLey(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push({
                "low": currentValue.n,
                "name": currentValue.nombre_final
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries: series }
};
const CongresoHoyCongresistasMasCitaciones_handlerGetChart = async (cuatrienio, legislatura, corporacion) => {
    let data = [];
    await DatosDataService.getCongresoHoyCongresistasMasCitaciones(cuatrienio, legislatura, corporacion).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            data.push({
                // "tipo_proyecto": currentValue.tipo_proyecto_ley,
                "nombre": currentValue.nombre,
                "n": currentValue.n,
                "y": currentValue.n,
                "name": currentValue.nombre
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyCongresistasMasCitacionesData: data }
};
// EXPORT
const CongresoHoyCongresistasActivos = ({ charts = ConstCharts, DSCorporacion = ConstCharts.ComboCongresoHoy.combo.corporacion.data, SelectedCorporacion = ConstCharts.ComboCongresoHoy.combo.corporacion.item, DSCuatrienio = ConstCharts.ComboCongresoHoy.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboCongresoHoy.combo.cuatrienio.item, DSLegislatura = ConstCharts.ComboCongresoHoy.combo.legislatura.data, SelectedLegislatura = ConstCharts.ComboCongresoHoy.combo.legislatura.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCorporacion, setDSCorporacion] = useState(DSCorporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [dataSelectLegislatura, setDSLegislatura] = useState(DSLegislatura);
    const [filterCorporacion, setSelectedCorporacion] = useState(SelectedCorporacion);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [filterLegislatura, setSelectedLegislatura] = useState(SelectedLegislatura);
    const [C_CamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica, setCamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica] = useState(JSON.parse(charts).CamaraRepresentantesMayorNumeroAutoriasProyectoLey.camara.representantes_mayor_numero_autorias_proyecto_ley.grafica);
    const [C_CongresoHoyCongresistasMasCitacionesGrafica, setCongresoHoyCongresistasMasCitacionesGrafica] = useState(JSON.parse(charts).CongresoHoyCongresistasMasCitaciones.control_politico.congresistas_mas_citaciones.grafica);

    useEffect(async () => {
        setSubloaders(false)

        let { CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries } = await CamaraRepresentantesMayorNumeroAutoriasProyectoLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setCamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica({ series: CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries, title: { text: `Representantes con mayor número de autorías de proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });

        let { CongresoHoyCongresistasMasCitacionesData } = await CongresoHoyCongresistasMasCitaciones_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, filterCorporacion.value)
        setCongresoHoyCongresistasMasCitacionesGrafica({ series: { data: CongresoHoyCongresistasMasCitacionesData }, title: { text: `Congresistas con más citaciones - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });

    }, []);

    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }

    const ComboCongresoHoy_handlerSelectComboLegislatura = async (selectLegislatura) => {
        setSubloaders(true)
        setSelectedLegislatura(selectLegislatura);

        let { CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries } = await CamaraRepresentantesMayorNumeroAutoriasProyectoLey_handlerGetChart(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setCamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica({ series: CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries, title: { text: `Representantes con mayor número de autorías de proyectos de ley - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${selectLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + selectLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CamaraRepresentantesMayorNumeroAutoriasProyectoLey: false })

        let { CongresoHoyCongresistasMasCitacionesData } = await CongresoHoyCongresistasMasCitaciones_handlerGetChart(filterCuatrienio.value, selectLegislatura.value, filterCorporacion.value)
        setCongresoHoyCongresistasMasCitacionesGrafica({ series: { data: CongresoHoyCongresistasMasCitacionesData }, title: { text: `Congresistas con más citaciones - ${filterCorporacion.value === "" ? "Cámara y Senado" : filterCorporacion.label}` }, subtitle: { text: `${selectLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + selectLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyCongresistasMasCitaciones: false })

        setSubloaders(false)
    }
    const ComboCongresoHoy_handlerSelectComboCorporacion = async (selectCorporacion) => {
        setSubloaders(true)
        setSelectedCorporacion(selectCorporacion);

        let { CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries } = await CamaraRepresentantesMayorNumeroAutoriasProyectoLey_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setCamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica({ series: CamaraRepresentantesMayorNumeroAutoriasProyectoLeySeries, title: { text: `Representantes con mayor número de autorías de proyectos de ley - ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CamaraRepresentantesMayorNumeroAutoriasProyectoLey: false })

        let { CongresoHoyCongresistasMasCitacionesData } = await CongresoHoyCongresistasMasCitaciones_handlerGetChart(filterCuatrienio.value, filterLegislatura.value, selectCorporacion.value)
        setCongresoHoyCongresistasMasCitacionesGrafica({ series: { data: CongresoHoyCongresistasMasCitacionesData }, title: { text: `Congresistas con más citaciones - ${selectCorporacion.value === "" ? "Cámara y Senado" : selectCorporacion.label}` }, subtitle: { text: `${filterLegislatura.value === "" ? "Todas las legislaturas" : "Legislatura " + filterLegislatura.label}` } });
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyCongresistasMasCitaciones: false })
        setSubloaders(false)
    }

    return (
        <>
            <Head>
                <title>Congreso Hoy | Congresistas más activos</title>
                <meta name="description" content="Reportes actuales sobre congresistas más activos" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li className="active">
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li>
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active" >
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li>
                                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Proyectos de ley</p></div>
                                                </a>
                                            </li>
                                            <li className="active">
                                                <a href={"/datos/congreso-hoy/congresistas-activos"}>
                                                    <div className="icon"><i className="fas fa-users"></i></div>
                                                    <div className="desc"><p>Congresistas más activos</p></div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href={"/datos/congreso-hoy/actividades-partidos-politicos"}>
                                                    <div className="icon"><i className="fa fa-vote-yea"></i></div>
                                                    <div className="desc"><p>Actividades por partido político</p></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        {/* <div className="item none">
                                            <label htmlFor="">Cuatrienio</label>
                                            <Select
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={this.state.ComboCongresoHoy.combo.cuatrienio.item}
                                                selectIsSearchable={true}
                                                disabled={true}
                                                selectoptions={this.state.ComboCongresoHoy.combo.cuatrienio.data}
                                                selectOnchange={this.ComboCongresohoy_handlerSelectComboCuatrienio}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass="error"
                                                spanError={""}
                                            />
                                        </div> */}
                                                        <div className="item">
                                                            <label htmlFor="">Legislatura</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterLegislatura}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectLegislatura}
                                                                selectOnchange={ComboCongresoHoy_handlerSelectComboLegislatura}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                        <div className="item">
                                                            <label htmlFor="">Corporación</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCorporacion}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCorporacion}
                                                                selectOnchange={ComboCongresoHoy_handlerSelectComboCorporacion}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CamaraRepresentantesMayorNumeroAutoriasProyectoLey ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CamaraRepresentantesMayorNumeroAutoriasProyectoLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyCongresistasMasCitaciones ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyCongresistasMasCitacionesGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default CongresoHoyCongresistasActivos;
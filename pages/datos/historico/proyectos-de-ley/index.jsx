import { useState, useEffect } from "react";
import Head from "next/head";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarHistoricoCantidadTemasUnCuatrienio from "../../../../GraficasConstantes/VarHistoricoCantidadTemasUnCuatrienio";
import VarHistoricoTotalProyectoDeLeySancionadosPorAño from "../../../../GraficasConstantes/VarHistoricoTotalProyectoDeLeySancionadosPorAño";
import VarHistoricoCantidadTemasTodosCuatrienios from "../../../../GraficasConstantes/VarHistoricoCantidadTemasTodosCuatrienios";
import VarHistoricoTiempoSancionProyectosDeLey from "../../../../GraficasConstantes/VarHistoricoTiempoSancionProyectosDeLey";


if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    HistoricoCantidadTemasUnCuatrienio: true,
    HistoricoTotalProyectoDeLeySancionadosPorAño: true,
    HistoricoCantidadTemasTodosCuatrienios: true,
    HistoricoTiempoSancionProyectosDeLey: true,
}

const ConstCharts = {
    HistoricoCantidadTemasUnCuatrienio: {
        default_historico: Object.assign({}, VarHistoricoCantidadTemasUnCuatrienio.default_historico()),
    },
    HistoricoTotalProyectoDeLeySancionadosPorAño: {
        default_historico: Object.assign({}, VarHistoricoTotalProyectoDeLeySancionadosPorAño.default_historico()),
    },
    HistoricoCantidadTemasTodosCuatrienios: {
        default_historico: Object.assign({}, VarHistoricoCantidadTemasTodosCuatrienios.default_historico()),
    },
    HistoricoTiempoSancionProyectosDeLey: {
        default_historico: Object.assign({}, VarHistoricoTiempoSancionProyectosDeLey.default_historico()),
    },
    ComboProyectosLey: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Ver todos",
                },
                data: [],
                error: "",
            }
        }
    }
}

export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();

    // let { HistoricoCantidadTemasUnCuatrienioSeries } = await HistoricoCantidadTemasUnCuatrienio_handlerGetChart(SelectedCuatrienio.value);
    // graficas.HistoricoCantidadTemasUnCuatrienio.default_historico.cantidad_temas_un_cuatrienio.grafica.series = HistoricoCantidadTemasUnCuatrienioSeries;

    // let { HistoricoTotalProyectoDeLeySancionadosPorAñoSeries } = await HistoricoTotalProyectoDeLeySancionadosPorAño_handlerGetChart();
    // graficas.HistoricoTotalProyectoDeLeySancionadosPorAño.default_historico.total_proyecto_de_ley_sancionados_por_año.grafica.series = HistoricoTotalProyectoDeLeySancionadosPorAñoSeries;

    // let { HistoricoCantidadTemasTodosCuatrieniosSeries, HistoricoCantidadTemasTodosCuatrieniosCategories } = await HistoricoCantidadTemasTodosCuatrienios_handlerGetChart();
    // graficas.HistoricoCantidadTemasTodosCuatrienios.default_historico.cantidad_temas_todos_cuatrienios.grafica.series = HistoricoCantidadTemasTodosCuatrieniosSeries;
    // graficas.HistoricoCantidadTemasTodosCuatrienios.default_historico.cantidad_temas_todos_cuatrienios.grafica.xAxis.categories = HistoricoCantidadTemasTodosCuatrieniosCategories;

    // let { HistoricoTiempoSancionProyectosDeLeySeries } = await HistoricoTiempoSancionProyectosDeLey_handlerGetChart();
    // graficas.HistoricoTiempoSancionProyectosDeLey.default_historico.tiempo_sancion_proyectos_de_ley.grafica.series = HistoricoTiempoSancionProyectosDeLeySeries;

    return {
        props: { charts: JSON.stringify(graficas), DSCuatrienio, SelectedCuatrienio }
    }
}

const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let year = new Date().getFullYear();
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };

            }
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboProyectosLey.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};

const HistoricoCantidadTemasUnCuatrienio_handlerGetChart = async (cuatrienio) => {
    let series = [{
        "group": "group",
        "data": [],
        "type": "treemap"
    }];
    let total = 0;

    await DatosDataService.getCantidadTemasUnCuatrienio(
        cuatrienio
    )
        .then((response) => {


            response.data.forEach((currentValue, index, array) => {
                total += currentValue.n;
                series[0].data.push(
                    {
                        "fecha_inicio_cuatri": currentValue.cuatrienio,
                        "tema": currentValue.tema,
                        "n": currentValue.n,
                        "totales": currentValue.n,
                        "name": currentValue.tema,
                    });
            });

            series.forEach((currentValue, index, array) => {
                currentValue.data.forEach((currentValueData, index, array) => {
                    currentValueData.porc = parseFloat(((currentValueData.n * 100) / total).toFixed(2));
                    currentValueData.value = currentValueData.porc;
                    currentValueData.totales = total;
                });
            });

            let cuatrienio = series[0]?.data[0]?.fecha_inicio_cuatri ?? "";


            // grafica: {
            //     ...prevState.HistoricoCantidadTemasUnCuatrienio.historico.cantidad_temas_un_cuatrienio.grafica,
            //     title: {
            //         ...prevState.HistoricoCantidadTemasUnCuatrienio.historico.cantidad_temas_un_cuatrienio.grafica.title,
            //         text: `Temas de los proyectos - ${cuatrienio_res.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + cuatrienio_res.label}`
            //     },
            //     series: series,
            // },

        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoCantidadTemasUnCuatrienioSeries: series };
};

// *** Histórico que NO reaccionan al filtro

// HistoricoTotalProyectoDeLeySancionadosPorAño

const HistoricoTotalProyectoDeLeySancionadosPorAño_handlerGetChart = async () => {
    let series = [{
        "group": "group",
        "data": [],
        "type": "bar"
    }];
    await DatosDataService.getTotalProyectoDeLeySancionadosPorAño().then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push(
                {
                    "cuatrienio": currentValue.cuatrienio,
                    "n": currentValue.n,
                    "y": currentValue.n,
                    "name": currentValue.cuatrienio,
                });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoTotalProyectoDeLeySancionadosPorAñoSeries: series }
};

// End HistoricoTotalProyectoDeLeySancionadosPorAño

// HistoricoCantidadTemasTodosCuatrienios

const HistoricoCantidadTemasTodosCuatrienios_handlerGetChart = async () => {
    let series = [];
    let cuatrienios = [];
    let categories = [];
    await DatosDataService.getCantidadTemasTodosCuatrienios()
        .then((response) => {


            response.data.forEach((currentValue, index, array) => {
                if (series.length === 0) {
                    series.push(
                        {
                            "name": currentValue.tema,
                            "type": "area",
                            data: [{
                                "fecha_inicio_cuatri": currentValue.cuatrienio,
                                "tema": currentValue.tema,
                                "n": currentValue.n,
                                "totales": currentValue.n
                            }],
                        });
                }
                else {
                    let encontrado = false;
                    let tema_actual = currentValue.tema;
                    for (let i = 0; i < series.length; i++) {
                        let currentValueSerie = series[i];
                        if (currentValueSerie.name === tema_actual) {

                            currentValueSerie.data.push({
                                "fecha_inicio_cuatri": currentValue.cuatrienio,
                                "tema": currentValue.tema,
                                "n": currentValue.n,
                                "totales": currentValue.cuatrienio === currentValueSerie.cuatrienio
                                    ? currentValue.total + currentValue.n
                                    : currentValue.total + 0
                            });
                            encontrado = true;
                            break;
                        }
                    }
                    if (!encontrado) {
                        series.push(
                            {
                                "name": currentValue.tema,
                                "type": "area",
                                data: [{
                                    "fecha_inicio_cuatri": currentValue.cuatrienio,
                                    "tema": currentValue.tema,
                                    "n": currentValue.n,
                                    "totales": currentValue.n
                                }],
                            });
                    }
                }

                if (cuatrienios.length === 0) {
                    cuatrienios.push({
                        cuatrienio: currentValue.cuatrienio,
                        total: currentValue.n
                    })
                }
                else {
                    let encontradoCuatrienio = false;
                    let cuatrienio_actual = currentValue.cuatrienio;
                    for (let i = 0; i < cuatrienios.length; i++) {
                        let currentValueCuatrienio = cuatrienios[i];

                        if (currentValueCuatrienio.cuatrienio === cuatrienio_actual) {

                            currentValueCuatrienio.total += currentValue.n;
                            encontradoCuatrienio = true;
                            break;
                        }
                    }
                    if (!encontradoCuatrienio) {
                        cuatrienios.push({
                            cuatrienio: currentValue.cuatrienio,
                            total: currentValue.n
                        })
                    }
                }
            });

            series.forEach((currentValue, index, array) => {
                currentValue.data.forEach((currentValueData, index, array) => {
                    let total = 0;
                    for (let i = 0; i < cuatrienios.length; i++) {
                        if (cuatrienios[i].cuatrienio === currentValueData.fecha_inicio_cuatri) {
                            total = cuatrienios[i].total;
                            break;
                        }
                    }
                    currentValueData.porc = parseFloat(((currentValueData.n * 100) / total).toFixed(2));
                    currentValueData.y = currentValueData.porc;
                });
            });

            categories = cuatrienios.map(a => a.cuatrienio);



        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoCantidadTemasTodosCuatrieniosSeries: series, HistoricoCantidadTemasTodosCuatrieniosCategories: categories }
};

// End HistoricoCantidadTemasTodosCuatrienios

// HistoricoTiempoSancionProyectosDeLey

const HistoricoTiempoSancionProyectosDeLey_handlerGetChart = async () => {
    let series = [];
    await DatosDataService.getTiempoSancionProyectosDeLey()
        .then((response) => {
            response.data.forEach((currentValue, index, array) => {
                series = response.data;
            });
        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoTiempoSancionProyectosDeLeySeries: series }
};

// End HistoricoTiempoSancionProyectosDeLey


const HistProyectosLey = ({ charts = ConstCharts, DSCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [C_HistoricoCantidadTemasUnCuatrienioGrafica, setHistoricoCantidadTemasUnCuatrienio] = useState(JSON.parse(charts).HistoricoCantidadTemasUnCuatrienio.default_historico.cantidad_temas_un_cuatrienio.grafica);
    const [C_HistoricoTotalProyectoDeLeySancionadosPorAñoGrafica, setHistoricoTotalProyectoDeLeySancionadosPorAño] = useState(JSON.parse(charts).HistoricoTotalProyectoDeLeySancionadosPorAño.default_historico.total_proyecto_de_ley_sancionados_por_año.grafica);
    const [C_HistoricoCantidadTemasTodosCuatrieniosGrafica, setHistoricoCantidadTemasTodosCuatrienios] = useState(JSON.parse(charts).HistoricoCantidadTemasTodosCuatrienios.default_historico.cantidad_temas_todos_cuatrienios.grafica);
    const [C_HistoricoTiempoSancionProyectosDeLeyGrafica, setHistoricoTiempoSancionProyectosDeLey] = useState(JSON.parse(charts).HistoricoTiempoSancionProyectosDeLey.default_historico.tiempo_sancion_proyectos_de_ley.grafica);

    useEffect(async () => {
        setSubloaders(false);

        let { HistoricoCantidadTemasUnCuatrienioSeries } = await HistoricoCantidadTemasUnCuatrienio_handlerGetChart(SelectedCuatrienio.value);
        setHistoricoCantidadTemasUnCuatrienio({
            ...C_HistoricoCantidadTemasUnCuatrienioGrafica, series: HistoricoCantidadTemasUnCuatrienioSeries,
            title: { text: `Temas de los proyectos - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}` }
        })

        let { HistoricoTotalProyectoDeLeySancionadosPorAñoSeries } = await HistoricoTotalProyectoDeLeySancionadosPorAño_handlerGetChart();
        setHistoricoTotalProyectoDeLeySancionadosPorAño({ series: HistoricoTotalProyectoDeLeySancionadosPorAñoSeries })

        let { HistoricoCantidadTemasTodosCuatrieniosSeries, HistoricoCantidadTemasTodosCuatrieniosCategories } = await HistoricoCantidadTemasTodosCuatrienios_handlerGetChart();
        setHistoricoCantidadTemasTodosCuatrienios({ series: HistoricoCantidadTemasTodosCuatrieniosSeries, xAxis: { categories: HistoricoCantidadTemasTodosCuatrieniosCategories } })

        let { HistoricoTiempoSancionProyectosDeLeySeries } = await HistoricoTiempoSancionProyectosDeLey_handlerGetChart();
        setHistoricoTiempoSancionProyectosDeLey({ series: HistoricoTiempoSancionProyectosDeLeySeries })

    }, []);
    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }
    const ComboHistorico_handlerSelectComboCuatrienio = async (selectCuatrienio) => {
        setAllSubloaders({ ...AllSubloaders, HistoricoCantidadTemasUnCuatrienio: true });
        setSelectedCuatrienio(selectCuatrienio);
        let { HistoricoCantidadTemasUnCuatrienioSeries } = await HistoricoCantidadTemasUnCuatrienio_handlerGetChart(selectCuatrienio.value);
        setHistoricoCantidadTemasUnCuatrienio({
            ...C_HistoricoCantidadTemasUnCuatrienioGrafica, series: HistoricoCantidadTemasUnCuatrienioSeries,
            title: { text: `Temas de los proyectos - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` }
        })
        setAllSubloaders({ ...AllSubloaders, HistoricoCantidadTemasUnCuatrienio: false });
        setSubloaders(false)
    }
    return (
        <>
            <Head>
                <title>Históricos | Proyectos de ley</title>
                <meta name="description" content="Reportes actuales sobre historicos de protectos de ley" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">

                        <div className="contentTab active" data-ref="4">
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li className="active" data-ref="9">

                                                <a href={"/datos/historico/proyectos-de-ley"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Proyectos de ley</p></div>
                                                </a>

                                            </li>
                                            <li data-ref="10">

                                                <a href={"/datos/historico/composicion-partidista"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Composición Partidista</p></div>
                                                </a>

                                            </li>
                                            <li data-ref="11">

                                                <a href={"/datos/historico/relacion-ejecutivo-legislativo"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Relación ejecutivo-legislativo</p></div>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        <div className="item">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCuatrienio}
                                                                selectOnchange={ComboHistorico_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="dataValueBox lg">
                                                    <div className="relative">
                                                        <div className={`subloader ${AllSubloaders.HistoricoCantidadTemasUnCuatrienio ? "active" : ""}`}></div>
                                                        <HighchartsReact
                                                            highcharts={Highcharts}
                                                            options={C_HistoricoCantidadTemasUnCuatrienioGrafica}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoTotalProyectoDeLeySancionadosPorAño ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoTotalProyectoDeLeySancionadosPorAñoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoCantidadTemasTodosCuatrienios ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoCantidadTemasTodosCuatrieniosGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoTiempoSancionProyectosDeLey ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoTiempoSancionProyectosDeLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default HistProyectosLey;
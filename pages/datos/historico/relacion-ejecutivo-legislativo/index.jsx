import { useState, useEffect } from "react";
import Head from "next/head";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarHistoricoNumeroIniciativasGubernamentales from "../../../../GraficasConstantes/VarHistoricoNumeroIniciativasGubernamentales";
import VarHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey from "../../../../GraficasConstantes/VarHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey";
import VarHistoricoTotalCitacionesPorCuatrienio from "../../../../GraficasConstantes/VarHistoricoTotalCitacionesPorCuatrienio";
if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstCharts = {
    HistoricoNumeroIniciativasGubernamentales: {
        default_historico: Object.assign({}, VarHistoricoNumeroIniciativasGubernamentales.default_historico()),
    },
    HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey: {
        default_historico: Object.assign({}, VarHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey.default_historico()),
    },
    HistoricoTotalCitacionesPorCuatrienio: {
        default_historico: Object.assign({}, VarHistoricoTotalCitacionesPorCuatrienio.default_historico()),
    }
}

export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    // let { HistoricoNumeroIniciativasGubernamentalesSeries } = await HistoricoNumeroIniciativasGubernamentales_handlerGetChart();
    // graficas.HistoricoNumeroIniciativasGubernamentales.default_historico.numero_iniciativas_gubernamentales.grafica.series = HistoricoNumeroIniciativasGubernamentalesSeries;
    // let { HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeySeries } = await HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey_handlerGetChart();
    // graficas.HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey.default_historico.numero_iniciativas_gubernamentales_sancionadas_como_ley.grafica.series = HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeySeries;
    // let { HistoricoTotalCitacionesPorCuatrienioSData } = await HistoricoTotalCitacionesPorCuatrienio_handlerGetChart();
    // graficas.HistoricoTotalCitacionesPorCuatrienio.default_historico.total_citaciones_por_cuatrienio.grafica.series[0].data = HistoricoTotalCitacionesPorCuatrienioSData;
    return {
        props: { charts: JSON.stringify(graficas) }
    }
}

// HistoricoNumeroIniciativasGubernamentales

const HistoricoNumeroIniciativasGubernamentales_handlerGetChart = async () => {
    let series = [{
        "group": "group",
        "data": [],
        "type": "bar"
    }];
    await DatosDataService.getNumeroIniciativasGubernamentales()
        .then((response) => {
            response.data.forEach((currentValue, index, array) => {
                series[0].data.push({
                    "cuatrienio": currentValue.cuatrienio,
                    "n": currentValue.n,
                    "y": currentValue.n,
                    "name": currentValue.cuatrienio
                });
            });
        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoNumeroIniciativasGubernamentalesSeries: series };
};

// End HistoricoNumeroIniciativasGubernamentales

// HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey

const HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey_handlerGetChart = async () => {
    let series = [{
        "group": "group",
        "data": [],
        "type": "bar"
    }];

    await DatosDataService.getNumeroIniciativasGubernamentalesSancionadasComoLey()
        .then((response) => {


            response.data.forEach((currentValue, index, array) => {
                series[0].data.push(
                    {
                        "cuatrienio": currentValue.cuatrienio,
                        "n": currentValue.n,
                        "y": currentValue.n,
                        "name": currentValue.cuatrienio,
                    });
            });
        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeySeries: series }
};

// End HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey

// HistoricoTotalCitacionesPorCuatrienio

const HistoricoTotalCitacionesPorCuatrienio_handlerGetChart = async () => {

    let data = [];
    await DatosDataService.getHistoricoTotalCitacionesPorCuatrienio()
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                data.push({
                    // "tipo_proyecto": currentValue.tipo_proyecto_ley,
                    "nombre": currentValue.nombre,
                    "n": currentValue.n,
                    "y": currentValue.n,
                    "name": currentValue.nombre
                });
            });
        })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoTotalCitacionesPorCuatrienioSData: data }
};

// End HistoricoTotalCitacionesPorCuatrienio

const HistRelacionEjecutivoLegislativo = ({ charts = ConstCharts }) => {

    const [C_HistoricoNumeroIniciativasGubernamentalesGrafica, setHistoricoNumeroIniciativasGubernamentales] = useState(JSON.parse(charts).HistoricoNumeroIniciativasGubernamentales.default_historico.numero_iniciativas_gubernamentales.grafica);
    const [C_HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeyGrafica, setHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey] = useState(JSON.parse(charts).HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey.default_historico.numero_iniciativas_gubernamentales_sancionadas_como_ley.grafica);
    const [C_HistoricoTotalCitacionesPorCuatrienioGrafica, setHistoricoTotalCitacionesPorCuatrienio] = useState(JSON.parse(charts).HistoricoTotalCitacionesPorCuatrienio.default_historico.total_citaciones_por_cuatrienio.grafica);

    useEffect(async () => {

        let { HistoricoNumeroIniciativasGubernamentalesSeries } = await HistoricoNumeroIniciativasGubernamentales_handlerGetChart();
        setHistoricoNumeroIniciativasGubernamentales({series: HistoricoNumeroIniciativasGubernamentalesSeries})
        
        let { HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeySeries } = await HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey_handlerGetChart();
        setHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey({series: HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeySeries})
        
        let { HistoricoTotalCitacionesPorCuatrienioSData } = await HistoricoTotalCitacionesPorCuatrienio_handlerGetChart();
        setHistoricoTotalCitacionesPorCuatrienio({series: {data: HistoricoTotalCitacionesPorCuatrienioSData}})

    }, []);

    return (
        <>
            <Head>
                <title>Históricos | Relación ejecutivo legislativo</title>
                <meta name="description" content="Reportes actuales sobre historicos de protectos de ley" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">

                        <div className="contentTab active" data-ref="4">
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li data-ref="9">

                                                <a href={"/datos/historico/proyectos-de-ley"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Proyectos de ley</p></div>
                                                </a>

                                            </li>
                                            <li data-ref="10">

                                                <a href={"/datos/historico/composicion-partidista"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Composición Partidista</p></div>
                                                </a>

                                            </li>
                                            <li className="active" data-ref="11">

                                                <a href={"/datos/historico/relacion-ejecutivo-legislativo"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Relación ejecutivo-legislativo</p></div>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" data-ref="8">
                                            <div className="info one-columns">

                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">

                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoNumeroIniciativasGubernamentalesGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">

                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoNumeroIniciativasGubernamentalesSancionadasComoLeyGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">

                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoTotalCitacionesPorCuatrienioGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default HistRelacionEjecutivoLegislativo;
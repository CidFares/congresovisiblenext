import { useState, useEffect } from "react";
import Head from "next/head";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'
import HighchartsSankey from "highcharts/modules/sankey";

import VarHistoricoDistribucionEdadCongresoTodosCuatrienios from "../../../../GraficasConstantes/VarHistoricoDistribucionEdadCongresoTodosCuatrienios";
import VarHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio from "../../../../GraficasConstantes/VarHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio";
import VarHistoricoProporcionDeMujeres from "../../../../GraficasConstantes/VarHistoricoProporcionDeMujeres";
import VarHistoricoProporcionDeMujeresPorCuatrienio from "../../../../GraficasConstantes/VarHistoricoProporcionDeMujeresPorCuatrienio";
import VarHistoricoTop5PartidosCongresistasMasJovenes from "../../../../GraficasConstantes/VarHistoricoTop5PartidosCongresistasMasJovenes";
import VarHistoricoTop5PartidosCongresistasMayorEdad from "../../../../GraficasConstantes/VarHistoricoTop5PartidosCongresistasMayorEdad";


if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    HighchartsSankey(Highcharts);
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    HistoricoProporcionDeMujeresPorCuatrienio: true,
    HistoricoTop5PartidosCongresistasMasJovenes: true,
    HistoricoTop5PartidosCongresistasMayorEdad: true,
    HistoricoProporcionDeMujeres: true,
    HistoricoDistribucionEdadCongresoTodosCuatrienios: true,
    HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio: true
}

const ConstCharts = {
    HistoricoProporcionDeMujeresPorCuatrienio: {
        historico: Object.assign({}, VarHistoricoProporcionDeMujeresPorCuatrienio.default_historico()),
    },
    HistoricoTop5PartidosCongresistasMasJovenes: {
        congreso_hoy: Object.assign({}, VarHistoricoTop5PartidosCongresistasMasJovenes.default_congreso_hoy()),
    },
    HistoricoTop5PartidosCongresistasMayorEdad: {
        congreso_hoy: Object.assign({}, VarHistoricoTop5PartidosCongresistasMayorEdad.default_congreso_hoy()),
    },
    HistoricoProporcionDeMujeres: {
        historico: Object.assign({}, VarHistoricoProporcionDeMujeres.default_historico()),
    },
    HistoricoDistribucionEdadCongresoTodosCuatrienios: {
        historico: Object.assign({}, VarHistoricoDistribucionEdadCongresoTodosCuatrienios.default_historico()),
    },
    HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio: {
        historico: Object.assign({}, VarHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio.default_historico()),
    },
    ComboHistorico: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Ver todos",
                },
                data: [],
                error: "",
            },
        },
    },
}

// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();

    // let { HistoricoProporcionDeMujeresPorCuatrienioSeries } = await HistoricoProporcionDeMujeresPorCuatrienio_handlerGetChart(SelectedCuatrienio.value);
    // graficas.HistoricoProporcionDeMujeresPorCuatrienio.historico.proporcion_de_mujeres_por_cuatrienio.grafica.series = HistoricoProporcionDeMujeresPorCuatrienioSeries;
    // graficas.HistoricoProporcionDeMujeresPorCuatrienio.historico.proporcion_de_mujeres_por_cuatrienio.grafica.title.text = `Proporción de mujeres por partido - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { HistoricoTop5PartidosCongresistasMasJovenesSeries } = await HistoricoTop5PartidosCongresistasMasJovenes_handlerGetChart(SelectedCuatrienio.value);
    // graficas.HistoricoTop5PartidosCongresistasMasJovenes.congreso_hoy.top_5_partidos_congresistas_mas_jovenes.grafica.series = HistoricoTop5PartidosCongresistasMasJovenesSeries;
    // graficas.HistoricoTop5PartidosCongresistasMasJovenes.congreso_hoy.top_5_partidos_congresistas_mas_jovenes.grafica.title.text = `Top 10 partidos con congresistas más jóvenes - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;

    // let { HistoricoTop5PartidosCongresistasMayorEdadSeries } = await HistoricoTop5PartidosCongresistasMayorEdad_handlerGetChart(SelectedCuatrienio.value);
    // graficas.HistoricoTop5PartidosCongresistasMayorEdad.congreso_hoy.top_5_partidos_congresistas_mayor_edad.grafica.series = HistoricoTop5PartidosCongresistasMayorEdadSeries;
    // graficas.HistoricoTop5PartidosCongresistasMayorEdad.congreso_hoy.top_5_partidos_congresistas_mayor_edad.grafica.title.text = `Top 10 partidos con congresistas de mayor edad - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;

    // let { HistoricoProporcionDeMujeresSeries, HistoricoProporcionDeMujeresCategories } = await HistoricoProporcionDeMujeres_handlerGetChart();
    // graficas.HistoricoProporcionDeMujeres.historico.proporcion_de_mujeres.grafica.series = HistoricoProporcionDeMujeresSeries;
    // graficas.HistoricoProporcionDeMujeres.historico.proporcion_de_mujeres.grafica.xAxis.categories = HistoricoProporcionDeMujeresCategories;

    // let { HistoricoDistribucionEdadCongresoTodosCuatrieniosSeries } = await HistoricoDistribucionEdadCongresoTodosCuatrienios_handlerGetChart();
    // graficas.HistoricoDistribucionEdadCongresoTodosCuatrienios.historico.distribucion_edad_congreso_todos_cuatrienios.grafica.series = HistoricoDistribucionEdadCongresoTodosCuatrieniosSeries;

    // let { HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioSeries } = await HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio_handlerGetChart();
    // graficas.HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio.historico.partidos_mayor_representacion_congreso_por_cuatrienio.grafica.series = HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioSeries;

    return {
        props: { charts: JSON.stringify(graficas), DSCuatrienio, SelectedCuatrienio }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let year = new Date().getFullYear();
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };

            }
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboHistorico.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const HistoricoProporcionDeMujeresPorCuatrienio_handlerGetChart = async (cuatrienio) => {
    let series = [{
        "allowDrillToNode": true,
        "levelIsConstant": true,
        "textOverflow": "clip",
        "dataLabels": {
            "color": "white"
        },
        "levels": [{
            "level": 1,
            "borderWidth": 1,
            "dataLabels": {
                "enabled": false,
                "verticalAlign": "top",
                "align": "left",
                "style": {
                    "fontSize": "12px",
                    "textOutline": false
                }
            }
        }, {
            "level": 2,
            "borderWidth": 0,
            "dataLabels": {
                "enabled": false
            }
        }
        ],
        data: []
    }];
    let partidos = [];
    let index_color = 0;
    await DatosDataService.getProporcionDeMujeresPorCuatrienio(cuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push(
                {
                    "name": currentValue.genero,
                    "color": currentValue.genero === "Masculino"
                        ? "#17789e"
                        : "#66CCFF",
                    "partido": currentValue.partido,
                    "value": currentValue.n,
                    "parent": currentValue.partido
                });

            if (partidos.length === 0) {
                partidos.push({
                    "name": currentValue.partido,
                    "id": currentValue.partido,
                    "color": VarHistoricoProporcionDeMujeresPorCuatrienio.colors()[index_color]
                });
                index_color++;
            }
            else {
                let insertar = true;
                for (let i = 0; i < partidos.length; i++) {
                    if (partidos[i].name === currentValue.partido) {
                        insertar = false;
                        break;
                    }
                }
                if (insertar) {
                    if (index_color > VarHistoricoProporcionDeMujeresPorCuatrienio.colors().length)
                        index_color = 0;

                    partidos.push({
                        "name": currentValue.partido,
                        "id": currentValue.partido,
                        "color": VarHistoricoProporcionDeMujeresPorCuatrienio.colors()[index_color]
                    });

                    index_color++;
                }
            }
        });

        series[0].data = partidos.concat(series[0].data);
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoProporcionDeMujeresPorCuatrienioSeries: series }
};
const HistoricoTop5PartidosCongresistasMasJovenes_handlerGetChart = async (cuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "bar" }];
    await DatosDataService.getTop5PartidosCongresistasMasJovenes(cuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push({
                "partido": currentValue.partido,
                "mediana": parseFloat(currentValue.mediana),
                "y": parseFloat(currentValue.mediana),
                "name": currentValue.partido
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoTop5PartidosCongresistasMasJovenesSeries: series }
};
const HistoricoTop5PartidosCongresistasMayorEdad_handlerGetChart = async (cuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "bar" }];
    await DatosDataService.getTop5PartidosCongresistasMayorEdad(cuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push({
                "partido": currentValue.partido,
                "mediana": parseFloat(currentValue.mediana),
                "y": parseFloat(currentValue.mediana),
                "name": currentValue.partido
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoTop5PartidosCongresistasMayorEdadSeries: series }
};
const HistoricoProporcionDeMujeres_handlerGetChart = async () => {
    let series = [];
    let annos = [];
    let categories = [];

    await DatosDataService.getProporcionDeMujeres().then((response) => {
        response.data.forEach((currentValue, index, array) => {
            if (series.length === 0) {
                series.push(
                    {
                        "name": currentValue.genero,
                        "type": "area",
                        data: [{
                            "fechaInicio": currentValue.fechaInicio,
                            "genero": currentValue.genero,
                            "n": currentValue.n,
                            "pct": 0,
                            "y": 0
                        }],
                    });
            }
            else {
                let encontrado = false;
                let genero_actual = currentValue.genero;
                for (let i = 0; i < series.length; i++) {
                    let currentValueSerie = series[i];
                    if (currentValueSerie.name === genero_actual) {
                        currentValueSerie.data.push({
                            "fechaInicio": currentValue.fechaInicio,
                            "genero": currentValue.genero,
                            "n": currentValue.n,
                            "pct": 0,
                            "y": 0
                        });
                        encontrado = true;
                        break;
                    }
                }
                if (!encontrado) {
                    series.push(
                        {
                            "name": currentValue.genero,
                            "type": "area",
                            data: [{
                                "fechaInicio": currentValue.fechaInicio,
                                "genero": currentValue.genero,
                                "n": currentValue.n,
                                "pct": 0,
                                "y": 0
                            }],
                        });
                }
            }

            if (annos.length === 0) {
                annos.push({
                    anno: currentValue.fechaInicio,
                    total: currentValue.n
                })
            }
            else {
                let encontrado_anno = false;
                let anno_actual = currentValue.fechaInicio;
                for (let i = 0; i < annos.length; i++) {
                    if (anno_actual === annos[i].anno) {
                        annos[i].total += currentValue.n;
                        encontrado_anno = true;
                        break;
                    }
                }
                if (!encontrado_anno) {
                    annos.push({
                        anno: currentValue.fechaInicio,
                        total: currentValue.n
                    })
                }
            }
        });

        series.forEach(currentValue => {
            currentValue.data.forEach((currentValueData, index, array) => {
                let total = 0;
                for (let i = 0; i < annos.length; i++) {
                    if (annos[i].anno === currentValueData.fechaInicio) {
                        total = annos[i].total;
                        break;
                    }
                }
                currentValueData.pct = parseFloat(((currentValueData.n * 100) / total).toFixed(2));
                currentValueData.y = currentValueData.pct;
            });
        })

        categories = annos.map(a => a.anno);
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoProporcionDeMujeresSeries: series, HistoricoProporcionDeMujeresCategories: categories }
};
const HistoricoDistribucionEdadCongresoTodosCuatrienios_handlerGetChart = async () => {
    let series = [];
    await DatosDataService.getDistribucionEdadCongresoTodosCuatrienios().then((response) => {
        let index_color = 0;
        response.data.forEach((currentValue, index, array) => {
            let dataSource = currentValue.data;

            let xiData = [];
            let range = 100,
                startPoint = 0;
            for (let i = 0; i < range; i++) {
                xiData[i] = startPoint + i;
            }
            let data = [];
            let N = dataSource.length;

            for (let i = 0; i < xiData.length; i++) {
                let temp = 0;
                for (let j = 0; j < dataSource.length; j++) {
                    let dataSourceItem = parseInt(dataSource[j]) || 0;
                    temp = temp + gaussKDE(xiData[i], dataSourceItem);
                }
                data.push([xiData[i], (1 / N) * temp]);
            }

            if (index_color >= VarHistoricoDistribucionEdadCongresoTodosCuatrienios.colors().length) {
                index_color = 0
            }
            else {
                index_color++;
            }
            series.push(
                {
                    "name": currentValue.cuatrienio,
                    "type": "area",
                    "color": VarHistoricoDistribucionEdadCongresoTodosCuatrienios.colors()[index_color],
                    data: data,
                });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoDistribucionEdadCongresoTodosCuatrieniosSeries: series }
};
const gaussKDE = (xi, x) => {
    return (1 / Math.sqrt(2 * Math.PI)) * Math.exp(Math.pow(xi - x, 2) / -2);
}
const HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio_handlerGetChart = async () => {
    let series = [{ "group": "group", "data": [], "type": "sankey" }];
    await DatosDataService.getPartidosMayorRepresentacionCongresoPorCuatrienio().then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push(
                {
                    "partido": currentValue.partido,
                    "cuatrienio": currentValue.cuatrienio,
                    "n": currentValue.n,
                    "from": currentValue.partido,
                    "to": currentValue.cuatrienio,
                    "weight": currentValue.n
                });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioSeries: series }
};
// EXPORT
const DatosHistoricoComposicionPartidista = ({ charts = ConstCharts, DSCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [C_HistoricoDistribucionEdadCongresoTodosCuatrieniosGrafica, setHistoricoDistribucionEdadCongresoTodosCuatrienios] = useState(JSON.parse(charts).HistoricoDistribucionEdadCongresoTodosCuatrienios.historico.distribucion_edad_congreso_todos_cuatrienios.grafica);
    const [C_HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioGrafica, setHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio] = useState(JSON.parse(charts).HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio.historico.partidos_mayor_representacion_congreso_por_cuatrienio.grafica);
    const [C_HistoricoProporcionDeMujeresGrafica, setHistoricoProporcionDeMujeresGrafica] = useState(JSON.parse(charts).HistoricoProporcionDeMujeres.historico.proporcion_de_mujeres.grafica);
    const [C_HistoricoProporcionDeMujeresPorCuatrienioGrafica, setHistoricoProporcionDeMujeresPorCuatrienioGrafica] = useState(JSON.parse(charts).HistoricoProporcionDeMujeresPorCuatrienio.historico.proporcion_de_mujeres_por_cuatrienio.grafica);
    const [C_HistoricoTop5PartidosCongresistasMasJovenesGrafica, setHistoricoTop5PartidosCongresistasMasJovenesGrafica] = useState(JSON.parse(charts).HistoricoTop5PartidosCongresistasMasJovenes.congreso_hoy.top_5_partidos_congresistas_mas_jovenes.grafica);
    const [C_HistoricoTop5PartidosCongresistasMayorEdadGrafica, setHistoricoTop5PartidosCongresistasMayorEdadGrafica] = useState(JSON.parse(charts).HistoricoTop5PartidosCongresistasMayorEdad.congreso_hoy.top_5_partidos_congresistas_mayor_edad.grafica);

    useEffect(async () => {
        setSubloaders(false)
        
        let { HistoricoProporcionDeMujeresPorCuatrienioSeries } = await HistoricoProporcionDeMujeresPorCuatrienio_handlerGetChart(SelectedCuatrienio.value);
        setHistoricoProporcionDeMujeresPorCuatrienioGrafica({ series: HistoricoProporcionDeMujeresPorCuatrienioSeries, title: { text: `Proporción de mujeres por partido - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}` } })

        let { HistoricoTop5PartidosCongresistasMasJovenesSeries } = await HistoricoTop5PartidosCongresistasMasJovenes_handlerGetChart(SelectedCuatrienio.value);
        setHistoricoTop5PartidosCongresistasMasJovenesGrafica({ series: HistoricoTop5PartidosCongresistasMasJovenesSeries, title: { text: `Top 10 partidos con congresistas más jóvenes - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}` } })

        let { HistoricoTop5PartidosCongresistasMayorEdadSeries } = await HistoricoTop5PartidosCongresistasMayorEdad_handlerGetChart(SelectedCuatrienio.value);
        setHistoricoTop5PartidosCongresistasMayorEdadGrafica({ series: HistoricoTop5PartidosCongresistasMayorEdadSeries, title: { text: `Top 10 partidos con congresistas de mayor edad - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}` } })

        let { HistoricoProporcionDeMujeresSeries, HistoricoProporcionDeMujeresCategories } = await HistoricoProporcionDeMujeres_handlerGetChart();
        setHistoricoProporcionDeMujeresGrafica({ series: HistoricoProporcionDeMujeresSeries, xAxis: { categories: HistoricoProporcionDeMujeresCategories } })

        let { HistoricoDistribucionEdadCongresoTodosCuatrieniosSeries } = await HistoricoDistribucionEdadCongresoTodosCuatrienios_handlerGetChart();
        setHistoricoDistribucionEdadCongresoTodosCuatrienios({ series: HistoricoDistribucionEdadCongresoTodosCuatrieniosSeries })

        let { HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioSeries } = await HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio_handlerGetChart();
        setHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio({ series: HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioSeries })

        

    }, []);
    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }

    const ComboHistorico_handlerSelectComboCuatrienio = async (selectCuatrienio) => {
        setSubloaders(true)
        setSelectedCuatrienio(selectCuatrienio);

        let { HistoricoProporcionDeMujeresPorCuatrienioSeries } = await HistoricoProporcionDeMujeresPorCuatrienio_handlerGetChart(selectCuatrienio.value);
        setHistoricoProporcionDeMujeresPorCuatrienioGrafica({ series: HistoricoProporcionDeMujeresPorCuatrienioSeries, title: { text: `Proporción de mujeres por partido - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        setAllSubloaders({ ...AllSubloaders, HistoricoProporcionDeMujeresPorCuatrienio: false })

        let { HistoricoTop5PartidosCongresistasMasJovenesSeries } = await HistoricoTop5PartidosCongresistasMasJovenes_handlerGetChart(selectCuatrienio.value);
        setHistoricoTop5PartidosCongresistasMasJovenesGrafica({ series: HistoricoTop5PartidosCongresistasMasJovenesSeries, title: { text: `Top 10 partidos con congresistas más jóvenes - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        setAllSubloaders({ ...AllSubloaders, HistoricoTop5PartidosCongresistasMasJovenes: false })

        let { HistoricoTop5PartidosCongresistasMayorEdadSeries } = await HistoricoTop5PartidosCongresistasMayorEdad_handlerGetChart(selectCuatrienio.value);
        setHistoricoTop5PartidosCongresistasMayorEdadGrafica({ series: HistoricoTop5PartidosCongresistasMayorEdadSeries, title: { text: `Top 10 partidos con congresistas de mayor edad - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        setAllSubloaders({ ...AllSubloaders, HistoricoTop5PartidosCongresistasMayorEdad: false })
    }
    return (
        <>
            <Head>
                <title>Históricos | Proyectos de ley</title>
                <meta name="description" content="Reportes actuales sobre historicos de protectos de ley" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">

                        <div className="contentTab active">
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li>

                                                <a href={"/datos/historico/proyectos-de-ley"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Proyectos de ley</p></div>
                                                </a>

                                            </li>
                                            <li className="active">

                                                <a href={"/datos/historico/composicion-partidista"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Composición Partidista</p></div>
                                                </a>

                                            </li>
                                            <li>

                                                <a href={"/datos/historico/relacion-ejecutivo-legislativo"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Relación ejecutivo-legislativo</p></div>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        <div className="item">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCuatrienio}
                                                                selectOnchange={ComboHistorico_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoProporcionDeMujeresPorCuatrienio ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoProporcionDeMujeresPorCuatrienioGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoTop5PartidosCongresistasMasJovenes ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoTop5PartidosCongresistasMasJovenesGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoTop5PartidosCongresistasMayorEdad ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoTop5PartidosCongresistasMayorEdadGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoProporcionDeMujeres ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoProporcionDeMujeresGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoDistribucionEdadCongresoTodosCuatrienios ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoDistribucionEdadCongresoTodosCuatrieniosGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_HistoricoPartidosMayorRepresentacionCongresoPorCuatrienioGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default DatosHistoricoComposicionPartidista;
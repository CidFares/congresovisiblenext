import { useState, useEffect } from "react";
import Head from "next/head";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarVotacionesPorTemaCamara from '../../../../GraficasConstantes/VarVotacionesPorTemaCamara';
import VarVotacionesPorTemaSenado from '../../../../GraficasConstantes/VarVotacionesPorTemaSenado';
import VarVotacionesPorIniciativaCamara from '../../../../GraficasConstantes/VarVotacionesPorIniciativaCamara';
import VarVotacionesPorIniciativaSenado from '../../../../GraficasConstantes/VarVotacionesPorIniciativaSenado';
import VarTipoVotacionesCamara from '../../../../GraficasConstantes/VarTipoVotacionesCamara';
import VarTipoVotacionesSenado from '../../../../GraficasConstantes/VarTipoVotacionesSenado';

if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    VotacionesPorTemaCamara: true,
    VotacionesPorTemaSenado: true,
    VotacionesPorIniciativaCamara: true,
    VotacionesPorIniciativaSenado: true,
    TipoVotacionesCamara: true,
    TipoVotacionesSenado: true
}

const ConstCharts = {
    VotacionesPorTemaCamara: {
        votacion: Object.assign({}, VarVotacionesPorTemaCamara.votacion()),
    },
    VotacionesPorTemaSenado: {
        votacion: Object.assign({}, VarVotacionesPorTemaSenado.votacion()),
    },
    VotacionesPorIniciativaCamara: {
        votacion: Object.assign({}, VarVotacionesPorIniciativaCamara.votacion()),
    },
    VotacionesPorIniciativaSenado: {
        votacion: Object.assign({}, VarVotacionesPorIniciativaSenado.votacion()),
    },
    TipoVotacionesCamara: {
        votacion: Object.assign({}, VarTipoVotacionesCamara.votacion()),
    },
    TipoVotacionesSenado: {
        votacion: Object.assign({}, VarTipoVotacionesSenado.votacion()),
    },
    ComboVotaciones: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Ver todos",
                },
                data: [],
                error: "",
            },
            legislatura: {
                item: {
                    value: "",
                    label: "Ver todas",
                },
                data: [],
                error: "",
            }
        },
    },
}

export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();
    let { DSLegislatura, SelectedLegislatura } = await getLegislatura(SelectedCuatrienio.value);
    // let { VotacionesPorTemaCamaraSeries, VotacionesPorTemaCamaraXAxis } = await VotacionesPorTemaCamara_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.VotacionesPorTemaCamara.votacion.votacion_por_tema_camara.grafica.series = VotacionesPorTemaCamaraSeries;
    // graficas.VotacionesPorTemaCamara.votacion.votacion_por_tema_camara.grafica.xAxis = VotacionesPorTemaCamaraXAxis;
    // graficas.VotacionesPorTemaCamara.votacion.votacion_por_tema_camara.grafica.title.text = `Votaciones por tema - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedLegislatura.label}`;
    // //graficas.VotacionesPorTemaCamara.votacion.votacion_por_tema_camara.grafica.subtitle.text = "Cámara de Representantes";

    // let { VotacionesPorTemaSenadoSeries, VotacionesPorTemaSenadoXAxis } = await VotacionesPorTemaSenado_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.VotacionesPorTemaSenado.votacion.votacion_por_tema_Senado.grafica.series = VotacionesPorTemaSenadoSeries;
    // graficas.VotacionesPorTemaSenado.votacion.votacion_por_tema_Senado.grafica.xAxis = VotacionesPorTemaSenadoXAxis;
    // graficas.VotacionesPorTemaSenado.votacion.votacion_por_tema_Senado.grafica.title.text = `Votaciones por tema - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;
    // //graficas.VotacionesPorTemaSenado.votacion.votacion_por_tema_Senado.grafica.subtitle.text = "Senado de la República";

    // let { VotacionesPorIniciativaCamaraSeries, VotacionesPorIniciativaCamaraXAxis } = await VotacionesPorIniciativaCamara_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.VotacionesPorIniciativaCamara.votacion.votacion_por_iniciativa_camara.grafica.series = VotacionesPorIniciativaCamaraSeries;
    // graficas.VotacionesPorIniciativaCamara.votacion.votacion_por_iniciativa_camara.grafica.xAxis = VotacionesPorIniciativaCamaraXAxis;
    // graficas.VotacionesPorIniciativaCamara.votacion.votacion_por_iniciativa_camara.grafica.title.text = `Votaciones por iniciativa - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;
    // //graficas.VotacionesPorIniciativaCamara.votacion.votacion_por_iniciativa_camara.grafica.subtitle.text = "Cámara de Representantes";;

    // let { VotacionesPorIniciativaSenadoSeries, VotacionesPorIniciativaSenadoXAxis } = await VotacionesPorIniciativaSenado_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.VotacionesPorIniciativaSenado.votacion.votacion_por_iniciativa_senado.grafica.series = VotacionesPorIniciativaSenadoSeries;
    // graficas.VotacionesPorIniciativaSenado.votacion.votacion_por_iniciativa_senado.grafica.xAxis = VotacionesPorIniciativaSenadoXAxis;
    // graficas.VotacionesPorIniciativaSenado.votacion.votacion_por_iniciativa_senado.grafica.title.text = `Votaciones por iniciativa - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;
    // //graficas.VotacionesPorIniciativaSenado.votacion.votacion_por_iniciativa_senado.grafica.subtitle.text = "Senado de la República";

    // let { TipoVotacionesCamaraData } = await TipoVotacionesCamara_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.TipoVotacionesCamara.votacion.tipo_votaciones_camara.grafica.series[0].data = TipoVotacionesCamaraData;
    // graficas.TipoVotacionesCamara.votacion.tipo_votaciones_camara.grafica.title.text = `Tipos de votaciones - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;
    // graficas.TipoVotacionesCamara.votacion.tipo_votaciones_camara.grafica.subtitle.text = "Cámara de Representantes";;
    // let { TipoVotacionesSenadoData } = await TipoVotacionesSenado_handlerGetChart(SelectedCuatrienio.value, SelectedLegislatura.value);
    // graficas.TipoVotacionesSenado.votacion.tipo_votaciones_senado.grafica.series[0].data = TipoVotacionesSenadoData;
    // graficas.TipoVotacionesSenado.votacion.tipo_votaciones_senado.grafica.title.text = `Tipos de votaciones - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;
    // graficas.TipoVotacionesSenado.votacion.tipo_votaciones_senado.grafica.subtitle.text = "Senado de la República";

    return {
        props: { charts: JSON.stringify(graficas), DSCuatrienio, SelectedCuatrienio, DSLegislatura, SelectedLegislatura }
    }
}

// PETICIONES

const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let getLegislatura = false;
        let year = new Date().getFullYear();

        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
                getLegislatura = true;
            }
        });

        if (!getLegislatura)
            selected = { value: response.data[0].id, label: response.data[0].nombre }

        combo.unshift(Object.assign({}, ConstCharts.ComboVotaciones.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const getLegislatura = async (cuatrienio_id) => {
    let combo = [];
    let selected = Object.assign({}, ConstCharts.ComboVotaciones.combo.legislatura.item);
    await UtilsDataService.getComboLegislatura(cuatrienio_id).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        combo.unshift(Object.assign({}, ConstCharts.ComboVotaciones.combo.legislatura.item));
    });
    return { DSLegislatura: combo, SelectedLegislatura: selected };
};

const VotacionesPorTemaCamara_handlerGetChart = async (cuatrienio, legislatura) => {
    let xAxis = {
        categories: []
    };
    let series = [
        {
            name: 'Votos-Abstención',
            data: []
        }, {
            name: 'En contra',
            data: []
        }, {
            name: 'A favor',
            data: []
        }
    ]

    let cont = 0;
    await DatosDataService.getVotacionesPorTemaCamara(
        cuatrienio,
        legislatura,
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                if (cont === 3)
                    cont = 0;
                if (cont === 0) {
                    xAxis.categories.push(currentValue.tema);
                }
                if (cont < 3) {
                    if (currentValue.tipo === "A favor")
                        series[2].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "Votos-Abstención")
                        series[0].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "En contra")
                        series[1].data.push(parseInt(currentValue.votos));
                    cont++;
                }
            });



        })
        .catch((error) => {
            console.log(error)
        });
    return { VotacionesPorTemaCamaraXAxis: xAxis, VotacionesPorTemaCamaraSeries: series }
};

// End VotacionesPorTemaCamara

// VotacionesPorTemaSenado

const VotacionesPorTemaSenado_handlerGetChart = async (cuatrienio, legislatura) => {
    let xAxis = {
        categories: []
    };
    let series = [
        {
            name: 'Votos-Abstención',
            data: []
        }, {
            name: 'En contra',
            data: []
        }, {
            name: 'A favor',
            data: []
        }
    ]

    let cont = 0;
    await DatosDataService.getVotacionesPorTemaSenado(
        cuatrienio,
        legislatura,
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                if (cont === 3)
                    cont = 0;
                if (cont === 0) {
                    xAxis.categories.push(currentValue.tema);
                }
                if (cont < 3) {
                    if (currentValue.tipo === "A favor")
                        series[2].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "Votos-Abstención")
                        series[0].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "En contra")
                        series[1].data.push(parseInt(currentValue.votos));
                    cont++;
                }
            });



        })
        .catch((error) => {
            console.log(error)
        });
    return { VotacionesPorTemaSenadoXAxis: xAxis, VotacionesPorTemaSenadoSeries: series }
};

// End VotacionesPorTemaSenado

// VotacionesPorIniciativaCamara

const VotacionesPorIniciativaCamara_handlerGetChart = async (cuatrienio, legislatura) => {

    let xAxis = {
        categories: []
    };
    let series = [
        {
            name: 'Votos-Abstención',
            data: []
        }, {
            name: 'En contra',
            data: []
        }, {
            name: 'A favor',
            data: []
        }
    ]

    let cont = 0;
    await DatosDataService.getVotacionesPorIniciativaCamara(
        cuatrienio,
        legislatura,
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                if (cont === 3)
                    cont = 0;
                if (cont === 0) {
                    xAxis.categories.push(currentValue.iniciativa);
                }
                if (cont < 3) {
                    if (currentValue.tipo === "A favor")
                        series[2].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "Votos-Abstención")
                        series[0].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "En contra")
                        series[1].data.push(parseInt(currentValue.votos));
                    cont++;
                }
            });



        })
        .catch((error) => {
            console.log(error)
        });
    return { VotacionesPorIniciativaCamaraXAxis: xAxis, VotacionesPorIniciativaCamaraSeries: series }
};
const VotacionesPorIniciativaSenado_handlerGetChart = async (cuatrienio, legislatura) => {

    let xAxis = {
        categories: []
    };
    let series = [
        {
            name: 'Votos-Abstención',
            data: []
        }, {
            name: 'En contra',
            data: []
        }, {
            name: 'A favor',
            data: []
        }
    ]

    let cont = 0;
    await DatosDataService.getVotacionesPorIniciativaSenado(
        cuatrienio,
        legislatura,
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                if (cont === 3)
                    cont = 0;
                if (cont === 0) {
                    xAxis.categories.push(currentValue.iniciativa);
                }
                if (cont < 3) {
                    if (currentValue.tipo === "A favor")
                        series[2].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "Votos-Abstención")
                        series[0].data.push(parseInt(currentValue.votos));
                    if (currentValue.tipo === "En contra")
                        series[1].data.push(parseInt(currentValue.votos));
                    cont++;
                }
            });



        })
        .catch((error) => {
            console.log(error)
        });
    return { VotacionesPorIniciativaSenadoXAxis: xAxis, VotacionesPorIniciativaSenadoSeries: series }
};

// End VotacionesPorIniciativaCamara

// TipoVotacionesCamara

const TipoVotacionesCamara_handlerGetChart = async (cuatrienio, legislatura) => {
    let data = [];
    await DatosDataService.getTipoVotacionesCamara(
        cuatrienio,
        legislatura
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                data.push({
                    "tipo": currentValue.tipo,
                    "n": parseInt(currentValue.n),
                    "y": parseInt(currentValue.n),
                    "name": currentValue.tipo
                });
            });

        })
        .catch((error) => {
            console.log(error)
        });
    return { TipoVotacionesCamaraData: data }
};

// End TipoVotacionesCamara

// TipoVotacionesSenado

const TipoVotacionesSenado_handlerGetChart = async (cuatrienio, legislatura) => {

    let data = [];
    await DatosDataService.getTipoVotacionesSenado(
        cuatrienio,
        legislatura
    )
        .then((response) => {

            response.data.forEach((currentValue, index, array) => {
                data.push({
                    "tipo": currentValue.tipo,
                    "n": parseInt(currentValue.n),
                    "y": parseInt(currentValue.n),
                    "name": currentValue.tipo
                });
            });
        })
        .catch((error) => {
            console.log(error)
        });
    return { TipoVotacionesSenadoData: data }
};

// End TipoVotacionesSenado

// *** End votaciones


const Votaciones = ({ charts = ConstCharts, DSCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboVotaciones.combo.cuatrienio.item, DSLegislatura = ConstCharts.ComboVotaciones.combo.legislatura.data, SelectedLegislatura = ConstCharts.ComboVotaciones.combo.legislatura.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [dataSelectLegislatura, setDSLegislatura] = useState(DSLegislatura);
    const [filterLegislatura, setSelectedLegislatura] = useState(SelectedLegislatura);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [C_VotacionesPorTemaCamaraGrafica, setVotacionesPorTemaCamaraGrafica] = useState(JSON.parse(charts).VotacionesPorTemaCamara.votacion.votacion_por_tema_camara.grafica);
    const [C_VotacionesPorTemaSenadoGrafica, setVotacionesPorTemaSenadoGrafica] = useState(JSON.parse(charts).VotacionesPorTemaSenado.votacion.votacion_por_tema_Senado.grafica);
    const [C_VotacionesPorIniciativaCamaraGrafica, setVotacionesPorIniciativaCamaraGrafica] = useState(JSON.parse(charts).VotacionesPorIniciativaCamara.votacion.votacion_por_iniciativa_camara.grafica);
    const [C_VotacionesPorIniciativaSenadoGrafica, setVotacionesPorIniciativaSenadoGrafica] = useState(JSON.parse(charts).VotacionesPorIniciativaSenado.votacion.votacion_por_iniciativa_senado.grafica);
    const [C_TipoVotacionesCamaraGrafica, setTipoVotacionesCamaraGrafica] = useState(JSON.parse(charts).TipoVotacionesCamara.votacion.tipo_votaciones_camara.grafica);
    const [C_TipoVotacionesSenadoGrafica, setTipoVotacionesSenadoGrafica] = useState(JSON.parse(charts).TipoVotacionesSenado.votacion.tipo_votaciones_senado.grafica);

    useEffect(async () => {
        
        let s = AllSubloaders;
        let { VotacionesPorTemaCamaraSeries, VotacionesPorTemaCamaraXAxis } = await VotacionesPorTemaCamara_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setVotacionesPorTemaCamaraGrafica({
            ...C_VotacionesPorTemaCamaraGrafica, series: VotacionesPorTemaCamaraSeries, xAxis: VotacionesPorTemaCamaraXAxis,
            title: { text: `Votaciones por tema - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        s.VotacionesPorTemaCamara = false;
        setAllSubloaders(s)

        let { VotacionesPorTemaSenadoSeries, VotacionesPorTemaSenadoXAxis } = await VotacionesPorTemaSenado_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setVotacionesPorTemaSenadoGrafica({
            ...C_VotacionesPorTemaSenadoGrafica, series: VotacionesPorTemaSenadoSeries, xAxis: VotacionesPorTemaSenadoXAxis,
            title: { text: `Votaciones por tema - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        s.VotacionesPorTemaSenado = false;
        setAllSubloaders(s)

        let { VotacionesPorIniciativaCamaraSeries, VotacionesPorIniciativaCamaraXAxis } = await VotacionesPorIniciativaCamara_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setVotacionesPorIniciativaCamaraGrafica({
            ...C_VotacionesPorIniciativaCamaraGrafica, series: VotacionesPorIniciativaCamaraSeries, xAxis: VotacionesPorIniciativaCamaraXAxis,
            title: { text: `Votaciones por iniciativa - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        s.VotacionesPorIniciativaCamara = false;
        setAllSubloaders(s)

        let { VotacionesPorIniciativaSenadoSeries, VotacionesPorIniciativaSenadoXAxis } = await VotacionesPorIniciativaSenado_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setVotacionesPorIniciativaSenadoGrafica({
            ...C_VotacionesPorIniciativaSenadoGrafica, series: VotacionesPorIniciativaSenadoSeries, xAxis: VotacionesPorIniciativaSenadoXAxis,
            title: { text: `Votaciones por iniciativa - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        s.VotacionesPorIniciativaSenado = false;
        setAllSubloaders(s)

        let { TipoVotacionesCamaraData } = await TipoVotacionesCamara_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setTipoVotacionesCamaraGrafica({
            ...C_TipoVotacionesCamaraGrafica, title: { text: `Tipos de votaciones - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }, series: { data: TipoVotacionesCamaraData }
        });
        s.TipoVotacionesCamara = false;
        setAllSubloaders(s)

        let { TipoVotacionesSenadoData } = await TipoVotacionesSenado_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setTipoVotacionesSenadoGrafica({
            ...C_TipoVotacionesSenadoGrafica, title: { text: `Tipos de votaciones - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }, series: { data: TipoVotacionesSenadoData }
        });
        setSubloaders(false)

    }, []);
    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }
    const ComboVotaciones_handlerSelectComboLegislatura = async (selectLegislatura) => {
        setSubloaders(true)
        setSelectedLegislatura(selectLegislatura);

        let { VotacionesPorTemaCamaraSeries, VotacionesPorTemaCamaraXAxis } = await VotacionesPorTemaCamara_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setVotacionesPorTemaCamaraGrafica({
            ...C_VotacionesPorTemaCamaraGrafica, series: VotacionesPorTemaCamaraSeries, xAxis: VotacionesPorTemaCamaraXAxis,
            title: { text: `Votaciones por tema - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorTemaCamara: false })


        let { VotacionesPorTemaSenadoSeries, VotacionesPorTemaSenadoXAxis } = await VotacionesPorTemaSenado_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setVotacionesPorTemaSenadoGrafica({
            ...C_VotacionesPorTemaSenadoGrafica, series: VotacionesPorTemaSenadoSeries, xAxis: VotacionesPorTemaSenadoXAxis,
            title: { text: `Votaciones por tema - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorTemaSenado: false })

        let { VotacionesPorIniciativaCamaraSeries, VotacionesPorIniciativaCamaraXAxis } = await VotacionesPorIniciativaCamara_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setVotacionesPorIniciativaCamaraGrafica({
            ...C_VotacionesPorIniciativaCamaraGrafica, series: VotacionesPorIniciativaCamaraSeries, xAxis: VotacionesPorIniciativaCamaraXAxis,
            title: { text: `Votaciones por iniciativa - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorIniciativaCamara: false })

        let { VotacionesPorIniciativaSenadoSeries, VotacionesPorIniciativaSenadoXAxis } = await VotacionesPorIniciativaSenado_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setVotacionesPorIniciativaSenadoGrafica({
            ...C_VotacionesPorIniciativaSenadoGrafica, series: VotacionesPorIniciativaSenadoSeries, xAxis: VotacionesPorIniciativaSenadoXAxis,
            title: { text: `Votaciones por iniciativa - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorIniciativaSenado: false })

        let { TipoVotacionesCamaraData } = await TipoVotacionesCamara_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setTipoVotacionesCamaraGrafica({
            ...C_TipoVotacionesCamaraGrafica, title: { text: `Tipos de votaciones - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }, series: { data: TipoVotacionesCamaraData }
        });
        setAllSubloaders({ ...AllSubloaders, TipoVotacionesCamara: false })

        let { TipoVotacionesSenadoData } = await TipoVotacionesSenado_handlerGetChart(filterCuatrienio.value, selectLegislatura.value);
        setTipoVotacionesSenadoGrafica({
            ...C_TipoVotacionesSenadoGrafica, title: { text: `Tipos de votaciones - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }, series: { data: TipoVotacionesSenadoData }
        });
        setAllSubloaders({ ...AllSubloaders, TipoVotacionesSenado: false })
        setSubloaders(false);

    }
    const ComboVotaciones_handlerSelectComboCuatrienio = async (selectCuatrienio) => {
        setSubloaders(true)
        setSelectedCuatrienio(selectCuatrienio);
        
        let { VotacionesPorTemaCamaraSeries, VotacionesPorTemaCamaraXAxis } = await VotacionesPorTemaCamara_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setVotacionesPorTemaCamaraGrafica({
            ...C_VotacionesPorTemaCamaraGrafica, series: VotacionesPorTemaCamaraSeries, xAxis: VotacionesPorTemaCamaraXAxis,
            title: { text: `Votaciones por tema - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorTemaCamara: false })

        let { VotacionesPorTemaSenadoSeries, VotacionesPorTemaSenadoXAxis } = await VotacionesPorTemaSenado_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setVotacionesPorTemaSenadoGrafica({
            ...C_VotacionesPorTemaSenadoGrafica, series: VotacionesPorTemaSenadoSeries, xAxis: VotacionesPorTemaSenadoXAxis,
            title: { text: `Votaciones por tema - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorTemaSenado: false })

        let { VotacionesPorIniciativaCamaraSeries, VotacionesPorIniciativaCamaraXAxis } = await VotacionesPorIniciativaCamara_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setVotacionesPorIniciativaCamaraGrafica({
            ...C_VotacionesPorIniciativaCamaraGrafica, series: VotacionesPorIniciativaCamaraSeries, xAxis: VotacionesPorIniciativaCamaraXAxis,
            title: { text: `Votaciones por iniciativa - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorIniciativaCamara: false })

        let { VotacionesPorIniciativaSenadoSeries, VotacionesPorIniciativaSenadoXAxis } = await VotacionesPorIniciativaSenado_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setVotacionesPorIniciativaSenadoGrafica({
            ...C_VotacionesPorIniciativaSenadoGrafica, series: VotacionesPorIniciativaSenadoSeries, xAxis: VotacionesPorIniciativaSenadoXAxis,
            title: { text: `Votaciones por iniciativa - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }
        });
        setAllSubloaders({ ...AllSubloaders, VotacionesPorIniciativaSenado: false })

        let { TipoVotacionesCamaraData } = await TipoVotacionesCamara_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setTipoVotacionesCamaraGrafica({
            ...C_TipoVotacionesCamaraGrafica, title: { text: `Tipos de votaciones - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Cámara de Representantes" }, series: { data: TipoVotacionesCamaraData }
        });
        setAllSubloaders({ ...AllSubloaders, TipoVotacionesCamara: false })

        let { TipoVotacionesSenadoData } = await TipoVotacionesSenado_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setTipoVotacionesSenadoGrafica({
            ...C_TipoVotacionesSenadoGrafica, title: { text: `Tipos de votaciones - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` },
            subtitle: { text: "Senado de la República" }, series: { data: TipoVotacionesSenadoData }
        });
        setAllSubloaders({ ...AllSubloaders, TipoVotacionesSenado: false })
        setSubloaders(false);
        
    }
    return (
        <>
            <Head>
                <title>Votación | Votaciones</title>
                <meta name="description" content="Reportes actuales sobre las votaciones" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li>
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li>
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">

                        <div className="contentTab active" data-ref="4">
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li className="active" data-ref="9">

                                                <a href={"/datos/votacion/votaciones"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Votaciones</p></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" data-ref="9">
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        <div className="item">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCuatrienio}
                                                                selectOnchange={ComboVotaciones_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                        <div className="item">
                                                            <label htmlFor="">Legislatura</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterLegislatura}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectLegislatura}
                                                                selectOnchange={ComboVotaciones_handlerSelectComboLegislatura}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.VotacionesPorIniciativaCamara ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_VotacionesPorIniciativaCamaraGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.VotacionesPorIniciativaSenado ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_VotacionesPorIniciativaSenadoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.VotacionesPorTemaCamara ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_VotacionesPorTemaCamaraGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.VotacionesPorTemaSenado ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_VotacionesPorTemaSenadoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.TipoVotacionesCamara ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_TipoVotacionesCamaraGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.TipoVotacionesSenado ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_TipoVotacionesSenadoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Votaciones;

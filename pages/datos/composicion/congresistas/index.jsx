import { useState, useEffect } from "react";
import Head from "next/head";
import DatosSenadoCongresoHoyDataService from "../../../../Services/Datos/Datos.Service";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'

import VarCamaraCongresoHoyPiramidePoblacional from "../../../../GraficasConstantes/VarCamaraCongresoHoyPiramidePoblacional";
import VarCongresoHoyMinimoPromedioMaximoCuatrienio from "../../../../GraficasConstantes/VarCongresoHoyMinimoPromedioMaximoCuatrienio";
import VarCongresoHoyTotalProyectoDeLeyPorGenero from "../../../../GraficasConstantes/VarCongresoHoyTotalProyectoDeLeyPorGenero";
import VarSenadoCongresoHoyPiramidePoblacional from "../../../../GraficasConstantes/VarSenadoCongresoHoyPiramidePoblacional";

if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    CamaraCongresoHoyPiramidePoblacional: true,
    SenadoCongresoHoyPiramidePoblacional: true,
    CongresoHoyTotalProyectoDeLeyPorGenero: true,
    CongresoHoyMinimoPromedioMaximoCuatrienio: true
}
const ConstCharts = {
    CamaraCongresoHoyPiramidePoblacional: {
        congreso_hoy: Object.assign({}, VarCamaraCongresoHoyPiramidePoblacional.default_congreso_hoy()),
    },
    SenadoCongresoHoyPiramidePoblacional: {
        congreso_hoy: Object.assign({}, VarSenadoCongresoHoyPiramidePoblacional.default_congreso_hoy()),
    },
    CongresoHoyTotalProyectoDeLeyPorGenero: {
        congreso_hoy: Object.assign({}, VarCongresoHoyTotalProyectoDeLeyPorGenero.default_congreso_hoy()),
    },
    CongresoHoyMinimoPromedioMaximoCuatrienio: {
        congreso_hoy: Object.assign({}, VarCongresoHoyMinimoPromedioMaximoCuatrienio.default_congreso_hoy()),
    },
    ComboComposicion: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Seleccione un cuatrienio",
                },
                data: [],
                error: "",
            },
            legislatura: {
                item: {
                    value: "",
                    label: "Ver todas",
                },
                data: [],
                error: "",
            }
        },
    },
}

// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();

    // let { CamaraCongresoHoyPiramidePoblacionalSeries } = await CamaraCongresoHoyPiramidePoblacional_handlerGetChart(SelectedCuatrienio.value, ConstCharts.ComboComposicion.combo.legislatura.item.value);
    // graficas.CamaraCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica.series = CamaraCongresoHoyPiramidePoblacionalSeries;
    // graficas.CamaraCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica.title.text = `Pirámide poblacional - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { SenadoCongresoHoyPiramidePoblacionalSeries } = await SenadoCongresoHoyPiramidePoblacional_handlerGetChart(SelectedCuatrienio.value, ConstCharts.ComboComposicion.combo.legislatura.item.value);
    // graficas.SenadoCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica.series = SenadoCongresoHoyPiramidePoblacionalSeries;
    // graficas.SenadoCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica.title.text = `Pirámide poblacional - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { CongresoHoyTotalProyectoDeLeyPorGeneroSeries } = await CongresoHoyTotalProyectoDeLeyPorGenero_handlerGetChart(SelectedCuatrienio.value);
    // graficas.CongresoHoyTotalProyectoDeLeyPorGenero.congreso_hoy.edad_mediana_por_partido.grafica.series = CongresoHoyTotalProyectoDeLeyPorGeneroSeries;
    // graficas.CongresoHoyTotalProyectoDeLeyPorGenero.congreso_hoy.edad_mediana_por_partido.grafica.title.text = `Total de proyectos de ley presentados por género - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { CongresoHoyMinimoPromedioMaximoCuatrienioSeries } = await CongresoHoyMinimoPromedioMaximoCuatrienio_handlerGetChart(SelectedCuatrienio.value);
    // graficas.CongresoHoyMinimoPromedioMaximoCuatrienio.congreso_hoy.minimo_promedio_maximo_cuatrienio.grafica.series = CongresoHoyMinimoPromedioMaximoCuatrienioSeries;
    // graficas.CongresoHoyMinimoPromedioMaximoCuatrienio.congreso_hoy.minimo_promedio_maximo_cuatrienio.grafica.title.text = `Experiencia de los congresistas - ${SelectedCuatrienio.value === "" ? "Todos" : SelectedCuatrienio.label}`

    return {
        props: { charts: JSON.stringify(graficas), DSCuatrienio, SelectedCuatrienio }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let getLegislatura = false;
        let year = new Date().getFullYear();

        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
                getLegislatura = true;
            }
        });

        if (!getLegislatura)
            selected = { value: response.data[0].id, label: response.data[0].nombre }

        combo.unshift(Object.assign({}, ConstCharts.ComboComposicion.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const CamaraCongresoHoyPiramidePoblacional_handlerGetChart = async (cuatrienio, legislatura) => {
    let series = [
        {
            "name": "Femenino",
            "data": [],
            "type": "bar"
        },
        {
            "name": "Masculino",
            "data": [],
            "type": "bar"
        }
    ]
    await DatosDataService.getCamaraPiramidePoblacional(cuatrienio, legislatura).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            if (currentValue.genero === "Femenino") {
                series[0].data.push({
                    "corporacion": currentValue.corporacion,
                    "legislatura": currentValue.legislatura,
                    "cuatrienio_id": currentValue.cuatrienio_id,
                    "genero": currentValue.genero,
                    "rango": currentValue.rango,
                    "n": parseInt(currentValue.n),
                    "n2": parseInt(currentValue.n2),
                    "y": parseInt(currentValue.n2),
                    "name": currentValue.rango
                });
            } else {
                series[1].data.push({
                    "corporacion": currentValue.corporacion,
                    "legislatura": currentValue.legislatura,
                    "cuatrienio_id": currentValue.cuatrienio_id,
                    "genero": currentValue.genero,
                    "rango": currentValue.rango,
                    "n": parseInt(currentValue.n),
                    "n2": parseInt(currentValue.n2),
                    "y": parseInt(currentValue.n2),
                    "name": currentValue.rango
                });
            }
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CamaraCongresoHoyPiramidePoblacionalSeries: series }
};
const SenadoCongresoHoyPiramidePoblacional_handlerGetChart = async (cuatrienio, legislatura) => {
    let series = [
        {
            "name": "Femenino",
            "data": [],
        },
        {
            "name": "Masculino",
            "data": [],
        }
    ]
    await DatosDataService.getSenadoPiramidePoblacional(cuatrienio, legislatura).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            if (currentValue.genero === "Femenino") {
                series[0].data.push({
                    "corporacion": currentValue.corporacion,
                    "legislatura": currentValue.legislatura,
                    "cuatrienio_id": currentValue.cuatrienio_id,
                    "genero": currentValue.genero,
                    "rango": currentValue.rango,
                    "n": parseInt(currentValue.n),
                    "n2": parseInt(currentValue.n2),
                    "y": parseInt(currentValue.n2),
                    "name": currentValue.rango
                });
            } else {
                series[1].data.push({
                    "corporacion": currentValue.corporacion,
                    "legislatura": currentValue.legislatura,
                    "cuatrienio_id": currentValue.cuatrienio_id,
                    "genero": currentValue.genero,
                    "rango": currentValue.rango,
                    "n": parseInt(currentValue.n),
                    "n2": parseInt(currentValue.n2),
                    "y": parseInt(currentValue.n2),
                    "name": currentValue.rango
                });
            }
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { SenadoCongresoHoyPiramidePoblacionalSeries: series }
};
const CongresoHoyTotalProyectoDeLeyPorGenero_handlerGetChart = async (cuatrienio) => {
    let series = [{
        "name": "Femenino",
        "data": [],
        "type": "bubble"
    }, {
        "name": "Masculino",
        "data": [],
        "type": "bubble"
    }];
    let total = 0;
    await DatosDataService.getTotalProyectoDeLeyPorGenero(cuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.n;
            if (currentValue.genero === "Femenino") {
                series[0].data.push(
                    {
                        "genero": currentValue.genero,
                        "n": currentValue.n,
                        "pct": 0,
                        "y": 1,
                        "size": currentValue.n,
                        "name": currentValue.genero,
                        "z": currentValue.n
                    });
            }
            else {
                series[1].data.push(
                    {
                        "genero": currentValue.genero,
                        "n": currentValue.n,
                        "pct": 0,
                        "y": 1,
                        "size": currentValue.n,
                        "name": currentValue.genero,
                        "z": currentValue.n
                    });
            }
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.pct = ((currentValueData.n * 100) / total).toFixed(2);
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyTotalProyectoDeLeyPorGeneroSeries: series }
};
const CongresoHoyMinimoPromedioMaximoCuatrienio_handlerGetChart = async (cuatrienio) => {
    let series = [];
    await DatosDataService.getMinimoPromedioMaximoCuatrienio(cuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series.push(
                {
                    "name": currentValue.corporacion,
                    "data": [{
                        "corporacion": currentValue.corporacion,
                        "media": parseFloat(currentValue.media),
                        "minimo": parseFloat(currentValue.minimo),
                        "maxi": parseFloat(currentValue.maxi),
                        "media2": parseFloat(currentValue.media2),
                        "high": parseFloat(currentValue.maxi),
                        "low": parseFloat(currentValue.minimo),
                        "name": currentValue.corporacion
                    }],
                    "type": "errorbar"
                });
            series.push(
                {
                    "name": currentValue.corporacion,
                    "data": [{
                        "corporacion": currentValue.corporacion,
                        "media": parseFloat(currentValue.media2),
                        "minimo": parseFloat(currentValue.minimo),
                        "maxi": parseFloat(currentValue.maxi),
                        "media2": parseFloat(currentValue.media2),
                        "y": parseFloat(currentValue.media2),
                        "name": currentValue.corporacion
                    }
                    ],
                    "type": "scatter"
                });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyMinimoPromedioMaximoCuatrienioSeries: series }
};

const DatosComposicionCongresistas = ({ charts = ConstCharts, DSCuatrienio = ConstCharts.ComboComposicion.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboComposicion.combo.cuatrienio.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [dataSelectLegislatura, setDSLegislatura] = useState(ConstCharts.ComboComposicion.combo.legislatura.data);
    const [filterLegislatura, setSelectedLegislatura] = useState(ConstCharts.ComboComposicion.combo.legislatura.item);
    const [C_CamaraCongresoHoyPiramidePoblacionalGrafica, setCamaraCongresoHoyPiramidePoblacionalGrafica] = useState(JSON.parse(charts).CamaraCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica);
    const [C_SenadoCongresoHoyPiramidePoblacionalGrafica, setSenadoCongresoHoyPiramidePoblacionalGrafica] = useState(JSON.parse(charts).SenadoCongresoHoyPiramidePoblacional.congreso_hoy.piramide_poblacional.grafica);
    const [C_CongresoHoyTotalProyectoDeLeyPorGeneroGrafica, setCongresoHoyTotalProyectoDeLeyPorGeneroGrafica] = useState(JSON.parse(charts).CongresoHoyTotalProyectoDeLeyPorGenero.congreso_hoy.edad_mediana_por_partido.grafica);
    const [C_CongresoHoyMinimoPromedioMaximoCuatrienioGrafica, setCongresoHoyMinimoPromedioMaximoCuatrienioGrafica] = useState(JSON.parse(charts).CongresoHoyMinimoPromedioMaximoCuatrienio.congreso_hoy.minimo_promedio_maximo_cuatrienio.grafica);

    useEffect(async () => {

        let { CamaraCongresoHoyPiramidePoblacionalSeries } = await CamaraCongresoHoyPiramidePoblacional_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setCamaraCongresoHoyPiramidePoblacionalGrafica({ series: CamaraCongresoHoyPiramidePoblacionalSeries, title: { text: `Pirámide poblacional - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CamaraCongresoHoyPiramidePoblacional: false })

        let { SenadoCongresoHoyPiramidePoblacionalSeries } = await SenadoCongresoHoyPiramidePoblacional_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setSenadoCongresoHoyPiramidePoblacionalGrafica({ series: SenadoCongresoHoyPiramidePoblacionalSeries, title: { text: `Pirámide poblacional - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, SenadoCongresoHoyPiramidePoblacional: false })

        let { CongresoHoyTotalProyectoDeLeyPorGeneroSeries } = await CongresoHoyTotalProyectoDeLeyPorGenero_handlerGetChart(filterCuatrienio.value);
        setCongresoHoyTotalProyectoDeLeyPorGeneroGrafica({ series: CongresoHoyTotalProyectoDeLeyPorGeneroSeries, title: { text: `Total de proyectos de ley presentados por género - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyTotalProyectoDeLeyPorGenero: false })

        let { CongresoHoyMinimoPromedioMaximoCuatrienioSeries } = await CongresoHoyMinimoPromedioMaximoCuatrienio_handlerGetChart(filterCuatrienio.value);
        setCongresoHoyMinimoPromedioMaximoCuatrienioGrafica({ series: CongresoHoyMinimoPromedioMaximoCuatrienioSeries, title: { text: `Experiencia de los congresistas - ${filterCuatrienio.value === "" ? "Todos" : filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyMinimoPromedioMaximoCuatrienio: false })

        setSubloaders(false)
    }, []);

    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }

    const ComboComposicion_handlerSelectComboCuatrienio = async (selectCuatrienio) => {
        setSubloaders(true)
        setDSCuatrienio(selectCuatrienio)

        let { CamaraCongresoHoyPiramidePoblacionalSeries } = await CamaraCongresoHoyPiramidePoblacional_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setCamaraCongresoHoyPiramidePoblacionalGrafica({ series: CamaraCongresoHoyPiramidePoblacionalSeries, title: { text: `Pirámide poblacional - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CamaraCongresoHoyPiramidePoblacional: false })

        let { SenadoCongresoHoyPiramidePoblacionalSeries } = await SenadoCongresoHoyPiramidePoblacional_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setSenadoCongresoHoyPiramidePoblacionalGrafica({ series: SenadoCongresoHoyPiramidePoblacionalSeries, title: { text: `Pirámide poblacional - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, SenadoCongresoHoyPiramidePoblacional: false })

        let { CongresoHoyTotalProyectoDeLeyPorGeneroSeries } = await CongresoHoyTotalProyectoDeLeyPorGenero_handlerGetChart(selectCuatrienio.value);
        setCongresoHoyTotalProyectoDeLeyPorGeneroGrafica({ series: CongresoHoyTotalProyectoDeLeyPorGeneroSeries, title: { text: `Total de proyectos de ley presentados por género - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyTotalProyectoDeLeyPorGenero: false })

        let { CongresoHoyMinimoPromedioMaximoCuatrienioSeries } = await CongresoHoyMinimoPromedioMaximoCuatrienio_handlerGetChart(selectCuatrienio.value);
        setCongresoHoyMinimoPromedioMaximoCuatrienioGrafica({ series: CongresoHoyMinimoPromedioMaximoCuatrienioSeries, title: { text: `Experiencia de los congresistas - ${selectCuatrienio.value === "" ? "Todos" : selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyMinimoPromedioMaximoCuatrienio: false })

        setSubloaders(false)
    };


    return (
        <>
            <Head>
                <title>Composición | Partidos</title>
                <meta name="description" content="Reportes actuales sobre partidos" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li>
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active" >
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li>
                                                <a href={"/datos/composicion/partidos"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Partidos</p></div>
                                                </a>
                                            </li>
                                            <li className="active">
                                                <a href={"/datos/composicion/congresistas"}>
                                                    <div className="icon"><i className="fas fa-users"></i></div>
                                                    <div className="desc"><p>Congresistas</p></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        <div className="item">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCuatrienio}
                                                                selectOnchange={ComboComposicion_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CamaraCongresoHoyPiramidePoblacional ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CamaraCongresoHoyPiramidePoblacionalGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.SenadoCongresoHoyPiramidePoblacional ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_SenadoCongresoHoyPiramidePoblacionalGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyTotalProyectoDeLeyPorGenero ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyTotalProyectoDeLeyPorGeneroGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyMinimoPromedioMaximoCuatrienio ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyMinimoPromedioMaximoCuatrienioGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default DatosComposicionCongresistas
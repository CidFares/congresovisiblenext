import { useState, useEffect } from "react";
import Head from "next/head";
import DatosSenadoCongresoHoyDataService from "../../../../Services/Datos/Datos.Service";
import DatosDataService from "../../../../Services/Datos/Datos.Service";
import UtilsDataService from "../../../../Services/General/Utils.Service";
import Select from "../../../../Components/Select";
import Link from "next/link";

import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import HC_More from 'highcharts/highcharts-more'
import Treemap from 'highcharts/modules/treemap'
import ItemSeries from "highcharts/modules/item-series";
import VarCamaraCongresoHoyAsientosPorPartido from "../../../../GraficasConstantes/VarCamaraCongresoHoyAsientosPorPartido";
import VarCongresoHoyEdadMedianaPorPartido from "../../../../GraficasConstantes/VarCongresoHoyEdadMedianaPorPartido";
import VarSenadoCongresoHoyAsientosPorPartido from "../../../../GraficasConstantes/VarSenadoCongresoHoyAsientosPorPartido";

if (typeof Highcharts === 'object') {
    Treemap(Highcharts)
    HC_More(Highcharts)
    ItemSeries(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}

const ConstAllSubloaders = {
    CamaraCongresoHoyAsientosPorPartido: true,
    SenadoCongresoHoyAsientosPorPartido: true,
    CongresoHoyEdadMedianaPorPartido: true
}

const ConstCharts = {
    CamaraCongresoHoyAsientosPorPartido: {
        congreso_hoy: Object.assign({}, VarCamaraCongresoHoyAsientosPorPartido.default_congreso_hoy()),
    },
    SenadoCongresoHoyAsientosPorPartido: {
        congreso_hoy: Object.assign({}, VarSenadoCongresoHoyAsientosPorPartido.default_congreso_hoy()),
    },
    CongresoHoyEdadMedianaPorPartido: {
        congreso_hoy: Object.assign({}, VarCongresoHoyEdadMedianaPorPartido.default_congreso_hoy()),
    },
    ComboComposicion: {
        combo: {
            cuatrienio: {
                item: {
                    value: "",
                    label: "Seleccione un cuatrienio",
                },
                data: [],
                error: "",
            },
            legislatura: {
                item: {
                    value: "",
                    label: "Ver todas",
                },
                data: [],
                error: "",
            }
        },
    },
}

// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();

    // let { CamaraCongresoHoyAsientosPorPartidoData } = await CamaraCongresoHoyAsientosPorPartido_handlerGetChart(SelectedCuatrienio.value, ConstCharts.ComboComposicion.combo.legislatura.item.value);
    // graficas.CamaraCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica.series[0].data = CamaraCongresoHoyAsientosPorPartidoData;
    // graficas.CamaraCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica.title.text = `Curules por partido - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { SenadoCongresoHoyAsientosPorPartidoData } = await SenadoCongresoHoyAsientosPorPartido_handlerGetChart(SelectedCuatrienio.value, ConstCharts.ComboComposicion.combo.legislatura.item.value);
    // graficas.SenadoCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica.series[0].data = SenadoCongresoHoyAsientosPorPartidoData;
    // graficas.SenadoCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica.title.text = `Curules por partido - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`

    // let { CongresoHoyEdadMedianaPorPartidoSeries } = await CongresoHoyEdadMedianaPorPartido_handlerGetChart(SelectedCuatrienio.value, ConstCharts.ComboComposicion.combo.legislatura.item.value);
    // graficas.CongresoHoyEdadMedianaPorPartido.congreso_hoy.edad_mediana_por_partido.grafica.series = CongresoHoyEdadMedianaPorPartidoSeries;
    // graficas.CongresoHoyEdadMedianaPorPartido.congreso_hoy.edad_mediana_por_partido.grafica.title.text = `Congresistas con mayor promedio de edad por partido - ${SelectedCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + SelectedCuatrienio.label}`;


    return {
        props: { charts: JSON.stringify(graficas), DSCuatrienio, SelectedCuatrienio }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCuatrienio().then((response) => {
        let getLegislatura = false;
        let year = new Date().getFullYear();

        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.fechaInicio <= year && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
                getLegislatura = true;
            }
        });

        if (!getLegislatura)
            selected = { value: response.data[0].id, label: response.data[0].nombre }

        combo.unshift(Object.assign({}, ConstCharts.ComboComposicion.combo.cuatrienio.item));
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};
const CamaraCongresoHoyAsientosPorPartido_handlerGetChart = async (cuatrienio, legislatura) => {
    let data = [];
    await DatosDataService.getCamaraAsientosPorPartidos(cuatrienio, legislatura).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            data.push({
                "corporacion": currentValue.corporacion,
                "partido": currentValue.partido,
                "cuatrienio_id": currentValue.cuatrienio_id,
                "legislatura": currentValue.legislatura_id,
                "color": currentValue.color,
                "n": currentValue.n,
                "miembros": currentValue.miembros,
                "pct": currentValue.n,
                "y": currentValue.n,
                "label": currentValue.partido,
                "name": currentValue.partido,
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CamaraCongresoHoyAsientosPorPartidoData: data }
};
const SenadoCongresoHoyAsientosPorPartido_handlerGetChart = async (cuatrienio, legislatura) => {
    let data = [];
    await DatosSenadoCongresoHoyDataService.getSenadoAsientosPorPartidos(cuatrienio, legislatura).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            data.push({
                "corporacion": currentValue.corporacion,
                "partido": currentValue.partido,
                "cuatrienio_id": currentValue.cuatrienio_id,
                "legislatura": currentValue.legislatura_id,
                "color": currentValue.color,
                "n": currentValue.n,
                "miembros": currentValue.miembros,
                "pct": currentValue.n,
                "y": currentValue.n,
                "label": currentValue.partido,
                "name": currentValue.partido,
            });
        });
    })
        .catch((error) => {
            console.log(error)
        });

    return { SenadoCongresoHoyAsientosPorPartidoData: data }
};
const CongresoHoyEdadMedianaPorPartido_handlerGetChart = async (cuatrienio, legislatura) => {
    let series = [{ "group": "group", "type": "lollipop", data: [] }];
    await DatosDataService.getEdadMedianaPorPartidos(cuatrienio, legislatura).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            series[0].data.push(
                {
                    "partido": currentValue.partido,
                    "mediana": parseInt(currentValue.mediana),
                    "y": parseInt(currentValue.mediana),
                    "name": currentValue.partido,
                    "color": currentValue.color
                });
        });
    })
        .catch((error) => {
            console.log(error)
        });
    return { CongresoHoyEdadMedianaPorPartidoSeries: series }
};


const DatosComposicionPartidos = ({ charts = ConstCharts, DSCuatrienio = ConstCharts.ComboComposicion.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboComposicion.combo.cuatrienio.item }) => {
    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [dataSelectLegislatura, setDSLegislatura] = useState(ConstCharts.ComboComposicion.combo.legislatura.data);
    const [filterLegislatura, setSelectedLegislatura] = useState(ConstCharts.ComboComposicion.combo.legislatura.item);
    const [C_CamaraCongresoHoyAsientosPorPartidoGrafica, setCamaraCongresoHoyAsientosPorPartidoGrafica] = useState(JSON.parse(charts).CamaraCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica);
    const [C_CongresoHoyEdadMedianaPorPartidoGrafica, setCongresoHoyEdadMedianaPorPartidoGrafica] = useState(JSON.parse(charts).CongresoHoyEdadMedianaPorPartido.congreso_hoy.edad_mediana_por_partido.grafica);
    const [C_SenadoCongresoHoyAsientosPorPartidoGrafica, setSenadoCongresoHoyAsientosPorPartidoGrafica] = useState(JSON.parse(charts).SenadoCongresoHoyAsientosPorPartido.congreso_hoy.asientos_por_partido.grafica);

    useEffect(async () => {

        let { CamaraCongresoHoyAsientosPorPartidoData } = await CamaraCongresoHoyAsientosPorPartido_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setCamaraCongresoHoyAsientosPorPartidoGrafica({ series: { data: CamaraCongresoHoyAsientosPorPartidoData }, title: { text: `Curules por partido - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CamaraCongresoHoyAsientosPorPartido: false })

        let { SenadoCongresoHoyAsientosPorPartidoData } = await SenadoCongresoHoyAsientosPorPartido_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setSenadoCongresoHoyAsientosPorPartidoGrafica({ series: { data: SenadoCongresoHoyAsientosPorPartidoData }, title: { text: `Curules por partido - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, SenadoCongresoHoyAsientosPorPartido: false })

        let { CongresoHoyEdadMedianaPorPartidoSeries } = await CongresoHoyEdadMedianaPorPartido_handlerGetChart(filterCuatrienio.value, filterLegislatura.value);
        setCongresoHoyEdadMedianaPorPartidoGrafica({ series: CongresoHoyEdadMedianaPorPartidoSeries, title: { text: `Congresistas con mayor promedio de edad por partido - ${filterCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + filterCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyEdadMedianaPorPartido: false })

        setSubloaders(false)
    }, []);

    const setSubloaders = (active) => {
        let temp = Object.assign({}, AllSubloaders);
        Object.keys(AllSubloaders).forEach(x => {
            temp[x] = active;
        })
        setAllSubloaders(temp)
    }

    const ComboComposicion_handlerSelectComboCuatrienio = async (selectCuatrienio) => {
        setSubloaders(true)
        setDSCuatrienio(selectCuatrienio)

        let { CamaraCongresoHoyAsientosPorPartidoData } = await CamaraCongresoHoyAsientosPorPartido_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setCamaraCongresoHoyAsientosPorPartidoGrafica({ series: { data: CamaraCongresoHoyAsientosPorPartidoData }, title: { text: `Curules por partido - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CamaraCongresoHoyAsientosPorPartido: false })

        let { SenadoCongresoHoyAsientosPorPartidoData } = await SenadoCongresoHoyAsientosPorPartido_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setSenadoCongresoHoyAsientosPorPartidoGrafica({ series: { data: SenadoCongresoHoyAsientosPorPartidoData }, title: { text: `Curules por partido - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, SenadoCongresoHoyAsientosPorPartido: false })

        let { CongresoHoyEdadMedianaPorPartidoSeries } = await CongresoHoyEdadMedianaPorPartido_handlerGetChart(selectCuatrienio.value, filterLegislatura.value);
        setCongresoHoyEdadMedianaPorPartidoGrafica({ series: CongresoHoyEdadMedianaPorPartidoSeries, title: { text: `Congresistas con mayor promedio de edad por partido - ${selectCuatrienio.value === "" ? "Todos los cuatrienios" : "Cuatrienio " + selectCuatrienio.label}` } })
        // setAllSubloaders({ ...AllSubloaders, CongresoHoyEdadMedianaPorPartido: false })

        setSubloaders(false)
    };

    return (
        <>
            <Head>
                <title>Composición | Partidos</title>
                <meta name="description" content="Reportes actuales sobre partidos" />
            </Head>
            <section className="nuestraDemocraciaSection pd-top-35">

                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <a href={"/datos/congreso-hoy/proyectos-de-ley"}><i className="fas fa-check-circle"></i> Congreso hoy</a>
                            </li>
                            <li className="active">
                                <a href={"/datos/composicion/partidos"}><i className="fas fa-user-tie"></i> Composición</a>
                            </li>
                            <li>
                                <a href={"/datos/historico/proyectos-de-ley"}><i className="fas fa-history"></i> Histórico</a>
                            </li>
                            <li>
                                <a href={"/datos/votacion/votaciones"}><i className="fas fa-vote-yea"></i> Votación</a>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active" >
                            <div className="profileContainer no-pad">
                                <div className="DatosGraficasContainer relative">
                                    <div className="verticalProfileTabsContainer lg">
                                        <ul>
                                            <li className="active">

                                                <a href={"/datos/composicion/partidos"}>
                                                    <div className="icon"><i className="fa fa-gavel"></i></div>
                                                    <div className="desc"><p>Partidos</p></div>
                                                </a>

                                            </li>
                                            <li>

                                                <a href={"/datos/composicion/congresistas"}>
                                                    <div className="icon"><i className="fas fa-users"></i></div>
                                                    <div className="desc"><p>Congresistas</p></div>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>

                                    <div className="verticalTabsContainer md">
                                        {/* <div className={`subloader ${this.state.subloader ? "active" : ""}`}></div> */}
                                        <div className="verticalTab no-left with-overflow active" >
                                            <div className="info one-columns">
                                                <div className="littleSection">
                                                    <div className="top-filters">
                                                        <div className="item">
                                                            <label htmlFor="">Cuatrienio</label>
                                                            <Select
                                                                divClass=""
                                                                selectplaceholder="Seleccione"
                                                                selectValue={filterCuatrienio}
                                                                selectIsSearchable={true}
                                                                selectoptions={dataSelectCuatrienio}
                                                                selectOnchange={ComboComposicion_handlerSelectComboCuatrienio}
                                                                selectclassNamePrefix="selectReact__value-container"
                                                                spanClass="error"
                                                                spanError={""}
                                                            />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div className="principalDataContainer">
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CamaraCongresoHoyAsientosPorPartido ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CamaraCongresoHoyAsientosPorPartidoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox md">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.SenadoCongresoHoyAsientosPorPartido ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_SenadoCongresoHoyAsientosPorPartidoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="dataValueBox lg">
                                                        <div className="relative">
                                                            <div className={`subloader ${AllSubloaders.CongresoHoyEdadMedianaPorPartido ? "active" : ""}`}></div>
                                                            <HighchartsReact
                                                                highcharts={Highcharts}
                                                                options={C_CongresoHoyEdadMedianaPorPartidoGrafica}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default DatosComposicionPartidos
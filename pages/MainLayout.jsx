import AuthLogin from "../Utils/AuthLogin";
import dynamic from "next/dynamic";
import 'suneditor/dist/css/suneditor.min.css';
import { useInfoGeneral } from "../Hooks/useInfoGeneral";
import Link from 'next/link'

const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
const Data1 = [
    {
        "@context":"http:\/\/schema.org\/",
        "@type":"wpHeader",
        "url":"https:\/\/congresovisible.uniandes.edu.co",
        "headline":"Congreso Visible",
        "description":"En Congreso Visible encontrarás toda la información del congreso colombiano. Visitanos ahora e informate"
    },
    {
        "@context":"http:\/\/schema.org\/",
        "@type":"wpFooter",
        "url":"https:\/\/congresovisible.uniandes.edu.co",
        "headline":"Congreso Visible",
        "description":"En Congreso Visible encontrarás toda la información del congreso colombiano",
        "copyrightYear":"2021"
    }
];

const Data2 = {
    "@context": "https://schema.org",
    "@type": "WebSite",
    "@id": "#website",
    "url": "https://congresovisible.uniandes.edu.co",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://congresovisible.uniandes.edu.co?s={search_term_string}",
        "query-input": "required name=search_term_string"
    }
}
const Data3 = {
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [
        {
            "@type": "ListItem",
            "position": 1,
            "item": {
                "@id": "https://congresovisible.uniandes.edu.co",
                "name": "Home"
            }
        }
    ]
}
// export async function getServerSideProps({ query }) {
//     const { state, loading, observacionesLegales } = useInfoGeneral();
//     return {
//         props: { contacto: state, infoGeneral: observacionesLegales }
//     }
// }

const MainLayout = ({ children }) => {
    const auth = new AuthLogin();
    const { state, loading, observacionesLegales } = useInfoGeneral();
    let datosContacto = state?.congreso_visible_datos_contacto;

    return (
        <body>
            <header>
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(Data1) }} />
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(Data2) }} />
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(Data3) }} />
                <nav>
                    <span className="building-info">
                        <button type="button" className="toggleMenu" onClick={(e) => {
                            let toggleButtonMenu = e.currentTarget;
                            if (toggleButtonMenu)
                                showMenu(toggleButtonMenu)
                        }}>
                            <i className="fas fa-bars"></i>
                        </button>
                    </span>
                    <div className="logos">
                        <a href="/">
                            <img
                                className="cv center-block"
                                src="/img/lg.svg"
                                alt="Universidad de los Andes"
                            />
                        </a>
                        {/* <a href="/home"><img className="andes center-block" src="/img/universidad-logo.svg" alt="Logo Universidad de los Andes" /></a> */}
                    </div>
                    <div className="social-links">
                        {datosContacto != null && datosContacto != "undefined"
                            ? datosContacto.map((x, i) => {
                                if (x.datos_contacto.tipo === 2) {
                                    return (

                                        <a rel="noreferrer" key={i} href={x.cuenta} target="_blank">
                                            <img src={typeof x.datos_contacto.datos_contacto_imagen[0] !== "undefined" ? auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0].imagen : ""} alt="" />
                                        </a>
                                    );
                                }
                            })
                            : ""}
                    </div>
                </nav>

                <div className="cv-navbar">
                    <button type="button" className="closeMenu" onClick={(e) => {
                        let closeButtonMenu = e.currentTarget;
                        if (closeButtonMenu)
                            hideMenu(closeButtonMenu)
                    }}>
                        <i className="fas fa-times"></i>
                    </button>
                    <nav className="cv-links">
                        <a href="/">Inicio</a>
                        <a href="/congresistas">Congresistas</a>
                        <a href="/democracia">Nuestra democracia</a>
                        <a href="/orden-del-dia">Actividad legislativa</a>
                        <a href="/comisiones">Comisiones</a>
                        <a href="/contenido-multimedia">Contenido multimedia</a>
                        <a href="/proyectos-de-ley">Proyectos de ley</a>
                        <a href="/datos/congreso-hoy/proyectos-de-ley">Datos</a>
                        <a href="/quesomos">¿Qué es Congreso Visible?</a>
                    </nav>
                    <div className="social-links">
                        {
                            datosContacto != null && datosContacto != "undefined" ?
                                datosContacto.map((x, y) => {
                                    if (x.datos_contacto.tipo === 2) {
                                        return (
                                            <a rel="noreferrer" key={x.id} href={x.cuenta} target="_blank">
                                                <img src={typeof x.datos_contacto.datos_contacto_imagen[0] !== 'undefined' ? auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0].imagen : ""} alt="" />
                                            </a>
                                        )
                                    }
                                }) : ""
                        }
                        {/* <a href=""><i className="fab fa-facebook-f"></i></a>
                        <a href=""><i className="fab fa-twitter"></i></a>
                        <a href=""><i className="fab fa-instagram"></i></a>
                        <a href=""><i className="fab fa-youtube"></i></a> */}
                    </div>
                </div>
            </header>
            <main>
                <div className="colorPicker"></div>
                <div className="blackCap" onClick={(e) => {
                    let blackCap = e.currentTarget;
                    if (blackCap)
                        hideMenu(blackCap)
                }}></div>
                {children}
            </main>
            <footer gtdtarget="5">
                <div className="footerInfo padding-35">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <h3>Observaciones legales</h3>
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: observacionesLegales || "Sin observaciones" }}></div>
                            </div>
                            <div className="col-md-4">
                                <h3>Enlaces de interés</h3>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="good-links">
                                            <a href="/congresistas">Congresistas</a>
                                            <a href="/democracia">Nuestra democracia</a>
                                            <a href="/orden-del-dia">Actividad legislativa</a>
                                            <a href="/comisiones">Comisiones</a>
                                            <a href="/contenido-multimedia">Contenido multimedia</a>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="good-links">
                                            <a href="/proyectos-de-ley">Proyectos de ley</a>
                                            <a href="/datos/congreso-hoy/proyectos-de-ley">Datos</a>
                                            <a href="/quesomos">¿Qué es Congreso Visible?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="logos">
                                    <a href="/">
                                        <img
                                            className="cv center-block"
                                            src="/img/congreso-logo-footer.svg"
                                            alt="Logo Congreso Visible"
                                        />
                                    </a>
                                    <a href="/">
                                        <img
                                            className="andes center-block"
                                            src="/img/universidad-logo-footer.svg"
                                            alt="Logo Universidad de los Andes"
                                        />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="social-copy padding-25">
                    <div className="social-links">
                        {datosContacto != null && datosContacto != "undefined"
                            ? datosContacto.map((x, i) => {
                                if (x.datos_contacto.tipo === 2) {
                                    return (
                                        <a rel="noreferrer" key={i} href={x.cuenta} target="_blank">
                                            <img alt="" src={
                                                typeof x.datos_contacto.datos_contacto_imagen[0] !== "undefined" ?
                                                    auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0].imagen :
                                                    ""
                                            }

                                            />
                                        </a>
                                    );
                                } else {
                                    return (
                                        <span key={x.id}>
                                            <img
                                                src={
                                                    typeof x.datos_contacto
                                                        .datos_contacto_imagen[0] !==
                                                        "undefined"
                                                        ? auth.pathApi() +
                                                        x.datos_contacto
                                                            .datos_contacto_imagen[0]
                                                            .imagen
                                                        : ""
                                                }
                                                alt=""
                                            />
                                            <small>{" " + x.cuenta}</small>
                                        </span>
                                    );
                                }
                            })
                            : ""}
                    </div>
                    <div className="copyright">
                        <p>
                            © 2021 - <a href="https://www.cidfares.com/">CID Fares</a> -
                            Todos los derechos reservados
                        </p>
                    </div>
                </div>
            </footer>
        </body>
    );
};

function showMenu(button) {
    let menu = document.querySelector(".cv-navbar");
    let blackCap = document.querySelector(".blackCap");
    if (menu && blackCap) {
        menu.classList.add("active");
        blackCap.classList.add("active");
    } else {
        console.warn("Falta definir el menú o la blackCap");
    }
    // button.classList.toggle("active");
}

function hideMenu(button) {
    let menu = document.querySelector(".cv-navbar");
    let blackCap = document.querySelector(".blackCap");
    if (menu && blackCap) {
        menu.classList.remove("active");
        blackCap.classList.remove("active");
    } else {
        console.warn("Falta definir el menú o la blackCap");
    }
}

export default MainLayout;

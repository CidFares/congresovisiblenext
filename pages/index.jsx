import Head from 'next/head'
import Image from 'next/image'
// import styles from '../styles/Home.module.css'
import Link from 'next/link'
import ActLegislativaEventosList from "../Components/CongresoVisible/ActLegislativaEventosList";

import ActividadesLegislativasDataService from "../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import BlogNdService from "../Services/BlogNd/BlogNd.Service";
import CVDataService from "../Services/CongresoVisible/CongresoVisible.Service";
import DatosDataService from "../Services/Datos/Datos.Service";
import infoSitioDataService from "../Services/General/informacionSitio.Service";

import { Constantes, URLBase } from "../Constants/Constantes.js";
import GoDownButton from "../Components/CongresoVisible/GoDownButton";
import LittleCalendar from "../Components/LittleCalendar";
import InputPercent from "../Components/InputPercent";
import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/dist/styles.css";
import QueEsCongresoVisible from "../Components/CongresoVisible/QueEsCongresoVisible";
import CongresoVisibleAliado from "../Components/CongresoVisible/CongresoVisibleAliado";
import * as FechaMysql from "../Utils/FormatDate";
import Select from "../Components/Select";
import { useState, useEffect } from "react";
import AuthLogin from "../Utils/AuthLogin";

const auth = new AuthLogin();
const formatDate = () => {
  let dt = new Date();
  let month = dt.getMonth();
  let day = dt.getDate();

  return (
    dt.getFullYear() +
    "-" +
    (month < 10 ? "0" + (month + 1) : month + 1) +
    "-" +
    (day < 10 ? "0" + day : day)
  );
};
const dateCalendar = (e) => {
  return (
    e.year +
    "-" +
    (e.month < 10 ? "0" + e.month : e.month) +
    "-" +
    (e.day < 10 ? "0" + e.day : e.day)
  );
};
const constDatosInicio = {
  proyectosSancionadosComoLeyCuatrienio: 0,
  totalTemasTratadosCuatrienio: 0,
  controlesPoliticosCuatrienio: 0,
  proyectosDeLeyCongresoCuatrienio: 0
}

const constDataHome = {
  id: 0,
  imgPrincipal: null,
  congresistas: "",
  nuestraDemocracia: "",
  actividadLegislativa: "",
  comisiones: "",
  contenidoMultimedia: "",
  proyectosDeLey: "",
  datos: "",
  conflictoInteres: "",
  slide_secundario: null,
  mediaCarousel: [],
};

const dataCV = {
  id: 0,
  queEs: "",
  objetivos: "",
  historiaymision: "",
  nuestroFuturo: "",
  nuestroReto: "",
  imagen: null,
  congreso_visible_datos_contacto: null,
};

const maxCaracteresMenus = 368;

const AgendaConst = {
  subloaderCalendario: false,
  subloaderAgendaActividades: false,
  fechaActual: formatDate(),
  year: 2022,
  month: 1,
  fechaCalendario: null,
  eventos: [],
  listByFecha: {
    data: [],
    dataCalendar: [],
    totalRows: 0,
    search: "",
    page: 1,
    rows: 8
  },
  filterTipoActividadA: { value: -1, label: "Ver todas" },
  dataSelectActividadA: [],
  filterCorporacionA: { value: -1, label: "Ver Cámara y Senado" },
  dataSelectCorporacionA: [],
  filterTComisionA: { value: -1, label: "Elija un tipo de comisión" },
  dataSelecTComisionA: [],
  filterComisionA: { value: -1, label: "Ver todas" },
  dataSelectComisionA: [],
  subloaderFilters: false
}

const listBlogsConst = [
  {
    id: 0,
    titulo: "",
    fecha_publicacion: "",
    tipo_publicacion_id: 0,
    activo: 1,
    blog_nd_portada: [
      {
        id: 0,
        blog_nd_id: 0,
        portada: "",
        activo: 0
      }
    ],
    conceptos: [
      {
        id: 0,
        blog_nd_id: 0,
        glosario_legislativo_id: 0,
        activo: 1,
        glosario_legislativo: [
          {
            id: 0,
            palabra: "",
            concepto: "",
            activo: 1
          }
        ]
      }
    ],
    tipo_publicacion: {
      id: 0,
      nombre: "",
      icono: "",
      activo: 1
    }
  }
];

//  SSR
export async function getServerSideProps({ query }) {
  let AgendaData = AgendaConst;

  let datosInicio = await getDatosInicio();
  let infoHome = Object.assign({}, constDataHome)
  infoHome = await getInformacionSitioHome();

  let mediaCarousel = []
  Object.entries(infoHome.slide_secundario).forEach(
    ([key, value]) => {
      mediaCarousel.push({
        source: auth.pathApi() + value.imagen,
      });
    }
  );

  infoHome.mediaCarousel = mediaCarousel;

  // Agenda
  let year = new Date().getFullYear();
  let month = new Date().getMonth() + 1;
  AgendaData.listByFecha.dataCalendar = await getDataByYearAndMonth(year, month);
  let fechaServer = FechaMysql.DateTimeFormatMySql(AgendaData.fechaActual + ' ' + '00:00:00');
  AgendaData.listByFecha = await getAgendaLegislativa(1, AgendaData.listByFecha.search, AgendaData.listByFecha.page, AgendaData.listByFecha.rows, fechaServer, AgendaData.filterTipoActividadA.value, AgendaData.filterCorporacionA.value, AgendaData.filterComisionA.value);

  let listBlogs = await getlastBlogs();
  let dataCV = await getAllCV();

  return {
    props: { datosInicio, infoHome, AgendaData, listBlogs, data: dataCV }
  }
}

// PETICIONES

const getDatosInicio = async () => {
  let data = null;
  await DatosDataService.getDatosInicio().then((response) => {
    data = {
      proyectosSancionadosComoLeyCuatrienio: response.data[0].n,
      totalTemasTratadosCuatrienio: response.data[1].n,
      controlesPoliticosCuatrienio: response.data[2].n,
      proyectosDeLeyCongresoCuatrienio: response.data[3].n
    }
  });

  return data;
}
const getInformacionSitioHome = async () => {
  let data = null;
  await infoSitioDataService.getInformacionSitioHome().then((response) => {
    data = response.data[0];
    data.slide_secundario = response.data[0].slide_secundario;
    data.imgPrincipal = response.data[0].imgPrincipal;
  })

  return data;
}
const getComboTipoActividadAgenda = async () => {
  let combo = [];
  await ActividadesLegislativasDataService.getComboTipoActividadAgenda().then(response => {
    response.data.forEach(i => {
      combo.push({ value: i.id, label: i.nombre })
    })
    combo.unshift({ value: -1, label: "Ver todas" })
  })
  return combo;
}
const getComboCorporacion = async () => {
  let combo = [];
  await ActividadesLegislativasDataService.getComboCorporacion().then((response) => {
    response.data.forEach((i) => {
      combo.push({ value: i.id, label: i.nombre });
    });
    combo.unshift({ value: -1, label: "Ver cámara y senado" });
  });

  return combo
};
const getComboTipoComision = async (idCorporacion) => {
  let combo = [];
  await ActividadesLegislativasDataService.getComboTipoComision(idCorporacion).then(response => {

    response.data.forEach(i => {
      combo.push({ value: i.id, label: i.nombre })
    })
    combo.unshift({ value: -1, label: "Elija tipo de comisión" })
  })
  return combo;
}
const getComboComisiones = async (idTipoComision, idCorporacion) => {
  let combo = [];
  await ActividadesLegislativasDataService.getComboComisiones(idTipoComision, idCorporacion).then(response => {
    response.data.forEach(i => {
      combo.push({ value: i.id, label: i.nombre })
    })
    combo.unshift({ value: -1, label: "Ver todas" })
  })
  return combo;
}
const getlastBlogs = async () => {
  let listBlogs = [];
  await BlogNdService.getUltimasPub().then((response) => {
    listBlogs = response.data;
  })
    .catch((e) => {
      console.error(e);
    });
  return listBlogs;
};
const getAllCV = async () => {
  let cv = null;
  await CVDataService.getAll().then((response) => {
    cv = response.data[0];
  })
    .catch((e) => {
      console.error(e);
    });

  return cv;
};
const getAgendaLegislativa = async (idFilterActive, search, page, rows, fecha, tipoactividad, corporacion, comision, destacado = 1) => {
  let f = FechaMysql.DateTimeFormatMySql(fecha + ' ' + '00:00:00');
  let listfecha = Object.assign({}, AgendaConst.listByFecha);
  await ActividadesLegislativasDataService.getAgendaLegislativaByFecha(idFilterActive, search, page, rows, f, tipoactividad, corporacion, comision, destacado).then((response) => {
    listfecha.data = response.data
  })
    .catch((e) => {
      console.error(e);
    });
  await ActividadesLegislativasDataService.getTotalRecordsAgendaActividad(idFilterActive, search, f, tipoactividad, corporacion, comision, destacado).then((response) => {
    listfecha.totalRows = response.data;
  })
    .catch((e) => {
      console.error(e);
    });
  return listfecha;
}
const getDataByYearAndMonth = async (year, month, destacado = 1) => {
  let data = [];
  let dataCalendario = [];
  await ActividadesLegislativasDataService.getDataByYearAndMonth(year, month, destacado).then((response) => {
    data = response.data;
    let fechaSlice;
    data.forEach(x => {
      fechaSlice = x.fecha.slice(0, 10);
      dataCalendario.push({ year: Number(fechaSlice.split("-")[0]), month: Number(fechaSlice.split("-")[1]), day: Number(fechaSlice.split("-")[2]), className: 'tieneEventos' });
    });
  })
    .catch((e) => {
      console.error(e);
    });
  return dataCalendario;
}

// EXPORT
export default function Home({ datosInicio = constDatosInicio, infoHome = constDataHome, AgendaData = AgendaConst, listBlogs = listBlogsConst, data = dataCV }) {

  const [DatosInicio, setDatosContacto] = useState(constDatosInicio);
  const [subloader, setSubloader] = useState(AgendaConst.subloader);
  const [subloaderFilters, setSubloaderFilters] = useState(AgendaConst.subloaderFilters);
  const [subloaderCalendario, setSubloaderCalendario] = useState(AgendaConst.subloaderCalendario);
  const [subloaderAgendaActividades, setSubloaderAgendaActividades] = useState(AgendaConst.subloaderAgendaActividades);
  const [fechaActual, setFechaActual] = useState(AgendaConst.fechaActual);
  const [year, setYear] = useState(AgendaConst.year);
  const [month, setMonth] = useState(AgendaConst.month);
  const [fechaCalendario, setFechaCalendario] = useState(AgendaConst.fechaCalendario);
  const [eventos, setEventos] = useState(AgendaConst.eventos);
  const [listByFecha, setListByFecha] = useState(AgendaConst.listByFecha);
  const [filterTipoActividadA, setFilterTA] = useState(AgendaConst.filterTipoActividadA);
  const [dataSelectActividadA, setDSActividad] = useState(AgendaConst.dataSelectActividadA);
  const [filterCorporacionA, setFilterCorp] = useState(AgendaConst.filterCorporacionA);
  const [dataSelectCorporacionA, setDSCorporacion] = useState(AgendaConst.dataSelectCorporacionA);
  const [filterTComisionA, setFilterTC] = useState(AgendaConst.filterTComisionA);
  const [dataSelecTComisionA, setDSTComision] = useState(AgendaConst.dataSelecTComisionA);
  const [filterComisionA, setFilterComision] = useState(AgendaConst.filterComisionA);
  const [dataSelectComisionA, setDSComision] = useState(AgendaConst.dataSelectComisionA);
  // setDatosContacto(datosContacto)

  useEffect(() => {
    PrepareCalendar();
    setListByFecha(AgendaData.listByFecha);
  }, []);

  const PrepareCalendar = () => {
    // Proceso para acondicionar calendario.
    let prevButton = document.querySelector(".Calendar__monthArrowWrapper.-right");
    let nextButton = document.querySelector(".Calendar__monthArrowWrapper.-left");
    let monthsButton = document.querySelectorAll(".Calendar__monthSelectorItemText");
    let yearsButton = document.querySelectorAll(".Calendar__yearSelectorText");
    if (prevButton) {
      prevButton.addEventListener("click", async (e) => {
        e.preventDefault();
        e.target.disabled = true;
        await handlerPrevAgenda(e);
        e.target.disabled = false;
      });
    }
    if (nextButton) {
      nextButton.addEventListener("click", async (e) => {
        e.preventDefault();
        e.target.disabled = true;
        await handlerNextAgenda(e)
        e.target.disabled = false;
      });
    }
    if (monthsButton) {
      monthsButton.forEach(month => { month.addEventListener("click", (e) => { handlerChooseMonth(e.currentTarget.innerText) }) })
    }
    if (yearsButton) {
      yearsButton.forEach(year => { year.addEventListener("click", (e) => { handlerChooseYear(e.currentTarget.innerText) }) })
    }
  }
  const handlerFilterActividadAgenda = async (selectActividad) => {
    setFilterTA(selectActividad);
    setSubloader(true);
    let list = await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fechaActual, selectActividad.value, filterCorporacionA.value, filterComisionA.value);
    setListByFecha(list)
    setSubloader(false);
  }
  const handlerFilterCorporacionAgenda = async (selectCorporacion) => {
    setFilterCorp(selectCorporacion);
    setSubloader(true);
    let comboTComision = await getComboTipoComision(selectCorporacion.value);
    setDSTComision(comboTComision);
    let list = await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fechaActual, filterTipoActividadA.value, selectCorporacion.value, filterComisionA.value);
    setListByFecha(list)
    setSubloader(false);
  }
  const handlerFilterTComisionAgenda = async (selectTComision) => {
    setFilterTC(selectTComision);
    setSubloaderFilters(true);
    let comboComisiones = await getComboComisiones(selectTComision.value, filterCorporacionA.value);
    setDSComision(comboComisiones);
    setSubloaderFilters(false);
  }
  const handlerFilterComisionAgenda = async (selectComision) => {
    setFilterComision(selectComision);
    setSubloader(true);
    let list = await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fechaActual, filterTipoActividadA.value, filterCorporacionA.value, selectComision.value);
    setListByFecha(list);
    setSubloader(false);
  }
  const handlerPrevAgenda = async (e) => {
    setSubloaderCalendario(true);
    let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
    let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
    let month = getMonthNumberByStr(prevNextMonth(monthTemp));
    let year = prevNextMonth(monthTemp) === "diciembre" ? Number(yearTemp) - 1 : Number(yearTemp)
    let dataCalendar = await getDataByYearAndMonth(year, month);
    setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
    setYear(year);
    setMonth(month);
    setSubloaderCalendario(false);
  }
  const handlerNextAgenda = async (e) => {
    setSubloaderCalendario(true);
    let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
    let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
    let month = getMonthNumberByStr(prevNextMonth(monthTemp, true));
    let year = prevNextMonth(monthTemp, true) === "enero" ? Number(yearTemp) + 1 : Number(yearTemp)
    let dataCalendar = await getDataByYearAndMonth(year, month);
    setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
    setYear(year);
    setMonth(month);
    setSubloaderCalendario(false);
  }
  const handlerDataSelectDay = async (e) => {
    setSubloader(true);

    let fecha = dateCalendar(e);
    setFechaActual(fecha);
    let list = await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fecha);
    setListByFecha({ ...listByFecha, data: list.data, totalRows: list.totalRows });
    setFechaCalendario(e);
    setSubloader(false);

  }
  const handlerChooseMonth = async (month) => {
    setSubloaderCalendario(true);
    let yearTemp = document.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
    let mes = getMonthNumberByStr(month);
    setYear(yearTemp);
    setMonth(mes);
    let dataCalendar = await getDataByYearAndMonth(Number(yearTemp), mes);
    setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
    setSubloaderCalendario(false);
  }
  const handlerChooseYear = async (anio) => {
    setSubloaderCalendario(true);
    let monthTemp = document.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
    let mes = getMonthNumberByStr(monthTemp);
    setYear(Number(anio));
    setMonth(mes);
    let dataCalendar = await getDataByYearAndMonth(Number(anio), mes);
    setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
    setSubloaderCalendario(false);
  }

  const handlerPaginationAgendaActividades = async (page, rows, search = "") => {
    setListByFecha({ ...listByFecha, page: page, rows: rows, search: search });
    let timeout;
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(
      async function () {
        setSubloader(true);
        let list = await getAgendaLegislativa(
          1,
          search,
          page,
          rows,
          fechaActual,
          filterTipoActividadA.value,
          filterCorporacionA.value,
          filterComisionA.value
        );
        setListByFecha(list);
        setSubloader(false);
      }.bind(this),
      1000
    );
  }


  return (
    <>
      <Head>
        <title>Conoce todo acerca del congreso colombiano | Congreso Visible</title>
        <meta name="description" content="En Congreso Visible encontrarás toda la información del congreso colombiano. Visitanos ahora e informate de todo acerca del congreso" />
        <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Conoce todo acerca del congreso colombiano | Congreso Visible" />
        <meta property="og:description" content="En Congreso Visible encontrarás toda la información del congreso colombiano. Visitanos ahora e informate de todo acerca del congreso" />
        <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
        <meta property="og:image:width" content="828" />
        <meta property="og:image:height" content="450" />
        <meta property="og:url" content={URLBase} />
        <meta property="og:site_name" content="Congreso Visible" />
        <meta name="twitter:url" content={URLBase} />
        <meta name="twitter:title" content="Conoce todo acerca del congreso colombiano | Congreso Visible"/>
        <meta name="twitter:description" content="En Congreso Visible encontrarás toda la información del congreso colombiano. Visitanos ahora e informate de todo acerca del congreso" />
        <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
        <link rel="canonical" href="https://congresovisible.uniandes.edu.co/"/>
      </Head>

      <main>
        <section className="CVBannerMenuContainer bg-blue" style={infoHome.imgPrincipal != null ? { backgroundImage: `url('${auth.pathApi() + infoHome.imgPrincipal}')` } : {}}>
          <GoDownButton className={"goToDown"} gtdtarget="1" />
          <div className="CVMenu">
            <div className="CVLink">
              <a href="/congresistas">
                <h2><span>Congresistas</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.congresistas.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.congresistas
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href={"/democracia"}>
                <h2><span>Nuestra democracia</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.nuestraDemocracia.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.nuestraDemocracia
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/orden-del-dia">
                <h2><span>Actividad legislativa</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.actividadLegislativa.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.actividadLegislativa
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/comisiones">
                <h2><span>Comisiones</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.comisiones.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.comisiones
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/contenido-multimedia">
                <h2><span>Contenido multimedia</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.contenidoMultimedia.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.contenidoMultimedia}
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/proyectos-de-ley">
                <h2><span>Proyectos de ley</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.proyectosDeLey.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.proyectosDeLey
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/datos/congreso-hoy/proyectos-de-ley">
                <h2><span>Datos</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368
                      ? infoHome.datos.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.datos
                  }
                </p>
              </div>
            </div>
            <div className="CVLink">
              <a href="/conflictos-interes">
                <h2><span>Conflictos de interés</span></h2>
              </a>
              <div className="CVMenuDescription">
                <p>
                  {
                    maxCaracteresMenus < 368 ?
                      infoHome.conflictoInteres.slice(0, maxCaracteresMenus) + "..."
                      : infoHome.conflictoInteres
                  }
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="no-full-height datosSection" gtdtarget="1">
          <GoDownButton className={"goToDown"} gtdtarget="2" />
          <div className="datosContainer">
            <h2>
              <span className="icon"><i className="fas fa-chart-pie"></i></span>{" "}
              Datos destacados en el cuatrienio actual
            </h2>
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-3 col-md-4 col-sm-6">
                  <div className="dato">
                    <InputPercent
                      value={100}
                      noShowValue={true}
                      color={"var(--circle-chart-color-one)"}
                      fontColor="#000"
                    />
                    <div className="value">{datosInicio.proyectosSancionadosComoLeyCuatrienio}</div>
                    <div className="desc">
                      <p className="text-center">Proyectos sancionados como ley</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  <div className="dato">
                    <InputPercent
                      value={100}
                      noShowValue={true}
                      color={"var(--circle-chart-color-two)"}
                      fontColor="#000"
                    />
                    <div className="value">{datosInicio.totalTemasTratadosCuatrienio}</div>
                    <div className="desc">
                      <p className="text-center">Total de temas tratados</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  <div className="dato">
                    <InputPercent
                      value={100}
                      noShowValue={true}
                      color={"var(--circle-chart-color-three)"}
                      fontColor="#000"
                    />
                    <div className="value">{datosInicio.controlesPoliticosCuatrienio}</div>
                    <div className="desc">
                      <p className="text-center">Controles políticos</p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  <div className="dato">
                    <InputPercent
                      value={100}
                      noShowValue={true}
                      color={"var(--circle-chart-color-four)"}
                      fontColor="#000"
                    />
                    <div className="value">{datosInicio.proyectosDeLeyCongresoCuatrienio}</div>
                    <div className="desc">
                      <p className="text-center">Proyectos de ley en el Congreso</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section gtdtarget="2" className="bg-blue bg-bogota no-full-height publicationSection noselect">
          <GoDownButton className={"goToDown"} gtdtarget="3" />
          <div className="latestNews">
            <h2 className="bg-blue">
              <span className="icon"><i className="fas fa-newspaper"></i></span>{" "}
              Últimas publicaciones
            </h2>
            <div className="pd-25">
              <div className="four-columns publicationsContainer">
                {listBlogs.map((item, i) => {
                  let href = auth.filterStringForURL(item.titulo);
                  return (
                    <div className="pubCol" key={i}>
                      <a href={`/democracia/${href}/${item.id}`}>
                        <div className="publication">
                          <div className="title">
                            <h5>{item.titulo}</h5>
                            <span><i className={item.tipo_publicacion.icono}></i></span>
                          </div>
                          <div className="date">
                            <p>{auth.coloquialDate(item.fecha_publicacion)}</p>
                          </div>
                          <div className="description">
                            <figure>
                              <img src={auth.pathApi() + item.blog_nd_portada[0]?.portada} alt={item.titulo} />
                            </figure>
                          </div>
                          <div className="link">
                            <a href={`/democracia/${href}/${item.id}`} className="linkButton right">
                              <i className="fas fa-chevron-right"></i>
                            </a>
                          </div>
                        </div>
                      </a>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </section>
        <section gtdtarget="3" className="no-full-height agendaSection">
          <GoDownButton className={"goToDown"} gtdtarget="4" />
          <div className="calendarContainerCV">
            <h2>
              <span className="icon"><i className="fas fa-calendar-alt"></i></span>{" "}
              Agenda Legislativa (Eventos destacados)
            </h2>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-5">
                  <div className="relative">
                    <div className={`subloader ${subloaderCalendario ? "active" : ""}`}></div>
                    <LittleCalendar value={fechaCalendario} onChange={(e) => { handlerDataSelectDay(e) }} customDaysClassName={listByFecha.dataCalendar} />
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="buscador pd-25">
                    <div className="input-group">
                      <input type="text" value={listByFecha.search}
                        onChange={async (e) => {
                          setListByFecha({ ...listByFecha, search: e.target.value })

                        }}
                        onKeyUp={async (e) => {
                          if (e.key === "Enter") {
                            await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, e.target.value)
                          }
                        }}
                        placeholder="Escriba para buscar" className="form-control" />

                      <span className="input-group-text"><button onClick={async () => { await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, listByFecha.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                      <span className="input-group-text"><button onClick={(e) => { toggleFilter(e.currentTarget); }} type="button" className="btn btn-primary"><i className="fa fa-filter"></i></button></span>
                    </div>
                    <div className="floatingFilters evenColors">
                      <div className="one-columns relative no-margin">
                        <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                        <div className="item">
                          <label htmlFor="">Filtrar por tipo de actividad</label>
                          <Select
                            handlerOnClick={async () => {
                              if (dataSelectActividadA.length <= 1) {
                                setSubloaderFilters(true);
                                let combo = await getComboTipoActividadAgenda();
                                setDSActividad(combo)
                                setSubloaderFilters(false);
                              }
                            }}
                            divClass=""
                            selectplaceholder="Seleccione"
                            selectValue={filterTipoActividadA}
                            selectOnchange={handlerFilterActividadAgenda}
                            selectoptions={dataSelectActividadA}
                            selectIsSearchable={true}
                            selectclassNamePrefix="selectReact__value-container"
                            spanClass=""
                            spanError="" >
                          </Select>
                        </div>
                        <div className="item">
                          <label htmlFor="">Filtrar por Corporación</label>
                          <Select
                            handlerOnClick={async () => {
                              if (dataSelectCorporacionA.length <= 1) {
                                setSubloaderFilters(true);
                                let combo = await getComboCorporacion();
                                setDSCorporacion(combo);
                                setSubloaderFilters(false);
                              }
                            }}
                            divClass=""
                            selectplaceholder="Seleccione"
                            selectValue={filterCorporacionA}
                            selectOnchange={handlerFilterCorporacionAgenda}
                            selectoptions={dataSelectCorporacionA}
                            selectIsSearchable={true}
                            selectclassNamePrefix="selectReact__value-container"
                            spanClass=""
                            spanError="" >
                          </Select>
                        </div>
                        <div className="item">
                          <label htmlFor="">Filtrar por Tipo de Comisión</label>
                          <Select
                            divClass=""
                            selectplaceholder="Seleccione"
                            selectValue={filterTComisionA}
                            selectOnchange={handlerFilterTComisionAgenda}
                            selectoptions={dataSelecTComisionA}
                            selectIsSearchable={true}
                            selectclassNamePrefix="selectReact__value-container"
                            spanClass=""
                            spanError="" >
                          </Select>
                        </div>
                        <div className="item">
                          <label htmlFor="">Filtrar por Comision</label>
                          <Select
                            divClass=""
                            selectplaceholder="Seleccione"
                            selectValue={filterComisionA}
                            selectOnchange={handlerFilterComisionAgenda}
                            selectoptions={dataSelectComisionA}
                            selectIsSearchable={true}
                            selectclassNamePrefix="selectReact__value-container"
                            spanClass=""
                            spanError="" >
                          </Select>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div className="relative">
                    <div className={`subloader ${subloaderAgendaActividades ? "active" : ""}`}></div>
                    <ActLegislativaEventosList
                      data={listByFecha.data}
                      handler={handlerPaginationAgendaActividades}
                      pageExtends={listByFecha.page}
                      pageSize={listByFecha.rows}
                      totalRows={listByFecha.totalRows}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div gtdtarget={4}></div>
        <QueEsCongresoVisible
          data={data}
          dataSlide={infoHome.mediaCarousel}
          button={true}
          subloader={false}
        />
        <section className="no-full-height">
          <CongresoVisibleAliado />
        </section>
      </main>
    </>
  )
}


function getMonthNumberByStr(str) {
  let month = str.toString().toLowerCase();
  switch (month) {
    case 'enero':
      return 1
      break;
    case 'febrero':
      return 2
      break;
    case 'marzo':
      return 3
      break;
    case 'abril':
      return 4
      break;
    case 'mayo':
      return 5
      break;
    case 'junio':
      return 6
      break;
    case 'julio':
      return 7
      break;
    case 'agosto':
      return 8
      break;
    case 'septiembre':
      return 9
      break;
    case 'octubre':
      return 10
      break;
    case 'noviembre':
      return 11
      break;
    case 'diciembre':
      return 12
      break;
    default:
      break;
  }
}
function prevNextMonth(str, next = false) {
  str = str.toString().toLowerCase();
  switch (str) {
    case 'enero':
      return !next ? "diciembre" : "febrero"
      break;
    case 'febrero':
      return !next ? "enero" : "marzo"
      break;
    case 'marzo':
      return !next ? "febrero" : "abril"
      break;
    case 'abril':
      return !next ? "marzo" : "mayo"
      break;
    case 'mayo':
      return !next ? "abril" : "junio"
      break;
    case 'junio':
      return !next ? "mayo" : "julio"
      break;
    case 'julio':
      return !next ? "junio" : "agosto"
      break;
    case 'agosto':
      return !next ? "julio" : "septiembre"
      break;
    case 'septiembre':
      return !next ? "agosto" : "octubre"
      break;
    case 'octubre':
      return !next ? "septiembre" : "noviembre"
      break;
    case 'noviembre':
      return !next ? "octubre" : "diciembre"
      break;
    case 'diciembre':
      return !next ? "noviembre" : "enero"
      break;
    default:
      break;
  }
}
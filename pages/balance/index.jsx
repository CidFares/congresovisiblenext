import { useState, useEffect } from "react";
import Link from "next/link";
import dynamic from 'next/dynamic';
import Head from "next/head"
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import 'suneditor/dist/css/suneditor.min.css'
import Select from '../../Components/Select';
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";

// Const
const auth = new AuthLogin();

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const PageConst = {
    subloaderBalanceCuatrienio: true,
    subloaderFilters: false,
    filterYearInicio: { value: -1, label: "Filtrar por año de inicio" },
    dataSelectYearInicio: [],
    listBalanceCuatrienio: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "titulo", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Año de inicio", text: "yearInicio", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Año de finalización", text: "yearFin", esImg: false, img: "", putOnFirstElement: false },
                ],
            generalActionTitle: "Informes",
            generalIcon: "fas fa-file-alt"
        },
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
};

const BalanceConst = {};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let BalanceData = BalanceConst;

    PageData.listBalanceCuatrienio.data = await getAllBalanceCuatrienio(
        1
        , query.year || PageData.filterYearInicio.value
        , query.search || PageData.listBalanceCuatrienio.search
        , query.page || PageData.listBalanceCuatrienio.page
        , query.rows || PageData.listBalanceCuatrienio.rows
    );
    PageData.listBalanceCuatrienio.totalRows = await getTotalRecordsBalanceCuatrienio(
        1
        , query.year || PageData.filterYearInicio.value
        , query.search || PageData.listBalanceCuatrienio.search
    );

    PageData.subloaderBalanceCuatrienio = false;

    return {
        props: {
            PageData, BalanceData, query
        }
    }
}

// Gets
const getComboYearInicio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboBalanceCuatrienioYearInicio().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.yearInicio })
            if (i.id === ID) {
                selected = { value: i.id, label: i.yearInicio };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos los años" })
        if (!selected)
            selected = combo[0];
    })

    return { DSYearInicio: combo, SelectedYearInicio: selected };
}


const getAllBalanceCuatrienio = async (idFilterActive, yearInicio, search, page, rows) => {
    let data = [];
    await ContenidoMultimediaDataService.getAllBalanceCuatrienio(idFilterActive, yearInicio, search, page, rows).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });
    return data;
};

const getTotalRecordsBalanceCuatrienio = async (idFilterActive, yearInicio, search) => {
    let totalRows = 0;
    await ContenidoMultimediaDataService.getTotalRecordsBalanceCuatrienio(idFilterActive, yearInicio, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
}

const Balance = ({ PageData = PageConst, BalanceData = BalanceConst, query }) => {
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [subloaderBalanceCuatrienio, setSubloaderBalanceCuatrienio] = useState(PageData.subloaderBalanceCuatrienio);
    const [listBalanceCuatrienio, setListBalanceCuatrienio] = useState(PageData.listBalanceCuatrienio)
    const [filterYearInicio, setFilterYearInicio] = useState(PageData.filterYearInicio);
    const [dataSelectYearInicio, setDataSelectYearInicio] = useState(PageData.dataSelectYearInicio);

    const [search, setSearch] = useState(query.search || PageData.listBalanceCuatrienio.search);
    const [rows, setRows] = useState(query.rows || PageData.listBalanceCuatrienio.rows);
    const [page, setPage] = useState(query.page || PageData.listBalanceCuatrienio.page);

    useEffect(async () => {
        if (query.year) {
            let { DSYearInicio, SelectedYearInicio } = await getComboYearInicio(Number(query.year));
            setDataSelectYearInicio(DSYearInicio); setFilterYearInicio(SelectedYearInicio);
        }
    }, []);

    // Handlers
    const handlerFilterYearInicio = async (selectYearInicio) => {
        setSubloaderBalanceCuatrienio(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('year', selectYearInicio.value, q)
        window.location = window.location.pathname + q

    }

    const handlerPaginationBalanceCuatrienio = async (page, rowsA, searchA = "") => {
        setSubloaderBalanceCuatrienio(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    // Others
    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
                <title>Balances de cuatrienio</title>
                <meta name="description" content="Aquí encontrarás todo lo relacionado a balances de cuatrienio y sus respectivos informes. Informate sobre el balance del cuatrienio actual" />
                <meta name="keywords" content="balance, cuatrienio, congresista, Congreso Colombia, Colombia, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Balances de cuatrienio" />
                <meta property="og:description" content="Aquí encontrarás todo lo relacionado a balances de cuatrienio y sus respectivos informes. Informate sobre el balance del cuatrienio actual" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/balance`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/balance`} />
                <meta name="twitter:title" content="Balances de cuatrienio"/>
                <meta name="twitter:description" content="Aquí encontrarás todo lo relacionado a balances de cuatrienio y sus respectivos informes. Informate sobre el balance del cuatrienio actual" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/balance/"/>
            </Head>
            <div className="pageTitle">
                <h1>Balances de cuatrienio</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                    <ul>
                        <li>
                            <h2>
                                <a href={"/contenido-multimedia"}>
                                    <i className="fas fa-photo-video"></i>
                                    Multimedia
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/articulo"}>
                                    <i className="fas fa-comment-dots"></i>
                                    Artículos
                                </a>
                            </h2>
                        </li>
                        <li>

                            <h2>
                                <a href={"/agora"}>
                                    <i className="fas fa-comments"></i>
                                    Opinión de congresistas
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/podcast"}>
                                    <i className="fas fa-microphone-alt"></i> Podcast
                                </a>
                            </h2>
                        </li>
                        <li className="active">
                            <h2>
                                <a href={"/balance"}>
                                    <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/informe-territorial"}>
                                    <i className="fas fa-file-alt"></i> Informes regionales
                                </a>
                            </h2>
                        </li>
                    </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="relative active">
                            <div className={`subloader ${subloaderBalanceCuatrienio ? "active" : ""}`}></div>
                            <div className="buscador pd-25">
                                <div className="input-group">
                                    <input type="text"
                                        value={search}
                                        onChange={async (e) => {
                                            setSearch(e.target.value)
                                        }}
                                        onKeyUp={async (e) => {
                                            if (e.key === "Enter") {
                                                await handlerPaginationBalanceCuatrienio(listBalanceCuatrienio.page, listBalanceCuatrienio.rows, e.target.value);
                                            }
                                        }}
                                        placeholder="Escriba para buscar" className="form-control" />

                                    <span className="input-group-text"><button onClick={async () => {
                                        await handlerPaginationBalanceCuatrienio(listBalanceCuatrienio.page, listBalanceCuatrienio.rows, listBalanceCuatrienio.search)
                                    }} type="button" className="btn btn-primary"><i
                                        className="fa fa-search"></i></button></span>
                                    <span className="input-group-text"><button onClick={async (e) => {
                                        toggleFilter(e.currentTarget);
                                    }} type="button" className="btn btn-primary"><i
                                        className="fa fa-filter"></i></button></span>
                                </div>
                                <div className="floatingFilters evenColors">
                                    <div className="one-columns relative no-margin">
                                        <div
                                            className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                        <div className="item">
                                            <label htmlFor="">Filtrar por año de inicio</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectYearInicio.length <= 1) {
                                                        let { DSYearInicio } = await getComboYearInicio();
                                                        setDataSelectYearInicio(DSYearInicio)
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterYearInicio}
                                                selectOnchange={handlerFilterYearInicio}
                                                selectoptions={dataSelectYearInicio}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError="">
                                            </Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ContenidoMultimediaList
                                propiedades={listBalanceCuatrienio.propiedades}
                                data={listBalanceCuatrienio.data}
                                handlerPagination={handlerPaginationBalanceCuatrienio}
                                defaultImage={Constantes.NoImagenPicture}
                                link="/balance" params={["titulo", "id"]}
                                pageExtends={page}
                                totalRows={listBalanceCuatrienio.totalRows}
                                pageSize={rows}
                                pathImgOrigen={auth.pathApi()}
                                className="pd-25"
                            />
                            <CIDPagination totalPages={listBalanceCuatrienio.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/balance"} hrefQuery={{
                                year: query.year || filterYearInicio.value
                            }} />
                        </div>
                        {/* End PodCast */}
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </div>
    );
}

export default Balance;
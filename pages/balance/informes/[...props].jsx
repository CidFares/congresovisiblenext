import { useState } from "react";
import dynamic from 'next/dynamic';
import Head from "next/head"
import 'suneditor/dist/css/suneditor.min.css';
import DetalleBalanceCuatrienioDataService from "../../../Services/ContenidoMultimedia/DetalleBalanceCuatrienio.Service";
import AuthLogin from "../../../Utils/AuthLogin";
import { Constantes, URLBase } from "../../../Constants/Constantes";

// Const 
const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const auth = new AuthLogin();

const PageConst = {
	subloader: true,
	loading: true,
};

const DetalleBalanceInformeConst = {
	data:[]
};

// SSR
export async function getServerSideProps({query}) {
	let PageData = PageConst;
	let DetalleBalanceInformeData = DetalleBalanceInformeConst;
	
	DetalleBalanceInformeData.data = await getByID(query.props[1]);
	PageData.subloader = false;
	PageData.loading = false;

	return {
		props:{
			PageData, DetalleBalanceInformeData
		}
	}
};

// Gets
const getByID = async (id) => {
	let data;
    await DetalleBalanceCuatrienioDataService.getInformeById(id).then(response => {
        data = response.data;
                    
    	}).catch(e => {
            this.setState({
                subloader: false
            });
            console.log(e);
        });

    return data;
}

// Page
const DetalleBalanceInforme = ({PageData = PageConst, DetalleBalanceInformeData = DetalleBalanceInformeConst}) => {
	// Const Data
	const [data, setData] = useState(DetalleBalanceInformeData.data);
	
	// Const Loader
	const [subloader, setSubloader] = useState(PageData.subloader);
	const [loading, setLoading] = useState(PageData.loading);

	// Others
	const estheticIn = () => {
	    let header = document.querySelector("header");
	    let mainP = document.querySelector(".mainPublicationContainer");
	    let pubSide = document.querySelector(".publicationsSide");
	    if (header)
	        header.classList.add("small")

	    if (mainP && pubSide) {
	        setTimeout(() => {
	            mainP.classList.add("active");
	            pubSide.classList.add("active");
	        }, 500);
	    }
	}

	const estheticOut = () => {
	    let header = document.querySelector("header");
	    let mainP = document.querySelector(".mainPublicationContainer");
	    let pubSide = document.querySelector(".publicationsSide");
	    if (header)
	        header.classList.remove("small")

	    if (mainP && pubSide) {
	        mainP.classList.remove("active");
	        pubSide.classList.remove("active");
	    }
	}

	return(
		<div>
			<Head>
                <title>{data.titulo}</title>
				<meta name="description" content={auth.filterStringHTML(data.resumen).slice(0, 165)} />
                <meta name="keywords" content="balance, informe, cuatrienio, congresista, Congreso Colombia, Colombia, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={data.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(data.resumen).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/balance/informes/${auth.filterStringForURL(data.titulo)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/balance/informes/${auth.filterStringForURL(data.titulo)}/${data.id}`} />
                <meta name="twitter:title" content={data.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(data.resumen).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/balance/informes/${auth.filterStringForURL(data.titulo)}/${data.id}/`}/>
            </Head>
			<section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${!subloader ? (typeof data.imagen[3] !== undefined ? auth.pathApi() + data.imagen[3]?.imagen : Constantes.NoImagenPicture) : ""}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon littleIcon"><i className="fas fa-file-alt"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{!subloader ? data.titulo : ""}</h1>
                    </div>
                </div>
            </section>
                
            <section className="nuestraDemocraciaSection">
                <div className="listadoPageContainer">
                	<div className="container">
                    	<div className={`subloader ${loading ? "active" : ""}`}></div>
                    	<div className="row">
	                        <div className="col-md-12">
		                        <div 
		                        	className="autor" 
			                        style={{
					                display: "flex",
					                justifyContent: "flex-start",
					                alignItems: "center",
					                margin: "15px 0"
					                }}
				                >
					                <div className="photo avatar" style={{marginRight: "13px"}}>
					                    <img src={!subloader ? (typeof data.equipo.equipo_imagen[0] !== "undefined" ? auth.pathApi() + data.equipo.equipo_imagen[0].imagen : Constantes.NoImagen) : ""} alt="CV" />
					                </div>
		                			{!subloader ? data.equipo.nombre : ""}
                				</div>
                        		<strong>Autores:</strong>
                        		<p>{!subloader ? data.autores : "Sin autores"}</p>
                        		<strong>Fecha de publicación:</strong>
                        		<p>{!subloader ? auth.coloquialDate(data.fechaPublicacion) : ""}</p>
                        		<strong>Fuente:</strong>
                        		<p>{!subloader ? data.fuente : "Sin fuente"}</p>
                        		<strong>Resumen:</strong>
								<div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: data.resumen || "Sin resumen" }}></div>
                        		<hr/>
								<strong>Contenido:</strong>
								<div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: data.textoPublicacion || "Sin contenido" }}></div>
                        	</div>
                    	</div>
               		</div>
            	</div>
            </section>
            <div style={{ clear: "both" }}></div>
		</div>
	);
};

export default DetalleBalanceInforme;
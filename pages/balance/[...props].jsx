import { useState, useEffect } from "react";
import Select from '../../Components/Select';
import PostInformesList from "../../Components/CongresoVisible/PostInformesList";
import Head from "next/head";
import DetalleBalanceCuatrienioDataService from "../../Services/ContenidoMultimedia/DetalleBalanceCuatrienio.Service";
import AuthLogin from "../../Utils/AuthLogin";
import { Constantes, URLBase, TypeCombos } from "../../Constants/Constantes.js";
import CIDPagination from "../../Components/CIDPagination";

// Const 
const auth = new AuthLogin();

const PageConst = {
    subloaderFilters: true,
    loading: true,
    subloaderInformes: true,
    search: "",
    listInformes: {
        data: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    filterEquipoCV: { value: -1, label: "Filtrar equipo CV" },
    filterTipoPublicacion: { value: -1, label: "Filtar tipo publicación" },
    filterConcepto: { value: -1, label: "Elegir concepto" },
    dataSelectEquipoCV: [],
    dataSelectTipoPublicacion: [],
    dataSelectConcepto: []
};

const DetalleBalanceCuatrienioConst = {
    data: {
        titulo: "",
        imagen: []
    }
};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let DetalleBalanceCuatrienioData = DetalleBalanceCuatrienioConst;
    DetalleBalanceCuatrienioData.data = await getByID(query.props[1]);
    PageData.listInformes.data = await getAllInformes(
        1
        , DetalleBalanceCuatrienioData.data.id
        , query.equipo || PageData.filterEquipoCV.value
        , query.tipoPublicacion || PageData.filterTipoPublicacion.value
        , query.concepto || PageData.filterConcepto.value
        , query.search || PageData.listInformes.search
        , query.page || PageData.listInformes.page
        , query.rows || PageData.listInformes.rows
    );
    PageData.listInformes.totalRows = await getTotalRecordsInformes(
        1
        , DetalleBalanceCuatrienioData.data.id
        , query.equipo || PageData.filterEquipoCV.value
        , query.tipoPublicacion || PageData.filterTipoPublicacion.value
        , query.concepto || PageData.filterConcepto.value
        , query.search || PageData.listInformes.search
    );
    PageData.loading = false;
    PageData.subloaderInformes = false;
    PageData.subloaderFilters = false;

    return {
        props: {
            PageData, DetalleBalanceCuatrienioData, query
        }
    }
};

// Gets
const getComboEquipoCVByType = async (ID = null) => {
    let combo = [];
    let selected = null;
    await DetalleBalanceCuatrienioDataService.getComboEquipoCVByType(TypeCombos.EquiposBalancesInformes).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos los equipos" })
        if (!selected)
            selected = combo[0];
    });

    return { DSEquipo: combo, SelectedEquipo: selected };
}

const getComboTipoPublicacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await DetalleBalanceCuatrienioDataService.getComboTipoPublicacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0];
    });

    return { DSTipoPublicacion: combo, SelectedPublicacion: selected };
}

const getComboGlosarioLegislativoByType = async (ID = null) => {
    let combo = [];
    let selected = null;
    await DetalleBalanceCuatrienioDataService.getComboGlosarioLegislativoByType(TypeCombos.ConceptosInformes).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.palabra })
            if (i.id === ID) {
                selected = { value: i.id, label: i.palabra };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0];
    });

    return { DSGlosario: combo, SelectedGlosario: selected };
}

const getByID = async (id) => {
    let data;

    await DetalleBalanceCuatrienioDataService.get(id).then(response => {
        data = response.data;
    }).catch(e => {
        console.log(e);
    });

    return data;
}

const getAllInformes = async (idFilterActive, id, equipo, publicacion, concepto, search, page, rows) => {
    let data = [];
    await DetalleBalanceCuatrienioDataService.getAllInformes(idFilterActive, id, equipo, publicacion, concepto, search, page, rows).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const getTotalRecordsInformes = async (idFilterActive, id, equipo, publicacion, concepto, search) => {
    let totalRows = 0;
    await DetalleBalanceCuatrienioDataService.getTotalRecordsInformes(idFilterActive, id, equipo, publicacion, concepto, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
}

// Page
const DetalleBalanceCuatrienio = ({ PageData = PageConst, DetalleBalanceCuatrienioData = DetalleBalanceCuatrienioConst, query }) => {
    //Const
    const [data, setData] = useState(DetalleBalanceCuatrienioData.data);
    const [listInformes, setListInformes] = useState(PageData.listInformes);

    // Const Loader
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [loading, setLoading] = useState(PageData.loading);
    const [subloaderInformes, setSubloaderInformes] = useState(PageData.subloaderInformes);

    //Const Selects
    const [filterEquipoCV, setFilterEquipoCV] = useState(PageData.filterEquipoCV);
    const [filterTipoPublicacion, setFilterTipoPublicacion] = useState(PageData.filterTipoPublicacion);
    const [filterConcepto, setFilterConcepto] = useState(PageData.filterConcepto);
    const [dataSelectEquipoCV, setDataSelectEquipoCV] = useState(PageData.dataSelectEquipoCV);
    const [dataSelectTipoPublicacion, setDataSelectTipoPublicacion] = useState(PageData.dataSelectTipoPublicacion);
    const [dataSelectConcepto, setDataSelectConcepto] = useState(PageData.dataSelectConcepto);

    const [search, setSearch] = useState(query.search || PageData.listInformes.search);
    const [rows, setRows] = useState(query.rows || PageData.listInformes.rows);
    const [page, setPage] = useState(query.page || PageData.listInformes.page);

    useEffect(async () => {
        if (query.equipo) {
            let { DSEquipo, SelectedEquipo } = await getComboEquipoCVByType(Number(query.equipo));
            setDataSelectEquipoCV(DSEquipo); setFilterEquipoCV(SelectedEquipo);
        }
        if (query.tipoPublicacion) {
            let { DSTipoPublicacion, SelectedPublicacion } = await getComboTipoPublicacion(Number(query.tipoPublicacion));
            setDataSelectTipoPublicacion(DSTipoPublicacion); setFilterTipoPublicacion(SelectedPublicacion)
        }
        if (query.concepto) {
            let { DSGlosario, SelectedGlosario } = await getComboGlosarioLegislativoByType(Number(query.concepto));
            setDataSelectConcepto(DSGlosario); setFilterConcepto(SelectedGlosario);
        }
    }, []);
    // Handlers
    const handlerClickTag = async (tag) => {
        setSubloaderInformes(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('concepto', tag.id, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterConcepto = async (selectConcepto) => {
        setSubloaderInformes(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('concepto', selectConcepto.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterTipoPublicacion = async (selectTipoPublicacion) => {
        setSubloaderInformes(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoPublicacion', selectTipoPublicacion.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterEquipoCV = async (selectEquipoCV) => {
        setSubloaderInformes(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('equipo', selectEquipoCV.value, q)
        window.location = window.location.pathname + q
    }

    const handlerPaginationInformes = async (page, rowsA, searchA = "") => {
        setSubloaderInformes(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
                <title>{data.titulo}</title>
                <meta name="description" content={auth.filterStringHTML(data.titulo).slice(0, 165)} />
                <meta name="keywords" content="balance, cuatrienio, congresista, Congreso Colombia, Colombia, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={data.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(data.titulo).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/balance/${auth.filterStringForURL(data.titulo)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/balance/${auth.filterStringForURL(data.titulo)}/${data.id}`} />
                <meta name="twitter:title" content={data.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(data.titulo).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/balance/${auth.filterStringForURL(data.titulo)}/${data.id}/`}/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${typeof data.imagen !== "undefined" ? auth.pathApi() + data.imagen[2]?.imagen : ""}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon littleIcon"><i className="fas fa-file-alt"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{!loading ? data.titulo : ""}</h1>
                    </div>
                </div>
            </section>
            <section className="nuestraDemocraciaSection">
                <div className="publicationsSide ">
                    <div className="CMTitle">
                        <h2><i className="fas fa-file-alt"></i> Informes de balance</h2>
                    </div>
                    <div className="relative">
                        <div className={`subloader ${subloaderInformes ? "active" : ""}`}></div>
                        <div className="buscador pd-25">
                            <div className="input-group">
                                <input type="text"
                                    value={search}
                                    onChange={async (e) => {
                                        setSearch(e.target.value)
                                    }}
                                    onKeyUp={async (e) => {
                                        if (e.key === "Enter") {
                                            await handlerPaginationInformes(listInformes.page, listInformes.rows, e.target.value)
                                        }
                                    }}
                                    placeholder="Escriba para buscar" className="form-control" />

                                <span className="input-group-text">
                                    <button
                                        onClick={async () => {
                                            await handlerPaginationInformes(listInformes.page, listInformes.rows, listInformes.search);
                                        }}
                                        type="button"
                                        className="btn btn-primary"
                                    ><i className="fa fa-search"></i>
                                    </button>
                                </span>
                                <span className="input-group-text">
                                    <button
                                        onClick={async (e) => {
                                            toggleFilter(e.currentTarget);
                                        }}
                                        type="button"
                                        className="btn btn-primary"
                                    ><i className="fa fa-filter"></i>
                                    </button>
                                </span>
                            </div>
                            <div className="floatingFilters evenColors">
                                <div className="one-columns relative no-margin">
                                    <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                    <div className="item">
                                        <label htmlFor="">Filtrar por equipo CV</label>
                                        <Select
                                            handlerOnClick={async () => {
                                                if (dataSelectEquipoCV.length <= 1) {
                                                    let { DSEquipo } = await getComboEquipoCVByType();
                                                    setDataSelectEquipoCV(DSEquipo)
                                                }
                                            }}
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterEquipoCV}
                                            selectOnchange={handlerFilterEquipoCV}
                                            selectoptions={dataSelectEquipoCV}
                                            selectIsSearchable={true}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass=""
                                            spanError="" >
                                        </Select>
                                    </div>
                                    <div className="item">
                                        <label htmlFor="">Filtrar tipo publicación</label>
                                        <Select
                                            handlerOnClick={async () => {
                                                if (dataSelectTipoPublicacion.length <= 1) {
                                                    let { DSTipoPublicacion } = await getComboTipoPublicacion();
                                                    setDataSelectTipoPublicacion(DSTipoPublicacion)
                                                }
                                            }}
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterTipoPublicacion}
                                            selectOnchange={handlerFilterTipoPublicacion}
                                            selectoptions={dataSelectTipoPublicacion}
                                            selectIsSearchable={true}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass=""
                                            spanError="" >
                                        </Select>
                                    </div>
                                    <div className="item">
                                        <label htmlFor="">Filtrar por concepto</label>
                                        <Select
                                            handlerOnClick={async () => {
                                                if (dataSelectConcepto.length <= 1) {
                                                    let { DSGlosario } = await getComboGlosarioLegislativoByType();
                                                    setDataSelectConcepto(DSGlosario)
                                                }
                                            }}
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterConcepto}
                                            selectOnchange={handlerFilterConcepto}
                                            selectoptions={dataSelectConcepto}
                                            selectIsSearchable={true}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass=""
                                            spanError="" >
                                        </Select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <PostInformesList
                            handlerClickTag={handlerClickTag}
                            data={typeof listInformes.data !== "undefined" ? listInformes.data : []}
                            defaultImage={Constantes.NoImagen}
                            pageExtends={page}
                            pageSize={rows}
                            totalRows={listInformes.totalRows}
                            pathImgOrigen={auth.pathApi()}
                            handler={handlerPaginationInformes}
                        />
                        <CIDPagination totalPages={listInformes.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={`/balance/${auth.filterStringForURL(data.titulo)}/${data.id}`} hrefQuery={{
                            equipo: query.equipo || filterEquipoCV.value,
                            tipoPublicacion: query.tipoPublicacion || filterTipoPublicacion.value,
                            concepto: query.concepto || filterConcepto.value
                        }} />
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </div>
    );
};

export default DetalleBalanceCuatrienio;
import { useState, useEffect } from "react";
import Head from "next/head"
import dynamic from 'next/dynamic';
import Select from '../../Components/Select';
import CIDPagination from "../../Components/CIDPagination";
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import 'suneditor/dist/css/suneditor.min.css'
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase, TypeCombos } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";

// Const
const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const auth = new AuthLogin();

const PageConst = {
    subloaderFilters: true,
    subloaderMultimedia: true,
    listMultimedia: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "titulo", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Fecha de publicación", text: "fechaPublicacion", esImg: false, img: "", putOnFirstElement: false }
                ],
            generalActionTitle: null,
            generalIcon: null,
            eachActionTitle: "tipo_multimedia.nombre",
            eachIcon: "tipo_multimedia.icono"
        },
        esModal: true,
        targetModal: "#modal-multimedia",
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    // Multimedia
    multimediaSelected: {},
    filterTipoMultimedia: { value: -1, label: "Ver todos" },
    dataSelectTipoMultimedia: [],
    // Modal
    subloaderModal: true
};

const ContenidoMultimediaConst = {

};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let ContenidoMultimediaData = ContenidoMultimediaConst;

    PageData.listMultimedia.data = await getAllMultimedia(1, query.tipoMultimedia || PageData.filterTipoMultimedia.value, query.search || PageData.listMultimedia.search, query.page || PageData.listMultimedia.page, query.rows || PageData.listMultimedia.rows);
    PageData.listMultimedia.totalRows = await getTotalRecordsMultimedia(1, query.tipoMultimedia || PageData.filterTipoMultimedia.value, query.search || PageData.listMultimedia.search);
    PageData.subloaderFilters = false;
    PageData.subloaderMultimedia = false;
    PageData.subloaderModal = false;

    return {
        props: {
            PageData, ContenidoMultimediaData, query
        }
    }
}

// Gets
const getComboTipoMultimedia = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboTipoMultimedia().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0];
    })

    return { DSTipoMultimedia: combo, SelectedMultimedia: selected };
}

const getAllMultimedia = async (idFilterActive, tipopublicacion, search, page, rows) => {
    let data = [];
    await ContenidoMultimediaDataService.getAllMultimedia(idFilterActive, tipopublicacion, search, page, rows).then((response) => {
        data = response.data;
        data.forEach(x => {
            x.fechaPublicacion = auth.coloquialDate(x.fechaPublicacion);
        })
    })
        .catch((e) => {
            console.error(e);
        });
    return data;
};

const getTotalRecordsMultimedia = async (idFilterActive, tipopublicacion, search) => {
    let totalRows = 0;
    await ContenidoMultimediaDataService.getTotalRecordsMultimedia(idFilterActive, tipopublicacion, search).then((response) => {
        totalRows = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    return totalRows;
};

// Page
const ContenidoMultimedia = ({ PageData = PageConst, ContenidoMultimediaData = ContenidoMultimediaConst, query }) => {
    // Const
    const [subloaderModal, setSubloaderModal] = useState(PageData.subloaderModal);
    const [subloaderMultimedia, setSubloaderMultimedia] = useState(PageData.subloaderMultimedia);
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [listMultimedia, setListMultimedia] = useState(PageData.listMultimedia);
    const [filterTipoMultimedia, setFilterTipoMultimedia] = useState(PageData.filterTipoMultimedia);
    const [dataSelectTipoMultimedia, setDataSelectTipoMultimedia] = useState(PageData.dataSelectTipoMultimedia);
    const [multimediaSelected, setMultimediaSelected] = useState(PageData.multimediaSelected);
    const [search, setSearch] = useState(query.search || PageData.listMultimedia.search);
    const [rows, setRows] = useState(query.rows || PageData.listMultimedia.rows);
    const [page, setPage] = useState(query.page || PageData.listMultimedia.page);

    useEffect(async () => {
        if (query.tipoMultimedia) {
            let { DSTipoMultimedia, SelectedMultimedia } = await getComboTipoMultimedia(Number(query.tipoMultimedia));
            setDataSelectTipoMultimedia(DSTipoMultimedia); setFilterTipoMultimedia(SelectedMultimedia);
        }
    }, []);

    // Handlers
    const handlerForLoadModalMultimedia = async (item) => {
        setSubloaderModal(true);
        setMultimediaSelected(item.data);
        setSubloaderModal(false);
    }

    const handlerFilterTipoMultimedia = async (selectTipoMultimedia) => {
        setSubloaderMultimedia(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoMultimedia', selectTipoMultimedia.value, q)
        window.location = window.location.pathname + q
    }

    const handlerPaginationMultimedia = async (page, rowsA, searchA = "") => {
        setSubloaderMultimedia(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    // Other
    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
                <title>Contenido multimedia</title>
                <meta name="description" content="Archivos multimedia, documentos y más relacionado con el congreso" />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Contenido multimedia" />
                <meta property="og:description" content="Archivos multimedia, documentos y más relacionado con el congreso" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/contenido-multimedia`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/contenido-multimedia`} />
                <meta name="twitter:title" content="Contenido multimedia" />
                <meta name="twitter:description" content="Archivos multimedia, documentos y más relacionado con el congreso" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/contenido-multimedia/" />
            </Head>
            <div className="pageTitle">
                <h1>Contenido multimedia</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                    <ul>
                        <li className="active">
                            <h2>
                                <a href={"/contenido-multimedia"}>
                                    <i className="fas fa-photo-video"></i>
                                    Multimedia
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/articulo"}>
                                    <i className="fas fa-comment-dots"></i>
                                    Artículos
                                </a>
                            </h2>
                        </li>
                        <li>

                            <h2>
                                <a href={"/agora"}>
                                    <i className="fas fa-comments"></i>
                                    Opinión de congresistas
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/podcast"}>
                                    <i className="fas fa-microphone-alt"></i> Podcast
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/balance"}>
                                    <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/informe-territorial"}>
                                    <i className="fas fa-file-alt"></i> Informes regionales
                                </a>
                            </h2>
                        </li>
                    </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active">
                            <div className="relative">
                                <div className={`subloader ${subloaderMultimedia ? "active" : ""}`}></div>
                                <div className="buscador pd-25">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            value={search}
                                            onChange={async (e) => {
                                                setSearch(e.target.value)
                                            }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerPaginationMultimedia(listMultimedia.page, listMultimedia.rows, e.target.value);
                                                }
                                            }}
                                            placeholder="Escriba para buscar" className="form-control" />

                                        <span className="input-group-text">
                                            <button
                                                onClick={async () => { await handlerPaginationMultimedia(listMultimedia.page, listMultimedia.rows, listMultimedia.search) }}
                                                type="button"
                                                className="btn btn-primary"
                                            >
                                                <i className="fa fa-search"></i>
                                            </button>
                                        </span>
                                        <span className="input-group-text">
                                            <button
                                                onClick={
                                                    async (e) => {
                                                        toggleFilter(e.currentTarget);
                                                    }
                                                }
                                                type="button"
                                                className="btn btn-primary"
                                            >
                                                <i className="fa fa-filter"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <div className="floatingFilters evenColors">
                                        <div className="one-columns relative no-margin">
                                            <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                            <div className="item">
                                                <label htmlFor="">Filtrar tipo multimedia</label>
                                                <Select
                                                    handlerOnClick={async () => {
                                                        if (dataSelectTipoMultimedia.length <= 1) {
                                                            let { DSTipoMultimedia } = await getComboTipoMultimedia();
                                                            setDataSelectTipoMultimedia(DSTipoMultimedia)
                                                        }
                                                    }}
                                                    divClass=""
                                                    selectplaceholder="Seleccione"
                                                    selectValue={filterTipoMultimedia}
                                                    selectoptions={dataSelectTipoMultimedia}
                                                    selectOnchange={handlerFilterTipoMultimedia}
                                                    selectIsSearchable={true}
                                                    selectclassNamePrefix="selectReact__value-container"
                                                    spanClass=""
                                                    spanError="" >
                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ContenidoMultimediaList
                                    propiedades={listMultimedia.propiedades}
                                    data={listMultimedia.data}
                                    handlerPagination={handlerPaginationMultimedia}
                                    defaultImage={Constantes.NoImagenPicture}
                                    link="/#" params={["id"]}
                                    pageExtends={page}
                                    totalRows={listMultimedia.totalRows}
                                    pageSize={rows}
                                    pathImgOrigen={auth.pathApi()}
                                    className="pd-25"
                                    esModal={listMultimedia.esModal}
                                    targetModal={listMultimedia.targetModal}
                                    handlerForLoadModal={handlerForLoadModalMultimedia}
                                />
                                <CIDPagination totalPages={listMultimedia.totalRows} totalRows={query.rows || listMultimedia.rows} searchBy={query.search || listMultimedia.search} currentPage={query.page || listMultimedia.page} hrefPath={"/contenido-multimedia"} hrefQuery={{
                                    tipoMultimedia: query.tipoMultimedia || filterTipoMultimedia.value
                                }} />
                            </div>
                            {/* End Multimedia */}
                        </div>
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>

            <div className="modal fade" id="modal-multimedia" aria-labelledby="modal-multimedia" aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="estudios">
                                <i className="fas fa-photo-video"></i> {`${multimediaSelected.titulo || ''}`}
                            </h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className={`subloader ${subloaderModal ? "active" : ""}`}></div>
                            <div className="container">
                                {
                                    multimediaSelected.tipo_multimedia_id === 2
                                        ? <audio src={auth.pathApi() + multimediaSelected.multimedia_archivo[0].archivo} controls={true} style={{ width: 730 }}></audio>
                                        : multimediaSelected.tipo_multimedia_id === 3
                                            ? <video src={auth.pathApi() + multimediaSelected.multimedia_archivo[0].archivo} controls ></video>
                                            : multimediaSelected.tipo_multimedia_id === 1
                                                ? <a rel="noreferrer" href={auth.pathApi() + multimediaSelected.multimedia_archivo[0].archivo} className="btn btn-primary center-block" target="_blank"><i className="fas fa-paperclip"></i> Ir documento</a>
                                                : multimediaSelected.tipo_multimedia_id === 4
                                                    ? <a rel="noreferrer" href={multimediaSelected.multimedia_archivo[0].urlVideo} className="btn btn-primary center-block" target="_blank"><i className="fas fa-link"></i> Ir a Video</a>
                                                    : multimediaSelected.tipo_multimedia_id === 5
                                                        ? <a rel="noreferrer" href={multimediaSelected.multimedia_archivo[0].urlAudio} className="btn btn-primary center-block" target="_blank"><i className="fas fa-link"></i> Ir a Audio</a>
                                                        : ""
                                }
                                <br />
                                <p><strong>Fecha de publicación:</strong> {multimediaSelected.fechaPublicacion || ''}</p>
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: multimediaSelected.descripcion || "Sin resumen" }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContenidoMultimedia;
import Document, { Head, Html, Main, NextScript } from "next/document";
import { URLBase } from "../Constants/Constantes"

class MyDocument extends Document {
    render() {
        return (
            <Html lang="es">
                <Head>
                    <link rel="shortcut icon" href={`${URLBase}/favicon.png`} type="image/png" />
                    <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                    <meta name="google-site-verification" content="GDgcMRIRIpR5vZCufqRK7H82DR7vDWcdOqMfOtszd70" />
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-12403503-1"></script>
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments);}
                            gtag('js', new Date());
                          
                            gtag('config', 'UA-12403503-1');
                            `,
                        }}
                    />
                </Head>
                <body>
                    <Main></Main>
                    <NextScript />
                    {/* <script src={`/js/menu.js`} /> */}
                    <script src={`${URLBase}/js/bootstrap/bootstrap.min.js`} />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
import VotacionesDataService from "../../Services/Votaciones/Votaciones.Service";
import dynamic from "next/dynamic";
import 'suneditor/dist/css/suneditor.min.css';
import AuthLogin from "../../Utils/AuthLogin";
import { Constantes, URLBase } from "../../Constants/Constantes.js";
import Head from "next/head";
import SelectCurul from '../../Components/SelectCurul';
import InputPercent from "../../Components/InputPercent";
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import Link from 'next/link'
import { useState, useEffect } from "react";
const auth = new AuthLogin();
const DetalleConst = {
    id: 0,
    subloader: false,
    proyecto_de_ley: {
        id: 0,
        cuatrienio_id: 0,
        legislatura_id: 0,
        corporacion_id: 0,
        titulo: "",
        alias: "",
        fecha_radicacion: "",
        numero_camara: "",
        numero_senado: "",
        iniciativa_id: 0,
        tipo_proyecto_id: 0,
        tema_id_principal: 0,
        tema_id_secundario: null,
        sinopsis: "",
        se_acumula_a_id: null,
        alcance_id: 0,
        iniciativa_popular: null,
        activo: 0
    },
    esComision: 0,
    esPlenaria: 0,
    votacion_estado: {
        id: 0,
        votacion_id: 0,
        proyecto_ley_estado_id: 0,
        activo: 0,
        estado: {
            id: 0,
            proyecto_ley_id: 0,
            fecha: "",
            estado_proyecto_ley_id: 0,
            gaceta_texto: "",
            gaceta_url: null,
            nota: "",
            corporacion_id: null,
            observaciones: "",
            orden: null,
            activo: 1,
            tipo_estado: {
                id: 0,
                nombre: "",
                activo: 1
            }
        }
    },
    acta: null,
    fecha: null,
    cuatrienio: {
        id: 0,
        nombre: "",
        fechaInicio: 2018,
        fechaFin: 2022,
        activo: 1
    },
    cuatrienio_id: 0,
    legislatura: {
        id: 0,
        nombre: "",
        fechaInicio: "",
        fechaFin: "",
        cuatrienio_id: 0,
        activo: 0
    },
    tipo_votacion: {
        id: 0,
        nombre: "",
        procedimental: 0,
        descripcion: "",
        activo: 1,
    },
    clase_votacion: {
        id: 0,
        nombre: "",
        descripcion: "",
        activo: 1
    },
    votacion_plenaria: {
        id: 0,
        votacion_id: 0,
        corporacion_id: 0,
        activo: 0,
        corporacion: {
            id: 0,
            nombre: "",
            descripcion: "",
            activo: 1
        }
    },
    votacion_comision: null,
    motivo: null,
    observaciones: null,
    voto_general: 0,
    votosFavor: 0,
    votosAbstencion: 0,
    numero_asistencias: 0,
    aprobada: '',
    datos_importacion: null,
    tiposRespuestas: [
        { id: 1, nombre: "Sí"},
        { id: 2, nombre: "No"},
        { id: 3, nombre: "Ausente"}
    ],
    votacion_congresista: [],
    urlGaceta: null

}
const CurulDataConst = [];
export async function getServerSideProps({ query }) {
    let DetalleVotacion = DetalleConst;
    let CurulData = CurulDataConst;
    DetalleVotacion = await getByID(query.id);
    DetalleVotacion.tiposRespuestas = await getComboTipoRespuestaVotacion();
    if (DetalleVotacion.esPlenaria === 1)
        CurulData = await getCurulesInVotacion(query.id, DetalleVotacion.cuatrienio_id, DetalleVotacion.votacion_plenaria?.corporacion_id);
    return {
        props: { DetalleVotacion, CurulData }
    }
}
const getByID = async (id) => {
    let DetalleVotacion;
    await VotacionesDataService.get(id)
        .then(response => {
            DetalleVotacion = response.data;
        })
    return DetalleVotacion;
}
const getComboTipoRespuestaVotacion = async () => {
    let tiposRespuestas;
    await VotacionesDataService.getComboTipoRespuestaVotacion().then(response => {
        tiposRespuestas = response.data;
    })
    return tiposRespuestas;
}
const getCurulesInVotacion = async (idvotacion, cuatrienio, corporacion) => {
    let curulData = [];
    await VotacionesDataService.getCurulesInVotacion(idvotacion, cuatrienio, corporacion).then(response => {
        curulData = response.data;
    })
    return curulData;
}
export default function DetalleVotacion({ DetalleVotacion = DetalleConst, CurulData = CurulDataConst }) {
    const [subloader, setSubloader] = useState(DetalleConst.subloader);
    let countEstadoVotacion = DetalleVotacion.votacion_estado.estado.tipo_estado.nombre.length;
    let href = auth.filterStringForURL(`p${DetalleVotacion.proyecto_de_ley.titulo}`)
    return (
        <>
            <Head>
                <title>Votaciones | {DetalleVotacion.proyecto_de_ley.titulo}</title>
                <meta name="description" content={`${DetalleVotacion.votacion_estado.estado.tipo_estado.nombre}. ${auth.filterStringHTML(DetalleVotacion.proyecto_de_ley.sinopsis).slice(0, 160 - countEstadoVotacion)}`} />
                <meta name="keywords" content="votacion, cuatrienio, congresista, Congreso Colombia, Colombia, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`Votaciones | ${DetalleVotacion.proyecto_de_ley.titulo}`} />
                <meta property="og:description" content={`${DetalleVotacion.votacion_estado.estado.tipo_estado.nombre}. ${auth.filterStringHTML(DetalleVotacion.proyecto_de_ley.sinopsis).slice(0, 160 - countEstadoVotacion)}`} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/votaciones/${DetalleVotacion.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/votaciones/${DetalleVotacion.id}`} />
                <meta name="twitter:title" content={`Votaciones | ${DetalleVotacion.proyecto_de_ley.titulo}`}/>
                <meta name="twitter:description" content={`${DetalleVotacion.votacion_estado.estado.tipo_estado.nombre}. ${auth.filterStringHTML(DetalleVotacion.proyecto_de_ley.sinopsis).slice(0, 160 - countEstadoVotacion)}`} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/votaciones/${DetalleVotacion.id}/`}/>
            </Head>
            <div className="topPanel">
                <div className="pageTitle">
                    <h1>Detalles de la votación</h1>
                </div>
            </div>
            <div className="listadoPageContainer">
                <div className="container">
                    {
                        !subloader ?
                            <div className="row">
                                <div className="col-md-12">
                                    <h2>Proyecto:{" "}
                                        <a className="decoration" href={`/proyectos-de-ley/${href}/${DetalleVotacion.proyecto_de_ley.id}`}>
                                            {DetalleVotacion.proyecto_de_ley?.titulo || "Sin título"}
                                        </a>
                                    </h2>
                                </div>
                                <div className="col-md-12">
                                    <strong>
                                        {
                                            DetalleVotacion.esComision
                                                ? "Votación en comisión"
                                                : DetalleVotacion.esPlenaria
                                                    ? 'Votación en plenaria'
                                                    : ''
                                        } </strong>
                                </div>
                                <div className="col-md-12">
                                    <strong>Estado del proyecto: </strong>
                                    <p>{DetalleVotacion.votacion_estado?.estado?.tipo_estado?.nombre || "Sin estado"}</p>
                                </div>
                                <div className="col-md-12">
                                    <strong>Síntesis:</strong>
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: DetalleVotacion.proyecto_de_ley?.sinopsis || 'Sin resumen' }}></div>
                                </div>
                                <div className="col-md-6">
                                    <strong>Acta: </strong>
                                    <p>{DetalleVotacion.acta || "Sin acta"}</p>
                                </div>
                                <div className="col-md-6">
                                    <strong>Fecha: </strong>
                                    <p>{auth.coloquialDate(DetalleVotacion.fecha) || "S/F"}</p>
                                </div>
                                <div className="col-md-6">
                                    <strong>Cuatrienio: </strong>
                                    <p>{DetalleVotacion.cuatrienio.nombre || "Sin cuatrienio"}</p>
                                </div>
                                <div className="col-md-6">
                                    <strong>Legislatura: </strong>
                                    <p>{DetalleVotacion.legislatura.nombre || "Sin legislatura"}</p>
                                </div>
                                <div className="col-md-6">
                                    <strong>Tipo de iniciativa: </strong>
                                    <p>{DetalleVotacion.tipo_votacion.nombre || "Sin tipo"}</p>
                                </div>
                                <div className="col-md-6">
                                    <strong>Clase de votación: </strong>
                                    <p>{DetalleVotacion.clase_votacion.nombre || "Sin clase"}</p>
                                </div>
                                {
                                    DetalleVotacion.esPlenaria === 1 ?
                                        <div className="col-md-6">
                                            <strong>Corporación: </strong>
                                            <p>Plenaria de {DetalleVotacion.votacion_plenaria?.corporacion.nombre || "Sin corporacion"}</p>
                                        </div>
                                        :
                                        <div className="col-md-6">
                                            <strong>Corporación: </strong>
                                            <p>Votación dada en {DetalleVotacion.votacion_comision?.corporacion.nombre || "Sin corporación"}</p>
                                        </div>
                                }
                                {
                                    DetalleVotacion.esComision === 1 ?
                                        <div className="col-md-6">
                                            <strong>Comisión: </strong>
                                            <p>{DetalleVotacion.votacion_comision?.comision.nombre || "Sin comisión"}</p>
                                        </div> : ""
                                }
                                <div className="col-md-6">
                                    <strong>Motivo de la votación: </strong>
                                    <p>{DetalleVotacion.motivo || "Sin motivo"}</p>
                                </div>
                                <div className="col-md-12">
                                    <strong>Observaciones:</strong>
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: DetalleVotacion.observaciones || 'Sin observaciones' }}></div>
                                </div>
                            </div>
                            : ""
                    }
                    <hr />
                    {
                        !subloader ?
                            DetalleVotacion.voto_general === 1 ?
                                <>
                                    <div className="row datosVotaciones">
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">Si: {DetalleVotacion.votosFavor}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">No: {DetalleVotacion.votosContra}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">Abstención: {DetalleVotacion.votosAbstencion}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">Ausentes: {DetalleVotacion.numero_no_asistencias}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">Asistencias: {DetalleVotacion.numero_asistencias}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 ">
                                            <div className="dato">
                                                <InputPercent value={100} noShowValue={true} color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                                <div className="desc">
                                                    <p className="text-center">Aprobada: {DetalleVotacion.aprobada}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <strong>Resultado de la votación:</strong>
                                            <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: DetalleVotacion.datos_importacion || 'Sin información' }}></div>
                                        </div>
                                        {/* <div className="col-md-12">
                                                <strong>Resultados:</strong>
                                                <SunEditor
                                                    disable={true}
                                                    enableToolbar={true}
                                                    showToolbar={false}
                                                    width="100%"
                                                    height="100%"
                                                    setOptions={{ resizingBar: false, showPathLabel: false, shortcutsDisable: true }}
                                                    setContents={this.state.data.resultados_importacion || 'Sin resultados'}
                                                />
                                            </div> */}
                                    </div>
                                    <hr />
                                </>
                                : <div className="row datosVotaciones">
                                    {
                                        !subloader && DetalleVotacion.tiposRespuestas !== null && DetalleVotacion.tiposRespuestas.length !== 0 && typeof DetalleVotacion.tiposRespuestas !== 'undefined' ?
                                            DetalleVotacion.tiposRespuestas.map((res, y) => {
                                                let conteo = DetalleVotacion.votacion_congresista.filter((v) => { return v.tipo_respuesta_votacion_id === res.id }).length;
                                                let total = DetalleVotacion.votacion_congresista.length;
                                                let color = getColor(res.id);
                                                let porcentaje = conteo === 0 ? 0 : ((conteo / total) * 100).toFixed(2);
                                                return (
                                                    <div key={y} className="col-md-4 ">
                                                        <div className="dato">
                                                            <InputPercent value={porcentaje} color={color} fontColor="#000" fontSize="2.6" />
                                                            <div className="desc">
                                                                <p className="text-center">{res.nombre}: {conteo} ({porcentaje}%)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                            : <p>No hay votaciones</p>
                                    }
                                </div>
                            : ""
                    }
                    <div className="row">
                        <div className="col-md-12">
                            {
                                !subloader && DetalleVotacion.urlGaceta !== null ?
                                    <a target="_blank" without rel="noreferrer" className="btn btn-primary center-block" href={DetalleVotacion.urlGaceta !== "" ? auth.pathApi() + DetalleVotacion.urlGaceta : "/"}>
                                        <i className="fas fa-download"></i> Descargar gaceta
                                    </a> :
                                    <button type="button" className="btn btn-primary center-block">Sin gaceta</button>
                            }
                        </div>
                    </div>
                    <hr />
                    {
                        !subloader ?
                            DetalleVotacion.voto_general === 0 ?
                                <>
                                    <div className="centerTabs with-li">
                                        <ul>
                                            { !subloader ? (DetalleVotacion.esPlenaria === 1 ? <li onClick={(e) => { changeTab(e) }} className="active" data-ref="1">Curules</li> : "") : ""}
                                            <li onClick={(e) => { changeTab(e) }} className={!subloader ? (DetalleVotacion.esComision === 1 ? "active" : "") : ""} data-ref="2">Lista</li>
                                        </ul>
                                    </div>
                                    <div className="contentForCenterTabs">
                                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                                        {
                                            DetalleVotacion.esPlenaria === 1 ?
                                                <div className="contentTab active" data-ref="1">
                                                    <h3 className="text-center tabsTitle">{!subloader ? DetalleVotacion.votacion_plenaria.corporacion.nombre : ""}</h3>
                                                    <SelectCurul corporacion={!subloader ? DetalleVotacion.votacion_plenaria.corporacion_id : ""} esVotacion={true} linkToClickCurul={"/perfil-congresista"} data={CurulData} curules={CurulData} curulSelectable={false} origen={auth.pathApi()} />
                                                </div>
                                                : ""
                                        }
                                        <div className={`contentTab ${DetalleVotacion.esComision === 1 ? "active" : ""}`} data-ref="2">
                                            <div className="container-fluid">
                                                <div className="row">
                                                    <div className="col-lg-12 col-md-12">
                                                        <div className="listadoPageContainer">
                                                            <div className="container-fluid">
                                                                <div className="listadoWPhotoContainer three-columns margin-initial">
                                                                    {
                                                                        !subloader && typeof DetalleVotacion.votacion_congresista !== "undefined" && DetalleVotacion.votacion_congresista !== null ?
                                                                            DetalleVotacion.votacion_congresista.map((x, i) => {
                                                                                let href = auth.filterStringForURL(`${x.congresista.persona_votaciones.nombres} ${x.congresista.persona_votaciones.apellidos}`);
                                                                                return (
                                                                                    <div key={i} className="listadoItem type-2">
                                                                                        <div className="itemBody">
                                                                                            <div className="itemSection with-photo content-sm">
                                                                                                <div className="headerPhoto">
                                                                                                    <img src={x.congresista !== null ? (typeof x.congresista.persona_votaciones.imagenes[1] !== "undefined" ? auth.pathApi() + x.congresista.persona_votaciones.imagenes[1].imagen : Constantes.NoImagen) : Constantes.NoImagen} alt="" />
                                                                                                </div>
                                                                                                <h4><a href={`/congresistas/perfil/${href}/${x.congresista.persona_votaciones.id}`} without rel="noreferrer" target="_blank">{x.congresista.persona_votaciones.nombres || ""} {x.congresista.persona_votaciones.apellidos || ""}</a></h4>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="itemFooter flex-column">
                                                                                            <div className="with-pdb">
                                                                                                <p>{x.congresista.detalle_votaciones !== null ? x.congresista.detalle_votaciones.partido.nombre : "Sin partido asignado" || 'Sin partido asignado'}</p>
                                                                                                <div className="sub">
                                                                                                    <p>Partido</p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="with-pdb">
                                                                                                <p>
                                                                                                    {
                                                                                                        x.tipo_respuesta_votacion === null ? "---" :
                                                                                                            DetalleVotacion.tiposRespuestas.find((v) => { return v.id === x.tipo_respuesta_votacion_id }).nombre
                                                                                                    }
                                                                                                </p>
                                                                                                <div className="sub">
                                                                                                    <p>Voto</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                )
                                                                            })
                                                                            :
                                                                            <p>Sin congresistas involucrados</p>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                                : ""
                            : ""
                    }
                </div>
            </div>
        </>
    )

}
function changeTab(e) {
    let tabs = document.querySelectorAll(".centerTabs ul li");
    tabs.forEach(y => {
        y.classList.remove("active");
    })
    tabs.forEach(z => {
        if (document.querySelector(`.contentForCenterTabs .contentTab[data-ref="${z.getAttribute("data-ref")}"]`)) {
            document.querySelector(`.contentForCenterTabs .contentTab[data-ref="${z.getAttribute("data-ref")}"]`).classList.remove("active")
        }
    })
    e.currentTarget.classList.add("active");
    if (document.querySelector(`.contentForCenterTabs .contentTab[data-ref="${e.currentTarget.getAttribute("data-ref")}"]`)) {
        document.querySelector(`.contentForCenterTabs .contentTab[data-ref="${e.currentTarget.getAttribute("data-ref")}"]`).classList.add("active")
    }
}
function getColor(id) {
    if (id === 1)
        return "#00b963"
    else if (id === 2)
        return "#e44e44"
    else
        return "#c9c043"
}


import Head from 'next/head'
import Select from '../../Components/Select';
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
import { useState, useEffect } from "react";
import { Constantes,URLBase } from "../../Constants/Constantes.js";
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import infoSitioDataService from "../../Services/General/informacionSitio.Service";
import ActLegislativaVotacionesList from "../../Components/CongresoVisible/ActLegislativaVotacionesList";
import CIDPagination from "../../Components/CIDPagination";

const auth = new AuthLogin();
const PageConst = { imgPrincipal: null, subloader: false }
const VotacionesConst = {
    listVotaciones: {
        data: [],
        tiposRespuestas: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 8
    },
    filterCuatrienio: { value: -1, label: "Ver todos" },
    dataSelectCuatrienio: [],
    filterLegislatura: { value: -1, label: "Ver todas" },
    dataSelectLegislatura: [],
    filterTipoComision: { value: -1, label: "Elija tipo de comisión" },
    dataSelectTipoComision: [],
    filterComision: { value: -1, label: "Ver todas" },
    dataSelectComision: [],
    filterTipoCorporacion: { value: -1, label: "Ver cámara y senado" },
    dataSelectTipoCorporacion: [],
    filterTipoVotacion: { value: -1, label: "Ver todos" },
    dataSelectTipoVotacion: [
        { value: -1, label: "Ver todos" },
        { value: 1, label: "Comisión" },
        { value: 2, label: "Plenaria" },
    ]
}

export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let VotacionesData = VotacionesConst;
    await infoSitioDataService.getInformacionSitioHome()
        .then((response) => {
            PageData.imgPrincipal = response.data[0].imgPrincipal || Constantes.NoImagenPicture;
        })
        .catch((e) => {
            console.error(e);
        });

    VotacionesData.listVotaciones = await getAllVotaciones(
        1
        , query.corporacion || VotacionesConst.filterTipoCorporacion.value
        , query.legislatura || VotacionesConst.filterLegislatura.value
        , query.cuatrienio || VotacionesConst.filterCuatrienio.value
        , query.comision || VotacionesConst.filterComision.value
        , query.tipoVotacion || VotacionesConst.filterTipoVotacion.value
        , query.search || VotacionesConst.listVotaciones.search
        , query.page || VotacionesConst.listVotaciones.page
        , query.rows || VotacionesConst.listVotaciones.rows
    );
    VotacionesData.listVotaciones.tiposRespuestas = await getComboTipoRespuestaVotacion();
    return {
        props: { PageData, VotacionesData, query }
    }
}
const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCorporacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver Cámara y Senado" })
        if (!selected)
            selected = combo[0]
    })
    return { DSCorporacion: combo, SelectedCorporacion: selected };
}
const getComboCuatrienio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCuatrienio().then(response => {

        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]

    })
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
}
const getComboLegislatura = async (cuatrienio, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboLegislatura(cuatrienio).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todas" })
        if (!selected)
            selected = combo[0]
    })
    return { DSLegislatura: combo, SelectedLegislatura: selected };
}
const getComboTipoComision = async (idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboTipoComision(idCorporacion).then(response => {

        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Elija tipo de comisión" })
        if (!selected)
            selected = combo[0]
    })
    return { DSTipoComision: combo, SelectedTipoComision: selected };
}
const getComboComisiones = async (idTipoComision, idCorporacion, ID = null) => {
    console.log(idTipoComision, idCorporacion, ID)
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboComisiones(idTipoComision, idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todas" })
        if (!selected)
            selected = combo[0]
    })
    return { DSComision: combo, SelectedComision: selected };
}
const getComboTipoRespuestaVotacion = async () => {
    let tipoRespuesta = [];
    await ActividadesLegislativasDataService.getComboTipoRespuestaVotacion().then(response => {
        tipoRespuesta = response.data;
    })
    return tipoRespuesta;
}
const getAllVotaciones = async (idFilterActive, corporacion, legislatura, cuatrienio, comision, tipoVotacion, search, page, rows) => {
    let listVotaciones = VotacionesConst.listVotaciones;
    await ActividadesLegislativasDataService.getAllVotaciones(idFilterActive, corporacion, legislatura, cuatrienio, comision, tipoVotacion, search, page, rows).then((response) => {
        listVotaciones.data = response.data;
    })
        .catch((e) => {
            console.error(e);
        });
    await ActividadesLegislativasDataService.getTotalRecordsVotaciones(idFilterActive, corporacion, legislatura, cuatrienio, comision, tipoVotacion, search).then((response) => {
        listVotaciones.totalRows = response.data;
    })
        .catch((e) => {
            console.error(e);
        });
    return listVotaciones;
};


export default function Votaciones({ PageData = PageConst, VotacionesData = VotacionesConst, query }) {
    const [subloader, setSubloader] = useState(PageConst.subloader);
    const [filterCuatrienio, setFilterCuatrienio] = useState(VotacionesData.filterCuatrienio);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(VotacionesData.dataSelectCuatrienio);
    const [filterLegislatura, setFilterLegislatura] = useState(VotacionesData.filterLegislatura);
    const [dataSelectLegislatura, setDSLegislatura] = useState(VotacionesData.dataSelectLegislatura);
    const [filterTipoComision, setFilterTComision] = useState(VotacionesData.filterTipoComision);
    const [dataSelectTipoComision, setDSTComision] = useState(VotacionesData.dataSelectTipoComision);
    const [filterComision, setFilterComision] = useState(VotacionesData.filterComision);
    const [dataSelectComision, setDSComision] = useState(VotacionesData.dataSelectComision);
    const [filterTipoCorporacion, setFilterTipoCorporacion] = useState(VotacionesData.filterTipoCorporacion);
    const [dataSelectTipoCorporacion, setDSCorporacion] = useState(VotacionesData.dataSelectTipoCorporacion);
    const [filterTipoVotacion, setFilterTipoVotacion] = useState(VotacionesData.filterTipoVotacion);
    const [listVotaciones, setlistVotaciones] = useState(VotacionesData.listVotaciones);
    const [search, setSearch] = useState(query.search || VotacionesData.listVotaciones.search);
    const [rows, setRows] = useState(query.rows || VotacionesData.listVotaciones.rows);
    const [page, setPage] = useState(query.page || VotacionesData.listVotaciones.page);

    useEffect(async () => {
        if (query.corporacion) {
            let { DSCorporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
            setDSCorporacion(DSCorporacion); setFilterTipoCorporacion(SelectedCorporacion);
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(SelectedCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if (query.cuatrienio) {
            let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(Number(query.cuatrienio));
            setDSCuatrienio(DSCuatrienio); setFilterCuatrienio(SelectedCuatrienio);
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(SelectedCuatrienio.value);
            setDSLegislatura(DSLegislatura); setFilterLegislatura(SelectedLegislatura);
        }
        if (query.legislatura) {
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(Number(query.cuatrienio), Number(query.legislatura));
            setDSLegislatura(DSLegislatura); setFilterLegislatura(SelectedLegislatura);
        }
        if(query.tipoComision){
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(filterTipoCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if(query.comision){
            let { DSComision, SelectedComision } = await getComboComisiones(Number(query.tipoComision), Number(query.corporacion), Number(query.comision));
            setDSComision(DSComision); setFilterComision(SelectedComision);
        }
        if (query.tipoVotacion) {
            setFilterTipoVotacion(VotacionesData.dataSelectTipoVotacion.find((x) => { return x.value === Number(query.tipoVotacion) }))
        }
    }, []);


    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('cuatrienio', selectCuatrienio.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoVotacion = async (selectTipoVotacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoVotacion', selectTipoVotacion.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterLegislatura = async (selectLegislatura) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('legislatura', selectLegislatura.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoComision = async (selectTipoComision) => {
        setFilterTComision(selectTipoComision)
        setFilterComision(Object.assign({}, VotacionesConst.filterComision))
        setDSComision([]);
        let { DSComision } = await getComboComisiones(selectTipoComision.value, filterTipoCorporacion.value);
        setDSComision(DSComision);
    }
    const handlerFilterComision = async (selectComision) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoComision', filterTipoComision.value, q)
        q = auth.replaceQueryParam('comision', selectComision.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoCorporacion = async (selectTipoCorporacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', selectTipoCorporacion.value, q)
        window.location = window.location.pathname + q
    }
    const handlerPaginationVotaciones = async (page, rowsA, searchA = "") => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    return (
        <>
            <Head>
            <title>Votaciones | Actividad Legislativa | Congreso Visible</title>
            <meta name="description" content="Aquí podrás encontrar todas las votaciones que han sido efectuadas a lo largo de la historia del congreso" />
            <meta name="keywords" content="Órdenes del día, Agenda Legislativa, Proyectos de Ley, Citaciones, Comisiones, Citantes, Citados" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="Votaciones | Actividad Legislativa | Congreso Visible" />
            <meta property="og:description" content="Aquí podrás encontrar todas las votaciones que han sido efectuadas a lo largo de la historia del congreso" />
            <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
            <meta property="og:image:width" content="828" />
            <meta property="og:image:height" content="450" />
            <meta property="og:url" content={`${URLBase}/votaciones`} />
            <meta property="og:site_name" content="Congreso Visible" />
            <meta name="twitter:url" content={`${URLBase}/votaciones`} />
            <meta name="twitter:title" content="Votaciones | Actividad Legislativa | Congreso Visible"/>
            <meta name="twitter:description" content="Aquí podrás encontrar todas las votaciones que han sido efectuadas a lo largo de la historia del congreso" />
            <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
            <link rel="canonical" href="https://congresovisible.uniandes.edu.co/votaciones/"/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + PageData.imgPrincipal}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-file-contract"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>Votaciones</h1>
                    </div>
                </div>
            </section>
            <main>
                <div className="listadoPageContainer">
                    <div className="container-fluid">
                        <div className="centerTabs lg min-height-85">
                            <ul>
                                <li>
                                    <h2>
                                        <a href="/orden-del-dia">Agenda legislativa</a>
                                    </h2>
                                </li>
                                <li className="active">
                                    <h2>
                                        <a href="/votaciones">Votaciones</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/citaciones">Control político</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/funcion-electoral">Función electoral</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/partidos">Partidos</a>
                                    </h2>
                                </li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="contentTab active">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="row">
                                                <div className="col-lg-3 col-md-12">
                                                    <div className="filtros-vertical evenColors">
                                                        <h3><i className="fa fa-filter"></i> Filtros de información</h3>
                                                        <div className="one-columns">
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de corporación</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectTipoCorporacion.length <= 1) {
                                                                            let { DSCorporacion } = await getComboCorporacion();
                                                                            setDSCorporacion(DSCorporacion)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoCorporacion}
                                                                    selectoptions={dataSelectTipoCorporacion}
                                                                    selectOnchange={handlerFilterTipoCorporacion}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Cuatrienio</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectCuatrienio.length <= 1) {
                                                                            let { DSCuatrienio } = await getComboCuatrienio();
                                                                            setDSCuatrienio(DSCuatrienio)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterCuatrienio}
                                                                    selectoptions={dataSelectCuatrienio}
                                                                    selectOnchange={handlerFilterCuatrienio}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Legislatura</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterLegislatura}
                                                                    selectoptions={dataSelectLegislatura}
                                                                    selectOnchange={handlerFilterLegislatura}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un cuatrienio"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de comisión</label>
                                                                <Select

                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoComision}
                                                                    selectoptions={dataSelectTipoComision}
                                                                    selectOnchange={handlerFilterTipoComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir una corporación"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Comisión</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterComision}
                                                                    selectoptions={dataSelectComision}
                                                                    selectOnchange={handlerFilterComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un tipo de comisión"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de votación</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoVotacion}
                                                                    selectoptions={VotacionesData.dataSelectTipoVotacion}
                                                                    selectOnchange={handlerFilterTipoVotacion}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-9 col-md-12">
                                                    <div className="simbologia">
                                                        <h4>Simbología</h4>
                                                        <ul>
                                                            <li className="green">Votaron Si</li>
                                                            <li className="danger">Votaron No</li>
                                                            <li className="gray">Estuvo ausente</li>
                                                        </ul>
                                                    </div>
                                                    <div className="buscador">
                                                        <div className="input-group">
                                                            <input type="text" value={search}
                                                                onChange={async (e) => {
                                                                    setSearch(e.target.value)
                                                                }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPaginationVotaciones(listVotaciones.page, listVotaciones.rows, e.target.value)
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar" className="form-control" />
                                                            <span className="input-group-text"><button onClick={async () => { await handlerPaginationVotaciones(listVotaciones.page, listVotaciones.rows, listVotaciones.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                                        </div>
                                                    </div>
                                                    <ActLegislativaVotacionesList data={listVotaciones.data} origen={auth.pathApi()} pageExtends={page} pageSize={rows} totalRows={listVotaciones.totalRows} tiposRespuesta={listVotaciones.tiposRespuestas} handler={handlerPaginationVotaciones} />
                                                    <CIDPagination totalPages={listVotaciones.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/votaciones"} hrefQuery={{
                                                        corporacion: query.corporacion || filterTipoCorporacion.value,
                                                        legislatura: query.legislatura || filterLegislatura.value,
                                                        cuatrienio: query.cuatrienio || filterCuatrienio.value,
                                                        tipoComision: query.tipoComision || filterTipoComision.value,
                                                        comision: query.comision || filterComision.value,
                                                        tipoVotacion: query.tipoVotacion || filterTipoVotacion.value 
                                                    }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

        </>
    )
}

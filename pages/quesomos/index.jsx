import Head from 'next/head'
import AwesomeSlider from 'react-awesome-slider';
import CongresoVisibleEquipo from '../../Components/CongresoVisible/congresoVisibleEquipo';
import QueEsCongresoVisible from '../../Components/CongresoVisible/QueEsCongresoVisible';
import AuthLogin from "../../Utils/AuthLogin";
import CongresoVisibleAliado from '../../Components/CongresoVisible/CongresoVisibleAliado';
import CVDataService from "../../Services/CongresoVisible/CongresoVisible.Service";
import { Constantes, URLBase } from "../../Constants/Constantes";
import SlideDataService from "../../Services/General/informacionSitio.Service";
import 'react-awesome-slider/dist/styles.css';

const auth = new AuthLogin();
const ConstData = {
    subloader: false,
    mediaCarouselPrincipal: [],
    mediaCarouselSecundario: [],
    data: {
        id: 0,
        queEs: "",
        objetivos: "",
        historiaymision: "",
        nuestroFuturo: "",
        nuestroReto: ""
    },
    slideCV: {
        slide_Principal: [],
        slide_secundario: []
    },
    dataEquipo: []
}

// SSR
export async function getServerSideProps({ query }) {

    let PageData = ConstData;
    PageData.data = await getAll();
    PageData.dataEquipo = await getAllEquipo();
    PageData.slideCV = await getAllSlide();

    Object.entries(PageData.slideCV.slide_principal).forEach(([key, value]) => {
        PageData.mediaCarouselPrincipal.push({ 'source': auth.pathApi() + value.imagen });
    });

    Object.entries(PageData.slideCV.slide_secundario).forEach(([key, value]) => {
        PageData.mediaCarouselSecundario.push({ 'source': auth.pathApi() + value.imagen });
    });

    return {
        props: { PageData, query }
    }
}

// PETICIONES
const getAll = async () => {
    let congresoVisible = null;
    await CVDataService.getAll().then((response) => {
        congresoVisible = response.data[0];
    })
        .catch((e) => {
            console.error(e);
        });
    return congresoVisible;
};
const getAllEquipo = async () => {
    let equipo = null;
    await CVDataService.getAllEquipo()
        .then((response) => {
            equipo = response.data;
        }).catch((e) => { console.log(e); })
    return equipo
}
const getAllSlide = async () => {
    let data = null;
    await SlideDataService.getslideCongresoVisible()
        .then((response) => {
            data = response.data[0];
        }).catch((e) => { console.log(e); })

    return data;
}

// EXPORT
const QueSomos = ({ PageData = ConstData, query }) => {
    return (
        <>
            <Head>
                <title>¿Qué es Congreso Visible? | Congreso Visible</title>
                <meta name="description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="¿Qué es Congreso Visible? | Congreso Visible" />
                <meta property="og:description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/quesomos`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/quesomos`} />
                <meta name="twitter:title" content="¿Qué es Congreso Visible? | Congreso Visible"/>
                <meta name="twitter:description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/quesomos/"/>
            </Head>
            <section className="CongresoVisibleSection">
                <AwesomeSlider
                    mobileTouch={true}
                    infinite={true}
                    media={PageData.mediaCarouselPrincipal}
                />
            </section>

            <QueEsCongresoVisible
                data={PageData.data}
                dataSlide={PageData.mediaCarouselSecundario}
                button={false}
            />

            <CongresoVisibleEquipo
                Equipos={PageData.dataEquipo}
            />
            <CongresoVisibleAliado
            />
        </>
    )
}
export default QueSomos;
import { useState } from "react";
import Head from "next/head"
import Link from "next/link";
import dynamic from 'next/dynamic';
import Select from '../../Components/Select';
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import 'suneditor/dist/css/suneditor.min.css'
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase, TypeCombos } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";

// Const
const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const auth = new AuthLogin();

const PageConst = {
    subloaderModal: true,
    subloaderInformesPNUD: true,
    listInformesPNUD: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "nombre", esImg: false, img: "", putOnFirstElement: false }
                ],
            generalActionTitle: "Documentos",
            generalIcon: "fas fa-file-alt"
        },
        esModal: true,
        targetModal: "#modal-informes-pnud",
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    informeSelected: {}
};

const InformeTerritorialConst = {

}

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let InformeTerritorialData = InformeTerritorialConst;

    PageData.listInformesPNUD.data = await getAllInformesPNUD(
        1
        , query.search || PageData.listInformesPNUD.search
        , query.page || PageData.listInformesPNUD.page
        , query.rows || PageData.listInformesPNUD.rows
    );
    PageData.listInformesPNUD.totalRows = await getTotalRecordsInformesPNUD(
        1
        , query.search || PageData.listInformesPNUD.search
    );

    PageData.subloaderInformesPNUD = false;

    return {
        props: {
            PageData, InformeTerritorialData, query
        }
    }
}

// Gets
const getTotalRecordsInformesPNUD = async (idFilterActive, search) => {
    let totalRows = 0;
    await ContenidoMultimediaDataService.getTotalRecordsInformesPNUD(idFilterActive, search).then((response) => {
        totalRows = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    return totalRows
};

const getAllInformesPNUD = async (idFilterActive, search, page, rows) => {
    let data = [];
    await ContenidoMultimediaDataService.getAllInformesPNUD(idFilterActive, search, page, rows).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const InformeTerritorial = ({ PageData = PageConst, InformeTerritorialData = InformeTerritorialConst, query }) => {
    // Const Loader
    const [subloaderModal, setSubloaderModal] = useState(PageData.subloaderModal);
    const [subloaderInformesPNUD, setSubloaderInformesPNUD] = useState(PageData.subloaderInformesPNUD);

    // Const Data
    const [listInformesPNUD, setListInformesPNUD] = useState(PageData.listInformesPNUD);
    const [informeSelected, setInformeSelected] = useState(PageData.informeSelected);
    const [search, setSearch] = useState(query.search || PageData.listInformesPNUD.search);
    const [rows, setRows] = useState(query.rows || PageData.listInformesPNUD.rows);
    const [page, setPage] = useState(query.page || PageData.listInformesPNUD.page);

    // Handlers
    const handlerPaginationInformesPNUD = async (page, rowsA, searchA = "") => {
        setSubloaderInformesPNUD(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    };

    const handlerForLoadModalInformesPNUD = async (item) => {
        setSubloaderModal(true);
        setInformeSelected(item.data);
        setSubloaderModal(false);
    };

    return (
        <div>
            <Head>
                <title>Informe territorial</title>
                <meta name="description" content="Aquí podrás encontrar todos los informes del congreso" />
                <meta name="keywords" content="Informes territoriales, Proyectos de Ley, Citaciones, Comisiones, Citantes, Citados" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Informe territorial" />
                <meta property="og:description" content="Aquí podrás encontrar todos los informes del congreso" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/informe-territorial`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/informe-territorial`} />
                <meta name="twitter:title" content="Informe territorial" />
                <meta name="twitter:description" content="Aquí podrás encontrar todos los informes del congreso" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/informe-territorial/" />
            </Head>
            <div className="pageTitle">
                <h1>Informes regionales</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <h2>
                                    <a href={"/contenido-multimedia"}>
                                        <i className="fas fa-photo-video"></i>
                                        Multimedia
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/articulo"}>
                                        <i className="fas fa-comment-dots"></i>
                                        Artículos
                                    </a>
                                </h2>
                            </li>
                            <li>

                                <h2>
                                    <a href={"/agora"}>
                                        <i className="fas fa-comments"></i>
                                        Opinión de congresistas
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/podcast"}>
                                        <i className="fas fa-microphone-alt"></i> Podcast
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/balance"}>
                                        <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                    </a>
                                </h2>
                            </li>
                            <li className="active">
                                <h2>
                                    <a href={"/informe-territorial"}>
                                        <i className="fas fa-file-alt"></i> Informes regionales
                                    </a>
                                </h2>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="contentTab active" data-ref="1">
                            <div className="relative">
                                <div className={`subloader ${subloaderInformesPNUD ? "active" : ""}`}></div>
                                <div className="buscador pd-25">
                                    <div className="input-group">
                                        <input type="text"
                                            value={search}
                                            onChange={async (e) => {
                                                setSearch(e.target.value)
                                            }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerPaginationInformesPNUD(listInformesPNUD.page, listInformesPNUD.rows, e.target.value)
                                                }
                                            }}
                                            placeholder="Escriba para buscar" className="form-control" />

                                        <span className="input-group-text">
                                            <button
                                                onClick={async () => {
                                                    await handlerPaginationInformesPNUD(listInformesPNUD.page, listInformesPNUD.rows, listInformesPNUD.search)
                                                }}
                                                type="button"
                                                className="btn btn-primary"
                                            ><i className="fa fa-search"></i></button></span>
                                    </div>
                                </div>
                                <ContenidoMultimediaList
                                    propiedades={listInformesPNUD.propiedades}
                                    data={listInformesPNUD.data}
                                    defaultImage={Constantes.NoImagenPicture}
                                    handlerPagination={handlerPaginationInformesPNUD}
                                    link="#/detalle" params={["id"]}
                                    pageExtends={page}
                                    totalRows={listInformesPNUD.totalRows}
                                    pageSize={rows}
                                    pathImgOrigen={auth.pathApi()}
                                    className="pd-25"
                                    esModal={listInformesPNUD.esModal}
                                    targetModal={listInformesPNUD.targetModal}
                                    handlerForLoadModal={handlerForLoadModalInformesPNUD}
                                />
                                <CIDPagination totalPages={listInformesPNUD.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/informe-territorial"} hrefQuery={{

                                }} />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
            <div className="modal fade" id="modal-informes-pnud" aria-labelledby="modal-informes-pnud" aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="estudios"><i className="fas fa-file-alt"></i> Informes Regionales {`- ${informeSelected.nombre || ''}`}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className={`subloader ${subloaderModal ? "active" : ""}`}></div>
                            <div className="container">
                                <div className="listDocumentos">
                                    {
                                        informeSelected.documentos_informe ?
                                            informeSelected.documentos_informe.map((item, i) =>
                                                <a rel="noreferrer" key={i} href={auth.pathApi() + item.documento} className="btn btn-primary center-block mb-2" target="_blank"><i className="fas fa-file-alt"></i>{" " + item.documento.split("/")[1]}</a>
                                            ) : ""
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default InformeTerritorial;
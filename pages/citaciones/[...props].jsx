import Link from 'next/link'
import ControlPoliticoDataService from "../../Services/ControlPolitico/ControlPolitico.Service";
import dynamic from "next/dynamic";
import Head from 'next/head'
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import { useState, useEffect } from "react";
import AuthLogin from "../../Utils/AuthLogin";
import { Constantes, CitacionesURLParameters, URLBase } from '../../Constants/Constantes'
const auth = new AuthLogin();
const ControlPoliticoConst = {
    id: 0,
    legislatura_id: 0,
    cuatrienio_id: 0,
    estado_control_politico_id: 0,
    comision_id: 0,
    titulo: "",
    fecha: '',
    cuatrienio: {
        activo: 0,
        fechaFin: new Date().toLocaleDateString(),
        fechaInicio: new Date().toLocaleDateString(),
        id: 0,
        nombre: ''
    },
    comision: {
        activo: 0,
        camara_id: 0,
        corporacion_id: 0,
        correo: '',
        descripcion: '',
        id: 0,
        imagen: '',
        nombre: '',
        oficina: '',
        orden: 0,
        permanente: '',
        telefono: '',
        tipo_comision_id: 0
    },
    corporacion: {},
    legislatura: {
        id: 0,
        nombre: '',
        fechaInicio: new Date().toLocaleDateString(),
        fechaFin: new Date().toLocaleDateString()
    },
    estado_control_politico: {},
    tema_principal_control_politico: {
        activo: 0,
        descripcion: '',
        id: 0,
        nombre: ''
    },
    tema_secundario_control_politico: {
        activo: 0,
        descripcion: '',
        id: 0,
        nombre: ''
    },
    control_politico_proposiciones: [],
    control_politico_respuestas: [],
    control_politico_documentos: [],
    control_politico_citantes: [
        {
            id: 0,
            control_politico_id: 0,
            congresista_id: 0,
            activo: 0,
            congresista_partido: {
                detalle_proyecto: {
                    partido: { nombre: "", partido_imagen: [{ imagen: "" }] }
                }
            }
        }],
    control_politico_citados: [{
        activo: 0,
        asistencia: 0,
        control_politico_id: 0,
        id: 0,
        persona_id: 0,
        representante: '',
        persona: {
            activo: 0,
            apellidos: '',
            fechaNacimiento: new Date().toLocaleDateString(),
            fecha_fallecimiento: new Date().toLocaleDateString(),
            genero_id: 0,
            grado_estudio_id: 0,
            id: 0,
            imagenes: [],
            municipio_id_nacimiento: 0,
            nombres: '',
            perfil_educativo: '',
            profesion_id: 0
        }
    }],
    control_politico_tags: [],
    subloaderCitados: false,
    subloaderCitantes: false,
    searchCitados: false,
    searchCitantes: false
}
export async function getServerSideProps({ query }) {
    let ControlPoliticoData = ControlPoliticoConst;
    //ControlPoliticoData.id=query.props[CitacionesURLParameters.id];
    ControlPoliticoData = await getByID(query.props[CitacionesURLParameters.id])
    return {
        props: { ControlPoliticoData }
    }
}

const getByID = async (id) => {
    let controlPolitico = ControlPoliticoConst;
    await ControlPoliticoDataService.get(id)
        .then((response) => {
            controlPolitico = response.data[0];
        })
        .catch((e) => {
            console.log(e);
        });
    return controlPolitico
};

const getCitantes = async (search, idCP) => {
    let listCitantes = ControlPoliticoConst.control_politico_citantes;
    await ControlPoliticoDataService.getCitantesFilter(search, idCP)
        .then((response) => {

            listCitantes = response.data;

        }).catch((e) => {
            console.log(e);
        });
    return listCitantes;
}

const getCitados = async (search, idCP) => {
    let listCitados = ControlPoliticoConst.control_politico_citados;
    await ControlPoliticoDataService.getCitadosFilter(search, idCP)
        .then((response) => {
            listCitados = response.data;
        }).catch((e) => {
            console.log(e);
        });
    return listCitados;
}
export default function DetalleControPolitico({ ControlPoliticoData = ControlPoliticoConst }) {
    const [loading, setLoading] = useState(false);
    const [subloaderCitados, setSubloaderCitados] = useState(ControlPoliticoData.subloaderCitados);
    const [subloaderCitantes, setSubloaderCitantes] = useState(ControlPoliticoData.subloaderCitantes);
    const [searchCitados, setSearchCitados] = useState(ControlPoliticoData.searchCitados);
    const [searchCitantes, setSearchCitantes] = useState(ControlPoliticoData.searchCitantes);
    const [controlPolitico, setControlPolitico] = useState(ControlPoliticoData);


    useEffect(() => {
        let ctrl = getByID(controlPolitico.id);
    }, []);
    const handlerCitantesFilter = async () => {
        setTimeout(async () => {
            setSubloaderCitantes(true);
            let list = await getCitantes(searchCitantes, controlPolitico.id);
            setControlPolitico({ ...controlPolitico, control_politico_citantes: list });
            setSubloaderCitantes(false);
        }, 500);
    }

    const handlerCitadosFilter = async () => {

        setTimeout(async () => {
            setSubloaderCitados(true);
            let list = await getCitados(searchCitados, controlPolitico.id);
            setControlPolitico({ ...controlPolitico, control_politico_citados: list });
            setSubloaderCitados(false);
        }, 500);
    }
    return (
        <>
            <Head>
                <title>{controlPolitico?.titulo.slice(0, 40)}| Trámite legislativo en Colombia | Congreso Visible </title>
                <meta name="description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`${controlPolitico?.titulo.slice(0, 40)} |Trámite legislativo en Colombia | Congreso Visible`} />
                <meta property="og:description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={URLBase} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={URLBase} />
                <meta name="twitter:title" content={`${controlPolitico?.titulo.slice(0, 40)} |Trámite legislativo en Colombia | Congreso Visible`} />
                <meta name="twitter:description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/citaciones/${auth.filterStringForURL(controlPolitico?.titulo)}/${controlPolitico.id}/`}/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${"https://www.definicionabc.com/wp-content/uploads/politica/Proyecto-de-Ley.jpg"}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-exclamation-circle"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{controlPolitico?.titulo}</h1>
                    </div>
                </div>
            </section>
            <section gtdtarget="1" className="text-justify no-full-height">
                <div className="container">
                    <div className={`subloader ${loading ? "active" : ""}`}></div>
                    <div className="row">
                        <div className="col-md-7">
                            <h2>Información de la citación</h2>
                            <hr />
                            <div className="two-columns">
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-university"></i></div>
                                    <div className="vertical-text">
                                        <small>Comisión</small>
                                        <p>{controlPolitico?.comision?.nombre || 'S/N'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-circle"></i></div>
                                    <div className="vertical-text">
                                        <small>Cuatrienio</small>
                                        <p>{controlPolitico?.cuatrienio.nombre || 'S/N'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-info"></i></div>
                                    <div className="vertical-text">
                                        <small>Estado</small>
                                        <p>{controlPolitico?.estado_control_politico?.nombre || 'S/E'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-question-circle"></i></div>
                                    <div className="vertical-text">
                                        <small>Tema principal</small>
                                        <p>{controlPolitico?.tema_principal_control_politico?.nombre || 'S/T'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="far fa-question-circle"></i></div>
                                    <div className="vertical-text">
                                        <small>Tema secundario</small>
                                        <p>{controlPolitico?.tema_secundario_control_politico?.nombre || 'S/T'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-hashtag"></i></div>
                                    <div className="vertical-text">
                                        <small>Número de proposición</small>
                                        <p>{controlPolitico?.numero_proposicion || 'S/N'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-circle"></i></div>
                                    <div className="vertical-text">
                                        <small>Legislatura</small>
                                        <p>{controlPolitico?.legislatura?.nombre || 'S/L'}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-calendar-alt"></i></div>
                                    <div className="vertical-text">
                                        <small>Fecha</small>
                                        <p>{controlPolitico?.fecha || 'S/F'}</p>
                                    </div>
                                </div>
                            </div>
                            {/* <h2>Detalles</h2>
                                <hr />
                                <SunEditor
                                    disable = {true}
                                    enableToolbar = {true} 
                                    showToolbar = {false}
                                    width = "100%" 
                                    height = "100%"                                                              
                                    setOptions = {{resizingBar: false, showPathLabel: false, shortcutsDisable: true }}
                                    setContents={this.state.controlPolitico?.detalles || 'Sin detalle'}
                                /> */}
                            <hr />
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h5>Cuestionarios</h5>
                                        <div className="listDocumentos text-left no-icons">
                                            <ul>
                                                {
                                                    controlPolitico?.control_politico_proposiciones.map((item, i) => {
                                                        return (
                                                            <a rel="noreferrer" key={i} href={auth.pathApi() + item.url} target="_blank">
                                                                <li><i className="fa fa-download"></i>{item.nombre}</li>
                                                            </a>
                                                        );
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <h5>Respuestas</h5>
                                        <div className="listDocumentos text-left no-icons">
                                            <ul>
                                                {
                                                    controlPolitico?.control_politico_respuestas.map((item, i) => {
                                                        return (
                                                            <a rel="noreferrer" key={i} href={auth.pathApi() + item.url} target="_blank">
                                                                <li><i className="fa fa-download"></i>{item.nombre}</li>
                                                            </a>
                                                        );
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <hr />
                                        <h5>Gacetas</h5>
                                        <div className="two-columns ">
                                            <div className="listDocumentos text-left no-icons">
                                                <ul>
                                                    {
                                                        controlPolitico?.control_politico_documentos.map((item, i) => {
                                                            return (
                                                                <a rel="noreferrer" key={i} href={auth.pathApi() + item.url} target="_blank">
                                                                    <li><i className="fa fa-download"></i>{item.nombre}</li>
                                                                </a>
                                                            );
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5">
                            <h3>Tags</h3>
                            <div className="publicationTags">
                                {
                                    controlPolitico?.control_politico_tags.map((item, i) => {
                                        return (
                                            <p key={i}>{item.glosario_legislativo?.palabra}</p>
                                        );
                                    })
                                }
                            </div>
                            <h3>Citantes</h3>
                            <div className="miembrosContainer">
                                <div className={`subloader ${subloaderCitantes ? "active" : ""}`}></div>
                                <div className="buscador">
                                    <div className="input-group">
                                        <input
                                            value={searchCitantes || ""}
                                            type="text"
                                            placeholder="Escriba para buscar"
                                            className="form-control"
                                            aria-label="Dollar amount (with dot and two decimal places)"
                                            onChange={async (e) => {

                                                setSearchCitantes(e.target.value);
                                            }
                                            }
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerCitantesFilter();
                                                }
                                            }}
                                        />
                                        <span className="input-group-text"><button onClick={async () => { await handlerCitantesFilter() }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    </div>
                                </div>
                                <div className="miembros">
                                    {
                                        controlPolitico?.control_politico_citantes.map((item, i) => {
                                            let params = auth.filterStringForURL(`${item.congresista_partido?.persona?.nombres} ${item.congresista_partido?.persona?.apellidos}`)
                                            if (item.congresista_partido !== null) {
                                                return (

                                                    <a href={`/congresistas/perfil/${params}/${item.congresista_partido?.persona_id}`}>
                                                        <div className="item" key={i}>
                                                            <div className="photo">
                                                                {
                                                                    item.congresista_partido.persona !== null ?
                                                                        <img src={typeof item.congresista_partido?.persona?.imagenes[1] !== "undefined" ? auth.pathApi() + item.congresista_partido?.persona?.imagenes[1].imagen : Constantes.NoImagenPicture} alt={item.congresista_partido?.persona?.nombres} />
                                                                        :
                                                                        <img src={Constantes.NoImagenPicture} alt={`Sin imagen`} />
                                                                }

                                                            </div>
                                                            <div className="subphoto">
                                                                {
                                                                    item.congresista_partido.detalle_proyecto !== null ?
                                                                        <img src={typeof item.congresista_partido.detalle_proyecto.partido.partido_imagen[0].imagen !== "undefined" ? auth.pathApi() + item.congresista_partido.detalle_proyecto.partido.partido_imagen[0].imagen : Constantes.NoImagenPicture} alt={item.congresista_partido.detalle_proyecto.partido.nombre} />
                                                                        :
                                                                        <img src={Constantes.NoImagenPicture} alt={`Sin imagen`} />
                                                                }

                                                            </div>
                                                            <div className="info">
                                                                <div className="name"><p>{`${item.congresista_partido?.persona?.nombres} ${item.congresista_partido?.persona?.apellidos}`}</p></div>
                                                                {/* <div className="job"><p>Presidencia</p></div> */}
                                                            </div>
                                                        </div>
                                                    </a>
                                                );
                                            }
                                        })
                                    }
                                </div>
                            </div>
                            <hr />
                            <h3>Citados</h3>
                            <hr />
                            <div className="miembrosContainer">
                                <div className={`subloader ${subloaderCitados ? "active" : ""}`}></div>
                                <div className="buscador">
                                    <div className="input-group">
                                        <input
                                            value={searchCitados || ""}
                                            type="text"
                                            placeholder="Escriba para buscar"
                                            className="form-control"
                                            aria-label="Dollar amount (with dot and two decimal places)"
                                            onChange={async (e) => {
                                                setSearchCitados(e.target.value);
                                            }
                                            }
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerCitadosFilter();
                                                }
                                            }}
                                        />
                                        <span className="input-group-text"><button onClick={async () => { await handlerCitadosFilter() }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    </div>
                                </div>
                                <div className="miembros">
                                    {
                                        controlPolitico?.control_politico_citados.map((item, i) => {
                                            let asistencia = "";
                                            switch (item.asistencia) {
                                                case 1:
                                                    asistencia = "Asiste";
                                                    break;
                                                case 2:
                                                    asistencia = "Delega asistencia";
                                                    break;
                                                case 3:
                                                    asistencia = "Asiste y delega asistencia";
                                                    break;
                                                case 4:
                                                    asistencia = "Se excusa";
                                                    break;
                                                case 5:
                                                    asistencia = "Asiste y se excusa";
                                                    break;
                                                case 6:
                                                    asistencia = "Delega asistencia y se excusa";
                                                    break;
                                                case 7:
                                                    asistencia = "No asistente, no delega asistencia ni se excusa";
                                                    break;
                                                default:
                                                    asistencia = "Sin tipo de asistencia asignada"
                                                    break;
                                            }
                                            if (item.persona !== null) {
                                                return (
                                                    <div className="item" key={i}>
                                                        <div className="photo">
                                                            <img src={typeof item?.persona?.imagenes[1] !== "undefined" ? auth.pathApi() + item?.persona?.imagenes[1].imagen : Constantes.NoImagen} alt={item?.persona?.nombres} />
                                                        </div>
                                                        {/* <div className="subphoto">
                                                                <img src={item.congresista.partido.partido_imagen[0] != undefined ? item.congresista.partido.partido_imagen[0].imagen : Constantes.NoImagen} alt={item.congresista.partido.nombre} />
                                                            </div> */}
                                                        <div className="info">
                                                            <div className="name"><p><strong>Entidad o persona: </strong>{`${item?.persona?.nombres || ""} ${item?.persona?.apellidos || ""}`}</p></div>
                                                            <div className="job">
                                                                <p><strong>Representante: </strong> {item?.representante || 'S/N'}</p>
                                                                <p>{asistencia}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )

}
import Head from 'next/head'
import Select from '../../Components/Select';
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
import { useState, useEffect } from "react";
import { Constantes, URLBase } from "../../Constants/Constantes.js";
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import infoSitioDataService from "../../Services/General/informacionSitio.Service";
import ActLegislativaControlPoliticoList from "../../Components/CongresoVisible/ActLegislativaControlPoliticoList";
import CIDPagination from "../../Components/CIDPagination";
const auth = new AuthLogin();
const PageConst = { imgPrincipal: null, subloader: false }
const CitacionesConst = {
    filterTipoCorporacion: { value: -1, label: "Ver cámara y senado" },
    dataSelectTipoCorporacion: [],
    filterTipoComision: { value: -1, label: "Elija tipo de comisión" },
    dataSelectTipoComision: [],
    filterComision: { value: -1, label: "Ver todas" },
    dataSelectComision: [],
    filterEstado: { value: -1, label: "Ver todos" },
    dataSelectEstado: [],
    filterTema: { value: -1, label: "Ver todos" },
    dataSelectTema: [],
    filterCuatrienio: { value: -1, label: "Ver todos" },
    dataSelectCuatrienio: [],
    filterLegislatura: { value: -1, label: "Ver todas" },
    dataSelectLegislatura: [],
    filterTipoCitacion: { value: -1, label: "Ver todos" },
    dataSelectTipoCitacion: [],
    listControlPolitico: {
        data: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 8
    }
}

export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let CitacionesData = CitacionesConst;

    await infoSitioDataService.getInformacionSitioHome()
        .then((response) => {
            PageData.imgPrincipal = response.data[0].imgPrincipal || Constantes.NoImagenPicture;
        })
        .catch((e) => {
            console.error(e);
        });

    CitacionesData.listControlPolitico = await getAllControlPolitico(
        1
        , query.corporacion || CitacionesData.filterTipoCorporacion.value
        , query.legislatura || CitacionesData.filterLegislatura.value
        , query.cuatrienio || CitacionesData.filterCuatrienio.value
        , query.comision || CitacionesData.filterComision.value
        , query.estado || CitacionesData.filterEstado.value
        , query.tema || CitacionesData.filterTema.value
        , query.tipoCitacion || CitacionesData.filterTipoCitacion.value
        , query.search || CitacionesData.listControlPolitico.search
        , query.page || CitacionesData.listControlPolitico.page
        , query.rows || CitacionesData.listControlPolitico.rows
        );
    return {
        props: { PageData, CitacionesData, query }
    }
}
const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCorporacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver Cámara y Senado" })
        if (!selected)
            selected = combo[0]
    })
    return { DSCorporacion: combo, SelectedCorporacion: selected };
}
const getComboCuatrienio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCuatrienio().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]

    })
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
}
const getComboLegislatura = async (cuatrienio, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboLegislatura(cuatrienio).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todas" })
        if (!selected)
            selected = combo[0]
    })
    return { DSLegislatura: combo, SelectedLegislatura: selected };
}
const getComboTipoComision = async (idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboTipoComision(idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Elija tipo de comisión" })
        if (!selected)
            selected = combo[0]
    })
    return { DSTipoComision: combo, SelectedTipoComision: selected };
}
const getComboComisiones = async (idTipoComision, idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboComisiones(idTipoComision, idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todas" })
        if (!selected)
            selected = combo[0]
    })
    return { DSComision: combo, SelectedComision: selected };
}
const getComboEstado = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboEstadoControlPolitico().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]
    })
    return { DSEstado: combo, SelectedEstado: selected };
}
const getComboTemaControlPolitico = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboTemaControlPolitico().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]
    })
    return { DSTema: combo, SelectedTema: selected };
}

const getComboTipoCitacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboTipoCitacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]
    })
    return { DSTipoCitacion: combo, SelectedTipoCitacion: selected };
}

const getAllControlPolitico = async (idFilterActive, corporacion, legislatura, cuatrienio, comision, estado, tema, tipoCitacion, search, page, rows) => {
    let listControlPolitico = CitacionesConst.listControlPolitico;
    await ActividadesLegislativasDataService.getAllControlPolitico(
        idFilterActive,
        corporacion, legislatura, cuatrienio, comision, tema, estado, tipoCitacion,
        search, page, rows
    )
        .then((response) => {
            listControlPolitico.data = response.data;
        })
        .catch((e) => {
            console.error(e);
        });
    await ActividadesLegislativasDataService.getTotalRecordsControlPolitico(
        idFilterActive,
        corporacion, legislatura, cuatrienio, comision, estado, tema, tipoCitacion,
        search
    )
        .then((response) => {
            listControlPolitico.totalRows = response.data;
        })
        .catch((e) => {
            console.error(e);
        });

    return listControlPolitico;
};

export default function Citaciones({ PageData = PageConst, CitacionesData = CitacionesConst, query }) {
    const [imgPrincipal, setImg] = useState(PageConst.imgPrincipal);
    const [subloader, setSubloader] = useState(PageConst.subloader);
    const [filterTipoCorporacion, setFilterTipoCorporacion] = useState(CitacionesData.filterTipoCorporacion);
    const [dataSelectTipoCorporacion, setDSCorporacion] = useState(CitacionesData.dataSelectTipoCorporacion);
    const [filterTipoComision, setFilterTComision] = useState(CitacionesData.filterTipoComision);
    const [dataSelectTipoComision, setDSTComision] = useState(CitacionesData.dataSelectTipoComision);
    const [filterComision, setFilterComision] = useState(CitacionesData.filterComision);
    const [dataSelectComision, setDSComision] = useState(CitacionesData.dataSelectComision);
    const [filterEstado, setFilterEstado] = useState(CitacionesData.filterTipoComision);
    const [dataSelectEstado, setDSEstado] = useState(CitacionesData.dataSelectTipoComision);
    const [filterTema, setFilterTema] = useState(CitacionesData.filterTema);
    const [dataSelectTema, setDSTema] = useState(CitacionesData.dataSelectTipoComision);
    const [filterCuatrienio, setFilterCuatrienio] = useState(CitacionesData.filterCuatrienio);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(CitacionesData.dataSelectCuatrienio);
    const [filterLegislatura, setFilterLegislatura] = useState(CitacionesData.filterLegislatura);
    const [dataSelectLegislatura, setDSLegislatura] = useState(CitacionesData.dataSelectLegislatura);
    const [filterTipoCitacion, setFilterTipoCitacion] = useState(CitacionesData.filterTipoCitacion);
    const [dataSelectTipoCitacion, setDSTipoCitacion] = useState(CitacionesData.dataSelectTipoCitacion);
    const [listControlPolitico, setlistControlPolitico] = useState(CitacionesData.listControlPolitico);
    const [search, setSearch] = useState(query.search || CitacionesData.listControlPolitico.search);
    const [rows, setRows] = useState(query.rows || CitacionesData.listControlPolitico.rows);
    const [page, setPage] = useState(query.page || CitacionesData.listControlPolitico.page);

    useEffect(async () => {
        if (query.corporacion) {
            let { DSCorporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
            setDSCorporacion(DSCorporacion); setFilterTipoCorporacion(SelectedCorporacion);
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(SelectedCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if (query.cuatrienio) {
            let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(Number(query.cuatrienio));
            setDSCuatrienio(DSCuatrienio); setFilterCuatrienio(SelectedCuatrienio);
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(SelectedCuatrienio.value);
            setDSLegislatura(DSLegislatura); setFilterLegislatura(SelectedLegislatura);
        }
        if (query.legislatura) {
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(Number(query.cuatrienio), Number(query.legislatura));
            setDSLegislatura(DSLegislatura); setFilterLegislatura(SelectedLegislatura);
        }
        if(query.tipoComision){
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(filterTipoCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if(query.comision){
            let { DSComision, SelectedComision } = await getComboComisiones(Number(query.tipoComision), Number(query.corporacion), Number(query.comision));
            setDSComision(DSComision); setFilterComision(SelectedComision);
        }
        if(query.estado){
            let { DSEstado, SelectedEstado } = await getComboEstado(Number(query.estado));
            setDSEstado(DSEstado); setFilterEstado(SelectedEstado);
        }
        if(query.tema){
            let { DSTema, SelectedTema } = await getComboTemaControlPolitico(Number(query.tema));
            setDSTema(DSTema); setFilterTema(SelectedTema);
        }
        if(query.tipoCitacion){
            let { DSTipoCitacion, SelectedTipoCitacion } = await getComboTipoCitacion(Number(query.tipoCitacion));
            setDSTipoCitacion(DSTipoCitacion); setFilterTipoCitacion(SelectedTipoCitacion);
        }
    }, []);

    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('cuatrienio', selectCuatrienio.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterLegislatura = async (selectLegislatura) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('legislatura', selectLegislatura.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoComision = async (selectTipoComision) => {
        setFilterTComision(selectTipoComision);
        setDSComision([]);
        setFilterComision(Object.assign({}, CitacionesConst.filterComision));
        let { DSComision, SelectedComision } = await getComboComisiones(selectTipoComision.value, filterTipoCorporacion.value);
        setDSComision(DSComision); setFilterComision(SelectedComision)
    }
    const handlerFilterComision = async (selectComision) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoComision', filterTipoComision.value, q)
        q = auth.replaceQueryParam('comision', selectComision.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoCitacion = async (selectCitacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoCitacion', selectCitacion.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoCorporacion = async (selectTipoCorporacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', selectTipoCorporacion.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTema = async (selectTema) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tema', selectTema.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterEstado = async (selectEstado) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('estado', selectEstado.value, q)
        window.location = window.location.pathname + q
    }
    const handlerPaginationControlPolitico = async (page, rowsA, searchA = "") => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }
    return (
        <>
            <Head>
                <title>Trámite legislativo en Colombia | Congreso Visible </title>
                <meta name="description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Trámite legislativo en Colombia | Congreso Visible" />
                <meta property="og:description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/citaciones`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/citaciones`} />
                <meta name="twitter:title" content="Trámite legislativo en Colombia | Congreso Visible"/>
                <meta name="twitter:description" content="Aquí encontrarás toda la información de el estatus de cualquier trámite legislativo, como iniciativas, solicitudes, propuestas y más. Visítanos para conocerlas" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/citaciones/"/>
            </Head>

            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + PageData.imgPrincipal}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-file-contract"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>Citaciones</h1>
                    </div>
                </div>
            </section>
            <main>
                <div className="listadoPageContainer">
                    <div className="container-fluid">
                        <div className="centerTabs lg min-height-85">
                            <ul>
                                <li>
                                    <h2>
                                        <a href="/orden-del-dia">Agenda legislativa</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/votaciones">Votaciones</a>
                                    </h2>
                                </li>
                                <li className="active">
                                    <h2>
                                        <a href="/citaciones">Control político</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/funcion-electoral">Función electoral</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/partidos">Partidos</a>
                                    </h2>
                                </li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="contentTab active">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="row">
                                                <div className="col-lg-3 col-md-12">
                                                    <div className="filtros-vertical evenColors">
                                                        <h3><i className="fa fa-filter"></i> Filtros de información</h3>
                                                        <div className="one-columns">
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de corporación</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectTipoCorporacion.length <= 1) {
                                                                            let { DSCorporacion } = await getComboCorporacion();
                                                                            setDSCorporacion(DSCorporacion)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoCorporacion}
                                                                    selectoptions={dataSelectTipoCorporacion}
                                                                    selectOnchange={handlerFilterTipoCorporacion}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de comisión</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoComision}
                                                                    selectoptions={dataSelectTipoComision}
                                                                    selectOnchange={handlerFilterTipoComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un tipo de comision"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Comisión</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterComision}
                                                                    selectoptions={dataSelectComision}
                                                                    selectOnchange={handlerFilterComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir una comisión"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Estado</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectEstado.length <= 1) {
                                                                            let { DSEstado } = await getComboEstado();
                                                                            setDSEstado(DSEstado)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterEstado}
                                                                    selectoptions={dataSelectEstado}
                                                                    selectOnchange={handlerFilterEstado}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tema</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectTema.length <= 1) {
                                                                            let { DSTema } = await getComboTemaControlPolitico();
                                                                            setDSTema(DSTema)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTema}
                                                                    selectoptions={dataSelectTema}
                                                                    selectOnchange={handlerFilterTema}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Cuatrienio</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectCuatrienio.length <= 1) {
                                                                            let { DSCuatrienio } = await getComboCuatrienio();
                                                                            setDSCuatrienio(DSCuatrienio)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterCuatrienio}
                                                                    selectoptions={dataSelectCuatrienio}
                                                                    selectOnchange={handlerFilterCuatrienio}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Legislatura</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterLegislatura}
                                                                    selectoptions={dataSelectLegislatura}
                                                                    selectOnchange={handlerFilterLegislatura}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un cuatrienio"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de Citación</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectTipoCitacion.length <= 1) {
                                                                            let { DSTipoCitacion } = await getComboTipoCitacion();
                                                                            setDSTipoCitacion(DSTipoCitacion)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoCitacion}
                                                                    selectoptions={dataSelectTipoCitacion}
                                                                    selectOnchange={handlerFilterTipoCitacion}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un tipo de citacion"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-9 col-md-12">
                                                    <div className="buscador">
                                                        <   div className="input-group">
                                                            <input type="text" value={search}
                                                                onChange={async (e) => {
                                                                    setSearch(e.target.value)
                                                                }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPaginationControlPolitico(listControlPolitico.page, listControlPolitico.rows, e.target.value)
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar" className="form-control" />

                                                            <span className="input-group-text"><button onClick={async () => { await handlerPaginationControlPolitico(listControlPolitico.page, listControlPolitico.rows, listControlPolitico.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                                        </div>
                                                    </div>
                                                    <ActLegislativaControlPoliticoList origen={auth.pathApi()} imgDefault={Constantes.NoImagen} data={listControlPolitico.data} handler={handlerPaginationControlPolitico} pageExtends={page} pageSize={rows} totalRows={listControlPolitico.totalRows} />
                                                    <CIDPagination totalPages={listControlPolitico.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/citaciones"} hrefQuery={{
                                                        corporacion: query.corporacion || filterTipoCorporacion.value,
                                                        legislatura: query.legislatura || filterLegislatura.value,
                                                        cuatrienio: query.cuatrienio || filterCuatrienio.value,
                                                        tipoComision: query.tipoComision || filterTipoComision.value,
                                                        comision: query.comision || filterComision.value,
                                                        estado: query.estado || filterEstado.value,
                                                        tema: query.tema || filterTema.value,
                                                        tipoCitacion: query.tipoCitacion || filterTipoCitacion.value
                                                    }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

        </>
    )
}

import { useState, useEffect } from "react";
import Head from "next/head";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import HighchartsDumbbell from "highcharts/modules/dumbbell";
import HighchartsLollipop from "highcharts/modules/lollipop";
import HighchartsMore from "highcharts/highcharts-more";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import Select from "../../Components/Select";
import VarValueBoxCamaraPublicaronConflictoInteres from '../../ConflictoInteresConstantes/VarValueBoxCamaraPublicaronConflictoInteres';
import VarValueBoxSenadoPublicaronConflictoInteres from '../../ConflictoInteresConstantes/VarValueBoxSenadoPublicaronConflictoInteres';
import VarValueBoxCamaraPublicaronReporteCampaña from '../../ConflictoInteresConstantes/VarValueBoxCamaraPublicaronReporteCampaña';
import VarValueBoxSenadoPublicaronReporteCampaña from '../../ConflictoInteresConstantes/VarValueBoxSenadoPublicaronReporteCampaña';
import VarTotalCongresistasConConflictoInteres from '../../ConflictoInteresConstantes/VarTotalCongresistasConConflictoInteres';
import VarTotalCongresistasParticipanJuntas from '../../ConflictoInteresConstantes/VarTotalCongresistasParticipanJuntas';
import VarProyectoLeyConflictoInteres from '../../ConflictoInteresConstantes/VarProyectoLeyConflictoInteres';
import VarPorcentajeIngresosPublicosDeCongresistas from '../../ConflictoInteresConstantes/VarPorcentajeIngresosPublicosDeCongresistas';
import GraficasDataService from "../../Services/CongresistasConflictoInteres/Graficas.Service";
import UtilsDataService from "../../Services/General/Utils.Service";
import HC_More from 'highcharts/highcharts-more';
import { URLBase } from '../../Constants/Constantes'


// good
if (typeof Highcharts === 'object') {
    HC_More(Highcharts)
    NoDataToDisplay(Highcharts);
    HighchartsMore(Highcharts);
    HighchartsDumbbell(Highcharts);
    HighchartsLollipop(Highcharts);
}


const ConstAllSubloaders = {
    CamaraPublicaronConflictoInteres: true,
    CamaraPublicaronReporteCampaña: true,
    SenadoPublicaronConflictoInteres: true,
    SenadoPublicaronReporteCampaña: true,
    TotalCongresistasConConflictoInteres: true,
    TotalCongresistasParticipanJuntas: true,
    PorcentajeIngresosPublicosDeCongresistas: true,
    DistribucionIngresosPublicosAño: true,
    ProyectoLeyConflictoInteres: true
}

const ConstCharts = {
    CamaraPublicaronConflictoInteres: {
        value_box: Object.assign({}, VarValueBoxCamaraPublicaronConflictoInteres.valueBox()),
    },
    CamaraPublicaronReporteCampaña: {
        value_box: Object.assign({}, VarValueBoxCamaraPublicaronReporteCampaña.valueBox()),
    },
    SenadoPublicaronConflictoInteres: {
        value_box: Object.assign({}, VarValueBoxSenadoPublicaronConflictoInteres.valueBox()),
    },
    SenadoPublicaronReporteCampaña: {
        value_box: Object.assign({}, VarValueBoxSenadoPublicaronReporteCampaña.valueBox()),
    },
    TotalCongresistasConConflictoInteres: {
        total: Object.assign({}, VarTotalCongresistasConConflictoInteres.total()),
    },
    TotalCongresistasParticipanJuntas: {
        total: Object.assign({}, VarTotalCongresistasParticipanJuntas.total()),
    },
    ProyectoLeyConflictoInteres: {
        total: Object.assign({}, VarProyectoLeyConflictoInteres.total()),
    },
    PorcentajeIngresosPublicosDeCongresistas: {
        total: Object.assign({}, VarPorcentajeIngresosPublicosDeCongresistas.total())
    },
    ComboGeneral: {
        combo: {
            cuatrienio: {
                item: { value: "", label: "Ver todos" },
                data: [],
                error: "",
            },
            corporacion: {
                item: { value: "", label: "Ver ambas" },
                data: [],
                error: "",
            },
        },
    }
}

// SSR
export async function getServerSideProps({ query }) {
    let graficas = Object.assign({}, ConstCharts);

    let { DSCorporacion, SelectedCorporacion } = await getCorporacion();
    let { DSCuatrienio, SelectedCuatrienio } = await getCuatrienio();

    let { SenadoPublicaronConflictoInteresSeries, SenadoPublicaronConflictoInteresTotal } = await SenadoPublicaronConflictoInteres_handlerGetChart(SelectedCuatrienio.value);
    graficas.SenadoPublicaronConflictoInteres.value_box.default_value_box.grafica.series = SenadoPublicaronConflictoInteresSeries;
    graficas.SenadoPublicaronConflictoInteres.value_box.default_value_box.grafica.subtitle.text = SenadoPublicaronConflictoInteresTotal;

    let { SenadoPublicaronReporteCampañaSeries, SenadoPublicaronReporteCampañaTotal } = await SenadoPublicaronReporteCampaña_handlerGetChart(SelectedCuatrienio.value);
    graficas.SenadoPublicaronReporteCampaña.value_box.default_value_box.grafica.series = SenadoPublicaronReporteCampañaSeries;
    graficas.SenadoPublicaronReporteCampaña.value_box.default_value_box.grafica.subtitle.text = SenadoPublicaronReporteCampañaTotal;

    let { CamaraPublicaronConflictoInteresSeries, CamaraPublicaronConflictoInteresTotal } = await CamaraPublicaronConflictoInteres_handlerGetChart(SelectedCuatrienio.value);
    graficas.CamaraPublicaronConflictoInteres.value_box.default_value_box.grafica.series = CamaraPublicaronConflictoInteresSeries;
    graficas.CamaraPublicaronConflictoInteres.value_box.default_value_box.grafica.subtitle.text = CamaraPublicaronConflictoInteresTotal;

    let { CamaraPublicaronReporteCampañaSeries, CamaraPublicaronReporteCampañaTotal } = await CamaraPublicaronReporteCampaña_handlerGetChart(SelectedCuatrienio.value);
    graficas.CamaraPublicaronReporteCampaña.value_box.default_value_box.grafica.series = CamaraPublicaronReporteCampañaSeries;
    graficas.CamaraPublicaronReporteCampaña.value_box.default_value_box.grafica.subtitle.text = CamaraPublicaronReporteCampañaTotal;

    let { TotalCongresistasConConflictoInteresSeries } = await TotalCongresistasConConflictoInteres_handlerGetChart(SelectedCorporacion.value, SelectedCuatrienio.value);
    graficas.TotalCongresistasConConflictoInteres.total.total_congresistas_conflicto_parientes.grafica.series = TotalCongresistasConConflictoInteresSeries;

    let { TotalCongresistasParticipanJuntasSeries } = await TotalCongresistasParticipanJuntas_handlerGetChart(SelectedCorporacion.value, SelectedCuatrienio.value)
    graficas.TotalCongresistasParticipanJuntas.total.total_congresistas_participan_juntas.grafica.series = TotalCongresistasParticipanJuntasSeries;

    let { ProyectoLeyConflictoInteresSeries } = await ProyectoLeyConflictoInteres_handlerGetChart(SelectedCorporacion.value, SelectedCuatrienio.value);
    graficas.ProyectoLeyConflictoInteres.total.total_proyecto_ley_conflicto.grafica.series = ProyectoLeyConflictoInteresSeries;

    let { PorcentajeIngresosPublicosDeCongresistasSeries } = await PorcentajeIngresosPublicosDeCongresistas_handlerGetChart(SelectedCorporacion.value, SelectedCuatrienio.value);
    graficas.PorcentajeIngresosPublicosDeCongresistas.total.porcentaje_ingresos_publicos_privados.grafica.series = PorcentajeIngresosPublicosDeCongresistasSeries;

    return {
        props: { charts: JSON.stringify(graficas), DSCorporacion, SelectedCorporacion, DSCuatrienio, SelectedCuatrienio }
    }
}
// PETICIONES
const getCuatrienio = async () => {
    let combo = [];
    let selected = {};
    let year = new Date().getFullYear();
    await UtilsDataService.getComboCuatrienio().then((response) => {
        response.data.forEach((i) => {
            if (i.fechaInicio >= 2018) {
                combo.push({ value: i.id, label: i.nombre });
                if (i.fechaInicio <= year && year <= i.fechaFin) {
                    selected = { value: i.id, label: i.nombre };
                }
            }
        });

        // combo.unshift({value: "", label: "Seleccione un cuatrienio"});
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected }
};

const getCorporacion = async () => {
    let combo = [];
    let selected = {};
    await UtilsDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });

        // combo.unshift({ value: "", label: "Seleccione una corporación" });
        selected = combo[0];
    });

    return { DSCorporacion: combo, SelectedCorporacion: selected }
};

const CamaraPublicaronConflictoInteres_handlerGetChart = async (idCuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await GraficasDataService.getTotalCongresistasConflictoInteresCamara(idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.total;
            series[0].data.push({
                "n": currentValue.total,
                "y": currentValue.total,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { CamaraPublicaronConflictoInteresSeries: series, CamaraPublicaronConflictoInteresTotal: total }
};

const CamaraPublicaronReporteCampaña_handlerGetChart = async (idCuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await GraficasDataService.getTotalCongresistasReportaronCampaniasCamara(idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.total;
            series[0].data.push({
                "n": currentValue.total,
                "y": currentValue.total,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { CamaraPublicaronReporteCampañaSeries: series, CamaraPublicaronReporteCampañaTotal: total }
};

const SenadoPublicaronConflictoInteres_handlerGetChart = async (idCuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await GraficasDataService.getTotalCongresistasConflictoInteresSenado(idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.total;
            series[0].data.push({
                "n": currentValue.total,
                "y": currentValue.total,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });
    return { SenadoPublicaronConflictoInteresSeries: series, SenadoPublicaronConflictoInteresTotal: total }
};

const SenadoPublicaronReporteCampaña_handlerGetChart = async (idCuatrienio) => {
    let series = [{ "group": "group", "data": [], "type": "spline" }];
    let total = 0;
    await GraficasDataService.getTotalCongresistasReportaronCampaniasSenado(idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += currentValue.total;
            series[0].data.push({
                "n": currentValue.total,
                "y": currentValue.total,
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { SenadoPublicaronReporteCampañaSeries: series, SenadoPublicaronReporteCampañaTotal: total }
};

const TotalCongresistasConConflictoInteres_handlerGetChart = async (idCorporacion, idCuatrienio) => {
    let series = [{
        "name": "Femenino",
        "data": [],
        "type": "bubble"
    },
    {
        "name": "Masculino",
        "data": [],
        "type": "bubble"
    }];

    let total = 0;
    await GraficasDataService.getTotalCongresistasConflictoInteresParientes(idCorporacion, idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += parseInt(currentValue.hombres) + parseInt(currentValue.mujeres);

            if (currentValue.mujeres) {
                series[0].data.push(
                    {
                        "genero": "Femenino",
                        "n": currentValue.mujeres,
                        "pct": 0,
                        "y": 1,
                        "size": currentValue.mujeres,
                        "name": "Femenino",
                        "z": currentValue.mujeres
                    });
            }

            if (currentValue.hombres) {
                series[1].data.push(
                    {
                        "genero": "Masculino",
                        "n": currentValue.hombres,
                        "pct": 0,
                        "y": 1,
                        "size": currentValue.hombres,
                        "name": "Masculino",
                        "z": currentValue.hombres
                    });
            }
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.pct = ((currentValueData.n * 100) / total).toFixed(2);
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { TotalCongresistasConConflictoInteresSeries: series }
};

const TotalCongresistasParticipanJuntas_handlerGetChart = async (idCorporacion, idCuatrienio) => {
    let series = [{
        "name": "Congresistas que participan en juntas",
        "data": [],
        "type": "bubble"
    }];

    let total = 0;

    await GraficasDataService.getTotalCongresistasParticipanJuntas(idCorporacion, idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += parseInt(currentValue.total);
            series[0].data.push(
                {
                    "genero": "Congresistas",
                    "n": currentValue.total,
                    "pct": 0,
                    "y": 1,
                    "size": currentValue.total,
                    "name": "Congresistas",
                    "z": currentValue.total
                });
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.pct = ((currentValueData.n * 100) / total).toFixed(2);
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { TotalCongresistasParticipanJuntasSeries: series }
};

const ProyectoLeyConflictoInteres_handlerGetChart = async (idCorporacion, idCuatrienio) => {
    let series = [{
        "name": "Proyectos de ley con conflictos de interés",
        "data": [],
        "type": "bubble"
    }];
    let total = 0;
    await GraficasDataService.getTotalProyectoLeyConflictoInteres(idCorporacion, idCuatrienio).then((response) => {
        response.data.forEach((currentValue, index, array) => {
            total += parseInt(currentValue.total);
            series[0].data.push(
                {
                    "genero": "Proyectos de ley",
                    "n": currentValue.total,
                    "pct": 0,
                    "y": 1,
                    "size": currentValue.total,
                    "name": "Proyectos de ley",
                    "z": currentValue.total
                });
        });

        series.forEach((currentValue, index, array) => {
            currentValue.data.forEach((currentValueData, index, array) => {
                currentValueData.pct = ((currentValueData.n * 100) / total).toFixed(2);
            });
        });
    })
        .catch((error) => {
            console.log(error);
        });

    return { ProyectoLeyConflictoInteresSeries: series }
};

const PorcentajeIngresosPublicosDeCongresistas_handlerGetChart = async (idCorporacion, idCuatrienio) => {
    let series = [
        {
            name: 'Totales',
            data: [],
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: '#ffffff',
                distance: -30
            }
        },
        {
            name: 'Ingreso',
            data: [],
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                        this.y + '%' : null;
                }
            },
            id: 'ingreso'
        }
    ];
    await GraficasDataService.getPorcentajeIngresosPublicosPrivados(idCorporacion, idCuatrienio).then((response) => {
        let totalesData = [
            {
                name: "Ingresos públicos",
                y: Number(Number(response.data[0].ingresos_publicos).toFixed(2)),
                color: "#515151"
            },
            {
                name: "Ingresos privados",
                y: Number(Number(response.data[0].ingresos_privados).toFixed(2)),
                color: "#00b963"
            }
        ];
        let ingresoData = [
            {
                name: "Salario anual",
                y: Number(Number(response.data[0].salario_anual).toFixed(2)),
                color: "#1E91D6"
            },
            {
                name: "Intereses cesantías anuales",
                y: Number(Number(response.data[0].interes_cesantias_anual).toFixed(2)),
                color: "#8FC93A"
            },
            {
                name: "Gastos de representación anual",
                y: Number(Number(response.data[0].gasto_representacion_anual).toFixed(2)),
                color: "#E4CC37"
            },
            {
                name: "Honorario anual",
                y: Number(Number(response.data[0].honorario_anual).toFixed(2)),
                color: "#E18335"
            },
            {
                name: "Arriendo anual",
                y: Number(Number(response.data[0].arriendo_anual).toFixed(2)),
                color: "#515151"
            },
            {
                name: "Otros anual",
                y: Number(Number(response.data[0].otros_anual).toFixed(2)),
                color: "#e44e44"
            }
        ];
        series[0].data = totalesData;
        series[1].data = ingresoData;
    })
        .catch((error) => {

            console.log(error);
        });
    return { PorcentajeIngresosPublicosDeCongresistasSeries: series }
};
// EXPORT
const ConflictosInteres = ({ charts = ConstCharts, DSCorporacion = ConstCharts.ComboGeneral.combo.corporacion.data, SelectedCorporacion = ConstCharts.ComboGeneral.combo.corporacion.item, DSCuatrienio = ConstCharts.ComboGeneral.combo.cuatrienio.data, SelectedCuatrienio = ConstCharts.ComboGeneral.combo.cuatrienio.item }) => {
    // C_ de chart

    const [AllSubloaders, setAllSubloaders] = useState(ConstAllSubloaders);
    const [dataSelectCorporacion, setDSCorporacion] = useState(DSCorporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCorporacion, setSelectedCorporacion] = useState(SelectedCorporacion);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [C_CamaraPublicaronConflictoInteresGrafica, setCamaraPublicaronConflictoInteresGrafica] = useState(JSON.parse(charts).CamaraPublicaronConflictoInteres.value_box.default_value_box.grafica);
    const [C_SenadoPublicaronConflictoInteresGrafica, setSenadoPublicaronConflictoInteresGrafica] = useState(JSON.parse(charts).SenadoPublicaronConflictoInteres.value_box.default_value_box.grafica);
    const [C_CamaraPublicaronReporteCampañaGrafica, setCamaraPublicaronReporteCampañaGrafica] = useState(JSON.parse(charts).CamaraPublicaronReporteCampaña.value_box.default_value_box.grafica);
    const [C_SenadoPublicaronReporteCampañaGrafica, setSenadoPublicaronReporteCampañaGrafica] = useState(JSON.parse(charts).SenadoPublicaronReporteCampaña.value_box.default_value_box.grafica);
    const [C_TotalCongresistasConConflictoInteresGrafica, setTotalCongresistasConConflictoInteresGrafica] = useState(JSON.parse(charts).TotalCongresistasConConflictoInteres.total.total_congresistas_conflicto_parientes.grafica);
    const [C_TotalCongresistasParticipanJuntasGrafica, setTotalCongresistasParticipanJuntasGrafica] = useState(JSON.parse(charts).TotalCongresistasParticipanJuntas.total.total_congresistas_participan_juntas.grafica);
    const [C_ProyectoLeyConflictoInteresGrafica, setProyectoLeyConflictoInteresGrafica] = useState(JSON.parse(charts).ProyectoLeyConflictoInteres.total.total_proyecto_ley_conflicto.grafica);
    const [C_PorcentajeIngresosPublicosDeCongresistasGrafica, setPorcentajeIngresosPublicosDeCongresistasGrafica] = useState(JSON.parse(charts).PorcentajeIngresosPublicosDeCongresistas.total.porcentaje_ingresos_publicos_privados.grafica);

    useEffect(() => {
        setAllSubloaders(false)
    }, []);

    const ComboGeneral_handlerSelectComboCuatrienio = async (cuatrienio) => {
        setAllSubloaders(true)
        setSelectedCuatrienio(cuatrienio);

        let { SenadoPublicaronConflictoInteresSeries, SenadoPublicaronConflictoInteresTotal } = await SenadoPublicaronConflictoInteres_handlerGetChart(cuatrienio.value);
        setSenadoPublicaronConflictoInteresGrafica({ series: SenadoPublicaronConflictoInteresSeries, subtitle: { text: SenadoPublicaronConflictoInteresTotal } });
        setAllSubloaders({ ...AllSubloaders, SenadoPublicaronConflictoInteres: false })

        let { SenadoPublicaronReporteCampañaSeries, SenadoPublicaronReporteCampañaTotal } = await SenadoPublicaronReporteCampaña_handlerGetChart(cuatrienio.value);
        setSenadoPublicaronReporteCampañaGrafica({ series: SenadoPublicaronReporteCampañaSeries, subtitle: { text: SenadoPublicaronReporteCampañaTotal } });
        setAllSubloaders({ ...AllSubloaders, SenadoPublicaronReporteCampaña: false })

        let { CamaraPublicaronConflictoInteresSeries, CamaraPublicaronConflictoInteresTotal } = await CamaraPublicaronConflictoInteres_handlerGetChart(cuatrienio.value);
        setCamaraPublicaronConflictoInteresGrafica({ series: CamaraPublicaronConflictoInteresSeries, subtitle: { text: CamaraPublicaronConflictoInteresTotal } });
        setAllSubloaders({ ...AllSubloaders, CamaraPublicaronConflictoInteres: false })

        let { CamaraPublicaronReporteCampañaSeries, CamaraPublicaronReporteCampañaTotal } = await CamaraPublicaronReporteCampaña_handlerGetChart(cuatrienio.value);
        setCamaraPublicaronReporteCampañaGrafica({ series: CamaraPublicaronReporteCampañaSeries, subtitle: { text: CamaraPublicaronReporteCampañaTotal } });
        setAllSubloaders({ ...AllSubloaders, CamaraPublicaronReporteCampaña: false })

        let { TotalCongresistasConConflictoInteresSeries } = await TotalCongresistasConConflictoInteres_handlerGetChart(filterCorporacion.value, cuatrienio.value);
        setTotalCongresistasConConflictoInteresGrafica({ series: TotalCongresistasConConflictoInteresSeries });
        setAllSubloaders({ ...AllSubloaders, TotalCongresistasConConflictoInteres: false })

        let { TotalCongresistasParticipanJuntasSeries } = await TotalCongresistasParticipanJuntas_handlerGetChart(filterCorporacion.value, cuatrienio.value)
        setTotalCongresistasParticipanJuntasGrafica({ series: TotalCongresistasParticipanJuntasSeries });
        setAllSubloaders({ ...AllSubloaders, TotalCongresistasParticipanJuntas: false })

        let { ProyectoLeyConflictoInteresSeries } = await ProyectoLeyConflictoInteres_handlerGetChart(filterCorporacion.value, cuatrienio.value);
        setProyectoLeyConflictoInteresGrafica({ series: ProyectoLeyConflictoInteresSeries });
        setAllSubloaders({ ...AllSubloaders, ProyectoLeyConflictoInteres: false })

        let { PorcentajeIngresosPublicosDeCongresistasSeries } = await PorcentajeIngresosPublicosDeCongresistas_handlerGetChart(filterCorporacion.value, cuatrienio.value);
        setPorcentajeIngresosPublicosDeCongresistasGrafica({ series: PorcentajeIngresosPublicosDeCongresistasSeries });
        setAllSubloaders({ ...AllSubloaders, PorcentajeIngresosPublicosDeCongresistas: false })
    }

    const ComboGeneral_handlerSelectComboCorporacion = async (corporacion) => {
        setAllSubloaders({ ...AllSubloaders, TotalCongresistasConConflictoInteres: true, TotalCongresistasParticipanJuntas: true, ProyectoLeyConflictoInteres: true, PorcentajeIngresosPublicosDeCongresistas: true })
        setSelectedCorporacion(corporacion);

        let { TotalCongresistasConConflictoInteresSeries } = await TotalCongresistasConConflictoInteres_handlerGetChart(corporacion.value, filterCuatrienio.value);
        setTotalCongresistasConConflictoInteresGrafica({ series: TotalCongresistasConConflictoInteresSeries });
        setAllSubloaders({ ...AllSubloaders, TotalCongresistasConConflictoInteres: false })

        let { TotalCongresistasParticipanJuntasSeries } = await TotalCongresistasParticipanJuntas_handlerGetChart(corporacion.value, filterCuatrienio.value)
        setTotalCongresistasParticipanJuntasGrafica({ series: TotalCongresistasParticipanJuntasSeries });
        setAllSubloaders({ ...AllSubloaders, TotalCongresistasParticipanJuntas: false })

        let { ProyectoLeyConflictoInteresSeries } = await ProyectoLeyConflictoInteres_handlerGetChart(corporacion.value, filterCuatrienio.value);
        setProyectoLeyConflictoInteresGrafica({ series: ProyectoLeyConflictoInteresSeries });
        setAllSubloaders({ ...AllSubloaders, ProyectoLeyConflictoInteres: false })

        let { PorcentajeIngresosPublicosDeCongresistasSeries } = await PorcentajeIngresosPublicosDeCongresistas_handlerGetChart(corporacion.value, filterCuatrienio.value);
        setPorcentajeIngresosPublicosDeCongresistasGrafica({ series: PorcentajeIngresosPublicosDeCongresistasSeries });
        setAllSubloaders({ ...AllSubloaders, PorcentajeIngresosPublicosDeCongresistas: false })

    }

    return (
        <div>
            <Head>
                <title>Conflictos de interés</title>
                <meta name="description" content="En este apartado se encuentran reportes actualizados del seguimiento en conflictos de interés de congresistas." />
                <meta name="keywords" content= {`conflictos, interes, graficas, datos, senadores, publicaciones, congresistas`} />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`Conflictos de interés`} />
                <meta property="og:description" content={`En este apartado se encuentran reportes actualizados del seguimiento en conflictos de interés de congresistas.`} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={URLBase} />
                <meta name="twitter:url" content={URLBase} />
                <meta name="twitter:title" content={`Conflictos de interés`}/>
                <meta name="twitter:description" content={`En este apartado se encuentran reportes actualizados del seguimiento en conflictos de interés de congresistas.`} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/conflictos-interes/"/>
            </Head>
            <div className="topPanel">
                <div className="pageTitle">
                    <h1>Conflictos de interés</h1>
                </div>
            </div>
            <section className="nuestraDemocraciaSection ">
                <div className="profileContainer no-pad">
                    <div className="DatosGraficasContainer relative">
                        <div className="verticalTabsContainer md">
                            <div className="verticalTab no-left with-overflow active" data-ref="1">
                                <div className="info one-columns">
                                    <div className="littleSection">
                                        <div className="top-filters">
                                            <div className="item">
                                                <label htmlFor="">Corporación</label>
                                                <Select
                                                    divClass=""
                                                    selectplaceholder="Seleccione"
                                                    selectValue={filterCorporacion}
                                                    selectIsSearchable={true}
                                                    selectoptions={dataSelectCorporacion}
                                                    selectOnchange={ComboGeneral_handlerSelectComboCorporacion}
                                                    selectclassNamePrefix="selectReact__value-container"
                                                    spanClass="error"
                                                    spanError={""}
                                                />
                                            </div>
                                            <div className="item">
                                                <label htmlFor="">Cuatrienio</label>
                                                <Select
                                                    divClass=""
                                                    selectplaceholder="Seleccione"
                                                    selectValue={filterCuatrienio}
                                                    selectIsSearchable={true}
                                                    selectoptions={dataSelectCuatrienio}
                                                    selectOnchange={ComboGeneral_handlerSelectComboCuatrienio}
                                                    selectclassNamePrefix="selectReact__value-container"
                                                    spanClass="error"
                                                    spanError={""}
                                                />
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    <div className="principalDataContainer">
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.SenadoPublicaronConflictoInteres ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_SenadoPublicaronConflictoInteresGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.SenadoPublicaronReporteCampaña ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_SenadoPublicaronReporteCampañaGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.CamaraPublicaronConflictoInteres ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_CamaraPublicaronConflictoInteresGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.CamaraPublicaronReporteCampaña ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_CamaraPublicaronReporteCampañaGrafica}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />

                                    <div className="principalDataContainer">
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.TotalCongresistasConConflictoInteres ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_TotalCongresistasConConflictoInteresGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.TotalCongresistasParticipanJuntas ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_TotalCongresistasParticipanJuntasGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.PorcentajeIngresosPublicosDeCongresistas ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_PorcentajeIngresosPublicosDeCongresistasGrafica}
                                                />
                                            </div>
                                        </div>
                                        <div className="dataValueBox md">
                                            <div className="relative">
                                                <div className={`subloader ${AllSubloaders.ProyectoLeyConflictoInteres ? "active" : ""}`}></div>
                                                <HighchartsReact
                                                    highcharts={Highcharts}
                                                    options={C_ProyectoLeyConflictoInteresGrafica}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default ConflictosInteres;
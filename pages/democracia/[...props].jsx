import { useEffect, useState } from "react";
import Head from "next/head";
import dynamic from 'next/dynamic';
import 'suneditor/dist/css/suneditor.min.css';
import BlogNdService from '../../Services/BlogNd/BlogNd.Service';
import AuthLogin from "../../Utils/AuthLogin";
import { Constantes,URLBase } from "../../Constants/Constantes";

// Const
const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const buttonList = [
    [
        "undo",
        "redo",
        "paragraphStyle",
        "blockquote",
        "bold",
        "underline",
        "italic",
        "strike",
        "subscript",
        "superscript",
        "fontColor",
        "hiliteColor",
        "textStyle",
        "removeFormat",
        "outdent",
        "indent",
        "align",
        "horizontalRule",
        "list",
        "lineHeight",
        "table",
        "link",
        "image",
        "video",
        "audio" /** 'math', */, // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
        /** 'imageGallery', */ "fullScreen",
        "showBlocks",
        "codeView",
        "preview",
        "print",
        "save",
        "template",
    ],
];

const auth = new AuthLogin();

const PageConst = {
    loading: true
};

const DemocraciaDetalleConst = {
    fields: {
        blog_nd_portada: [],
        titulo: "",
        fecha_publicacion: "",
        tipo_publicacion: "",
        tema_blog: "",
        conceptos: [],
        descripcion: "",
        id: 0
    }
};

// SSR
export async function getServerSideProps({query}){
    let PageData = PageConst;
    let DemocraciaDetalleData = DemocraciaDetalleConst;
    DemocraciaDetalleData.fields = await getBloga(query.props[1]);
    
    PageData.loading = false;

    return {
        props: {
            PageData, DemocraciaDetalleData
        }
    }
}

// Gets
const getBloga = async (id) => {
    let fields;

    await BlogNdService.get(id).then((response) => {
        fields = response.data[0];
    }).catch((e) => {
        console.error(e);
    });

    return fields;
};

// Page
const DemocraciaDetalle = ({PageData = PageConst, DemocraciaDetalleData = DemocraciaDetalleConst}) => {
    const [loading, setLoading] = useState(PageData.loading);
    const [blog_nd_portada, setBlog_nd_portada] = useState(DemocraciaDetalleData.fields.blog_nd_portada);
    const [titulo, setTitulo] = useState(DemocraciaDetalleData.fields.titulo);
    const [fecha_publicacion, setfecha_publicacion] = useState(DemocraciaDetalleData.fields.fecha_publicacion);
    const [tipo_publicacion, setTipo_publicacion] = useState(DemocraciaDetalleData.fields.tipo_publicacion);
    const [tema_blog, setTema_blog] = useState(DemocraciaDetalleData.fields.tema_blog);
    const [conceptos, setConceptos] = useState(DemocraciaDetalleData.fields.conceptos);
    const [descripcion, setDescripcion] = useState(DemocraciaDetalleData.fields.descripcion);

    return(
        <div>
            <Head>
            <title>{titulo} | Congreso Visible</title>
            <meta name="description" content={auth.filterStringHTML(descripcion).slice(0, 160)} />
            <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={`${titulo} | Congreso Visible`} />
            <meta property="og:description" content={auth.filterStringHTML(descripcion).slice(0, 160)} />
            <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
            <meta property="og:image:width" content="828" />
            <meta property="og:image:height" content="450" />
            <meta property="og:url" content={`${URLBase}/democracia/${auth.filterStringForURL(titulo)}/${DemocraciaDetalleData.fields.id}`} />
            <meta property="og:site_name" content="Congreso Visible" />
            <meta name="twitter:url" content={`${URLBase}/democracia/${auth.filterStringForURL(titulo)}/${DemocraciaDetalleData.fields.id}`} />
            <meta name="twitter:title" content={`${titulo} | Congreso Visible`}/>
            <meta name="twitter:description" content={auth.filterStringHTML(descripcion).slice(0, 160)} />
            <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
            <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/democracia/${auth.filterStringForURL(titulo)}/${DemocraciaDetalleData.fields.id}`}/>
            </Head>
        <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${!loading ? (typeof blog_nd_portada[3] !== "undefined" ? auth.pathApi() + blog_nd_portada[3].portada : Constantes.NoImagenPicture) : ""}')` }}>
            <div className="CVBannerCentralInfo">
                <div className="CVBanerIcon littleIcon"><i className="fas fa-file-alt"></i></div>
                <div className="CVBannerTitle text-center">
                    <h1>{!loading ? titulo : ""}</h1>
                </div>
            </div>
        </section>
            <section className="nuestraDemocraciaSection">
                <div className="listadoPageContainer">
                    <div className="container">
                        <div className={`subloader ${loading ? "active" : ""}`}></div>
                        <div className="row">
                            <div className="col-md-12">
                                <strong>Fecha de publicación:</strong>
                                <p>{!loading ? auth.coloquialDate(fecha_publicacion) : ""}</p>
                                <strong>Tipo de publicación:</strong>
                                <p>{!loading ? tipo_publicacion.nombre : "Sin autores"}</p>
                                <strong>Tema:</strong>
                                <p>{!loading ? tema_blog.nombre : "Sin autores"}</p>
                                <strong>Tags</strong>
                                <div className="publicationTags">
                                    {
                                        !loading ? conceptos?.map((item, i) =>{
                                            return (
                                                <p key={i}>{item.glosario_legislativo[0].palabra}</p>
                                            );
                                        }): ""
                                    }
                                </div>
                                <hr />
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: descripcion || "Sin descripción" }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>                
    );
};

export default DemocraciaDetalle;
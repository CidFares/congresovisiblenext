import { useState, useEffect } from 'react';
import Head from 'next/head';
import Select from '../../Components/Select';
import CardsBlogdND from "../../Components/CongresoVisible/CardsBlogdND";
import BlogNdService from '../../Services/BlogNd/BlogNd.Service';
import { Constantes, URLBase } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";
const auth = new AuthLogin();

const PageConst = {
    data: [],
    subloader: false,
    page: 1,
    rows: 18,
    search: "",
    totalRows: 0,
    filterTema: { value: -1, label: "Seleccione tema del blog" },
    filterTipoPublicacion: { value: -1, label: "Seleccione tipo de publicacion" },
    listBlog: [],
    dataSelectTipoPublicacion: [],
    dataSelectTema: [],
};

const DemocraciaConst = {
};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let DemocraciaData = DemocraciaConst;

    PageData.data = await getAll(
        query.tema || PageData.filterTema.value
        , query.tipoPublicacion || PageData.filterTipoPublicacion.value
        , query.page || PageData.page
        , query.rows || PageData.rows
        , query.search || PageData.search
    );
    PageData.totalRows = await getTotalRecords(
        query.tema || PageData.filterTema.value
        , query.tipoPublicacion || PageData.filterTipoPublicacion.value
        , query.search || PageData.search
    );

    return {
        props: {
            PageData, DemocraciaData, query
        }
    }
}

// Gets
const getComboTema = async (ID = null) => {
    let combo = [];
    let selected = null;
    await BlogNdService.getComboTema().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]
    })
    return { DStema: combo, SelectedTema: selected };
};

const getComboTipo = async (ID = null) => {
    let combo = [];
    let selected = null;
    await BlogNdService.getComboTipo().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]
    });

    return { DSTipoPublicacion: combo, SelectedTipoPublicacion: selected };
};

const getAll = async (tema, tipo, page, rows, search) => {
    let data = [];
    await BlogNdService.getAll(tema, tipo, search, page, rows).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const getTotalRecords = async (tema, tipo, search) => {
    let totalRows = 0;
    await BlogNdService.getTotalRecords(tema, tipo, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
};

// Page
const Democracia = ({ PageData = PageConst, DemocraciaData = DemocraciaConst, query }) => {
    // Const Loader
    const [subloader, setSubloader] = useState(PageData.subloader);

    // Const Select
    const [filterTema, setFilterTema] = useState(PageData.filterTema);

    // Const Data
    const [data, setData] = useState(PageData.data);
    const [search, setSearch] = useState(query.search || PageData.search);
    const [rows, setRows] = useState(query.rows || PageData.rows);
    const [page, setPage] = useState(query.page || PageData.page);
    const [totalRows, setTotalRows] = useState(PageData.totalRows);
    const [filterTipoPublicacion, setFilterTipoPublicacion] = useState(PageData.filterTipoPublicacion);
    const [dataSelectTipoPublicacion, setDataSelectTipoPublicacion] = useState(PageData.dataSelectTipoPublicacion);
    const [dataSelectTema, setDataSelectTema] = useState(PageData.dataSelectTema);

    useEffect(async () => {
        if (query.tema) {
            let { DStema, SelectedTema } = await getComboTema(Number(query.tema));
            setDataSelectTema(DStema); setFilterTema(SelectedTema);
        }
        if (query.tipoPublicacion) {
            let { DSTipoPublicacion, SelectedTipoPublicacion } = await getComboTipo(Number(query.tipoPublicacion));
            setDataSelectTipoPublicacion(DSTipoPublicacion); setFilterTipoPublicacion(SelectedTipoPublicacion)
        }
    }, []);

    // Handlers
    const handlerTema = async (selectTema) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tema', selectTema.value, q)
        window.location = window.location.pathname + q
    }
    const handlerTipo = async (selectTipo) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoPublicacion', selectTipo.value, q)
        window.location = window.location.pathname + q
    }
    const handlerPagination = async (page, rowsA, searchA = "") => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    // Others
    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
            <title>Conozca nuestra Democracia | Congreso Visible</title>
            <meta name="description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
            <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="Nuestra Democracia | Congreso Visible" />
            <meta property="og:description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
            <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
            <meta property="og:image:width" content="828" />
            <meta property="og:image:height" content="450" />
            <meta property="og:url" content={`${URLBase}/democracia`} />
            <meta property="og:site_name" content="Congreso Visible" />
            <meta name="twitter:url" content={`${URLBase}/democracia`} />
            <meta name="twitter:title" content="Nuestra Democracia | Congreso Visible"/>
            <meta name="twitter:description" content="La base de datos más completa de la actividad legislativa del Congreso Colombiano" />
            <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
            <link rel="canonical" href="https://congresovisible.uniandes.edu.co/democracia/"/>
            </Head>
            <section className="nuestraDemocraciaSection">
                <div className="publicationsSide">
                <div className="topPanel">
                <div className="pageTitle">
                    <h1>Nuestra democracia</h1>
                </div>
            </div>
                    <div className="relative">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="buscador pd-25">
                            <div className="input-group">
                                <input type="text"
                                    value={search}
                                    onChange={async (e) => {
                                        setSearch(e.target.value)
                                    }}
                                    onKeyUp={async (e) => {
                                        if (e.key === "Enter") {
                                            await handlerPagination(page, rows, e.target.value);
                                        }
                                    }}
                                    placeholder="¿Qué quieres aprender hoy?" className="form-control" />

                                <span className="input-group-text"><button onClick={async () => { await handlerPagination(page, rows, search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                <span className="input-group-text">
                                    <button
                                        onClick={async (e) => {
                                            toggleFilter(e.currentTarget);
                                        }}
                                        type="button"
                                        className="btn btn-primary"
                                    ><i className="fa fa-filter"></i></button></span>
                            </div>
                            <div className="floatingFilters evenColors">
                                <div className="one-columns relative no-margin">
                                    <div className={`subloader ${subloader ? "active" : ""}`}></div>
                                    <div className="item">
                                        <label htmlFor="">Filtrar por tipo de publicación</label>
                                        <Select
                                            handlerOnClick={async () => {
                                                if (dataSelectTipoPublicacion.length <= 1) {
                                                    let { DSTipoPublicacion } = await getComboTipo();
                                                    setDataSelectTipoPublicacion(DSTipoPublicacion)
                                                }
                                            }}
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterTipoPublicacion}
                                            selectOnchange={handlerTipo}
                                            selectoptions={dataSelectTipoPublicacion}
                                            selectIsSearchable={true}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass=""
                                            spanError="" >
                                        </Select>
                                    </div>
                                    <div className="item">
                                        <label htmlFor="">Filtrar por tema</label>
                                        <Select
                                            handlerOnClick={async () => {
                                                if (dataSelectTema.length <= 1) {
                                                    let { DStema } = await getComboTema();
                                                    setDataSelectTema(DStema)
                                                }
                                            }}
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterTema}
                                            selectOnchange={handlerTema}
                                            selectoptions={dataSelectTema}
                                            selectIsSearchable={true}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass=""
                                            spanError="" >
                                        </Select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <CardsBlogdND
                            data={data}
                            handler={handlerPagination}
                            pageExtends={page}
                            pageSize={rows}
                            totalRows={totalRows}
                            defaultImage={Constantes.NoImagen}
                            pathImgOrigen={auth.pathApi()} />
                            <CIDPagination totalPages={totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/democracia"} hrefQuery={{
                            tema: query.tema || filterTema.value,
                            tipoPublicacion: query.tipoPublicacion || filterTipoPublicacion.value
                        }} />
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </div>
    );
};

export default Democracia;

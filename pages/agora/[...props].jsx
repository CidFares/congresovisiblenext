import { useState } from "react";
import dynamic from 'next/dynamic';
import Head from "next/head"
import OpinionDataService from "../../Services/ContenidoMultimedia/Opinion.Service";
import 'suneditor/dist/css/suneditor.min.css';
import { Constantes, URLBase } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";

// Const
const auth = new AuthLogin();

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const PageConst = {
    id: 0,
    loading: true,
    imagenes: [],
    equipo: [],
    imagenEquipo: [],
    icono: ""
};

const DetalleOpinionCongresistaConst = {
    id: 0,
    titulo: "",
    persona_id: 0,
    tipo_publicacion_id: 0,
    fechaPublicacion: "",
    resumen: "",
    opinion: "",
    activo: 1,
    opinion_congresista_imagen: [
        { imagen: "" }
    ],
    persona: {
        id: 0,
        nombres: "",
        apellidos: "",
        fechaNacimiento: "",
        municipio_id_nacimiento: 0,
        profesion_id: 0,
        genero_id: 0,
        fecha_fallecimiento: "",
        perfil_educativo: "",
        grado_estudio_id: "",
        activo: null,
        imagenes: []
    },
    tipo_publicacion: { id: 0, nombre: "", icono: "", activo: 1 }
};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let DetalleOpinionCongresistaData = DetalleOpinionCongresistaConst;
    PageData.id = query.props[1];
    DetalleOpinionCongresistaData = await getById(PageData.id);
    PageData.imagenes = DetalleOpinionCongresistaData.opinion_congresista_imagen[0];
    PageData.loading = false;

    return {
        props: {
            PageData, DetalleOpinionCongresistaData
        }
    }
}

// Gets
const getById = async (id) => {
    let data = {};
    await OpinionDataService.getOpinionCongresista(id).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });
    return data;
}

// Page
const DetalleOpinionCongresista = ({ PageData = PageConst, DetalleOpinionCongresistaData = DetalleOpinionCongresistaConst }) => {
    // Const
    const [detalleOpinionCongresista, setDetalleOpinionCongresista] = useState(DetalleOpinionCongresistaData);
    const [loading, setLoading] = useState(PageData.loading);
    const [equipo, setEquipo] = useState(PageData.equipo);
    const [imagenes, setImagenes] = useState(PageData.imagenes);

    return (
        <>
            <Head>
                <title>{detalleOpinionCongresista.titulo}</title>
                <meta name="description" content={auth.filterStringHTML(detalleOpinionCongresista.resumen).slice(0, 165)} />
                <meta name="keywords" content="Opinion, congresista, Congreso Colombia, Democracia, Colombia, Partidos Políticos, Proyectos de Ley, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={detalleOpinionCongresista.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(detalleOpinionCongresista.resumen).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/agora/${auth.filterStringForURL(detalleOpinionCongresista.titulo)}/${detalleOpinionCongresista.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/agora/${auth.filterStringForURL(detalleOpinionCongresista.titulo)}/${detalleOpinionCongresista.id}`} />
                <meta name="twitter:title" content={detalleOpinionCongresista.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(detalleOpinionCongresista.resumen).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/agora/${auth.filterStringForURL(detalleOpinionCongresista.titulo)}/${detalleOpinionCongresista.id}/`}/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + imagenes.imagen}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon littleIcon"><i className="fas fa-file-alt"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{!loading ? detalleOpinionCongresista.titulo : ""}</h1>
                    </div>
                </div>
            </section>
            <section className="nuestraDemocraciaSection">
                <div className="listadoPageContainer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                            <div className="autor" style={{ display: "flex", justifyContent: "flex-start", alignItems: "center", margin: "15px 0"}}>
					                <div className="photo avatar" style={{marginRight: "13px"}}>
                                    <img src={!loading ? typeof detalleOpinionCongresista.persona?.imagenes[0] !== "undefined" ? auth.pathApi() + detalleOpinionCongresista.persona.imagenes[0].imagen : Constantes.NoImagen : ""} alt={`${detalleOpinionCongresista.persona.nombres} ${detalleOpinionCongresista.persona.apellidos}`} />
					                </div>
                                    <h2>{!loading ? `${detalleOpinionCongresista.persona?.nombres || ''} ${detalleOpinionCongresista.persona?.apellidos || ''}` : ""}</h2>
		                			
                				</div>
                                <div className="description">
                                    <strong>Resumen:</strong>
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: detalleOpinionCongresista.resumen || "Sin resumen" }}></div>
                                    <hr />
                                    <strong>Contenido:</strong>
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: detalleOpinionCongresista.opinion || "Sin contenido" }}></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </>
    )
}

export default DetalleOpinionCongresista;
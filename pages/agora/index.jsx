import { useState, useEffect } from "react";
import Head from "next/head"
import Link from "next/link";
import Select from '../../Components/Select';
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase, TypeCombos } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";

// Const
const auth = new AuthLogin();

const PageConst = {
    subloaderFilters: true,
    subloaderOpinionesCongresistas: true,
    listOpinionesCongresistas: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "titulo", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Autor", text: "persona.nombres", esImg: true, img: "persona.imagenes.0.imagen", putOnFirstElement: false },
                    { title: "Fecha de publicación", text: "fechaPublicacion", esImg: false, img: "", putOnFirstElement: false },
                ],
            generalActionTitle: null,
            generalIcon: null,
            eachActionTitle: "tipo_publicacion.nombre",
            eachIcon: "tipo_publicacion.icono"
        },
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    filterCongresistas: { value: -1, label: "Filtrar por congresista" },
    filterTipoPublicacionOpinionesCongresistas: { value: -1, label: "Filtrar por tipo de publicación" },
    dataSelectOpCongresistas: [],
    dataSelectTipoPublicacion: [],
};

const AgoraConst = {
};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let AgoraData = AgoraConst;

    PageData.listOpinionesCongresistas.data = await getAllOpinionesCongresistas(
        1
        , query.congresista || PageData.filterCongresistas.value
        , query.tipoPublicacion || PageData.filterTipoPublicacionOpinionesCongresistas.value
        , query.search || PageData.listOpinionesCongresistas.search
        , query.page || PageData.listOpinionesCongresistas.page
        , query.rows || PageData.listOpinionesCongresistas.rows
    );
    PageData.listOpinionesCongresistas.totalRows = await getTotalRecordsOpinionesCongresistas(
        1
        , query.congresista || PageData.filterCongresistas.value
        , query.tipoPublicacion || PageData.filterTipoPublicacionOpinionesCongresistas.value
        , query.search || PageData.listOpinionesCongresistas.search
    );
    PageData.subloaderOpinionesCongresistas = false;
    PageData.subloaderFilters = false;

    return {
        props: {
            PageData, AgoraData, query
        }
    }
}

// Gets
const getComboCongresistaByType = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboCongresistaByType(TypeCombos.CongresistasEnOpinionesCongresista).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0];
    })

    return { DSCongresista: combo, SelectedCongresista: selected };
}

const getComboTipoPublicacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboTipoPublicacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0];

    })

    return { DSTipoPublicacion: combo, SelectedTipoPublicacion: selected };
}

const getAllOpinionesCongresistas = async (idFilterActive, congresista, tipopublicacion, search, page, rows) => {
    let data = [];
    await ContenidoMultimediaDataService.getAllOpinionesCongresistas(idFilterActive, congresista, tipopublicacion, search, page, rows).then((response) => {
        data = response.data;
        data.forEach(x => {
            x.fechaPublicacion = auth.coloquialDate(x.fechaPublicacion)
        })
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const getTotalRecordsOpinionesCongresistas = async (idFilterActive, congresista, tipopublicacion, search) => {
    let totalRows = 0;
    await ContenidoMultimediaDataService.getTotalRecordsOpinionesCongresistas(idFilterActive, congresista, tipopublicacion, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
};

// Page
const Agora = ({ PageData = PageConst, AgoraData = AgoraConst, query }) => {
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [subloaderOpinionesCongresistas, setSubloaderOpinionesCongresistas] = useState(PageData.subloaderOpinionesCongresistas);
    const [listOpinionesCongresistas, setListOpinionesCongresistas] = useState(PageData.listOpinionesCongresistas);
    const [filterCongresistas, setFilterCongresistas] = useState(PageData.filterCongresistas);
    const [filterTipoPublicacionOpinionesCongresistas, setFilterTipoPublicacionOpinionesCongresistas] = useState(PageData.filterTipoPublicacionOpinionesCongresistas);
    const [dataSelectOpCongresistas, setDataSelectOpCongresistas] = useState(PageData.dataSelectOpCongresistas);
    const [dataSelectTipoPublicacion, setDataSelectTipoPublicacion] = useState(PageData.dataSelectTipoPublicacion);
    const [search, setSearch] = useState(query.search || PageData.listOpinionesCongresistas.search);
    const [rows, setRows] = useState(query.rows || PageData.listOpinionesCongresistas.rows);
    const [page, setPage] = useState(query.page || PageData.listOpinionesCongresistas.page);

    useEffect(async () => {
        if (query.congresista) {
            let { DSCongresista, SelectedCongresista } = await getComboCongresistaByType(Number(query.congresista));
            setDataSelectOpCongresistas(DSCongresista); setFilterCongresistas(SelectedCongresista);
        }
        if (query.tipoPublicacion) {
            let { DSTipoPublicacion, SelectedTipoPublicacion } = await getComboTipoPublicacion(Number(query.tipoPublicacion));
            setDataSelectTipoPublicacion(DSTipoPublicacion); setFilterTipoPublicacionOpinionesCongresistas(SelectedTipoPublicacion);
        }
    }, []);
    // Handlers
    const handlerFilterCongresistas = async (selectCongresistas) => {
        setSubloaderOpinionesCongresistas(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('congresista', selectCongresistas.value, q)
        window.location = window.location.pathname + q
    }

    const handlerTipoPublicacionOpinionesCongresistas = async (selectTipoPublicacionCongresista) => {
        setSubloaderOpinionesCongresistas(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoPublicacion', selectTipoPublicacionCongresista.value, q)
        window.location = window.location.pathname + q
    };

    const handlerPaginationOpinionesCongresistas = async (page, rowsA, searchA = "") => {
        setSubloaderOpinionesCongresistas(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    // Others
    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
                <title>Opiniones de congresistas</title>
                <meta name="description" content="Aquí encontrarás todas las opiniones dadas por congresistas. Consulta si el congresista que buscas ha dejado una nueva opinión" />
                <meta name="keywords" content="Opinion, congresista, Congreso Colombia, Democracia, Colombia, Partidos Políticos, Proyectos de Ley, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Opiniones de congresistas" />
                <meta property="og:description" content="Aquí encontrarás todas las opiniones dadas por congresistas. Consulta si el congresista que buscas ha dejado una nueva opinión" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/agora`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/agora`} />
                <meta name="twitter:title" content="Opiniones de congresistas" />
                <meta name="twitter:description" content="Aquí encontrarás todas las opiniones dadas por congresistas. Consulta si el congresista que buscas ha dejado una nueva opinión" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/agora/" />
            </Head>
            <div className="pageTitle">
                <h1>Opinión de congresistas</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <h2>
                                    <a href={"/contenido-multimedia"}>
                                        <i className="fas fa-photo-video"></i>
                                        Multimedia
                                    </a>
                                </h2>
                            </li>
                            <li >
                                <h2>
                                    <a href={"/articulo"}>
                                        <i className="fas fa-comment-dots"></i>
                                        Artículos
                                    </a>
                                </h2>
                            </li>
                            <li className="active">

                                <h2>
                                    <a href={"/agora"}>
                                        <i className="fas fa-comments"></i>
                                        Opinión de congresistas
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/podcast"}>
                                        <i className="fas fa-microphone-alt"></i> Podcast
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/balance"}>
                                        <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/informe-territorial"}>
                                        <i className="fas fa-file-alt"></i> Informes regionales
                                    </a>
                                </h2>
                            </li>
                        </ul>
                    </div>
                    <div className="contentTab" data-ref="3">
                        <div className="relative">
                            <div className={`subloader ${subloaderOpinionesCongresistas ? "active" : ""}`}></div>
                            <div className="buscador pd-25">
                                <div className="input-group">
                                    <input
                                        type="text"
                                        value={search}
                                        onChange={async (e) => {
                                            setSearch(e.target.value)
                                        }}
                                        onKeyUp={async (e) => {
                                            if (e.key === "Enter") {
                                                await handlerPaginationOpinionesCongresistas(listOpinionesCongresistas.page, listOpinionesCongresistas.rows, e.target.value);
                                            }
                                        }}
                                        placeholder="Escriba para buscar" className="form-control" />

                                    <span className="input-group-text"><button onClick={async () => { await handlerPaginationOpinionesCongresistas(listOpinionesCongresistas.page, listOpinionesCongresistas.rows, listOpinionesCongresistas.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    <span className="input-group-text">
                                        <button
                                            onClick={async (e) => {
                                                toggleFilter(e.currentTarget);
                                            }
                                            }
                                            type="button"
                                            className="btn btn-primary"
                                        ><i className="fa fa-filter"></i></button></span>
                                </div>
                                <div className="floatingFilters evenColors">
                                    <div className="one-columns relative no-margin">
                                        <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                        <div className="item">
                                            <label htmlFor="">Filtrar por tipo de publicación</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectTipoPublicacion.length <= 1) {
                                                        let { DSTipoPublicacion } = await getComboTipoPublicacion();
                                                        setDataSelectTipoPublicacion(DSTipoPublicacion)
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterTipoPublicacionOpinionesCongresistas}
                                                selectOnchange={handlerTipoPublicacionOpinionesCongresistas}
                                                selectoptions={dataSelectTipoPublicacion}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Filtrar por congresista</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectOpCongresistas.length <= 1) {
                                                        let { DSCongresista } = await getComboCongresistaByType();
                                                        setDataSelectOpCongresistas(DSCongresista)
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterCongresistas}
                                                selectOnchange={handlerFilterCongresistas}
                                                selectoptions={dataSelectOpCongresistas}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ContenidoMultimediaList
                                propiedades={listOpinionesCongresistas.propiedades}
                                data={listOpinionesCongresistas.data}
                                handlerPagination={handlerPaginationOpinionesCongresistas}
                                defaultImage={Constantes.NoImagen}
                                link="/agora" params={["titulo", "id"]}
                                pageExtends={page}
                                totalRows={listOpinionesCongresistas.totalRows}
                                pageSize={rows}
                                pathImgOrigen={auth.pathApi()}
                                className="pd-25"
                            />
                            <CIDPagination totalPages={listOpinionesCongresistas.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/agora"} hrefQuery={{
                                tipoPublicacion: query.tipoPublicacion || filterTipoPublicacionOpinionesCongresistas.value,
                                congresista: query.congresista || filterCongresistas.value
                            }} />
                        </div>
                        {/* End Opiniones congresistas */}
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </div>
    );
}

export default Agora;
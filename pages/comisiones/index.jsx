import React, { useState, useEffect } from "react";
import Head from "next/head";
import Select from "../../Components/Select";
import UtilsDataService from "../../Services/General/Utils.Service";
import ComisionDataService from "../../Services/Catalogo/Comision.Service";
import ListComisiones from "../../Components/ListComisiones";
import CIDPagination from "../../Components/CIDPagination";
import AuthLogin from "../../Utils/AuthLogin";
import { URLBase } from "../../Constants/Constantes";

const auth = new AuthLogin();
const ComisionesConst = {
    loading: false,
    filterSelectCorporacion: { value: 0, label: "Ver cámara y senado" },
    dataSelectCorporacion: [],
    filterSelectTipoComision: { value: 0, label: "Ver todos" },
    dataSelectTipoComision: [],
    tableInfo: {
        data: [],
        totalRows: 10,
        page: 1,
        rows: 7,
        search: "",
    },
};

//SSR
export async function getServerSideProps({ query }) {
    let ComisionesData = Object.assign({}, ComisionesConst);
    ComisionesData.tableInfo = await getAll(
        query.tipoComision || ComisionesData.filterSelectTipoComision.value,
        query.page || ComisionesData.tableInfo.page,
        query.rows || ComisionesData.tableInfo.rows,
        query.search || ComisionesData.tableInfo.search,
        query.corporacion || ComisionesData.filterSelectCorporacion.value
    );

    return {
        props: {
            ComisionesData, query
        },
    };
}

const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: 0, label: "Ver Cámara y Senado" });
        if (!selected)
            selected = combo[0];
    });
    return { DSCorporacion: combo, SelectedCorporacion: selected };
};

const getComboTipoComision = async (corporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboTipoComision(corporacion).then(
        (response) => {
            response.data.forEach((i) => {
                combo.push({ value: i.id, label: i.nombre });
                if (i.id === ID) {
                    selected = { value: i.id, label: i.nombre };
                }
            });
            combo.unshift({ value: 0, label: "Ver todos" });
            if (!selected)
                selected = combo[0];
        }
    );
    return { DSTipoComision: combo, SelectedTipoComision: selected };
};

const getAll = async (idTipoComision, page, rows, search, idCorporacion) => {
    let tableInfo = Object.assign({}, ComisionesConst.tableInfo);
    await ComisionDataService.getAll(1, idCorporacion, idTipoComision, search, page, rows).then((response) => {
        tableInfo["data"] = response.data;
    })
        .catch((e) => {
            console.log(e);
        });

    await ComisionDataService.getTotalRecordsComision(1, idCorporacion, idTipoComision, search).then((response) => {
        tableInfo["totalRows"] = response.data;
    })
        .catch((e) => {
            console.log(e);
        });
    return tableInfo;
};

export default function Comisiones({ ComisionesData = ComisionesConst, query }) {
    const [loading, setLoading] = useState(false);
    const [filterCorporacion, setSelectCorporacion] = useState(ComisionesData.filterSelectCorporacion);
    const [dataSelectCorporacion, setDSCorporacion] = useState(ComisionesData.dataSelectCorporacion);
    const [filterTipoComision, setSelectTipoComision] = useState(ComisionesData.filterSelectTipoComision);
    const [dataSelectTipoComision, setDSTipoComision] = useState(ComisionesData.dataSelectTipoComision);
    const [tableInfo, setTableInfo] = useState(ComisionesData.tableInfo);

    const [search, setSearch] = useState(query.search || ComisionesData.tableInfo.search);
    const [rows, setRows] = useState(query.rows || ComisionesData.tableInfo.rows);
    const [page, setPage] = useState(query.page || ComisionesData.tableInfo.page);

    useEffect(async () => {
        if (query.corporacion) {
            let { DSCorporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
            setDSCorporacion(DSCorporacion); setSelectCorporacion(SelectedCorporacion);
        }
        if (query.tipoComision) {
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(filterCorporacion.value, Number(query.tipoComision));
            setDSTipoComision(DSTipoComision); setSelectTipoComision(SelectedTipoComision);
        }
    }, []);

    const handlerCorporacion = async (select) => {
        setLoading(true)
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', select.value, q)
        window.location = window.location.pathname + q
    };

    const handlerTipoComision = async (select) => {
        setLoading(true)
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoComision', select.value, q)
        window.location = window.location.pathname + q
    };

    const tableHandler = async (page, rowsA, searchA, isDelay) => {
        setLoading(true)
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    };

    return (
        <div>
            <Head>
                <title>Comisiones del Senado de Colombia | Congreso Visible</title>
                <meta name="description" content="Aquí podrás informarte acerca de todos los miembros que forman parte de las comisiones y de qué se encarga cada una de las comisiones existentes." />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Comisiones del Senado de Colombia | Congreso Visible" />
                <meta property="og:description" content="Aquí podrás informarte acerca de todos los miembros que forman parte de las comisiones y de qué se encarga cada una de las comisiones existentes." />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/comisiones`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/comisiones`} />
                <meta name="twitter:title" content="Comisiones del Senado de Colombia | Congreso Visible"/>
                <meta name="twitter:description" content="Aquí podrás informarte acerca de todos los miembros que forman parte de las comisiones y de qué se encarga cada una de las comisiones existentes." />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/comisiones/"/>
            </Head>
            <div className="pageTitle">
                <h1>Comisiones</h1>
            </div>
            <div className="listadoPageContainer">
                <div className="container-fluid">
                    <div className="listadoWPhotoContainer">
                        <div className={`subloader ${loading ? "active" : ""}`} />
                        <div className="row">
                            <div className="col-lg-3 col-md-12">
                                <div className="filtros-vertical evenColors">
                                    <h2>
                                        <i className="fa fa-filter" /> Filtros de
                                        información
                                    </h2>
                                    <div className="one-columns">
                                        <div className="item">
                                            <label htmlFor="">Tipo de corporación</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectCorporacion.length <= 1) {
                                                        let { DSCorporacion } = await getComboCorporacion();
                                                        setDSCorporacion(DSCorporacion);
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterCorporacion}
                                                selectoptions={dataSelectCorporacion}
                                                selectIsSearchable={true}
                                                selectOnchange={handlerCorporacion}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError=""
                                            ></Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Tipo de comisión</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectTipoComision.length <= 1) {
                                                        let { DSTipoComision } = await getComboTipoComision(filterCorporacion.value);
                                                        setDSTipoComision(DSTipoComision);
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterTipoComision}
                                                selectoptions={dataSelectTipoComision}
                                                selectIsSearchable={true}
                                                selectOnchange={handlerTipoComision}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError=""
                                            ></Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-9 col-md-12">
                                <div className="buscador">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            placeholder="Escriba para buscar"
                                            className="form-control"
                                            aria-label=""
                                            value={search}
                                            onChange={async (e) => {
                                                setSearch(e.target.value)
                                            }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await tableHandler(page, rows, e.target.value, false)
                                                }
                                            }}
                                        />
                                        <span className="input-group-text">
                                            <button
                                                onClick={async () => {
                                                    await tableHandler(page, rows, e.target.value, false)
                                                }}
                                                type="button"
                                                className="btn btn-primary"
                                            >
                                                <i className="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <ListComisiones
                                    data={tableInfo.data}
                                    handler={tableHandler}
                                    pageExtends={page}
                                    pageSizeExtends={rows}
                                    totalRows={tableInfo.totalRows}
                                    search={search}
                                    tipoComisionSelected={filterTipoComision.value}
                                />
                                <CIDPagination totalPages={tableInfo.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={`/comisiones/`} hrefQuery={{
                                    corporacion: query.corporacion || filterCorporacion.value,
                                    tipoComision: query.tipoComision || filterTipoComision.value
                                }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

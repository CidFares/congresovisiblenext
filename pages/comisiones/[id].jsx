import React, { useEffect, useState } from 'react'
import ComisionDataService from '../../Services/Catalogo/Comision.Service'
import UtilsDataService from '../../Services/General/Utils.Service'
import AuthLogin from '../../Utils/AuthLogin'
import { Constantes, URLBase } from '../../Constants/Constantes'
import ActividadesLegislativasDataService from '../../Services/ActividadesLegislativas/ActividadesLegislativas.Service'
import * as FechaMysql from '../../Utils/FormatDate'
import dynamic from 'next/dynamic'
import LittleCalendar from '../../Components/LittleCalendar'
import Select from '../../Components/Select'
import ActLegislativaEventosList from '../../Components/CongresoVisible/ActLegislativaEventosList'
import 'suneditor/dist/css/suneditor.min.css'
import Link from "next/link";
import Head from "next/head";

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const auth = new AuthLogin()

const formatDate = () => {
    let dt = new Date()
    let month = dt.getMonth();
    let day = dt.getDate();

    return dt.getFullYear() + "-" + (month < 10 ? "0" + (month + 1) : (month + 1)) + "-" + (day < 10 ? "0" + day : day)
};

const PageConst = {
    id: 0,
    searchControlPolitico: '',
    searchProyectoLey: '',
    searchMiembros: '',
    searchMesa: '',
    searchSecretarios: '',
    partidos: [],
    partidosMiembros: [],
    loading: true,
    subloaderControP: true,
    subloaderProyectoL: true,
    subloaderMiembros: true,
    subloaderMesa: true,
    subloaderSecretarios: true,
    partidoIdActive: 0,
    partidoIdActiveMesa: 0,
    imagenes: [],
    subloaderCalendario: true,
    subloaderAgendaActividades: true,
    subloaderFilters: true,
    subloader: true,
    fechaActual: formatDate(),
    year: new Date().getFullYear(),
    mesActual: new Date().getMonth() < 10 ? "0" + (new Date().getMonth() + 1) : (new Date().getMonth() + 1),
    fechaCalendario: null,
    eventos: [],
    listByFecha: {
        data: [],
        dataCalendar: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 5
    },
    //Agenda
    filterTipoActividadA: { value: -1, label: "Filtrar tipo de actividad" },
    dataSelectActividadA: [],
    filterCorporacionA: { value: -1, label: "Filtrar por corporación" },
    dataSelectCorporacionA: [],
    filterTComisionA: { value: -1, label: "Filtrar por tipo de comisión" },
    dataSelecTComisionA: [],
    filterComisionA: { value: -1, label: "Filtrar por comisión" },
    dataSelectComisionA: [],
    dataAgendaLegislativa: [],
    imageOg: Constantes.NoImagen,
}

const ComisionConst = {
    id: 0,
    tipo_comision_id: 0,
    corporacion_id: 0,
    nombre: '',
    descripcion: '',
    imagen: null,
    datosContacto: [
        {
            id2: 0,
            dato_contacto_id: null,
            comision_id: null,
            cuenta: null,
            activo: 1
        }
    ],
    comision_miembro: [],
    comision_mesa: [],
    comision_secretario: [],
    tipo_comision: [],
    corporacion: [],
    proyecto_ley_comision: [],
    control_politico: [],
    comision_imagen: [],
    comision_datos_contacto: []
}

// SSR
export async function getServerSideProps({ query, req }) {
    let PageData = PageConst;
    let ComisionData = ComisionConst;
    PageData.id = query.id;
    ComisionData = await getByID(PageData.id);
    PageData.imagenes = getImagen(ComisionData);
    PageData.imageOg = getImagenOg(ComisionData);
    PageData.partidosMiembros = await getComboPartido(PageData.id);
    PageData.partidos = await getComboPartidoMesa(PageData.id);
    ComisionData.proyecto_ley_comision = await getProyectoLeyFilter(PageData.searchProyectoLey, PageData.id);
    ComisionData.control_politico = await getControlPoliticoFilter(PageData.searchControlPolitico, PageData.id);
    ComisionData.comision_miembro = await getMiembrosFilter(PageData.partidoIdActive, PageData.searchMiembros, PageData.id);
    ComisionData.comision_mesa = await getMesaFilter(PageData.partidoIdActiveMesa, PageData.searchMesa, PageData.id);
    ComisionData.comision_secretario = await getSecretariosFilter(PageData.searchSecretarios, PageData.id);
    PageData.dataSelectActividadA = await getTipoActividad();
    PageData.loading = false;
    PageData.subloaderControP = false;
    PageData.subloaderProyectoL = false;
    PageData.subloaderMiembros = false;
    PageData.subloaderMesa = false;
    PageData.subloaderSecretarios = false;
    PageData.subloaderCalendario = false;
    PageData.subloaderAgendaActividades = false;
    PageData.subloaderFilters = false;
    PageData.subloader = false;

    return {
        props: {
            PageData, ComisionData
        }
    }
}

// Gets
const getImagenOg = (fields) => {
    let imagen = '';
    if (fields.comision_imagen.length > 0) {
        if (fields.comision_imagen[0] !== undefined) {
            imagen = auth.pathApi() + fields.comision_imagen[0].imagen;
        }
        else
            imagen = Constantes.NoImagen;
    }
    else
        imagen = Constantes.NoImagen;

    return imagen;
}

const getTipoActividad = async () => {
    let combo = [];
    await ActividadesLegislativasDataService.getComboTipoActividadAgenda().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
        })
        combo.unshift({ value: -1, label: "Ver todas" })
    })

    return combo;
}

const getSecretariosFilter = async (searchSecretarios, id) => {
    let comision_secretario = [];
    await ComisionDataService.getSecretariosFilter(searchSecretarios, id)
        .then((response) => {
            comision_secretario = response.data;
        }).catch((e) => {
            console.log(e);
        });

    return comision_secretario;
}

const getMesaFilter = async (partidoMesaId, searchMesa, id) => {
    let comision_mesa = [];
    await ComisionDataService.getMiembrosFilter(searchMesa, id, partidoMesaId, 1).then((response) => {
        comision_mesa = response.data;
    }).catch((e) => {
        console.log(e);
    });

    return comision_mesa;
}

const getMiembrosFilter = async (partidoId, searchMiembros, id) => {
    let comision_miembro = [];

    await ComisionDataService.getMiembrosFilter(searchMiembros, id, partidoId, 0)
        .then((response) => {
            comision_miembro = response.data;
        }).catch((e) => {
            console.log(e);
        });

    return comision_miembro;
}

const getControlPoliticoFilter = async (searchControlPolitico, id) => {
    let control_politico = [];
    await ComisionDataService.getControlPoliticoFilter(searchControlPolitico, id)
        .then((response) => {
            control_politico = response.data;
        }).catch((e) => {
            console.log(e);
        });
    return control_politico;
}

const getProyectoLeyFilter = async (searchProyectoLey, id) => {
    let proyecto_ley_comision = [];
    await ComisionDataService.getProyectoLeyFilter(searchProyectoLey, id)
        .then((response) => {
            proyecto_ley_comision = response.data;
        }).catch((e) => {
            console.log(e);
        });

    return proyecto_ley_comision;
}

const getComboPartidoMesa = async (idcomision) => {
    let combo = [];

    await UtilsDataService.getComboPartidoPorCongresistaEnComision(idcomision, 1).then(response => {
        combo = response.data;
    })

    return combo;
}

const getComboPartido = async (idcomision) => {
    let combo = [];
    await UtilsDataService.getComboPartidoPorCongresistaEnComision(idcomision, 0).then(response => {
        combo = response.data;
    })
    return combo;
}

const getByID = async (id) => {
    let fields;
    await ComisionDataService.get(id)
        .then((response) => {
            fields = response.data[0];
            fields.comision_mesa = [];
        })
        .catch((e) => {
            console.log(e);
        });

    return fields;
};

const getImagen = (fields) => {
    let imagen = '';
    if (fields.comision_imagen.length > 0) {
        if (fields.comision_imagen[1] !== undefined) {
            imagen = auth.pathApi() + fields.comision_imagen[1].imagen;
        }
        else
            imagen = Constantes.NoImagen;
    }
    else
        imagen = Constantes.NoImagen;

    return imagen;
}

const DetalleComision = ({ PageData = PageConst, ComisionData = ComisionConst }) => {
    const [currentUrl, setCurrentUrl] = useState(PageData.currentUrl);
    const [imageOg, setImageOg] = useState(PageData.imageOg);
    const [comision, setComision] = useState(ComisionData);
    const [id, setId] = useState(PageData.id);
    const [searchControlPolitico, setSearchControlPolitico] = useState(PageData.searchControlPolitico);
    const [searchProyectoLey, setSearchProyectoLey] = useState(PageData.searchProyectoLey);
    const [searchMiembros, setSearchMiembros] = useState(PageData.searchMiembros);
    const [searchMesa, setSearchMesa] = useState(PageData.searchMesa);
    const [searchSecretarios, setSearchSecretarios] = useState(PageData.searchSecretarios);
    const [partidos, setPartidos] = useState(PageData.partidos);
    const [partidosMiembros, setPartidosMiembros] = useState(PageData.partidosMiembros);
    const [loading, setLoading] = useState(PageData.loading);
    const [subloaderControP, setSubloaderControP] = useState(PageData.subloaderControP);
    const [subloaderProyectoL, setSubloaderProyectoL] = useState(PageData.subloaderProyectoL);
    const [subloaderMiembros, setSubloaderMiembros] = useState(PageData.subloaderMiembros);
    const [subloaderMesa, setSubloaderMesa] = useState(PageData.subloaderMesa);
    const [subloaderSecretarios, setSubloaderSecretario] = useState(PageData.subloaderSecretarios);
    const [partidoIdActive, setPartidoIdActive] = useState(PageData.partidoIdActive);
    const [partidoIdActiveMesa, setPartidoIdActiveMesa] = useState(PageData.partidoIdActiveMesa);
    const [imagenes, setImagenes] = useState(PageData.imagenes);
    const [subloaderCalendario, setSubloaderCalendario] = useState(PageData.subloaderCalendario);
    const [subloaderAgendaActividades, setSubloaderAgendaActividades] = useState(PageData.subloaderAgendaActividades);
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [fechaActual, setFechaActual] = useState(PageData.fechaActual);
    const [year, setYear] = useState(PageData.year);
    const [mesActual, setMesActual] = useState(PageData.mesActual);
    const [fechaCalendario, setFechaCalendario] = useState(PageData.fechaCalendario);
    const [eventos, setEventos] = useState(PageData.eventos);
    const [listByFecha, setListByFecha] = useState(PageData.listByFecha);
    const [subloader, setSubloader] = useState(PageData.subloader);
    // Agenda
    const [filterTipoActividadA, setFilterTipoActividadA] = useState(PageData.filterTipoActividadA);
    const [dataSelectActividadA, setDataSelectActividadA] = useState(PageData.dataSelectActividadA);
    const [filterCorporacionA, setFilterCorporacionA] = useState(PageData.filterCorporacionA);
    const [dataSelectCorporacionA, setDataSelectCorporacionA] = useState(PageData.dataSelectCorporacionA);
    const [filterTComisionA, setFilterTComisionA] = useState(PageData.filterTComisionA);
    const [dataSelecTComisionA, setDataSelecTComisionA] = useState(PageData.dataSelecTComisionA);
    const [filterComisionA, setFilterComisionA] = useState(PageData.filterComisionA);
    const [dataAgendaLegislativa, setDataAgendaLegislativa] = useState(PageData.dataAgendaLegislativa);

    // Init
    useEffect(async () => {
        if (findCalendarElements()) {
            let prevButton = document.querySelector(".Calendar__monthArrowWrapper.-right");
            let nextButton = document.querySelector(".Calendar__monthArrowWrapper.-left");
            let monthsButton = document.querySelectorAll(".Calendar__monthSelectorItemText");
            let yearsButton = document.querySelectorAll(".Calendar__yearSelectorText");
            prevButton.addEventListener("click", async (e) => {
                e.preventDefault();
                e.target.disabled = true;
                await handlerPrevAgenda(e);
                e.target.disabled = false;
            });
            nextButton.addEventListener("click", async (e) => {
                e.preventDefault();
                e.target.disabled = true;
                await handlerNextAgenda(e)
                e.target.disabled = false;
            });
            monthsButton.forEach(month => { month.addEventListener("click", (e) => { handlerChooseMonth(e) }) })
            yearsButton.forEach(year => { year.addEventListener("click", (e) => { handlerChooseYear(e) }) })
        }

        await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fechaActual, filterTipoActividadA.value);
        await getDataByYearAndMonth(year, mesActual);
    }, [])

    // Gets
    const getAgendaLegislativa = async (idFilterActive, search, page, rows, fecha, idactividad) => {
        setSubloaderAgendaActividades(true);
        let listByFechaAux = listByFecha;
        let f = FechaMysql.DateTimeFormatMySql(fecha + ' ' + '00:00:00');
        await ComisionDataService.getAgendaLegislativaByFecha(
            idFilterActive,
            search, page, rows, f, idactividad, id, comision.corporacion_id)
            .then((response) => {
                listByFechaAux.data = response.data;
            })
            .catch((e) => {
                console.error(e);
            });

        await ComisionDataService.getTotalRecordsAgendaActividad(
            idFilterActive,
            search, f, idactividad, id, comision.corporacion_id)
            .then((response) => {
                listByFechaAux.totalRows = response.data;
            })
            .catch((e) => {
                console.error(e);
            });

        setSubloaderAgendaActividades(false);
        setListByFecha(listByFechaAux);
    }

    const getDataByYearAndMonth = async (year, month) => {
        setSubloaderCalendario(true);
        let listByFechaAux = listByFecha;
        await ComisionDataService.getDataByYearAndMonth(year, month, filterTipoActividadA.value, id, comision.corporacion_id)
            .then((response) => {
                let data = response.data;
                let dataCalendar = [];
                let f;
                data.forEach(x => {
                    f = x.fecha.slice(0, 10);
                    dataCalendar.push({ year: Number(f.split("-")[0]), month: Number(f.split("-")[1]), day: Number(f.split("-")[2]), className: 'tieneEventos' });
                });
                listByFechaAux.dataCalendar = dataCalendar;
            })
            .catch((e) => {
                console.error(e);
            });

        setListByFecha(listByFechaAux);
        setSubloaderCalendario(false);
    }

    // Handlers
    const handlerNextAgenda = async (e) => {
        setSubloaderCalendario(true);
        let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
        let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
        let month = getMonthNumberByStr(prevNextMonth(monthTemp, true));
        let year = prevNextMonth(monthTemp, true) === "enero" ? Number(yearTemp) + 1 : Number(yearTemp)
        await getDataByYearAndMonth(year, month);
        setSubloaderCalendario(false);
        setYear(year);
        setMonth(month);
    }

    const handlerPrevAgenda = async (e) => {
        setSubloaderCalendario(true);
        let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
        let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
        let month = getMonthNumberByStr(prevNextMonth(monthTemp));
        let year = prevNextMonth(monthTemp) === "diciembre" ? Number(yearTemp) - 1 : Number(yearTemp)
        await getDataByYearAndMonth(year, month);
        setSubloaderCalendario(false);
        setYear(year);
        setMesActual(month);
    }

    const handlerPaginationAgendaActividades = async (page, rows, search = "") => {
        let listByFechaAux = listByFecha;
        listByFechaAux.page = page;
        listByFechaAux.rows = rows;
        listByFechaAux.search = search;
        setListByFecha(listByFechaAux);

        await getAgendaLegislativa(
            1,
            search,
            page,
            rows,
            fechaActual,
            filterTipoActividadA.value,
            id
        )
    }

    const handlerFilterActividadAgenda = async (selectActividad) => {
        setFilterTipoActividadA(selectActividad);
        await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fechaActual, selectActividad.value);
    }

    // Handlers Calendario
    const handlerDataSelectDay = async (e) => {
        let fecha = dateCalendar(e);
        setSubloader(true);
        setFechaActual(fecha);
        await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fecha);
        setSubloader(false);
        setFechaCalendario(e);
    }

    const handlerChooseMonth = async (e) => {
        let monthAux = getMonthNumberByStr(e.currentTarget.innerText);
        setMesActual(month)
        await getDataByYearAndMonth(year, monthAux);
    }

    const handlerChooseYear = async (e) => {
        let yearAux = Number(e.currentTarget.innerText);
        setYear(year)
        await getDataByYearAndMonth(yearAux, mesActual);
    }

    // Others
    const dateCalendar = (e) => {
        return e.year + "-" + (e.month < 10 ? "0" + e.month : e.month) + "-" + (e.day < 10 ? "0" + e.day : e.day)
    }

    const getImgComision = () => {
        let item = comision;
        if (item.comision_imagen.length > 0) {
            let img = item.comision_imagen[1];
            if (img !== undefined) {
                return auth.pathApi() + img.imagen;
            }
            else {
                return Constantes.NoImagenPicture;

            }
        }
        else {
            return Constantes.NoImagenPicture;
        }
    }

    const findCalendarElements = async () => {
        let valido = true;
        if (document.querySelector(".Calendar__monthArrowWrapper"))
            valido = false;
        if (document.querySelector(".Calendar__monthSelectorItemText"))
            valido = false;
        if (document.querySelector(".Calendar__yearSelectorText"))
            valido = false;

        return valido;
    }

    const mouseDownHandler = function (e) {
        let pos = {
            left: e.currentTarget.scrollLeft,
            top: e.currentTarget.scrollTop,
            // Get the current mouse position
            x: e.clientX,
            y: e.clientY,
        };

        e.currentTarget.addEventListener('mousemove', mouseMoveHandler);
        e.currentTarget.addEventListener('mouseup', mouseUpHandler);
    };

    const mouseMoveHandler = function (e) {
        // How far the mouse has been moved
        const dx = e.clientX - pos.x;
        const dy = e.clientY - pos.y;

        // Scroll the element
        e.currentTarget.scrollTop = pos.top - dy;
        e.currentTarget.scrollLeft = pos.left - dx;
    };

    const mouseUpHandler = function (e, curul) {
        e.currentTarget.removeEventListener('mousemove', mouseMoveHandler);
        e.currentTarget.removeEventListener('mouseup', mouseUpHandler);
    };

    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    const getMonthNumberByStr = (str) => {
        let month = str.toString().toLowerCase();
        switch (month) {
            case 'enero':
                return 1
                break;
            case 'febrero':
                return 2
                break;
            case 'marzo':
                return 3
                break;
            case 'abril':
                return 4
                break;
            case 'mayo':
                return 5
                break;
            case 'junio':
                return 6
                break;
            case 'julio':
                return 7
                break;
            case 'agosto':
                return 8
                break;
            case 'septiembre':
                return 9
                break;
            case 'octubre':
                return 10
                break;
            case 'noviembre':
                return 11
                break;
            case 'diciembre':
                return 12
                break;
            default:
                break;
        }
    }

    const prevNextMonth = (str, next = false) => {
        str = str.toString().toLowerCase();
        switch (str) {
            case 'enero':
                return !next ? "diciembre" : "febrero"
                break;
            case 'febrero':
                return !next ? "enero" : "marzo"
                break;
            case 'marzo':
                return !next ? "febrero" : "abril"
                break;
            case 'abril':
                return !next ? "marzo" : "mayo"
                break;
            case 'mayo':
                return !next ? "abril" : "junio"
                break;
            case 'junio':
                return !next ? "mayo" : "julio"
                break;
            case 'julio':
                return !next ? "junio" : "agosto"
                break;
            case 'agosto':
                return !next ? "julio" : "septiembre"
                break;
            case 'septiembre':
                return !next ? "agosto" : "octubre"
                break;
            case 'octubre':
                return !next ? "septiembre" : "noviembre"
                break;
            case 'noviembre':
                return !next ? "octubre" : "diciembre"
                break;
            case 'diciembre':
                return !next ? "noviembre" : "enero"
                break;
            default:
                break;
        }
    }

    return (
        <>
        <Head>
            <title>Comision {comision.nombre} {comision.corporacion.id==1?"de representantes ":"de la republica"} | Congreso Visible</title>
            <meta name="description" content={auth.filterStringHTML(comision.descripcion).slice(0, 160)} />
            <meta name="keywords" content= {`${comision.nombre}, ${comision.corporacion.nombre}, Congreso, Senado, Congresistas, Miembros, Proyectos de Ley, Votaciones, Orden del día, Agenda Legislativa, Citaciones, Debates de Control Político`} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={`${comision.nombre} | Congreso Visible`} />
            <meta property="og:description" content={auth.filterStringHTML(comision.descripcion).slice(0, 160)} />
            <meta property="og:image" content={`${imageOg}`} />
            <meta property="og:image:width" content="828" />
            <meta property="og:image:height" content="450" />
            <meta property="og:url" content={URLBase} />
            <meta name="twitter:url" content={URLBase} />
            <meta name="twitter:title" content={`${comision.nombre} | Congreso Visible`}/>
            <meta name="twitter:description" content={auth.filterStringHTML(comision.descripcion).slice(0, 160)} />
            <meta name="twitter:image" content={`${imageOg}`} />
            <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/comisiones/${comision.id}`}/>
        </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{
                backgroundImage: `url('${getImgComision()}')`
            }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-university"></i></div>
                    <div className="CVBannerTitle">
                        <h1>{comision.nombre}</h1>
                    </div>
                </div>
            </section>
            <section gtdtarget="1" className="DetalleComisionSection text-justify no-full-height">
                <div className="container">
                    <div className={`subloader ${loading ? "active" : ""}`}></div>
                    <div className="row">
                        <div className="col-lg-8 col-md-12">
                            <div className="two-columns">
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-university"></i></div>
                                    <div className="vertical-text">
                                        <small>Tipo de comisión</small>
                                        <p>{!loading ? comision.tipo_comision.nombre : ""}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-university"></i></div>
                                    <div className="vertical-text">
                                        <small>Origen</small>
                                        <p>{!loading ? comision.corporacion.nombre : ""}</p>
                                    </div>
                                </div>
                            </div>

                            <hr />
                            <h2>Información de la comisión</h2>
                            <hr />
                            <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: comision.descripcion || 'Sin resumen' }}></div>
                            <div className="row">
                                <div className="col-lg-12 col-md-12">
                                    <h3>Proyectos de ley</h3>
                                    <div className="miembrosContainer">
                                        <div className={`subloader ${subloaderProyectoL ? "active" : ""}`}></div>
                                        <div className="buscador">
                                            <div className="input-group">
                                                <input
                                                    type="text"
                                                    placeholder="Escriba para buscar"
                                                    className="form-control"
                                                    onBlur={e => setSearchProyectoLey(e.target.value)}
                                                    onKeyUp={async (e) => {
                                                        if (e.key === "Enter") {
                                                            setSubloaderProyectoL(true);
                                                            let proyecto_ley_comision_aux = await getProyectoLeyFilter(searchProyectoLey, id);
                                                            setComision({ ...comision, proyecto_ley_comision: proyecto_ley_comision_aux });
                                                            setSubloaderProyectoL(false);
                                                        }
                                                    }}
                                                />
                                                <span className="input-group-text">
                                                    <button
                                                        onClick={async () => {
                                                            setSubloaderProyectoL(true);
                                                            let proyecto_ley_comision_aux = await getProyectoLeyFilter(searchProyectoLey, id);
                                                            setComision({ ...comision, proyecto_ley_comision: proyecto_ley_comision_aux });
                                                            setSubloaderProyectoL(false);
                                                        }}
                                                        type="button"
                                                        className="btn btn-primary"
                                                    ><i className="fa fa-search"></i></button></span>
                                            </div>
                                        </div>
                                        <div className="miembros">
                                            {
                                                !subloaderProyectoL ?
                                                    (comision.proyecto_ley_comision.length > 0 ?
                                                        comision.proyecto_ley_comision.map((item, i) => {
                                                            let href = auth.filterStringForURL(item.titulo)
                                                            return (
                                                                <a key={i} href={`/proyectos-de-ley/p${href}/${item.proyecto_ley_id}`}>
                                                                    <div className="item no-photo" key={i}>
                                                                        <div className="info">
                                                                            <div className="name strong"><p>{item.titulo || ""}</p></div>
                                                                            <div className="job"><p>{`No. Proyecto en Cámara: ${item.numero_camara === null ? 'sin numero' : item.numero_camara} - No. Proyecto en Senado: ${item.numero_senado === null ? 'sin numero' : item.numero_senado}`}</p></div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            );
                                                        })
                                                        :
                                                        <div className="item no-photo">
                                                            <div className="info">
                                                                <div className="name strong"><p>Sin proyectos de ley vinculados</p></div>
                                                            </div>
                                                        </div>
                                                    )
                                                    : ""
                                            }
                                        </div>
                                        <hr />

                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-12">
                                    <h3>Control político</h3>
                                    <div className="miembrosContainer">
                                        <div className={`subloader ${subloaderControP ? "active" : ""}`}></div>
                                        <div className="buscador">
                                            <div className="input-group">
                                                <input
                                                    type="text"
                                                    placeholder="Escriba para buscar"
                                                    className="form-control"
                                                    onBlur={e => setSearchControlPolitico(e.target.value)}
                                                    onKeyUp={async (e) => {
                                                        if (e.key === "Enter") {
                                                            setSubloaderControP(true);
                                                            let control_politico_aux = await getControlPoliticoFilter(searchControlPolitico, id);
                                                            setComision({ ...comision, control_politico: control_politico_aux });
                                                            setSubloaderControP(false);
                                                        }
                                                    }}
                                                />
                                                <span className="input-group-text">
                                                    <button
                                                        onClick={
                                                            async () => {
                                                                setSubloaderControP(true);
                                                                let control_politico_aux = await getControlPoliticoFilter(searchControlPolitico, id);
                                                                setComision({ ...comision, control_politico: control_politico_aux });
                                                                setSubloaderControP(true);
                                                            }
                                                        }
                                                        type="button" className="btn btn-primary">
                                                        <i className="fa fa-search"></i></button></span>
                                            </div>
                                        </div>
                                        <div className="miembros">
                                            {
                                                !subloaderControP ?
                                                    (comision.control_politico.length > 0 ?
                                                        comision.control_politico.map((item, i) => {
                                                            let href = auth.filterStringForURL(item.titulo)
                                                            return (
                                                                    <a key={i} href={`/citaciones/${href}/${item.id}`}>
                                                                        <div className="item no-photo">
                                                                            <div className="info">
                                                                                <div className="name strong"><p>{item.titulo}</p></div>
                                                                                <div className="job"><p>{`Estado: ${item.estado_control_politico?.nombre || 'No disponible'}`}</p>
                                                                                    <p>{`Fecha: ${item.fecha || 'No disponible'}`}</p></div>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                            );
                                                        })
                                                        :
                                                        <div className="item no-photo">
                                                            <div className="info">
                                                                <div className="name strong"><p>Sin asuntos de CP asignados</p></div>
                                                            </div>
                                                        </div>
                                                    )
                                                    : ""
                                            }
                                        </div>
                                        <hr />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-12">
                            <div className="miembrosContainer">
                                <div className={`subloader ${subloaderMesa ? "active" : ""}`}></div>
                                <div className="miniCarousel">
                                    <div className="row flex-nowrap scroll-y" onMouseDown={(e) => { mouseDownHandler(e) }}>
                                        {
                                            partidos.map((item, i) => {
                                                return (
                                                    <button type="button"
                                                        key={i}
                                                        className={item.id === partidoIdActiveMesa ? "active" : ""}
                                                        onClick={async () => {
                                                            setSubloaderMesa(true);
                                                            let partidoIdActiveMesaAux = partidoIdActiveMesa === item.id ? 0 : item.id;
                                                            let comision_mesa_aux = await getMesaFilter(partidoIdActiveMesaAux, searchMesa, id);
                                                            setComision({ ...comision, comision_mesa: comision_mesa_aux });
                                                            setPartidoIdActiveMesa(partidoIdActiveMesaAux);
                                                            setSubloaderMesa(false);
                                                        }
                                                        }
                                                    >
                                                        <img draggable="false" src={typeof item.partido_imagen[1] !== "undefined" ? auth.pathApi() + item.partido_imagen[1].imagen : Constantes.NoImagenPicture} alt={item.nombre} />
                                                    </button>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="miembros">
                                    <h4>Mesa directiva</h4>
                                    {!subloaderMesa ?
                                        comision.comision_mesa.map((item, i) => {
                                            if (item.cargo_legislativo_id_comision !== null && (item.cargo_legislativo_id_comision === 1 || item.cargo_legislativo_id_comision === 2)) {
                                                let href = auth.filterStringForURL(`${item.congresista_cliente.persona_comisiones.nombres} ${item.congresista_cliente.persona_comisiones.apellidos}`)
                                                return (
                                                    <a key={i} href={`/congresistas/perfil/${href}/${item.congresista_cliente.persona_comisiones.id}`}>
                                                        <div className="item" key={item.id}>
                                                            <div className="photo">
                                                                <img src={typeof item.congresista_cliente.persona_comisiones.imagenes[1] !== "undefined" ? auth.pathApi() + item.congresista_cliente.persona_comisiones.imagenes[1].imagen : Constantes.NoImagen} alt={item.congresista_cliente.persona_comisiones.nombres} />
                                                            </div>
                                                            <div className="subphoto">
                                                                {
                                                                    typeof item.congresista_cliente.detalle_comisiones.partido !== "undefined" ?
                                                                        <img src={typeof item.congresista_cliente.detalle_comisiones.partido.partido_imagen[0] !== "undefined" ? auth.pathApi() + item.congresista_cliente.detalle_comisiones.partido.partido_imagen[0].imagen : Constantes.NoImagenPicture} alt="partido" />
                                                                        :
                                                                        <img src={Constantes.NoImagenPicture} alt="partido" />
                                                                }
                                                            </div>
                                                            <div className="info">
                                                                <div className="name"><p>{`${item.congresista_cliente.persona_comisiones.nombres} ${item.congresista_cliente.persona_comisiones.apellidos}`}</p></div>
                                                                <div className="job"><p>{item.comision_cargo_congresista.nombre}</p></div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                );
                                            }
                                        })
                                        : ""
                                    }
                                </div>
                            </div>
                            <hr />
                            <div className="miembrosContainer">
                                <div className={`subloader ${subloaderMiembros ? "active" : ""}`}></div>
                                <div className="miniCarousel">
                                    <div className="row flex-nowrap scroll-y" onMouseDown={(e) => { mouseDownHandler(e) }}>
                                        {
                                            partidosMiembros.map((item, i) => {
                                                return (
                                                    <button type="button"
                                                        key={i}
                                                        className={item.id === partidoIdActive ? "active" : ""}
                                                        onClick={async () => {
                                                            setSubloaderMiembros(true);
                                                            let partidoIdActiveAux = partidoIdActive === item.id ? 0 : item.id;
                                                            let comision_miembro_aux = await getMiembrosFilter(searchMiembros, id);
                                                            setPartidoIdActiveMesa(partidoIdActiveAux);
                                                            setComision({ ...comision, comision_miembro: comision_miembro_aux });
                                                            setSubloaderMiembros(false);
                                                        }
                                                        }
                                                    >
                                                        <img draggable="false" src={typeof item.partido_imagen[1] !== "undefined" ? auth.pathApi() + item.partido_imagen[1].imagen : Constantes.NoImagenPicture} alt={item.nombre} />
                                                    </button>
                                                );
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="miembros">
                                    <h4>Miembros</h4>
                                    {!subloaderMesa ?
                                        comision.comision_mesa.map((item, i) => {
                                            if (item.cargo_legislativo_id_comision !== null && (item.cargo_legislativo_id_comision === 3)) {
                                                let href = auth.filterStringForURL(`${item.congresista_cliente.persona_comisiones.nombres} ${item.congresista_cliente.persona_comisiones.apellidos}`)
                                                return (

                                                        <a key={i} href={`/congresistas/perfil/${href}/${item.congresista_cliente.persona_comisiones.id}`}>
                                                            <div className="item" key={item.id}>
                                                                <div className="photo">
                                                                    <img src={typeof item.congresista_cliente.persona_comisiones.imagenes[1] !== "undefined" ? auth.pathApi() + item.congresista_cliente.persona_comisiones.imagenes[1].imagen : Constantes.NoImagen} alt={item.congresista_cliente.persona_comisiones.nombres} />
                                                                </div>
                                                                <div className="subphoto">
                                                                    {
                                                                        typeof item.congresista_cliente.detalle_comisiones.partido !== "undefined" ?
                                                                            <img src={typeof item.congresista_cliente.detalle_comisiones.partido.partido_imagen[0] !== "undefined" ? auth.pathApi() + item.congresista_cliente.detalle_comisiones.partido.partido_imagen[0].imagen : Constantes.NoImagenPicture} alt="partido" />
                                                                            :
                                                                            <img src={Constantes.NoImagenPicture} alt="partido" />
                                                                    }
                                                                </div>
                                                                <div className="info">
                                                                    <div className="name"><p>{`${item.congresista_cliente.persona_comisiones.nombres} ${item.congresista_cliente.persona_comisiones.apellidos}`}</p></div>
                                                                    <div className="job"><p>{item.comision_cargo_congresista.nombre}</p></div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                );
                                            }
                                        })
                                        : ""
                                    }
                                </div>
                            </div>
                            <hr />
                            <div className="miembrosContainer">
                                <div className={`subloader ${subloaderSecretarios ? "active" : ""}`}></div>
                                <div className="miembros">
                                    <h4>Secretaría</h4>
                                    {
                                        !subloaderSecretarios ?
                                            comision.comision_secretario.map((item, i) => {
                                                return (
                                                    <div key={i} className="item">
                                                        <div className="photo">
                                                            <img src={typeof item.persona.imagenes[1] !== "undefined" ? auth.pathApi() + item.persona.imagenes[1].imagen : Constantes.NoImagen} alt={item.persona.nombres} />
                                                        </div>
                                                        <div className="info">
                                                            <div className="name"><p>{`${item.persona.nombres} ${item.persona.apellidos}`}</p></div>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                            : ""
                                    }
                                </div>
                            </div>
                            <h3>Datos de contacto</h3>
                            <hr />
                            <div className="social-links text-left circle-icons">
                                {
                                    comision.comision_datos_contacto.map((item, i) => {
                                        if (item.datos_contacto.tipo === 2) {
                                            return (
                                                <a key={i} href={item.cuenta}>
                                                    <div className="icon">
                                                        <img src={typeof item.datos_contacto.datos_contacto_imagen[0] !== 'undefined' ? auth.pathApi() + item.datos_contacto.datos_contacto_imagen[0].imagen : ""} alt="" />
                                                    </div>
                                                    <p>{item.datos_contacto.nombre}</p>
                                                </a>
                                            );
                                        } else {
                                            return (
                                                <div key={i}>
                                                    <div className="icon">
                                                        <img src={typeof item.datos_contacto.datos_contacto_imagen[0] !== 'undefined' ? auth.pathApi() + item.datos_contacto.datos_contacto_imagen[0].imagen : ""} alt="" />
                                                    </div>
                                                    <p>{item.cuenta}</p>
                                                </div>
                                            )
                                        }
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="no-full-height agendaSection">
                <div className="calendarContainerCV">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-5">
                                <div className="relative">
                                    <div className={`subloader ${subloaderCalendario ? "active" : ""}`}></div>
                                    <LittleCalendar value={fechaCalendario} onChange={(e) => { handlerDataSelectDay(e) }} customDaysClassName={listByFecha.dataCalendar} />
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div className="buscador pd-25">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            onBlur={async (e) => {
                                                let data = listByFecha;
                                                data.search = e.target.value;
                                                setListByFecha(data);
                                            }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, e.target.value)
                                                }
                                            }}
                                            placeholder="Escriba para buscar" className="form-control" />

                                        <span className="input-group-text"><button onClick={async () => { await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, listByFecha.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                        <span className="input-group-text"><button onClick={(e) => { toggleFilter(e.currentTarget); }} type="button" className="btn btn-primary"><i className="fa fa-filter"></i></button></span>
                                    </div>
                                    <div className="floatingFilters evenColors">
                                        <div className="one-columns relative no-margin">
                                            <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                            <div className="item">
                                                <label htmlFor="">Filtrar por Tipo de actividad</label>
                                                <Select
                                                    divClass=""
                                                    selectplaceholder="Seleccione"
                                                    selectValue={filterTipoActividadA}
                                                    selectOnchange={handlerFilterActividadAgenda}
                                                    selectoptions={dataSelectActividadA}
                                                    selectIsSearchable={true}
                                                    selectclassNamePrefix="selectReact__value-container"
                                                    spanClass=""
                                                    spanError="" >
                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="relative">
                                    <div className={`subloader ${subloaderAgendaActividades ? "active" : ""}`}></div>
                                    <ActLegislativaEventosList
                                        data={listByFecha.data}
                                        handler={handlerPaginationAgendaActividades}
                                        pageExtends={listByFecha.page}
                                        pageSize={listByFecha.rows}
                                        totalRows={listByFecha.totalRows}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )

}

export default DetalleComision

import Head from 'next/head'
import LittleCalendar from "../../Components/LittleCalendar";
import ActLegislativaEventosList from "../../Components/CongresoVisible/ActLegislativaEventosList";
import Select from '../../Components/Select';
import Image from 'next/image'
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
import { useState, useEffect } from "react";
import { Constantes,URLBase } from "../../Constants/Constantes.js";
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import infoSitioDataService from "../../Services/General/informacionSitio.Service";
import * as FechaMysql from "../../Utils/FormatDate";

const auth = new AuthLogin();
const formatDate = () => {
    let dt = new Date()
    let month = dt.getMonth();
    let day = dt.getDate();

    return dt.getFullYear() + "-" + (month < 10 ? "0" + (month + 1) : (month + 1)) + "-" + (day < 10 ? "0" + day : day)
}
const dateCalendar = (e) => {
    return e.year + "-" + (e.month < 10 ? "0" + e.month : e.month) + "-" + (e.day < 10 ? "0" + e.day : e.day)
}
const PageConst = { imgPrincipal: null, subloader: false }
const AgendaConst = {
    subloaderCalendario: false,
    subloaderAgendaActividades: false,
    fechaActual: formatDate(),
    year: 2022,
    month: 1,
    fechaCalendario: null,
    eventos: [],
    listByFecha: {
        data: [],
        dataCalendar: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 8
    },
    filterTipoActividadA: { value: -1, label: "Ver todas" },
    dataSelectActividadA: [],
    filterCorporacionA: { value: -1, label: "Ver Cámara y Senado" },
    dataSelectCorporacionA: [],
    filterTComisionA: { value: -1, label: "Elija un tipo de comisión" },
    dataSelecTComisionA: [],
    filterComisionA: { value: -1, label: "Ver todas" },
    dataSelectComisionA: [],
    subloaderFilters: false
}

// SSR
export async function getServerSideProps({ query }) {

    let PageData = PageConst;
    let AgendaData = AgendaConst;

    await infoSitioDataService.getInformacionSitioHome().then((response) => {
        PageData.imgPrincipal = response.data[0].imgPrincipal;
    })
        .catch((e) => {
            console.error(e);
        });
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;

    AgendaData.listByFecha.dataCalendar = await getDataByYearAndMonth(year, month);

    let fechaServer = FechaMysql.DateTimeFormatMySql(AgendaData.fechaActual + ' ' + '00:00:00');
    AgendaData.listByFecha = await getAgendaLegislativa(1, AgendaData.listByFecha.search, AgendaData.listByFecha.page, AgendaData.listByFecha.rows, fechaServer, AgendaData.filterTipoActividadA.value, AgendaData.filterCorporacionA.value, AgendaData.filterComisionA.value);
    return {
        props: { PageData, AgendaData }
    }
}

// PETICIONES

const getComboCorporacion = async () => {
    let combo = [];
    await ActividadesLegislativasDataService.getComboCorporacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
        })
        combo.unshift({ value: -1, label: "Ver Cámara y Senado" })
    })
    return combo;
}
const getComboTipoActividadAgenda = async () => {
    let combo = [];
    await ActividadesLegislativasDataService.getComboTipoActividadAgenda().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
        })
        combo.unshift({ value: -1, label: "Ver todas" })
    })
    return combo;
}
const getComboTipoComision = async (idCorporacion) => {
    let combo = [];
    await ActividadesLegislativasDataService.getComboTipoComision(idCorporacion).then(response => {

        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
        })
        combo.unshift({ value: -1, label: "Elija tipo de comisión" })
    })
    return combo;
}
const getComboComisiones = async (idTipoComision, idCorporacion) => {
    let combo = [];
    await ActividadesLegislativasDataService.getComboComisiones(idTipoComision, idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
        })
        combo.unshift({ value: -1, label: "Ver todas" })
    })
    return combo;
}
const getDataByYearAndMonth = async (year, month, destacado = 0) => {
    let data = [];
    let dataCalendario = [];
    await ActividadesLegislativasDataService.getDataByYearAndMonth(year, month, destacado).then((response) => {
        data = response.data;
        let fechaSlice;
        data.forEach(x => {
            fechaSlice = x.fecha.slice(0, 10);
            dataCalendario.push({ year: Number(fechaSlice.split("-")[0]), month: Number(fechaSlice.split("-")[1]), day: Number(fechaSlice.split("-")[2]), className: 'tieneEventos' });
        });
    })
        .catch((e) => {
            console.error(e);
        });
    return dataCalendario;
}
const getAgendaLegislativa = async (idFilterActive, search, page, rows, fecha, tipoactividad, corporacion, comision) => {
    let f = FechaMysql.DateTimeFormatMySql(fecha + ' ' + '00:00:00');
    let listfecha = Object.assign({}, AgendaConst.listByFecha);
    await ActividadesLegislativasDataService.getAgendaLegislativaByFecha(
        idFilterActive,
        search, page, rows, f, tipoactividad, corporacion, comision)
        .then((response) => {
            listfecha.data = response.data
        })
        .catch((e) => {
            console.error(e);
        });
    await ActividadesLegislativasDataService.getTotalRecordsAgendaActividad(
        idFilterActive,
        search, f, tipoactividad, corporacion, comision)
        .then((response) => {
            listfecha.totalRows = response.data;
        })
        .catch((e) => {
            console.error(e);
        });
    return listfecha;
}


// EXPORT
export default function ActividadesLegislativas({ PageData = PageConst, AgendaData = AgendaConst }) {
    const [imgPrincipal, setImg] = useState(PageConst.imgPrincipal);
    const [subloader, setSubloader] = useState(AgendaConst.subloader);
    const [subloaderFilters, setSubloaderFilters] = useState(AgendaConst.subloaderFilters);
    const [subloaderCalendario, setSubloaderCalendario] = useState(AgendaConst.subloaderCalendario);
    const [subloaderAgendaActividades, setSubloaderAgendaActividades] = useState(AgendaConst.subloaderAgendaActividades);
    const [fechaActual, setFechaActual] = useState(AgendaConst.fechaActual);
    const [year, setYear] = useState(AgendaConst.year);
    const [month, setMonth] = useState(AgendaConst.month);
    const [fechaCalendario, setFechaCalendario] = useState(AgendaConst.fechaCalendario);
    const [eventos, setEventos] = useState(AgendaConst.eventos);
    const [listByFecha, setListByFecha] = useState(AgendaData.listByFecha);
    const [filterTipoActividadA, setFilterTA] = useState(AgendaConst.filterTipoActividadA);
    const [dataSelectActividadA, setDSActividad] = useState(AgendaConst.dataSelectActividadA);
    const [filterCorporacionA, setFilterCorp] = useState(AgendaConst.filterCorporacionA);
    const [dataSelectCorporacionA, setDSCorporacion] = useState(AgendaConst.dataSelectCorporacionA);
    const [filterTComisionA, setFilterTC] = useState(AgendaConst.filterTComisionA);
    const [dataSelecTComisionA, setDSTComision] = useState(AgendaConst.dataSelecTComisionA);
    const [filterComisionA, setFilterComision] = useState(AgendaConst.filterComisionA);
    const [dataSelectComisionA, setDSComision] = useState(AgendaConst.dataSelectComisionA);

    useEffect(() => {
        PrepareCalendar();
    }, []);

    const PrepareCalendar = () => {
        // Proceso para acondicionar calendario.
        let prevButton = document.querySelector(".Calendar__monthArrowWrapper.-right");
        let nextButton = document.querySelector(".Calendar__monthArrowWrapper.-left");
        let monthsButton = document.querySelectorAll(".Calendar__monthSelectorItemText");
        let yearsButton = document.querySelectorAll(".Calendar__yearSelectorText");
        if (prevButton) {
            prevButton.addEventListener("click", async (e) => {
                e.preventDefault();
                e.target.disabled = true;
                await handlerPrevAgenda(e);
                e.target.disabled = false;
            });
        }
        if (nextButton) {
            nextButton.addEventListener("click", async (e) => {
                e.preventDefault();
                e.target.disabled = true;
                await handlerNextAgenda(e)
                e.target.disabled = false;
            });
        }
        if (monthsButton) {
            monthsButton.forEach(month => { month.addEventListener("click", (e) => { handlerChooseMonth(e.currentTarget.innerText) }) })
        }
        if (yearsButton) {
            yearsButton.forEach(year => { year.addEventListener("click", (e) => { handlerChooseYear(e.currentTarget.innerText) }) })
        }
    }
    const handlerFilterActividadAgenda = async (selectActividad) => {
        setFilterTA(selectActividad);
        setSubloader(true);
        let list = await getAgendaLegislativa(1, listByFecha.search, 1, listByFecha.rows, fechaActual, selectActividad.value, filterCorporacionA.value, filterComisionA.value);
        setListByFecha({ ...listByFecha, data: list.data, totalRows: listByFecha.totalRows, page: 1 });
        setSubloader(false);
    }
    const handlerFilterCorporacionAgenda = async (selectCorporacion) => {
        setFilterCorp(selectCorporacion);
        setSubloader(true);
        let comboTComision = await getComboTipoComision(selectCorporacion.value);
        setDSTComision(comboTComision);
        let list = await getAgendaLegislativa(1, listByFecha.search, 1, listByFecha.rows, fechaActual, filterTipoActividadA.value, selectCorporacion.value, filterComisionA.value);
        setListByFecha({ ...listByFecha, data: list.data, totalRows: list.totalRows, page: 1 });
        setSubloader(false);
    }
    const handlerFilterTComisionAgenda = async (selectTComision) => {
        setFilterTC(selectTComision);
        setSubloaderFilters(true);
        let comboComisiones = await getComboComisiones(selectTComision.value, filterCorporacionA.value);
        setDSComision(comboComisiones);
        setSubloaderFilters(false);
    }
    const handlerFilterComisionAgenda = async (selectComision) => {
        setFilterComision(selectComision);
        setSubloader(true);
        let list = await getAgendaLegislativa(1, listByFecha.search, 1, listByFecha.rows, fechaActual, filterTipoActividadA.value, filterCorporacionA.value, selectComision.value);
        setListByFecha({ ...listByFecha, data: list.data, totalRows: list.totalRows, page: 1 });
        setSubloader(false);
    }
    const handlerPrevAgenda = async (e) => {
        setSubloaderCalendario(true);
        let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
        let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
        let month = getMonthNumberByStr(prevNextMonth(monthTemp));
        let year = prevNextMonth(monthTemp) === "diciembre" ? Number(yearTemp) - 1 : Number(yearTemp)
        let dataCalendar = await getDataByYearAndMonth(year, month);
        setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
        setYear(year);
        setMonth(month);
        setSubloaderCalendario(false);
    }
    const handlerNextAgenda = async (e) => {
        setSubloaderCalendario(true);
        let monthTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
        let yearTemp = e.currentTarget.parentNode.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
        let month = getMonthNumberByStr(prevNextMonth(monthTemp, true));
        let year = prevNextMonth(monthTemp, true) === "enero" ? Number(yearTemp) + 1 : Number(yearTemp)
        let dataCalendar = await getDataByYearAndMonth(year, month);
        setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
        setYear(year);
        setMonth(month);
        setSubloaderCalendario(false);
    }
    const handlerDataSelectDay = async (e) => {
        setSubloader(true);
        let fecha = dateCalendar(e);
        setFechaActual(fecha);
        let list = await getAgendaLegislativa(1, listByFecha.search, listByFecha.page, listByFecha.rows, fecha);
        setListByFecha({ ...listByFecha, data: list.data, totalRows: list.totalRows });
        setFechaCalendario(e);
        setSubloader(false);

    }
    const handlerChooseMonth = async (month) => {
        setSubloaderCalendario(true);
        let yearTemp = document.querySelector(".Calendar__monthYear.-shown .Calendar__yearText").innerText;
        let mes = getMonthNumberByStr(month);
        setYear(yearTemp);
        setMonth(mes);
        let dataCalendar = await getDataByYearAndMonth(Number(yearTemp), mes);
        setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
        setSubloaderCalendario(false);
    }
    const handlerChooseYear = async (anio) => {
        setSubloaderCalendario(true);
        let monthTemp = document.querySelector(".Calendar__monthYear.-shown .Calendar__monthText").innerText;
        let mes = getMonthNumberByStr(monthTemp);
        setYear(Number(anio));
        setMonth(mes);
        let dataCalendar = await getDataByYearAndMonth(Number(anio), mes);
        setListByFecha({ ...listByFecha, dataCalendar: dataCalendar });
        setSubloaderCalendario(false);
    }

    const handlerPaginationAgendaActividades = async (page, rows, search = "") => {
        setTimeout(async () => {
            setSubloader(true);
            let list = await getAgendaLegislativa(
                1,
                search,
                page,
                rows,
                fechaActual,
                filterTipoActividadA.value,
                filterCorporacionA.value,
                filterComisionA.value
            );
            setListByFecha({ ...listByFecha, data: list.data, totalRows: list.totalRows, page, rows, search });
            setSubloader(false);
        }, 1000);
    }

    return (
        <>
            <Head>
                <title>Órdenes del día - Agenda | Congreso Visible</title>
                <meta name="description" content="Aquí podrás encontrar todos los proyectos y citaciones agendadas en comisiones constitucionales y plenarias de Cámara o Senado, para las próximas sesiones. Igualmente, podrás acceder a la información histórica de la agenda legislativa." />
                <meta name="keywords" content="Órdenes del día, Agenda Legislativa, Proyectos de Ley, Citaciones, Comisiones, Citantes, Citados" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Órdenes del día - Agenda | Congreso Visible" />
                <meta property="og:description" content="Aquí podrás encontrar todos los proyectos y citaciones agendadas en comisiones constitucionales y plenarias de Cámara o Senado, para las próximas sesiones. Igualmente, podrás acceder a la información histórica de la agenda legislativa." />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/orden-del-dia`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/orden-del-dia`} />
                <meta name="twitter:title" content="Órdenes del día - Agenda | Congreso Visible"/>
                <meta name="twitter:description" content="Aquí podrás encontrar todos los proyectos y citaciones agendadas en comisiones constitucionales y plenarias de Cámara o Senado, para las próximas sesiones. Igualmente, podrás acceder a la información histórica de la agenda legislativa." />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/orden-del-dia/"/>
            </Head>

            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + PageData.imgPrincipal}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-file-contract"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>Agenda legislativa</h1>
                    </div>
                </div>
            </section>
            <main>
                <div className="listadoPageContainer">
                    <div className="container-fluid">
                        <div className="centerTabs lg min-height-85">
                            <ul>
                                <li className="active">
                                    <a href="/orden-del-dia">Agenda legislativa</a>
                                </li>
                                <li>
                                    <a href="/votaciones">Votaciones</a>
                                </li>
                                <li>
                                    <a href="/citaciones">Control político</a>
                                </li>
                                <li>
                                    <a href="/funcion-electoral">Función electoral</a>
                                </li>
                                <li>
                                    <a href="/partidos">Partidos</a>
                                </li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="contentTab active" data-ref="1">
                                <section gtdtarget="3" className="no-full-height agendaSection">
                                    <div className="calendarContainerCV">
                                        <div className="container-fluid">
                                            <div className="row">
                                                <div className="col-md-5">
                                                    <div className="relative">
                                                        <div className={`subloader ${subloaderCalendario ? "active" : ""}`}></div>
                                                        <LittleCalendar value={fechaCalendario} onChange={(e) => { handlerDataSelectDay(e) }} customDaysClassName={listByFecha.dataCalendar} />
                                                    </div>
                                                </div>
                                                <div className="col-md-7">
                                                    <div className="buscador pd-25">
                                                        <div className="input-group">
                                                            <input type="text" value={listByFecha.search}
                                                                onChange={async (e) => {
                                                                    setListByFecha({ ...listByFecha, search: e.target.value })

                                                                }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, e.target.value)
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar" className="form-control" />

                                                            <span className="input-group-text"><button onClick={async () => { await handlerPaginationAgendaActividades(listByFecha.page, listByFecha.rows, listByFecha.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                                            <span className="input-group-text"><button onClick={(e) => { toggleFilter(e.currentTarget); }} type="button" className="btn btn-primary"><i className="fa fa-filter"></i></button></span>
                                                        </div>
                                                        <div className="floatingFilters evenColors">
                                                            <div className="one-columns relative no-margin">
                                                                <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                                                <div className="item">
                                                                    <label htmlFor="">Filtrar por tipo de actividad</label>
                                                                    <Select
                                                                        handlerOnClick={async () => {
                                                                            if (dataSelectActividadA.length <= 1) {
                                                                                setSubloaderFilters(true);
                                                                                let combo = await getComboTipoActividadAgenda();
                                                                                setDSActividad(combo)
                                                                                setSubloaderFilters(false);
                                                                            }
                                                                        }}
                                                                        divClass=""
                                                                        selectplaceholder="Seleccione"
                                                                        selectValue={filterTipoActividadA}
                                                                        selectOnchange={handlerFilterActividadAgenda}
                                                                        selectoptions={dataSelectActividadA}
                                                                        selectIsSearchable={true}
                                                                        selectclassNamePrefix="selectReact__value-container"
                                                                        spanClass=""
                                                                        spanError="" >
                                                                    </Select>
                                                                </div>
                                                                <div className="item">
                                                                    <label htmlFor="">Filtrar por Corporación</label>
                                                                    <Select
                                                                        handlerOnClick={async () => {
                                                                            if (dataSelectCorporacionA.length <= 1) {
                                                                                setSubloaderFilters(true);
                                                                                let combo = await getComboCorporacion();
                                                                                setDSCorporacion(combo);
                                                                                setSubloaderFilters(false);
                                                                            }
                                                                        }}
                                                                        divClass=""
                                                                        selectplaceholder="Seleccione"
                                                                        selectValue={filterCorporacionA}
                                                                        selectOnchange={handlerFilterCorporacionAgenda}
                                                                        selectoptions={dataSelectCorporacionA}
                                                                        selectIsSearchable={true}
                                                                        selectclassNamePrefix="selectReact__value-container"
                                                                        spanClass=""
                                                                        spanError="" >
                                                                    </Select>
                                                                </div>
                                                                <div className="item">
                                                                    <label htmlFor="">Filtrar por Tipo de Comisión</label>
                                                                    <Select
                                                                        divClass=""
                                                                        selectplaceholder="Seleccione"
                                                                        selectValue={filterTComisionA}
                                                                        selectOnchange={handlerFilterTComisionAgenda}
                                                                        selectoptions={dataSelecTComisionA}
                                                                        selectIsSearchable={true}
                                                                        noOptionsMessage="Debe elegir una corporación"
                                                                        selectclassNamePrefix="selectReact__value-container"
                                                                        spanClass=""
                                                                        spanError="" >
                                                                    </Select>
                                                                </div>
                                                                <div className="item">
                                                                    <label htmlFor="">Filtrar por Comision</label>
                                                                    <Select
                                                                        divClass=""
                                                                        selectplaceholder="Seleccione"
                                                                        selectValue={filterComisionA}
                                                                        selectOnchange={handlerFilterComisionAgenda}
                                                                        selectoptions={dataSelectComisionA}
                                                                        selectIsSearchable={true}
                                                                        noOptionsMessage="Debe elegir un tipo de comisión"
                                                                        selectclassNamePrefix="selectReact__value-container"
                                                                        spanClass=""
                                                                        spanError="" >
                                                                    </Select>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="relative">
                                                        <div className={`subloader ${subloaderAgendaActividades ? "active" : ""}`}></div>
                                                        <ActLegislativaEventosList
                                                            data={listByFecha.data}
                                                            handler={handlerPaginationAgendaActividades}
                                                            pageExtends={listByFecha.page}
                                                            pageSize={listByFecha.rows}
                                                            totalRows={listByFecha.totalRows}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

        </>
    )
}


function toggleFilter(element) {
    element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
}

function getMonthNumberByStr(str) {
    let month = str.toString().toLowerCase();
    switch (month) {
        case 'enero':
            return 1
            break;
        case 'febrero':
            return 2
            break;
        case 'marzo':
            return 3
            break;
        case 'abril':
            return 4
            break;
        case 'mayo':
            return 5
            break;
        case 'junio':
            return 6
            break;
        case 'julio':
            return 7
            break;
        case 'agosto':
            return 8
            break;
        case 'septiembre':
            return 9
            break;
        case 'octubre':
            return 10
            break;
        case 'noviembre':
            return 11
            break;
        case 'diciembre':
            return 12
            break;
        default:
            break;
    }
}
function prevNextMonth(str, next = false) {
    str = str.toString().toLowerCase();
    switch (str) {
        case 'enero':
            return !next ? "diciembre" : "febrero"
            break;
        case 'febrero':
            return !next ? "enero" : "marzo"
            break;
        case 'marzo':
            return !next ? "febrero" : "abril"
            break;
        case 'abril':
            return !next ? "marzo" : "mayo"
            break;
        case 'mayo':
            return !next ? "abril" : "junio"
            break;
        case 'junio':
            return !next ? "mayo" : "julio"
            break;
        case 'julio':
            return !next ? "junio" : "agosto"
            break;
        case 'agosto':
            return !next ? "julio" : "septiembre"
            break;
        case 'septiembre':
            return !next ? "agosto" : "octubre"
            break;
        case 'octubre':
            return !next ? "septiembre" : "noviembre"
            break;
        case 'noviembre':
            return !next ? "octubre" : "diciembre"
            break;
        case 'diciembre':
            return !next ? "noviembre" : "enero"
            break;
        default:
            break;
    }
}

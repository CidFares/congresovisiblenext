import Link from 'next/link'
import Head from 'next/head'
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import dynamic from "next/dynamic";
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import { useState, useEffect } from "react";
import AuthLogin from "../../Utils/AuthLogin";
const auth = new AuthLogin();
import { Constantes, URLBase, OrdenDiaURLParameters } from '../../Constants/Constantes'
const DetalleOrdenConst={
    id:0,
    loading:false,
    fecha:new Date(),
    tipo_actividad_id:null,
    tipo_actividad:null,
    numero_camara:"N/D",
    numero_senado:"N/D",
    tituloproyecto:"N/D",
    proyecto:false,
    citac:false,
    citaciontitulo:"",
    numprop:"",
    legislatura:"",
    realizado:0,
    descripcion:"",
    agendaComision:[],
    selected:null,
    citacion:null
}
export async function getServerSideProps({ query }) {
    let DetalleOrden=DetalleOrdenConst;
    DetalleOrden= await getDetalleAgenda(query.props[OrdenDiaURLParameters.id]);
    return {
        props: { DetalleOrden }
    }
}
const getDetalleAgenda = async (id) => {
    let DetalleOrden=DetalleOrdenConst;        
    await ActividadesLegislativasDataService.getAgendaDetalle(id)
        .then((response) => {   
            DetalleOrden=response.data[0];
            DetalleOrden.fecha=DetalleOrden.agenda.fecha;
            DetalleOrden.agendaComision=DetalleOrden.agenda.agenda_comision;
            
            if( DetalleOrden.selected!=null){
                DetalleOrden.proyecto=true;
                DetalleOrden.numero_camara=DetalleOrden.selected.numero_camara;
                DetalleOrden.numero_senado=DetalleOrden.selected.numero_senado;
                DetalleOrden.tituloproyecto=DetalleOrden.selected.titulo;                              
            }
            if(DetalleOrden.citacion!=null){
                DetalleOrden.citac=true;
                DetalleOrden.citaciontitulo=DetalleOrden.citacion.titulo;
                DetalleOrden.numprop=DetalleOrden.citacion.numero_proposicion;
                DetalleOrden.legislatura=DetalleOrden.citacion.legislatura !== null ? DetalleOrden.citacion.legislatura.nombre : "Sin legislatura";
            }
        })
        .catch((e) => {
            console.error(e);
        });
       return DetalleOrden;
};
export default function DetalleOrdenDia({DetalleOrden=DetalleOrdenConst}){
  
        return (
            <>
                 <Head>
                <title>Orden del día | {DetalleOrden.titulo}</title>
                <meta name="description" content={auth.filterStringHTML(DetalleOrden.descripcion).slice(0, 165)} />
                <meta name="keywords" content="balance, cuatrienio, congresista, Congreso Colombia, Colombia, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={DetalleOrden.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(DetalleOrden.descripcion).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/orden-del-dia/${auth.filterStringForURL(DetalleOrden.titulo)}/${DetalleOrden.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/orden-del-dia/${auth.filterStringForURL(DetalleOrden.titulo)}/${DetalleOrden.id}`} />
                <meta name="twitter:title" content={DetalleOrden.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(DetalleOrden.descripcion).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/orden-del-dia/${auth.filterStringForURL(DetalleOrden.titulo)}/${DetalleOrden.id}/`}/>
                </Head>
                <div className="topPanel">
                    <div className="pageTitle">
                        <h1>Detalles de la orden del día</h1>
                    </div>
                </div>
                <div className="listadoPageContainer">
                
                    <div className="container">
                        {/* <div className={`subloader ${this.state.loading ? "active" : ""}`}></div> */}
                        <div className="row">
                            <div className="col-md-12">
                                <h4>Título: {DetalleOrden.titulo}</h4>
                                <div className="three-columns">
                                    <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-calendar"></i></div>
                                        <div className="vertical-text">
                                            <small>Fecha</small>
                                            <p>{auth.coloquialDate(DetalleOrden.fecha, true)}</p>
                                        </div>
                                    </div>
                                    <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-clock"></i></div>
                                        <div className="vertical-text">
                                            <small>Hora</small>
                                            <p>{new Date(DetalleOrden.fecha).toLocaleTimeString()}</p>
                                        </div>
                                    </div>
                                    <div className="littleProfileCard">
                                        <div className="icon"><i className={`fas fa-${DetalleOrden.realizado==1?"check":"times"}`}></i></div>
                                        <div className="vertical-text">
                                            <small>¿Realizado?</small>
                                            <p>{DetalleOrden.realizado==1?"Sí":"No"}</p>
                                        </div>
                                    </div>
                                    <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-male"></i></div>
                                        <div className="vertical-text">
                                            <small>Tipo de actividad</small>
                                            <p>{DetalleOrden.tipo_actividad_id!==0 && DetalleOrden.tipo_actividad_id!==null?
                                            DetalleOrden.tipo_actividad.nombre:"N/D"}</p>
                                        </div>
                                    </div>
                                    {
                                        DetalleOrden.proyecto?                                        
                                        <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-circle"></i></div>
                                        <div className="vertical-text">
                                            <small>Número de proyecto en cámara</small>
                                            <p>{DetalleOrden.numero_camara!=null?DetalleOrden.numero_camara:"N/D"}</p>
                                        </div>
                                        </div>:""
                                    }
                                    {
                                       DetalleOrden.proyecto?                                        
                                        <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-circle"></i></div>
                                        <div className="vertical-text">
                                            <small>Número de proyecto en senado</small>
                                            <p>{DetalleOrden.numero_senado!=null?DetalleOrden.numero_senado:"N/D"}</p>
                                        </div>
                                        </div>:""
                                    }
                                    {
                                        DetalleOrden.citac?                                        
                                        <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-circle"></i></div>
                                        <div className="vertical-text">
                                            <small>Numero de proposición</small>
                                            <p>{DetalleOrden.citacion!==null?DetalleOrden.numprop:"N/D"}</p>
                                        </div>
                                        </div>:""
                                    }
                                     {
                                        DetalleOrden.citac?                                        
                                        <div className="littleProfileCard">
                                        <div className="icon"><i className="fas fa-circle"></i></div>
                                        <div className="vertical-text">
                                            <small>Legislatura</small>
                                            <p>{DetalleOrden.citacion!==null?DetalleOrden.legislatura:"N/D"}</p>
                                        </div>
                                        </div>:""
                                    }
                                   
                                </div>
                                
                                {
                                    
                                    DetalleOrden.proyecto === true || DetalleOrden.citac === true ?
                                    <div className="ordenTitle">
                                        <strong>{DetalleOrden.proyecto?"Titulo del proyecto de ley: ":"Titulo de la citación:"}</strong>
                                    <a href={`/${DetalleOrden.proyecto?'proyectos-de-ley':'citaciones'}/${auth.filterStringForURL(`${DetalleOrden.proyecto === true ? 'p'+DetalleOrden.tituloproyecto : DetalleOrden.citaciontitulo}`)}/${DetalleOrden.proyecto?DetalleOrden.proyecto_ley_id:DetalleOrden.citacion.id}`}>  {DetalleOrden.proyecto === true ? DetalleOrden.tituloproyecto : DetalleOrden.citaciontitulo}</a>
                                    </div>:""
                                }
                                
                                <hr/>
                                <strong>Comisiones asociadas</strong>
                                <div className="two-columns">
                                    {        
                                        DetalleOrden.agendaComision.map((z,j)=>{
                                            return(
                                                <div className="littleProfileCard" key={j}>
                                                    <div className="icon"><i className="fas fa-university"></i></div>
                                                    <div className="vertical-text">
                                                        <small>{z.corporacion.nombre}</small>
                                                        <p>{z.comision.nombre}</p>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }  
                                </div>
                                <hr/>
                                <strong>Detalles:</strong>
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: !DetalleOrden.descripcion=="" ? (DetalleOrden.descripcion) : "Sin descripción" }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    
}


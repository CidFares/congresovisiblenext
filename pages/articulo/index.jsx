import { useState, useEffect } from "react";
import Link from "next/link";
import Head from "next/head"
import Select from '../../Components/Select';
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase, TypeCombos } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";

// Const
const auth = new AuthLogin();

const PageConst = {
    subloaderOpiniones: true,
    subloaderFilters: true,
    listOpiniones: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "titulo", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Fecha de publicación", text: "fechaPublicacion", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Autor", text: "equipo.nombre", esImg: true, img: "equipo.equipo_imagen.1.imagen", putOnFirstElement: false },
                ],
            generalActionTitle: null,
            generalIcon: null,
            eachActionTitle: "tipo_publicacion.nombre",
            eachIcon: "tipo_publicacion.icono"
        },
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    filterEquipoCV: { value: -1, label: "Filtrar equipo" },
    filterTipoPublicacionOpiniones: { value: -1, label: "Filtrar por tipo de publicación" },
    dataSelectEquipoCV: [],
    dataSelectTipoPublicacion: [],
};

const ArticuloConst = {

};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let ArticuloData = ArticuloConst;

    PageData.listOpiniones.data = await getAllOpiniones(
        1
        , query.equipo || PageData.filterEquipoCV.value
        , query.tipoPublicacion || PageData.filterTipoPublicacionOpiniones.value
        , query.search || PageData.listOpiniones.search
        , query.page || PageData.listOpiniones.page
        , query.rows || PageData.listOpiniones.rows
    );
    PageData.listOpiniones.totalRows = await getTotalRecordsOpiniones(
        1
        , query.equipo || PageData.filterEquipoCV.value
        , query.tipoPublicacion || PageData.filterTipoPublicacionOpiniones.value
        , query.search || PageData.listOpiniones.search
    );
    PageData.subloaderOpiniones = false;
    PageData.subloaderFilters = false;

    return {
        props: {
            PageData, ArticuloData, query
        }
    }
}

// Gets 
const getComboEquipoCVByType = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboEquipoCVByType(TypeCombos.EquipoOpiniones).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Filtrar equipo CV" })
        if (!selected)
            selected = combo[0];
    }).catch((e) => {
        console.error(e);
    });

    return { DSEquipo: combo, SelectedEquipo: selected };
}

const getComboTipoPublicacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ContenidoMultimediaDataService.getComboTipoPublicacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Filtrar tipo publicación" })
        if (!selected)
            selected = combo[0];
    }).catch((e) => {
        console.error(e);
    });

    return { DSTipoPublicacion: combo, SelectedTipoPublicacion: selected };
}

const getAllOpiniones = async (idFilterActive, equipo, tipopublicacion, search, page, rows) => {
    let data = [];
    await ContenidoMultimediaDataService.getAllOpiniones(idFilterActive, equipo, tipopublicacion, search, page, rows).then((response) => {
        data = response.data;
        data.forEach(x => {
            x.fechaPublicacion = auth.coloquialDate(x.fechaPublicacion)
        })
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const getTotalRecordsOpiniones = async (idFilterActive, equipo, tipopublicacion, search) => {
    let totalRows = 0;
    await ContenidoMultimediaDataService.getTotalRecordsOpiniones(idFilterActive, equipo, tipopublicacion, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
}


const Articulo = ({ PageData = PageConst, ArticuloData = ArticuloConst, query }) => {
    // Const
    const [subloaderFilters, setSubloaderFilters] = useState(PageData.subloaderFilters);
    const [subloaderOpiniones, setSubloaderOpiniones] = useState(PageData.subloaderOpiniones);
    const [listOpiniones, setListOpiniones] = useState(PageData.listOpiniones);
    const [filterEquipoCV, setFilterEquipoCV] = useState(PageData.filterEquipoCV);
    const [filterTipoPublicacionOpiniones, setFilterTipoPublicacionOpiniones] = useState(PageData.filterTipoPublicacionOpiniones);
    const [dataSelectEquipoCV, setDataSelectEquipoCV] = useState(PageData.dataSelectEquipoCV);
    const [dataSelectTipoPublicacion, setDataSelectTipoPublicacion] = useState(PageData.dataSelectTipoPublicacion);
    const [search, setSearch] = useState(query.search || PageData.listOpiniones.search);
    const [rows, setRows] = useState(query.rows || PageData.listOpiniones.rows);
    const [page, setPage] = useState(query.page || PageData.listOpiniones.page);

    useEffect(async () => {
        if (query.equipo) {
            let { DSEquipo, SelectedEquipo } = await getComboEquipoCVByType(Number(query.equipo));
            setDataSelectEquipoCV(DSEquipo); setFilterEquipoCV(SelectedEquipo);
        }
        if (query.tipoPublicacion) {
            let { DSTipoPublicacion, SelectedTipoPublicacion } = await getComboTipoPublicacion(Number(query.tipoPublicacion));
            setDataSelectTipoPublicacion(DSTipoPublicacion); setFilterTipoPublicacionOpiniones(SelectedTipoPublicacion);
        }
    }, []);

    //Handlers
    const handlerFilterEquipoCV = async (selectEquipoCV) => {
        setSubloaderOpiniones(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('equipo', selectEquipoCV.value, q)
        window.location = window.location.pathname + q
    }

    const handlerTipoPublicacionOpiniones = async (selectTipoPublicacion) => {
        setSubloaderOpiniones(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoPublicacion', selectTipoPublicacion.value, q)
        window.location = window.location.pathname + q
    };

    const handlerPaginationOpiniones = async (page, rows, search = "") => {
        setSubloaderOpiniones(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rows, q)
        q = auth.replaceQueryParam('search', search, q)
        window.location = window.location.pathname + q
    }

    // Others
    const toggleFilter = (element) => {
        element.parentNode.parentNode.parentNode.querySelector(".floatingFilters").classList.toggle("active");
    }

    return (
        <div>
            <Head>
                <title>Artículos</title>
                <meta name="description" content="Aquí encontrarás todo lo relacionado a artículos publicados del congreso de colombia" />
                <meta name="keywords" content="articulo, congresista, Congreso Colombia, Democracia, Colombia, Partidos Políticos, Proyectos de Ley, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Artículos" />
                <meta property="og:description" content="Aquí encontrarás todo lo relacionado a artículos publicados del congreso de colombia" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/articulo`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/articulo`} />
                <meta name="twitter:title" content="Artículos" />
                <meta name="twitter:description" content="Aquí encontrarás todo lo relacionado a artículos publicados del congreso de colombia" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/articulo/" />
            </Head>
            <div className="pageTitle">
                <h1>Artículos</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="centerTabs small-icons lg min-height-85">
                    <ul>
                        <li>
                            <h2>
                                <a href={"/contenido-multimedia"}>
                                    <i className="fas fa-photo-video"></i>
                                    Multimedia
                                </a>
                            </h2>
                        </li>
                        <li className="active">
                            <h2>
                                <a href={"/articulo"}>
                                    <i className="fas fa-comment-dots"></i>
                                    Artículos
                                </a>
                            </h2>
                        </li>
                        <li>

                            <h2>
                                <a href={"/agora"}>
                                    <i className="fas fa-comments"></i>
                                    Opinión de congresistas
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/podcast"}>
                                    <i className="fas fa-microphone-alt"></i> Podcast
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/balance"}>
                                    <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                </a>
                            </h2>
                        </li>
                        <li>
                            <h2>
                                <a href={"/informe-territorial"}>
                                    <i className="fas fa-file-alt"></i> Informes regionales
                                </a>
                            </h2>
                        </li>
                    </ul>
                </div>
                <div className="contentForCenterTabs">
                    <div className="contentTab active">
                        <div className="relative">
                            <div className={`subloader ${subloaderOpiniones ? "active" : ""}`}></div>
                            <div className="buscador pd-25">
                                <div className="input-group">
                                    <input
                                        type="text"
                                        value={search}
                                        onChange={async (e) => {
                                            setSearch(e.target.value)
                                        }}
                                        onKeyUp={async (e) => {
                                            if (e.key === "Enter") {

                                                await handlerPaginationOpiniones(listOpiniones.page, listOpiniones.rows, e.target.value);
                                            }
                                        }}
                                        placeholder="Escriba para buscar" className="form-control" />

                                    <span className="input-group-text"><button onClick={async () => { await handlerPaginationOpiniones(listOpiniones.page, listOpiniones.rows, listOpiniones.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    <span className="input-group-text">
                                        <button
                                            onClick={async (e) => {
                                                toggleFilter(e.currentTarget);
                                            }}
                                            type="button" className="btn btn-primary"><i className="fa fa-filter"></i></button></span>
                                </div>
                                <div className="floatingFilters evenColors">
                                    <div className="one-columns relative no-margin">
                                        <div className={`subloader ${subloaderFilters ? "active" : ""}`}></div>
                                        <div className="item">
                                            <label htmlFor="">Filtrar por tipo de publicación</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectTipoPublicacion.length <= 1) {
                                                        let { DSTipoPublicacion } = await getComboTipoPublicacion();
                                                        setDataSelectTipoPublicacion(DSTipoPublicacion)
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterTipoPublicacionOpiniones}
                                                selectOnchange={handlerTipoPublicacionOpiniones}
                                                selectoptions={dataSelectTipoPublicacion}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Filtrar equipo CV</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectEquipoCV.length <= 1) {
                                                        let { DSEquipo } = await getComboEquipoCVByType();
                                                        setDataSelectTipoPublicacion(DSEquipo)
                                                    }
                                                }}
                                                divClass=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterEquipoCV}
                                                selectOnchange={handlerFilterEquipoCV}
                                                selectoptions={dataSelectEquipoCV}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClass=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ContenidoMultimediaList
                                propiedades={listOpiniones.propiedades}
                                data={listOpiniones.data}
                                handlerPagination={handlerPaginationOpiniones}
                                defaultImage={Constantes.NoImagenPicture}
                                link="/articulo" params={["titulo", "id"]}
                                pageExtends={page}
                                totalRows={listOpiniones.totalRows}
                                pageSize={rows}
                                pathImgOrigen={auth.pathApi()}
                                className="pd-25"
                            />
                            <CIDPagination totalPages={listOpiniones.totalRows} totalRows={query.rows || listOpiniones.rows} searchBy={query.search || listOpiniones.search} currentPage={query.page || listOpiniones.page} hrefPath={"/articulo"} hrefQuery={{
                                tipoPublicacion: query.tipoPublicacion || filterTipoPublicacionOpiniones.value,
                                equipo: query.equipo || filterEquipoCV.value
                            }} />
                        </div>
                        {/* End Opiniones */}
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Articulo;
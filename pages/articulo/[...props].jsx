import { useState } from "react";
import dynamic from 'next/dynamic';
import Head from "next/head"
import OpinionDataService from "../../Services/ContenidoMultimedia/Opinion.Service";
import 'suneditor/dist/css/suneditor.min.css';
import { Constantes, URLBase } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";

// Const
const auth = new AuthLogin();

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const PageConst = {
    id: 0,
    loading: true,
    equipo: [],
    icono: "",
    cVBannerImage: Constantes.NoImagen,
    autorImage: Constantes.NoImagen
};

const DetalleOpinionConst = {
    id: 0,
    titulo: "",
    equipo_id: 0,
    tipo_publicacion_id: 0,
    fechaPublicacion: '',
    resumen: '',
    opinion: '',
    activo: 1,
    opinion_imagen: [{ imagen: "" }],
    equipo: {
        id: 0,
        nombre: '',
        descripcion: '',
        congreso_visible_id: 1,
        activo: 1,
        equipo_imagen: [{ imagen: "" }]
    },
    tipo_publicacion: { id: 0, nombre: '', icono: '', activo: 1 }
}

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let DetalleOpinionData = DetalleOpinionConst;
    PageData.id = query.props[1];
    DetalleOpinionData = await getById(PageData.id);

    if (DetalleOpinionData.opinion_imagen) {
        PageData.cVBannerImage = auth.pathApi() + DetalleOpinionData.opinion_imagen[2].imagen;
        PageData.autorImage = auth.pathApi() + DetalleOpinionData.equipo.equipo_imagen[0].imagen;
    }

    PageData.loading = false;

    return {
        props: {
            PageData, DetalleOpinionData
        }
    }
}

// Gets
const getById = async (id) => {
    let data = {};
    await OpinionDataService.getOpinion(id).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return data;
}

// Page
const DetalleOpinion = ({ PageData = PageConst, DetalleOpinionData = DetalleOpinionConst }) => {
    // Const
    const [detalleOpinion, setDetalleOpinion] = useState(DetalleOpinionData);
    const [loading, setLoading] = useState(PageData.loading);
    const [equipo, setEquipo] = useState(PageData.equipo);
    const [cVBannerImage, setCVBannerImage] = useState(PageData.cVBannerImage);
    const [autorImage, setAutorImage] = useState(PageData.autorImage);

    return (
        <div>
            <Head>
                <title>{detalleOpinion.titulo}</title>
                <meta name="description" content={auth.filterStringHTML(detalleOpinion.resumen).slice(0, 165)} />
                <meta name="keywords" content="Opinion, congresista, Congreso Colombia, Democracia, Colombia, Partidos Políticos, Proyectos de Ley, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={detalleOpinion.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(detalleOpinion.resumen).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/articulo/${auth.filterStringForURL(detalleOpinion.titulo)}/${detalleOpinion.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/articulo/${auth.filterStringForURL(detalleOpinion.titulo)}/${detalleOpinion.id}`} />
                <meta name="twitter:title" content={detalleOpinion.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(detalleOpinion.resumen).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/articulo/${auth.filterStringForURL(detalleOpinion.titulo)}/${detalleOpinion.id}/`}/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${cVBannerImage}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon littleIcon"><i className="fas fa-comment-dots"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{!loading ? detalleOpinion.titulo : ""}</h1>
                    </div>
                </div>
            </section>
            <section className="nuestraDemocraciaSection">
                <div className="listadoPageContainer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div
                                    className="autor"
                                    style={{
                                        display: "flex",
                                        justifyContent: "flex-start",
                                        alignItems: "center",
                                        margin: "15px 0"
                                    }}
                                >
                                    <div className="photo avatar" style={{ marginRight: "13px" }}>
                                        <img src={autorImage} alt={DetalleOpinionData.equipo.nombre} />
                                    </div>
                                    <h2>{!loading ? DetalleOpinionData.equipo.nombre : ""}</h2>
                                </div>
                            </div>
                            <div className="description">
                                <strong>Resumen:</strong>
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: detalleOpinion.resumen || "Sin resumen" }}></div>
                                <hr />
                                <strong>Contenido:</strong>
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: detalleOpinion.opinion || "Sin contenido" }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>
        </div>
    );
}

export default DetalleOpinion;

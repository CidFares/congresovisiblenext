import { useState } from "react";
import Link from "next/link";
import Head from "next/head"
import dynamic from 'next/dynamic';
import ContenidoMultimediaList from "../../Components/CongresoVisible/ContenidoMultimediaList";
import 'suneditor/dist/css/suneditor.min.css'
import ContenidoMultimediaDataService from "../../Services/ContenidoMultimedia/ContenidoMultimedia.Service";
import { Constantes, URLBase } from "../../Constants/Constantes";
import AuthLogin from "../../Utils/AuthLogin";
import CIDPagination from "../../Components/CIDPagination";

// Const
const auth = new AuthLogin();

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});

const PageConst = {
    subloaderPodcast: true,
    subloaderModal: true,
    listPodcast: {
        data: [],
        propiedades:
        {
            id: 'id',
            description:
                [
                    { title: "Título", text: "titulo", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Presentadores", text: "presentadores", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Invitados", text: "invitados", esImg: false, img: "", putOnFirstElement: false },
                    { title: "Fecha de publicación", text: "fecha", esImg: false, img: "" },
                ],
            generalActionTitle: "Ver",
            generalIcon: "fas fa-volume-up"
        },
        esModal: true,
        targetModal: "#modal-podcast",
        totalRows: 0,
        search: "",
        page: 1,
        rows: 18
    },
    podcastSelected: {}
};

const PodcastConst = {
    data: {}
};

// SSR
export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let PodcastData = PodcastConst;

    PageData.listPodcast.data = await getAllPodcast(
        1
        , query.search || PageData.listPodcast.search
        , query.page || PageData.listPodcast.page
        , query.rows || PageData.listPodcast.rows
    );
    PageData.listPodcast.totalRows = await getTotalRecordsPodcast(
        1
        , query.search || PageData.listPodcast.search
    );

    PageData.subloaderPodcast = false;
    PageData.subloaderModal = false;

    return {
        props: {
            PageData, PodcastData, query
        }
    }
}

// Gets
const getAllPodcast = async (idFilterActive, search, page, rows) => {
    let data = [];

    await ContenidoMultimediaDataService.getAllPodcast(idFilterActive, search, page, rows).then((response) => {
        data = response.data;
        data.forEach(x => {
            x.fecha = auth.coloquialDate(x.fecha)
        })
    }).catch((e) => {
        console.error(e);
    });

    return data;
};

const getTotalRecordsPodcast = async (idFilterActive, search) => {
    let totalRows = 0;

    await ContenidoMultimediaDataService.getTotalRecordsPodcast(idFilterActive, search).then((response) => {
        totalRows = response.data;
    }).catch((e) => {
        console.error(e);
    });

    return totalRows;
};

// Page
const Podcast = ({ PageData = PageConst, PodcastData = PodcastConst, query }) => {
    const [subloaderModal, setSubloaderModal] = useState(PageData.subloaderModal);
    const [subloaderPodcast, setSubloaderPodcast] = useState(PageData.subloaderPodcast);
    const [listPodcast, setListPodcast] = useState(PageData.listPodcast);
    const [podcastSelected, setPodcastSelected] = useState(PageData.podcastSelected);
    const [search, setSearch] = useState(query.search || PageData.listPodcast.search);
    const [rows, setRows] = useState(query.rows || PageData.listPodcast.rows);
    const [page, setPage] = useState(query.page || PageData.listPodcast.page);

    // Handlers
    const handlerForLoadModalPodcast = async (item) => {
        setSubloaderModal(true);
        setPodcastSelected(item.data)
        setSubloaderModal(false);
    }

    const handlerPaginationPodcast = async (page, rowsA, searchA = "") => {
        setSubloaderPodcast(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    return (
        <div>
            <Head>
                <title>Podcast</title>
                <meta name="description" content="Aquí podrás encontrar en audio lo relacionado al congreso" />
                <meta name="keywords" content="audio, debate, Órdenes del día, Agenda Legislativa, Proyectos de Ley, Citaciones, Comisiones, Citantes, Citados" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Podcast" />
                <meta property="og:description" content="Aquí podrás encontrar en audio lo relacionado al congreso" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/podcast`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/podcast`} />
                <meta name="twitter:title" content="Podcast" />
                <meta name="twitter:description" content="Aquí podrás encontrar en audio lo relacionado al congreso" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/podcast/" />
            </Head>
            <div className="pageTitle">
                <h1>Podcast</h1>
            </div>
            <section className="nuestraDemocraciaSection pd-top-35">
                <div className="container-fluid">
                    <div className="centerTabs small-icons lg min-height-85">
                        <ul>
                            <li>
                                <h2>
                                    <a href={"/contenido-multimedia"}>
                                        <i className="fas fa-photo-video"></i>
                                        Multimedia
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/articulo"}>
                                        <i className="fas fa-comment-dots"></i>
                                        Artículos
                                    </a>
                                </h2>
                            </li>
                            <li>

                                <h2>
                                    <a href={"/agora"}>
                                        <i className="fas fa-comments"></i>
                                        Opinión de congresistas
                                    </a>
                                </h2>
                            </li>
                            <li className="active">
                                <h2>
                                    <a href={"/podcast"}>
                                        <i className="fas fa-microphone-alt"></i> Podcast
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/balance"}>
                                        <i className="fas fa-balance-scale"></i> Balances cuatrienio
                                    </a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href={"/informe-territorial"}>
                                        <i className="fas fa-file-alt"></i> Informes regionales
                                    </a>
                                </h2>
                            </li>
                        </ul>
                    </div>
                    <div className="contentForCenterTabs">
                        <div className="relative active">
                            <div className={`subloader ${subloaderPodcast ? "active" : ""}`}></div>
                            <div className="buscador pd-25">
                                <div className="input-group">
                                    <input type="text"

                                        value={search}
                                        onChange={async (e) => {
                                            setSearch(e.target.value)
                                        }}
                                        onKeyUp={async (e) => {
                                            if (e.key === "Enter") {
                                                await handlerPaginationPodcast(listPodcast.page, listPodcast.rows, e.target.value);
                                            }
                                        }}
                                        placeholder="Escriba para buscar" className="form-control" />

                                    <span className="input-group-text"><button onClick={async () => { handlerPaginationPodcast(listPodcast.page, listPodcast.rows, listPodcast.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                </div>
                            </div>
                            <ContenidoMultimediaList
                                propiedades={listPodcast.propiedades}
                                data={listPodcast.data}
                                handlerPagination={handlerPaginationPodcast}
                                defaultImage={Constantes.NoImagenPicture}
                                link="/podcast" params={["id"]}
                                pageExtends={page}
                                totalRows={listPodcast.totalRows}
                                pageSize={rows}
                                pathImgOrigen={auth.pathApi()}
                                className="pd-25"
                                esModal={listPodcast.esModal}
                                targetModal={listPodcast.targetModal}
                                handlerForLoadModal={handlerForLoadModalPodcast}
                            />
                            <CIDPagination totalPages={listPodcast.totalRows} totalRows={query.rows || listPodcast.rows} searchBy={query.search || listPodcast.search} currentPage={query.page || listPodcast.page} hrefPath={"/podcast"} hrefQuery={{

                            }} />
                        </div>
                        {/* End PodCast */}
                    </div>
                </div>
            </section>
            <div style={{ clear: "both" }}></div>

            <div className="modal fade" id="modal-podcast" aria-labelledby="modal-podcast" aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="estudios"><i className="fas fa-microphone-alt"></i> Podcast {`- ${podcastSelected.titulo || ''}`}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="portrait">
                                <img src={podcastSelected.podcast_imagen ? auth.pathApi() + podcastSelected.podcast_imagen[1].imagen : ""} alt={podcastSelected.titulo || ''} />
                            </div>
                            <div className={`subloader ${subloaderModal ? "active" : ""}`}></div>
                            <p><strong>Presentadores:</strong> {podcastSelected.presentadores || ''}</p>
                            <p><strong>Invitados:</strong> {podcastSelected.invitados || ''}</p>
                            <p><strong>Fecha de publicación:</strong> {podcastSelected.fecha || ''}</p>
                            {
                                podcastSelected.esEnlace ?
                                    <a rel="noreferrer" href={podcastSelected.urlExterno} className="btn btn-primary center-block" target="_blank"><i className="fas fa-play-circle"></i> Ir a audio</a>
                                    :
                                    <audio className="center-block w-100" src={auth.pathApi() + podcastSelected.urlAudio} controls={true}></audio>
                            }
                            <br />
                            <strong>Resumen:</strong>
                            <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: podcastSelected.resumen || "Sin resumen" }}></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Podcast;
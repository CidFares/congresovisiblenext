import React, { useState, useEffect } from "react";
import Head from "next/head";
import Select from "../../Components/Select";
import ProyectoLeyDataService from '../../Services/CongresoVisible/ProyectoLey.Service';
import ListProyectosLey from "../../Components/ListProyectosLey";
import UtilsDataService from "../../Services/General/Utils.Service";
import { URLBase } from "../../Constants/Constantes";
import CIDPagination from "../../Components/CIDPagination";

const defaultLegislatura = { value: -1, label: "Ver todas" };
const defaultCorporacion = { value: -1, label: "Ver Cámara y Senado" };
const defaultCuatrienio = { value: -1, label: "Ver todos" };
const defaultTipoProyecto = { value: -1, label: "Ver todos" };
const defaultPublicacion = { value: -1, label: "Ver todos" };
const defaultIniciativa = { value: -1, label: "Ver todas" };
const defaultEstado = { value: -1, label: "Ver todos" };
const defaultTema = { value: -1, label: "Ver todos" };
import AuthLogin from "../../Utils/AuthLogin";
const auth = new AuthLogin();

const ProyectosConst = {
    subloader: false,
    filterLegislatura: defaultLegislatura,
    filterCuatrienio: defaultCuatrienio,
    filterCorporacion: defaultCorporacion,
    filterTipoProyecto: defaultTipoProyecto,
    filterEstado: defaultEstado,
    filterIniciativa: defaultIniciativa,
    filterPublicacion: defaultPublicacion,
    filterTema: defaultTema,
    dataSelectLegislatura: [],
    dataSelectCuatrienio: [{ defaultCuatrienio }],
    dataSelectCorporacion: [{ defaultCorporacion }],
    dataSelectTipoProyecto: [{ defaultTipoProyecto }],
    dataSelectEstadoProyecto: [{ defaultEstado }],
    dataSelectPublicacion: [{ defaultPublicacion }],
    dataSelectIniciativa: [{ defaultIniciativa }],
    dataSelectTema: [{ defaultTema }],
    tableInfo: {
        data: [],
        totalRows: 0,
        page: 1,
        rows: 15,
        search: '',
    },
};

//SSR
export async function getServerSideProps({ query }) {
    let { DataProyectos, TotalProyectos } = await getAll(
        query.corporacion || ProyectosConst.filterCorporacion.value
        , query.cuatrienio || ProyectosConst.filterCuatrienio.value
        , query.legislatura || ProyectosConst.filterLegislatura.value
        , query.iniciativa || ProyectosConst.filterIniciativa.value
        , query.tema || ProyectosConst.filterTema.value
        , query.estado || ProyectosConst.filterEstado.value
        , query.tipoProyecto || ProyectosConst.filterTipoProyecto.value
        , query.page || ProyectosConst.tableInfo.page
        , query.rows || ProyectosConst.tableInfo.rows
        , query.search || ProyectosConst.tableInfo.search
    )
    return {
        props: { ListProyectos: DataProyectos, TotalProyectos, query },
    };
}

const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultCorporacion));
        if (!selected)
            selected = combo[0]
    });
    return { DSCorporacion: combo, SelectedCorporacion: selected };
};
const getPublicacion = async (id, nombre, activo, ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboTipoPublicacionProyectoLeyFilter({ id: id, nombre: nombre, activo: activo }).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultPublicacion));
        if (!selected)
            selected = combo[0]
    });
    return combo;
}

const getIniciativa = async (id, nombre, activo, ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboIniciativaFilter({ id: id, nombre: nombre, activo: activo }).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultIniciativa));
        if (!selected)
            selected = combo[0]
    });
    return { DSIniciativa: combo, SelectedIniciativa: selected };
}

const getComboCuatrienio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboCuatrienio().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultCuatrienio));
        if (!selected)
            selected = combo[0]
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};

const getComboLegislatura = async (id, nombre, fechaInicio, fechaFin, cuatrienio_id, activo, ID = null) => {
    let ids = [];
    let cuatrienio_ids = [];
    if (id) {
        if (Array.isArray(id)) {
            for (let i = 0; i < id.length; i++) {
                ids.push(id[i]);
            }
        }
        else {
            ids = JSON.stringify(ids);
        }
    }

    if (cuatrienio_id) {
        if (Array.isArray(cuatrienio_id)) {
            for (let i = 0; i < cuatrienio_id.length; i++) {
                cuatrienio_ids.push(cuatrienio_id[i]);
            }
        }
        else {
            cuatrienio_ids.push(cuatrienio_id);
        }

        cuatrienio_ids = JSON.stringify(cuatrienio_ids);
    }
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboLegislaturaFilter({ id: ids, nombre: nombre, fechaInicio: fechaInicio, fechaFin: fechaFin, cuatrienio_id: cuatrienio_ids, activo: activo }).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultLegislatura));
        if (!selected)
            selected = combo[0]
    });

    return { DSLegislatura: combo, SelectedLegislatura: selected };
};

const getComboTipoProyecto = async (ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboTipoProyecto().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultTipoProyecto));
        if (!selected)
            selected = combo[0]
    });
    return { DSTipoProyecto: combo, SelectedTipoProyecto: selected };
};
const getComboEstadoProyecto = async (ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboEstadoProyecto().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultEstado));
        if (!selected)
            selected = combo[0]
    });
    return { DSEstadoProyecto: combo, SelectedEstadoProyecto: selected };
};

const getComboTema = async (id, nombre, activo, ID = null) => {
    let combo = [];
    let selected = null;
    await UtilsDataService.getComboTemaProyectoLeyFilter({ id: id, nombre: nombre, activo: activo }).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift(Object.assign({}, defaultTema));
        if (!selected)
            selected = combo[0]
    });
    return { DSTema: combo, SelectedTema: selected };
};

const getAll = async (corporacion, cuatrienio, legislatura, iniciativa, tema, estado, tipoProyecto, page, rows, search) => {
    let DataProyectos = [];
    let TotalProyectos = 0;

    await ProyectoLeyDataService.getAll(corporacion, cuatrienio, legislatura, iniciativa, tema, estado, tipoProyecto, search, page, rows).then((response) => {
        DataProyectos = response.data;
    })
        .catch((e) => {
            console.log(e);
        });

    await ProyectoLeyDataService.getTotalRecords(corporacion, cuatrienio, legislatura, iniciativa, tema, estado, tipoProyecto, search).then((response) => {
        TotalProyectos = response.data;
    })
        .catch((e) => {
            console.log(e);
        });

    return { DataProyectos, TotalProyectos }
};

export default function ProyectosDeLey({ ListProyectos = ProyectosConst.tableInfo.data, TotalProyectos = ProyectosConst.tableInfo.data, query }) {
    const [subloader, setSubloader] = useState(ProyectosConst.subloader);
    const [filterCorporacion, setSelectCorporacion] = useState(ProyectosConst.filterCorporacion);
    const [filterCuatrienio, setSelectCuatrienio] = useState(ProyectosConst.filterCuatrienio);
    const [filterLegislatura, setSelectLegislatura] = useState(ProyectosConst.filterLegislatura);
    const [filterEstado, setSelectEstado] = useState(ProyectosConst.filterEstado);
    const [filterIniciativa, setSelectIniciativa] = useState(ProyectosConst.filterIniciativa);
    const [filterTema, setSelectTema] = useState(ProyectosConst.filterTema);
    const [filterTipoProyecto, setSelectTipoProyecto] = useState(ProyectosConst.filterTipoProyecto);
    const [filterPublicacion, setSelectPublicacion] = useState(ProyectosConst.filterPublicacion);
    const [dataSelectCorporacion, setDSCorporacion] = useState(ProyectosConst.dataSelectCorporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(ProyectosConst.dataSelectCuatrienio);
    const [dataSelectEstadoProyecto, setDSEstadoProyecto] = useState(ProyectosConst.dataSelectEstadoProyecto);
    const [dataSelectIniciativa, setDSIniciativa] = useState(ProyectosConst.dataSelectIniciativa);
    const [dataSelectLegislatura, setDSLegislatura] = useState(ProyectosConst.dataSelectLegislatura);
    const [dataSelectTema, setDSTema] = useState(ProyectosConst.dataSelectTema);
    const [dataSelectTipoProyecto, setDSTipoProyecto] = useState(ProyectosConst.dataSelectTipoProyecto);
    const [dataSelectPublicacion, setDSPublicacion] = useState(ProyectosConst.dataSelectPublicacion);
    const [proyectos, setListProyectos] = useState(ListProyectos);
    const [totalRowsProyectos, setTotalProyectos] = useState(TotalProyectos);
    const [pageProyectos, setPageProyectos] = useState(query.page || ProyectosConst.tableInfo.page);
    const [rowsProyectos, setRowsProyectos] = useState(query.rows || ProyectosConst.tableInfo.rows);
    const [searchProyectos, setSearchProyectos] = useState(query.search || ProyectosConst.tableInfo.search);

    useEffect(async () => {
        if (query.corporacion) {
            let { DSCorporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
            setDSCorporacion(DSCorporacion); setSelectCorporacion(SelectedCorporacion);
        }
        if (query.cuatrienio) {
            let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(Number(query.cuatrienio));
            setDSCuatrienio(DSCuatrienio); setSelectCuatrienio(SelectedCuatrienio);
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(null, null, null, null, Number(query.cuatrienio), 1);
            setDSLegislatura(DSLegislatura)
        }
        if (query.legislatura) {
            let { DSLegislatura, SelectedLegislatura } = await getComboLegislatura(null, null, null, null, Number(query.cuatrienio), 1, Number(query.legislatura));
            setDSLegislatura(DSLegislatura); setSelectLegislatura(SelectedLegislatura);
        }
        if (query.iniciativa) {
            let { DSIniciativa, SelectedIniciativa } = await getIniciativa(null, null, 1, Number(query.iniciativa));
            setDSIniciativa(DSIniciativa); setSelectIniciativa(SelectedIniciativa);
        }
        if (query.tema) {
            let { DSTema, SelectedTema } = await getComboTema(null, null, 1, Number(query.tema));
            setDSTema(DSTema); setSelectTema(SelectedTema);
        }
        if (query.estado) {
            let { DSEstadoProyecto, SelectedEstadoProyecto } = await getComboEstadoProyecto(Number(query.estado));
            setDSEstadoProyecto(DSEstadoProyecto); setSelectEstado(SelectedEstadoProyecto);
        }
        if (query.tipoProyecto) {
            let { DSTipoProyecto, SelectedTipoProyecto } = await getComboTipoProyecto(Number(query.tipoProyecto));
            setDSTipoProyecto(DSTipoProyecto); setSelectTipoProyecto(SelectedTipoProyecto);
        }
    }, []);

    const handlerFilterCorporacion = async (selectCorporacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', selectCorporacion.value, q)
        window.location = window.location.pathname + q
    };

    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('cuatrienio', selectCuatrienio.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterTipoProyecto = async (selectTipoProyecto) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoProyecto', selectTipoProyecto.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterLegislatura = async (selectLegisltatura) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('legislatura', selectLegisltatura.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterEstado = async (selectEstado) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('estado', selectEstado.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterTema = async (selectTema) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tema', selectTema.value, q)
        window.location = window.location.pathname + q
    }

    const handlerFilterIniciativa = async (selectIniciativa) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('iniciativa', selectIniciativa.value, q)
        window.location = window.location.pathname + q
    }

    const tableHandler = async (page, rowsA, searchA, isDelay) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }
    return (
        <div>
            <Head>
                <title>Proyectos de ley en el congreso | Congreso Visible</title>
                <meta name="description" content={`La información de los proyectos de ley discutidos en el congreso a nivel nacional la podrás encontar aquí. Visítanos`} />
                <meta name="keywords" content= {`Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring`} />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Proyectos de ley en el congreso | Congreso Visible" />
                <meta property="og:description" content={`La información de los proyectos de ley discutidos en el congreso a nivel nacional la podrás encontar aquí. Visítanos`} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:url" content={`${URLBase}/proyectos-de-ley`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/proyectos-de-ley`} />
                <meta name="twitter:title" content={"Proyectos de ley en el congreso | Congreso Visible"}/>
                <meta name="twitter:description" content={`La información de los proyectos de ley discutidos en el congreso a nivel nacional la podrás encontar aquí. Visítanos`} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/proyectos-de-ley/"/>
            </Head>
            <div className="pageTitle">
                <h1>Proyectos de Ley</h1>
            </div>
            <div className="listadoPageContainer">
                <div className="container-fluid">
                    <div className="listadoWPhotoContainer">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="row">
                            <div className="col-lg-3 col-md-12">
                                <div className="filtros-vertical">
                                    <h3><i className="fa fa-filter"></i> Filtros de información</h3>
                                    <div className="one-columns">
                                        <div className="item">
                                            <label htmlFor="">Corporación</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectCorporacion.length <= 1) {
                                                        let { DSCorporacion } = await getComboCorporacion();
                                                        setDSCorporacion(DSCorporacion);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterCorporacion}
                                                selectoptions={dataSelectCorporacion}
                                                selectOnchange={handlerFilterCorporacion}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Cuatrienio</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectCuatrienio.length <= 1) {
                                                        let { DSCuatrienio } = await getComboCuatrienio();
                                                        setDSCuatrienio(DSCuatrienio);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterCuatrienio}
                                                selectoptions={dataSelectCuatrienio}
                                                selectOnchange={handlerFilterCuatrienio}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Legislatura</label>
                                            <Select
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterLegislatura}
                                                selectoptions={dataSelectLegislatura}
                                                selectOnchange={handlerFilterLegislatura}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                noOptionsMessage="Debe elegir un cuatrienio"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Iniciativa</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectIniciativa.length <= 1) {
                                                        let { DSIniciativa } = await getIniciativa(null, null, 1);
                                                        setDSIniciativa(DSIniciativa);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterIniciativa}
                                                selectoptions={dataSelectIniciativa}
                                                selectOnchange={handlerFilterIniciativa}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Tema</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectTema.length <= 1) {
                                                        let { DSTema } = await getComboTema(null, null, 1);
                                                        setDSTema(DSTema);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterTema}
                                                selectoptions={dataSelectTema}
                                                selectOnchange={handlerFilterTema}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Tipo proyecto</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectTipoProyecto.length <= 1) {
                                                        let { DSTipoProyecto } = await getComboTipoProyecto();
                                                        setDSTipoProyecto(DSTipoProyecto);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterTipoProyecto}
                                                selectoptions={dataSelectTipoProyecto}
                                                selectOnchange={handlerFilterTipoProyecto}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                        <div className="item">
                                            <label htmlFor="">Estado de proyecto</label>
                                            <Select
                                                handlerOnClick={async () => {
                                                    if (dataSelectEstadoProyecto.length <= 1) {
                                                        let { DSEstadoProyecto } = await getComboEstadoProyecto();
                                                        setDSEstadoProyecto(DSEstadoProyecto);
                                                    }
                                                }}
                                                divClassclassName=""
                                                selectplaceholder="Seleccione"
                                                selectValue={filterEstado}
                                                selectoptions={dataSelectEstadoProyecto}
                                                selectOnchange={handlerFilterEstado}
                                                selectIsSearchable={true}
                                                selectclassNamePrefix="selectReact__value-container"
                                                spanClassclassName=""
                                                spanError="" >
                                            </Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-9 col-md-12">
                                <div className="buscador">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            placeholder="Escriba para buscar"
                                            className="form-control"
                                            value={searchProyectos}
                                            onChange={async (e) => { setSearchProyectos(e.target.value) }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await tableHandler(pageProyectos, rowsProyectos, e.target.value, false)
                                                }
                                            }}
                                        />
                                        <span className="input-group-text"><button onClick={async () => {
                                            await tableHandler(pageProyectos, rowsProyectos, e.target.value, false)
                                        }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    </div>
                                </div>
                                <ListProyectosLey
                                    data={proyectos}
                                    handler={tableHandler}
                                    pageExtends={pageProyectos}
                                    pageSizeExtends={rowsProyectos}
                                    totalRows={totalRowsProyectos}
                                    search={searchProyectos}
                                />
                                <CIDPagination totalPages={totalRowsProyectos} totalRows={query.rows || rowsProyectos} searchBy={query.search || searchProyectos} currentPage={query.page || pageProyectos} hrefPath={"/proyectos-de-ley"} hrefQuery={{
                                    corporacion: query.corporacion || filterCorporacion.value,
                                    legislatura: query.legislatura || filterLegislatura.value,
                                    cuatrienio: query.cuatrienio || filterCuatrienio.value,
                                    iniciativa: query.iniciativa || filterIniciativa.value,
                                    tema: query.tema || filterTema.value,
                                    estado: query.estado || filterEstado.value,
                                    tipoProyecto: query.tipoProyecto || filterTipoProyecto.value,
                                }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

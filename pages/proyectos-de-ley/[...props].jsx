import React, { useEffect, useState } from 'react'
import Link from "next/link";
import InputPercent from "../../Components/InputPercent";
import Head from "next/head"
import ProyectoLeyDataService from '../../Services/CongresoVisible/ProyectoLey.Service';
import AuthLogin from '../../Utils/AuthLogin'
import { Constantes, ProyectoLeyURLParameters, URLBase } from '../../Constants/Constantes'
import dynamic from 'next/dynamic'
import 'suneditor/dist/css/suneditor.min.css'

const SunEditor = dynamic(() => import('suneditor-react'), {
    ssr: false
});
const auth = new AuthLogin()

const ConstPage = {
    subloader: true
}
const ConstProyecto = {
    id: 0,
    cuatrienio_id: 0,
    legislatura_id: 0,
    corporacion_id: 0,
    titulo: "",
    alias: "",
    fecha_radicacion: "",
    numero_camara: "",
    numero_senado: "",
    iniciativa_id: 0,
    tipo_proyecto_id: 0,
    tema_id_principal: 0,
    tema_id_secundario: 0,
    sinopsis: "",
    se_acumula_a_id: null,
    alcance_id: 1,
    iniciativa_popular: null,
    activo: 1,
    legislatura: {
        id: 0,
        nombre: "",
        fechaInicio: "",
        fechaFin: "",
        cuatrienio_id: 0,
        activo: 0
    },
    cuatrienio: {
        id: 0,
        nombre: "",
        fechaInicio: 0,
        fechaFin: 0,
        activo: 0
    },
    tipo_proyecto_ley: {
        id: 0,
        nombre: "",
        activo: 0
    },
    iniciativa: {
        id: 0,
        nombre: "",
        activo: 0,
        aplica_persona: 0,
        aplica_congresista: 0
    },
    tema_principal: {
        id: 0,
        nombre: "",
        descripcion: "",
        activo: 0
    },
    tema_secundario: {
        id: 0,
        nombre: "",
        descripcion: "",
        activo: 0
    },
    proyecto_ley_autor_legislativos_cliente: [
        {
            id: 0,
            proyecto_ley_id: 0,
            congresista_id: 0,
            activo: 0,
            congresista_autor_legislativo: {
                id: 0,
                persona_id: 0,
                urlHojaVida: null,
                reemplazado: 0,
                activo: 1,
                persona_autor_legislativo: {
                    id: 0,
                    nombres: "",
                    apellidos: "",
                    fechaNacimiento: "",
                    municipio_id_nacimiento: 0,
                    profesion_id: 0,
                    genero_id: 0,
                    fecha_fallecimiento: null,
                    perfil_educativo: "",
                    grado_estudio_id: 0,
                    activo: 0,
                    lugar_nacimiento: {
                        id: 0,
                        nombre: "",
                        departamento_id: 0,
                        departamento: {
                            id: 0,
                            nombre: ""
                        }
                    },
                    imagenes: [
                        {
                            id: 0,
                            persona_id: 0,
                            imagen: "",
                            activo: 0
                        }
                    ]
                },
                detalle_proyecto: {
                    id: 0,
                    congresista_id: 0,
                    corporacion_id: 0,
                    cuatrienio_id: 0,
                    departamento_id_mayor_votacion: 0,
                    partido_id: 0,
                    corporacion: { nombre: "" },
                    partido: { nombre: "", partido_imagen: [{ imagen: "" }] },
                    curul_id: 0,
                    circunscripcion_id: 0,
                }
            },
        }
    ],
    proyecto_ley_autor_personas: [
        {
            id: 0,
            nombres: "",
            apellidos: "",
            fechaNacimiento: "",
            municipio_id_nacimiento: 0,
            profesion_id: 0,
            genero_id: 0,
            fecha_fallecimiento: null,
            perfil_educativo: "",
            grado_estudio_id: 0,
            activo: 0,
            lugar_nacimiento: {
                id: 0,
                nombre: "",
                departamento_id: 0,
                departamento: {
                    id: 0,
                    nombre: ""
                }
            },
            imagenes: [
                {
                    id: 0,
                    persona_id: 0,
                    imagen: "",
                    activo: 0
                }
            ]
        }
    ],
    proyecto_ley_estado: [
        {
            id: 0,
            proyecto_ley_id: 0,
            fecha: "",
            estado_proyecto_ley_id: 0,
            gaceta_texto: null,
            gaceta_url: null,
            nota: null,
            corporacion_id: 0,
            observaciones: null,
            orden: null,
            activo: 0,
            tipo_estado: {
                id: 0,
                nombre: "",
                activo: 1
            },
            corporacion: {
                id: 0,
                nombre: "",
                descripcion: "",
                activo: 0
            },
            comisiones: [
                {
                    id: 0,
                    proyecto_ley_estado_id: 0,
                    comision_id: 0,
                    activo: 0,
                    comision: {
                        id: 0,
                        nombre: "",
                        corporacion_id: 0,
                        tipo_comision_id: 0,
                        descripcion: "",
                        activo: 0,
                        camara_id: null,
                        permanente: null,
                        imagen: null,
                        orden: 0,
                        correo: null,
                        oficina: null,
                        telefono: null,
                        url: null,
                        corporacion: {
                            id: 0,
                            nombre: "",
                            descripcion: "",
                            activo: 0
                        },
                        tipo_comision: {
                            id: 0,
                            corporacion_id: 0,
                            nombre: "",
                            activo: 0
                        },
                        comision_imagen: [
                            {
                                id: 0,
                                comision_id: 0,
                                imagen: "",
                                activo: 0
                            }
                        ]
                    }
                }
            ],
            ponentes: [
                {
                    id: 0,
                    proyecto_ley_estado_id: 0,
                    congresista_id: 0,
                    activo: 0,
                    congresista_ponentes: {
                        id: 0,
                        persona_id: 0,
                        urlHojaVida: null,
                        reemplazado: 0,
                        activo: 0,
                        persona_autor_legislativo: {
                            id: 0,
                            nombres: "",
                            apellidos: "",
                            fechaNacimiento: "",
                            municipio_id_nacimiento: 0,
                            profesion_id: 0,
                            genero_id: 0,
                            fecha_fallecimiento: null,
                            perfil_educativo: "",
                            grado_estudio_id: 0,
                            activo: 0,
                            imagenes: [
                                {
                                    id: 0,
                                    persona_id: 0,
                                    imagen: "",
                                    activo: 0
                                }
                            ]
                        },
                        detalle_proyecto: {
                            id: 0,
                            congresista_id: 0,
                            corporacion_id: 0,
                            cuatrienio_id: 0,
                            corporacion: { nombre: "" },
                            departamento_id_mayor_votacion: 0,
                            partido_id: 0,
                            partido: { nombre: "", partido_imagen: [{ imagen: "" }] },
                            curul_id: 0,
                            circunscripcion_id: 0,
                        }
                    }
                }
            ]
        }
    ],
    proyecto_ley_detalle_votacion: [
        {
            id: 0,
            fecha: "",
            legislatura_id: 0,
            cuatrienio_id: 0,
            proyecto_de_ley_id: 0,
            esPlenaria: 1,
            esComision: 0,
            urlGaceta: null,
            motivo: "",
            tipo_votacion_id: 0,
            votosFavor: 0,
            votosContra: 0,
            acta: 0,
            observaciones: 0,
            aprobada: 0,
            votosAbstencion: 0,
            numero_no_asistencias: 0,
            clase_votacion_id: 0,
            numero_asistencias: 0,
            voto_general: 0,
            datos_importacion: null,
            resultados_importacion: null,
            activo: 1
        }
    ]
}
// SSR
export async function getServerSideProps({ query }) {
    let data = await getById(query.props[ProyectoLeyURLParameters.id]);
    return {
        props: { data }
    }
}

// PETICIONES

const getById = async (id) => {
    let data = null;
    await ProyectoLeyDataService.get(id).then((response) => {
        data = response.data;
    }).catch((e) => {
        console.error(e);
    });
    return data;
}
const getCountVotos = async (votacion) => {
    let promedios = [];
    await ProyectoLeyDataService.getCountVotos(votacion.id)
        .then((response) => {
            let result = response.data;
            let votos_totales = votacion.votacion_congresista.length;
            let promedio = {
                votacion_id: votacion.id,
                fecha_votacion: votacion.fecha,
                count_si: result.length === 0 ? 0 : result[0].conteo,
                porcentaje: Math.round(((result.length === 0 ? 0 : result[0].conteo) / votos_totales) * 100)
            }
            promedios.push(promedio);
        }).catch((e) => {
            console.log(e);
        });
    return promedios;
}
// EXPORT
const DetalleProyectoLey = ({ data = ConstProyecto }) => {
    const [subloader, setSubloader] = useState(ConstPage.subloader);
    useEffect(() => {
        setSubloader(false)
        console.log(data)
    }, [])
    return (
        <>
            <Head>
                <title>{auth.filterStringHTML(data.titulo || '').slice(0, 30)}... | Proyectos de Ley | Congreso Visible</title>
                <meta name="description" content={auth.filterStringHTML(data.titulo || '').slice(0, 160)} />
                <meta name="keywords" content= {`${data.titulo || ''}`} />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`${auth.filterStringHTML(data.titulo || '').slice(0, 30)}... | Proyectos de Ley | Congreso Visible`} />
                <meta property="og:description" content={auth.filterStringHTML(data.titulo || '').slice(0, 160)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/proyectos-de-ley/${auth.filterStringForURL("p" + data.titulo)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/proyectos-de-ley/${auth.filterStringForURL("p" + data.titulo)}/${data.id}`} />
                <meta name="twitter:title" content={`${auth.filterStringHTML(data.titulo || '').slice(0, 30)}... | Proyectos de Ley | Congreso Visible`}/>
                <meta name="twitter:description" content={auth.filterStringHTML(data.titulo || '').slice(0, 160)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/proyectos-de-ley/${auth.filterStringForURL("p" + data.titulo)}/${data.id}`}/>
            </Head>
            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${"https://blogs.udima.es/administracion-y-direccion-de-empresas/wp-content/uploads/2020/11/GettyImages-956243400-800x350.jpg"}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon littleIcon"><i className="fas fa-gavel"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>{data.titulo || ''}</h1>
                    </div>
                </div>
            </section>
            <section gtdtarget="1" className="text-justify no-full-height">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <h2>Información general</h2>
                            <hr />
                            <div className="two-columns no-margin text-left">
                                <p><strong className="strong">No. de proyecto en Cámara: </strong> {data.numero_camara}</p>
                                <p><strong className="strong">No. de proyecto en Senado: </strong> {data.numero_senado}</p>
                                <p><strong className="strong">Tema principal: </strong> {data.tema_principal?.nombre || ''}</p>
                                <p><strong className="strong">Tema secundario: </strong> {data.tema_secundario?.nombre || ''}</p>
                                <p><strong className="strong">Cuatrienio: </strong> {data.cuatrienio?.nombre || ''}</p>
                                <p><strong className="strong">Legislatura: </strong> {data.legislatura?.nombre || ''}</p>
                                <p><strong className="strong">Iniciativa: </strong> {data.iniciativa?.nombre || ''}</p>
                                <p><strong className="strong">Tipo de proyecto de ley: </strong> {data.tipo_proyecto_ley?.nombre || ''}</p>
                            </div>
                            <hr />

                            <hr />
                            <strong className="strong">Sinapsis</strong>
                            <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: data.sinopsis || "Sin sinapsis" }}></div>
                        </div>
                        {/* <div className="container-fluid">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="row">
                                <div className="col-md-12 center-block">
                                    <div className="dato">
                                        <div className="three-columns">
                                            {
                                                data.proyecto_ley_detalle_votacion.map((item, i) => {
                                                    return (
                                                        <div className="item relative">
                                                            <InputPercent value={item.porcentaje} fontSize={2.8} color={"var(--circle-chart-color-one)"} fontColor="#000" />
                                                            <div className="desc">
                                                                <p className="text-center">{`Resultado de la votación: ${item.fecha_votacion}`}</p>
                                                                <div className="link center">
                                                                    <a href={`#/detalle-votacion/${item.id}`} className="linkButton center"><i className="fas fa-chevron-right"></i> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    );
                                                })
                                            }

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> */}
                        <div className="col-md-4">
                            <div className="miembrosContainer">
                                <h3>Autores</h3>
                                <hr />
                                <div className="miembros">
                                    {
                                        data.iniciativa_id === 1 && data.iniciativa_id !== 5 ?
                                            data.proyecto_ley_autor_legislativos_cliente.map((item, i) => {
                                                if (item.congresista !== null) {
                                                    let href = auth.filterStringForURL(`${item.congresista_autor_legislativo.persona_autor_legislativo.nombres} ${item.congresista_autor_legislativo.persona_autor_legislativo.apellidos}`)
                                                    return (

                                                        <a href={`/congresistas/perfil/${href}/${item.congresista_autor_legislativo.persona_autor_legislativo.id}`} key={item.id}>
                                                            <div className="item">
                                                                <div className="photo">
                                                                    <img src={item.congresista_autor_legislativo.persona_autor_legislativo.imagenes[2] != undefined ? auth.pathApi() + item.congresista_autor_legislativo.persona_autor_legislativo.imagenes[2].imagen : Constantes.NoImagen} alt={item.congresista_autor_legislativo.persona_autor_legislativo.nombres + item.congresista_autor_legislativo.persona_autor_legislativo.apellidos}
                                                                    />
                                                                </div>
                                                                <div className="subphoto">
                                                                    {
                                                                        item.congresista_autor_legislativo.detalle_proyecto !== null ?
                                                                            <img src={item.congresista_autor_legislativo.detalle_proyecto.partido.partido_imagen[0] != undefined ?
                                                                                auth.pathApi() + item.congresista_autor_legislativo.detalle_proyecto.partido.partido_imagen[0].imagen : Constantes.NoImagenPicture} alt="partido" />
                                                                            :
                                                                            <img src={Constantes.NoImagenPicture} alt="partido" />
                                                                    }
                                                                </div>
                                                                <div className="info">
                                                                    <div className="name">
                                                                        <p>{item.congresista_autor_legislativo.persona_autor_legislativo.nombres || ''}{' '}{item.congresista_autor_legislativo.persona_autor_legislativo.apellidos || ''}</p>
                                                                    </div>
                                                                    <div className="job">
                                                                        <p>
                                                                            {
                                                                                item.congresista_autor_legislativo.detalle_proyecto !== null
                                                                                    ? item.congresista_autor_legislativo.detalle_proyecto.corporacion.nombre
                                                                                    : "Sin corporación" || ""
                                                                            }
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    );
                                                }
                                            })
                                            :
                                            ""
                                    }
                                    {
                                        (data.iniciativa_id === 1 || data.iniciativa_id === 2 || data.iniciativa_id === 3) && data.iniciativa_id !== 5 ?
                                            data.proyecto_ley_autor_personas.map((item, i) => {
                                                if (item.persona !== null) {
                                                    return (
                                                        <div className="item" key={item.id}>
                                                            <div className="photo">
                                                                <img
                                                                    src={typeof item?.persona?.imagenes[2] !== "undefined" ? auth.pathApi() + item.persona.imagenes[2].imagen : Constantes.NoImagen}
                                                                    alt={item.persona.nombres + item.persona.apellidos}
                                                                />
                                                            </div>
                                                            <div className="info">
                                                                <div className="name">
                                                                    <p>{item.persona.nombres || ''}{' '}{item.persona.apellidos || ''}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    );
                                                }
                                            })
                                            :
                                            ""
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-md-12">
                            <div className="coloquial-timeline">
                                <h3>Línea del tiempo</h3>
                                <ul>
                                    {
                                        data.proyecto_ley_estado.map((value, index, array) => {
                                            let url = '';
                                            let className = 'documento'

                                            if (value.gaceta_url) {
                                                url = auth.pathApi() + value.gaceta_url;
                                            }
                                            else {
                                                className += ' invisible'
                                            }

                                            return (
                                                <li key={index} onClick={(e) => { toggleTimelineRow(e.currentTarget, e.target) }}>
                                                    <div className="rowInfo">
                                                        <div className="fecha">{new Date(value.fecha).toLocaleDateString()}</div>
                                                        <div className="estado">{value.tipo_estado?.nombre || ''} - Gaceta: {
                                                            value.gaceta_texto !== null ?
                                                                <strong>{value.gaceta_texto}</strong>
                                                                : "S/N"
                                                        }</div>
                                                        <div className="debate"><p style={{ whiteSpace: "pre-line" }}>{value.nota || 'Sin anotaciones'}</p></div>

                                                    </div>
                                                    <div className="rowPonentes">
                                                        <hr />
                                                        <h3>Documento Gaceta</h3>
                                                        <hr />
                                                        {
                                                            value.gaceta_url ?
                                                                <a href={url} className="gaceta" rel="noreferrer" target="_blank"><i className="fas fa-download"></i></a>
                                                                :
                                                                <p>No disponible</p>
                                                        }
                                                        <p>Corporación: {value.corporacion?.nombre || 'Sin corporación'}</p>
                                                        <hr />
                                                        <h3>Ponentes</h3>
                                                        <hr />
                                                        <ul>
                                                            {
                                                                value.ponentes !== null && typeof value.ponentes !== "undefined" && typeof value.ponentes.length !== "undefined" ?
                                                                    value.ponentes.map((ponente, y) => {
                                                                        let href = auth.filterStringForURL(`${ponente.congresista_ponentes.persona_autor_legislativo.nombres} ${ponente.congresista_ponentes.persona_autor_legislativo.apellidos}`)
                                                                        return (
                                                                            <li key={y}>
                                                                                <div className="foto"><img src={typeof ponente.congresista_ponentes?.persona_autor_legislativo?.imagenes[1] !== "undefined" ? auth.pathApi() + ponente.congresista_ponentes?.persona_autor_legislativo?.imagenes[1].imagen : Constantes.NoImagen} alt="" /></div>
                                                                                <div className="info">
                                                                                    <p>
                                                                                        <a href={`/congresistas/perfil/${href}/${ponente.congresista_ponentes.persona_autor_legislativo.id}`} rel="noreferrer" target="_blank">{ponente.congresista_ponentes?.persona_autor_legislativo?.nombres + " " + ponente.congresista_ponentes?.persona_autor_legislativo?.apellidos || ""}</a>
                                                                                    </p>
                                                                                    <small>{ponente.congresista_ponentes?.partido?.nombre || ""}</small>
                                                                                </div>
                                                                            </li>
                                                                        );
                                                                    })
                                                                    : "Sin ponentes"
                                                            }
                                                        </ul>
                                                        <hr />
                                                        <h3>Comisiones asociadas</h3>
                                                        <hr />
                                                        <div className="rowComisiones">
                                                            <ul>
                                                                {
                                                                    value.comisiones !== null && typeof value.comisiones !== "undefined" && typeof value.comisiones.length !== "undefined" ?
                                                                        value.comisiones.map((estadoComision, y) => {
                                                                            return (
                                                                                <li key={y}>
                                                                                    <div className="foto"><img src={typeof estadoComision.comision?.comision_imagen[4] !== "undefined" ? auth.pathApi() + estadoComision.comision?.comision_imagen[4].imagen : Constantes.NoImagenPicture} alt="" /></div>
                                                                                    <div className="info">
                                                                                        <p>
                                                                                            <a href={`/comisiones/${estadoComision.comision?.id}`} rel="noreferrer" target="_blank">{estadoComision.comision?.nombre || ""}</a>
                                                                                        </p>
                                                                                        <small>{estadoComision.comision?.tipo_comision?.nombre || "Sin tipo"}</small>
                                                                                    </div>
                                                                                </li>
                                                                            );
                                                                        })
                                                                        : "Sin comisiones"
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            );
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )

}

function toggleTimelineRow(element, target) {
    if (target.classList.contains('gaceta') || target.parentNode.classList.contains('gaceta'))
        return false;

    element.querySelector(".rowInfo").classList.toggle("active")
    element.querySelector(".rowPonentes").classList.toggle("active")
}


export default DetalleProyectoLey

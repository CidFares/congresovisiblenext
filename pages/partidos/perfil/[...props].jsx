import Head from 'next/head';
import Link from "next/link";
import { PartidosURLParameters, Constantes, URLBase } from "../../../Constants/Constantes"
import PartidosDataService from "../../../Services/Catalogo/Partidos.Service.js";
import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import AuthLogin from "../../../Utils/AuthLogin";
const auth = new AuthLogin();

const PageData = {
    subloader: true
}
const ConstData = {
    id: 0,
    nombre: "",
    resenaHistorica: null,
    lineamientos: null,
    lugar: null,
    fechaDeCreacion: "",
    estatutos: "",
    color: null,
    partidoActivo: 0,
    activo: 0,
    partido_datos_contacto: [
        {
            id: 0,
            dato_contacto_id: 0,
            partido_id: 0,
            cuenta: "",
            activo: 0,
            datos_contacto: {
                id: 0,
                nombre: "",
                tipo: 0,
                activo: 0,
                datos_contacto_imagen: [{imagen: ""}]
            }
        }
    ],
    partido_imagen: [
        {
            id: 0,
            partido_id: 0,
            imagen: "",
            activo: 0
        }
    ]
}

// SSR
export async function getServerSideProps({ query }) {
    let data = await PartidosDataService.get(query.props[PartidosURLParameters.id]).then((response) => {
        return response.data;
    }).catch(e => {
        console.error(e)
    })

    return {
        props: { data, URLNombre: query.props[PartidosURLParameters.nombre], URLId: query.props[PartidosURLParameters.id] }
    }
}

// PETICIONES


// EXPORT
const PerfilCPartidos = ({ data = ConstData, URLNombre, URLId }) => {
    const [subloader, setSubloader] = useState(PageData.subloader);
    useEffect(() => {
        setSubloader(false)
    }, []);

    return (
        <>
            <Head>
                <title>Partido {data.nombre} | Congreso Visible</title>
                <meta name="description" content={`Perfil de Partido ${data.nombre}. Representantes, Senadores, Proyectos de Ley, Votaciones, Citaciones, Debates de Control Político, Actividad Legislativa.`} />
                <meta name="keywords" content= {`${data.nombre}, Partido Político, Bancada, Congresistas, Representantes, Senadores, Proyectos de Ley, Citaciones, Debates de Control Político, Actividad Legislativa, Reseña Histórica, Fecha de Creación, Estatutos`} />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`Partido ${data.nombre} | Congreso Visible`} />
                <meta property="og:description" content={`Perfil de Partido ${data.nombre}. Representantes, Senadores, Proyectos de Ley, Votaciones, Citaciones, Debates de Control Político, Actividad Legislativa.`} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/partidos/perfil/${auth.filterStringForURL(data.nombre)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/partidos/perfil/${auth.filterStringForURL(data.nombre)}/${data.id}`} />
                <meta name="twitter:title" content={`Partido ${data.nombre} | Congreso Visible`}/>
                <meta name="twitter:description" content={`Perfil de Partido ${data.nombre}. Representantes, Senadores, Proyectos de Ley, Votaciones, Citaciones, Debates de Control Político, Actividad Legislativa.`} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/partidos/perfil/${auth.filterStringForURL(data.nombre)}/${data.id}`} />
            </Head>
            <section className="profileSection no-overflow">
                <div className="background gradient">
                    <ul className="bg-bubbles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div className="profileContainer">
                    <div className="profilePhoto">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="photo">
                            <img src={typeof data.partido_imagen[2] !== "undefined" ? auth.pathApi() + data.partido_imagen[2].imagen : Constantes.NoImagen} alt={`${data.nombre}`} />
                        </div>
                        <div className="name">
                            <h1>{data.nombre || ''}</h1>
                            {data.activo === 0 ? <i style={{ color: "#e44e44" }} className="fas fa-arrow-alt-circle-down"></i> : ""}
                        </div>
                        <div className="corporacion">
                            <p>{data.lugar || ''}</p>
                        </div>
                        <div className="contact">
                            <div className="social-links text-center">
                                {
                                    data?.partido_datos_contacto.length !== 0 ?
                                        data?.partido_datos_contacto.map((x, i) => {
                                            if (x.datos_contacto !== null) {
                                                if (x.datos_contacto.tipo === 2) {
                                                    let href = x.cuenta.includes("http") ? x.cuenta : `https://${x.cuenta}`
                                                    return (
                                                        <a key={i} target="_blank" href={href} rel="noreferrer">
                                                            <img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombre}-${x.datos_contacto.nombre}`} />
                                                        </a>
                                                    )
                                                }
                                            }
                                        })
                                        :
                                        <p>No hay redes sociales asociadas</p>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="verticalTabsContainer">
                    <div className="verticalTab active">
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h2>Información del partido</h2></div>
                                    <hr />
                                    <div className="description">
                                        <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: data.resenaHistorica || "Sin reseña añadida" }}></div>
                                    </div>
                                </div>
                            </div>
                            <div className="info two-columns">
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Lugar</small>
                                        <p>{data.lugar || "S/N"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-user"></i></div>
                                    <div className="vertical-text">
                                        <small>Fecha de creación</small>
                                        <p>{ 
                                            data.fechaDeCreacion 
                                            ? new Date(data?.fechaDeCreacion + "T00:00:00").toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'
                                            : "S/F"
                                        }</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Estatutos</small>
                                        <div dangerouslySetInnerHTML={{ __html: data.estatutos }} />
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Lineamientos</small>
                                        <div dangerouslySetInnerHTML={{ __html: data.lineamientos }} />
                                    </div>
                                </div>
                            </div>
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h3>Datos de contacto</h3></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info three-columns">
                                {
                                    data?.partido_datos_contacto.map((x, i) => {
                                        if(x.partido_datos_contacto !== null){
                                            if (x.datos_contacto?.tipo === 1 && x.dato_contacto_id !== 7) { // no se imprime cédula
                                                return (
                                                    <div key={i} className="littleProfileCard">
                                                        <div className="icon"><img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombre}-${x.datos_contacto.nombre}`} /></div>
                                                        <div className="vertical-text">
                                                            <small>{x.datos_contacto.nombre}</small>
                                                            <p>{x.cuenta}</p>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        }
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}


export default PerfilCPartidos;
import Head from 'next/head'
import Select from '../../Components/Select';
import Image from 'next/image'
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
import { useState, useEffect } from "react";
import { Constantes, URLBase } from "../../Constants/Constantes";
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import infoSitioDataService from "../../Services/General/informacionSitio.Service";
import CIDPagination from "../../Components/CIDPagination";
import SquareCardsPartidos from "../../Components/CongresoVisible/SquareCardsPartidos";

const auth = new AuthLogin();
const PageConst = { imgPrincipal: null, subloader: false }
const PartidosConst = {
    listPartidos: {
        data: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 16,
        esModal: false,
        targetModal: '#modal-partido'
    },
    filterSelectActivo: { value: -1, label: "Ver todos" },
    dataSelectActivo: [{ value: -1, label: "Ver todos" }, { value: 1, label: "Activo" }, { value: 2, label: "Inactivo" }],
    selectPartido: {},
    subloaderModalPartido: false,
    subloaderFilters: false
}

export async function getServerSideProps({ query }) {
    let PageData = PageConst;
    let PartidosData = PartidosConst;
    await infoSitioDataService.getInformacionSitioHome()
        .then((response) => {
            PageData.imgPrincipal = response.data[0].imgPrincipal || Constantes.NoImagenPicture;
        })
        .catch((e) => {
            console.error(e);
        });
    PartidosData.listPartidos = await getAllPartidos(
        query.activo || PartidosData.filterSelectActivo.value
        , query.search || PartidosData.listPartidos.search
        , query.page || PartidosData.listPartidos.page
        , query.rows || PartidosData.listPartidos.rows
    );
    return {
        props: { PageData, PartidosData, query }
    }
}
const getAllPartidos = async (idFilterActive, search, page, rows) => {
    let listPartidos = PartidosConst.listPartidos;
    await ActividadesLegislativasDataService.getAllPartidos(
        idFilterActive,
        search, page, rows
    )
        .then((response) => {
            listPartidos.data = response.data;
        })
        .catch((e) => {
            console.error(e);
        });
    await ActividadesLegislativasDataService.getTotalRecordsPartidos(
        idFilterActive,
        search
    )
        .then((response) => {
            listPartidos.totalRows = response.data;
        })
        .catch((e) => {
            console.error(e);
        });
    return listPartidos;
}
export default function Partidos({ PageData = PageConst, PartidosData = PartidosConst, query }) {
    const [imgPrincipal, setImg] = useState(PageData.imgPrincipal);
    const [subloader, setSubloader] = useState(PageData.subloader);
    const [filterSelectActivo, setFilterActivo] = useState(PartidosData.filterSelectActivo);
    const [dataSelectActivo, setDSActivo] = useState(PartidosData.dataSelectActivo);
    const [listPartidos, setlistPartidos] = useState(PartidosData.listPartidos);
    const [search, setSearch] = useState(query.search || PartidosData.listPartidos.search);
    const [rows, setRows] = useState(query.rows || PartidosData.listPartidos.rows);
    const [page, setPage] = useState(query.page || PartidosData.listPartidos.page);

    useEffect(async () => {
        if (query.activo) {
            setFilterActivo(PartidosData.dataSelectActivo.find((x) => { return x.value === Number(query.activo) }))
        }
    }, []);

    const handlerFilterActivoPartido = async (select) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('activo', select.value, q)
        window.location = window.location.pathname + q
    }

    const handlerPaginationPartidos = async (page, rowsA, searchA = "") => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }


    return (
        <>
            <Head>
                <title>Partidos políticos en el congreso de Colombia | Congreso Visible</title>
                <meta name="description" content="En Congreso Visible encontrarás toda la información necesaria de los partidos políticos que representan al congreso. Visítanos para más información" />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Partidos políticos en el congreso de Colombia | Congreso Visible " />
                <meta property="og:description" content="En Congreso Visible encontrarás toda la información necesaria de los partidos políticos que representan al congreso. Visítanos para más información" />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/partidos`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/partidos`} />
                <meta name="twitter:title" content="Partidos políticos en el congreso de Colombia | Congreso Visible" />
                <meta name="twitter:description" content="En Congreso Visible encontrarás toda la información necesaria de los partidos políticos que representan al congreso. Visítanos para más información" />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/partidos/" />
            </Head>

            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + imgPrincipal}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-file-contract"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>Partidos</h1>
                    </div>
                </div>
            </section>
            <main>
                <div className="listadoPageContainer">
                    <div className="container-fluid">
                        <div className="centerTabs lg min-height-85">
                            <ul>
                                <li>
                                    <h2>
                                        <a href="/orden-del-dia">Agenda legislativa</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/votaciones">Votaciones</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/citaciones">Control político</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/funcion-electoral">Función electoral</a>
                                    </h2>
                                </li>
                                <li className="active">
                                    <h2>
                                        <a href="/partidos">Partidos</a>
                                    </h2>
                                </li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="contentTab active">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="row">
                                                <div className="col-lg-3 col-md-12">
                                                    <div className="filtros-vertical evenColors">
                                                        <h3><i className="fa fa-filter"></i> Filtros de información</h3>
                                                        <div className="one-columns">
                                                            <div className="item">
                                                                <label htmlFor="">Filtrar por activo</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterSelectActivo}
                                                                    selectoptions={dataSelectActivo}
                                                                    selectOnchange={handlerFilterActivoPartido}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-9 col-md-12">
                                                    <div className="buscador">
                                                        <div className="input-group">
                                                            <input type="text" value={search}
                                                                onChange={async (e) => {
                                                                    setSearch(e.target.value)
                                                                }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPaginationPartidos(listPartidos.page, listPartidos.rows, e.target.value)
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar" className="form-control" />

                                                            <span className="input-group-text"><button onClick={async () => { await handlerPaginationPartidos(listPartidos.page, listPartidos.rows, listPartidos.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                                        </div>
                                                    </div>
                                                    <SquareCardsPartidos
                                                        data={listPartidos.data}
                                                        handler={handlerPaginationPartidos}
                                                        defaultImage={Constantes.NoImagenPicture}
                                                        pageExtends={page}
                                                        pageSize={rows}
                                                        totalRows={listPartidos.totalRows}
                                                        pathImgOrigen={auth.pathApi()}
                                                        esModal={listPartidos.esModal}
                                                    />
                                                    <CIDPagination totalPages={listPartidos.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/partidos"} hrefQuery={{
                                                        activo: query.activo || filterSelectActivo.value
                                                    }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

        </>
    )
}

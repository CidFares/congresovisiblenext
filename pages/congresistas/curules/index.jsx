import Head from "next/head";
import CongresistasDataService from "../../../Services/Catalogo/Congresistas.Service";
import SelectCurul from "../../../Components/SelectCurul";
import Select from "../../../Components/Select";
import Link from "next/link";
import { useState, useEffect } from "react";
import AuthLogin from "../../../Utils/AuthLogin";
import {URLBase } from "../../../Constants/Constantes"
const auth = new AuthLogin();

const ConstGeneral = {
    subloader: true,
    filterCorporacion: { value: 1, label: "Cámara de Representantes" },
    filterCuatrienio: { value: 8, label: "2018 - 2022" },
    dataSelectCorporacion: [{ value: 1, label: "Cámara de Representantes" }],
    dataSelectCuatrienio: [{ value: 8, label: "2018 - 2022" }],
    curulData: [],
}

// SSR
export async function getServerSideProps({ query }) {
    let { DSCoroporacion, SelectedCorporacion } = await getComboCorporacion();
    let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio();
    let curulData = await getDataCuruls(SelectedCuatrienio.value, SelectedCorporacion.value)

    return {
        props: { DSCoroporacion, SelectedCorporacion, DSCuatrienio, SelectedCuatrienio, DataCurules: curulData },
    };
}

// PETICIONES
const getComboCuatrienio = async () => {
    let combo = [];
    let selected = {};
    await CongresistasDataService.getComboCuatrienio().then((response) => {
        let year = new Date().getFullYear();
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (year >= i.fechaInicio && year <= i.fechaFin) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        if (typeof selected.value === "undefined")
            selected = combo[combo.length - 1];
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};
const getComboCorporacion = async () => {
    let combo = [];
    let selected = {};
    await CongresistasDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
        });
        selected = combo[0];
    });
    return { DSCoroporacion: combo, SelectedCorporacion: selected };
};
const getDataCuruls = async (cuatrienio, corporacion) => {
    let curulData = [];
    await CongresistasDataService.getCurules(cuatrienio, corporacion).then((response) => {
        curulData = response.data;
    });
    return curulData;
};
// EXPORT
const CongresistasCurules = ({ DSCoroporacion = ConstGeneral.dataSelectCorporacion, SelectedCorporacion = ConstGeneral.filterCorporacion, DSCuatrienio = ConstGeneral.dataSelectCuatrienio, SelectedCuatrienio = ConstGeneral.filterCuatrienio, DataCurules = ConstGeneral.curulData }) => {
    const [subloader, setSubloader] = useState(ConstGeneral.subloader);
    const [dataSelectCorporacion, setDSCoroporacion] = useState(ConstGeneral.dataSelectCorporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(ConstGeneral.dataSelectCuatrienio);
    const [filterCorporacion, setSelectedCorporacion] = useState(ConstGeneral.filterCorporacion);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(ConstGeneral.filterCuatrienio);
    const [curulData, setCurulData] = useState(DataCurules);

    useEffect(() => {
        setDSCoroporacion(DSCoroporacion);
        setDSCuatrienio(DSCuatrienio);
        setSelectedCorporacion(SelectedCorporacion);
        setSelectedCuatrienio(SelectedCuatrienio);
        setSubloader(false)
    }, []);

     // HANDLERS

    const handlerFilterCorporacion = async (selectCorporacion) => {
        setSubloader(true);
        setSelectedCorporacion(selectCorporacion)
        let curules = await getDataCuruls(filterCuatrienio.value, selectCorporacion.value);
        setCurulData(curules);
        setSubloader(false)
    };
    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        setSelectedCuatrienio(selectCuatrienio);
        let curules = await getDataCuruls(selectCuatrienio.value, filterCorporacion.value);
        setCurulData(curules);
        setSubloader(false)
    };

    return (
        <>
            <Head>
            <title>Conoce a todos los congresistas {filterCuatrienio.label} de Colombia | Congreso Visible</title>
                <meta name="description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`Conoce a todos los congresistas ${filterCuatrienio.label} de Colombia | Congreso Visible`} />
                <meta property="og:description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/congresistas`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/congresistas`} />
                <meta name="twitter:title" content={`Conoce a todos los congresistas ${filterCuatrienio.label} de Colombia | Congreso Visible`}/>
                <meta name="twitter:description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/congresistas/curules"/>
            </Head>
            <div className="topPanel">
                <div className="pageTitle">
                    <h1>Curules de Congresistas</h1>
                </div>
            </div>
            <div className="listadoPageContainer">
                <div className="container-fluid">
                    <div className="filtros">
                        <div className="two-columns j-lr-margin">
                            <div className="item">
                                <label htmlFor="">Corporación</label>
                                <Select
                                    divClass=""
                                    selectplaceholder="Seleccione"
                                    selectValue={filterCorporacion}
                                    selectIsSearchable={true}
                                    selectoptions={dataSelectCorporacion}
                                    selectOnchange={handlerFilterCorporacion}
                                    selectclassNamePrefix="selectReact__value-container"
                                    spanClass="error"
                                    spanError=""
                                ></Select>
                            </div>
                            <div className="item">
                                <label htmlFor="">Cuatrienio</label>
                                <Select
                                    divClass=""
                                    selectplaceholder="Seleccione"
                                    selectValue={filterCuatrienio}
                                    selectIsSearchable={true}
                                    selectoptions={dataSelectCuatrienio}
                                    selectOnchange={handlerFilterCuatrienio}
                                    selectclassNamePrefix="selectReact__value-container"
                                    spanClass="error"
                                    spanError=""
                                ></Select>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="relative">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="centerTabs min-height-85" style={{ paddingTop: "25px" }}>
                            <ul>
                                <li><a href="/congresistas" onClick={()=>{setSubloader(true)}}>Lista</a></li>
                                <li className="active"><a href="/congresistas/curules" onClick={()=>{setSubloader(true)}}>Curules</a></li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className="contentTab active">
                                <h2 className="text-center tabsTitle">
                                    {filterCorporacion.label}
                                </h2>
                                <SelectCurul
                                    corporacion={filterCorporacion.value}
                                    linkToClickCurul={"/perfil-congresista"}
                                    data={curulData}
                                    curules={curulData}
                                    curulSelectable={false}
                                    origen={auth.pathApi()}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default CongresistasCurules;

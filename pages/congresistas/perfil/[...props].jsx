import Head from 'next/head';
import Link from "next/link";
import { useRouter } from 'next/router'
import { CongresistasURLParameters, Constantes, URLBase } from "../../../Constants/Constantes"
import CongresistasDataService from "../../../Services/Catalogo/Congresistas.Service";
import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Select from "../../../Components/Select";
import 'suneditor/dist/css/suneditor.min.css';
// const SunEditor = dynamic(() => import("suneditor-react"), {
//     ssr: false,
// });
import AuthLogin from "../../../Utils/AuthLogin";
const auth = new AuthLogin();

const PageData = {
    subloader: true
}
const ConstData = {
    id: 0,
    nombres: "",
    apellidos: "",
    fechaNacimiento: "",
    municipio_id_nacimiento: 0,
    profesion_id: 0,
    genero_id: 0,
    fecha_fallecimiento: null,
    perfil_educativo: "",
    grado_estudio_id: 0,
    activo: 0,
    lugar_nacimiento: {
        id: 0,
        departamento_id: 0,
        nombre: ""
    },
    grado_estudio: {
        id: 0,
        nombre: ""
    },
    genero: {
        id: 0,
        nombre: "",
        genero_imagen: [{ id: 0, genero_id: 0, imagen: "" }]
    },
    profesion: {
        id: 0,
        nombre: ""
    },
    congresista: {
        id: 0,
        persona_id: 0,
        urlHojaVida: null,
        reemplazado: 0,
        activo: 1,
        reemplazo: null,
        cargo: null,
        detalle: [
            {
                id: 0,
                congresista_id: 0,
                corporacion_id: 0,
                cuatrienio_id: 0,
                partido_id: 0,
                curul_id: 0,
                circunscripcion_id: 0,
                departamento_id_mayor_votacion: 0,
                activo: 0,
                departamento: {
                    id: 0,
                    region_id: 0,
                    nombre: "",
                    activo: 0
                },
                corporacion: {
                    id: 0,
                    nombre: "",
                    descripcion: "",
                    activo: 0
                },
                cuatrienio: {
                    id: 0,
                    nombre: "",
                    fechaInicio: 0,
                    fechaFin: 0,
                    activo: 0
                },
                partido: {
                    id: 0,
                    nombre: "",
                    resenaHistorica: "",
                    lineamientos: "",
                    lugar: "",
                    fechaDeCreacion: "",
                    estatutos: "",
                    color: "",
                    partidoActivo: 0,
                    activo: 0,
                    partido_imagen: [
                        {
                            id: 0,
                            partido_id: 0,
                            imagen: "",
                            activo: 0
                        }
                    ]
                },
                circunscripcion: {
                    id: 0,
                    nombre: "",
                    departamento_id: 0,
                    activo: 0
                },

            }
        ],
        investigaciones: [
            {
                id: 0, congresista_id: 0, tipo_investigacion_id: 0, descripcion: "",
                tipo_investigacion: { id: 0, nombre: "" }
            }
        ]
    },
    persona_trayectoria_publica: [
        {
            id: 0, persona_id: 0, cargo: "", fecha: "", fecha_final: "",
            partido: {
                id: 0,
                nombre: "",
                color: "",
                partido_imagen: [{ id: 0, partido_id: 0, imagen: "" }]
            }
        }
    ],
    persona_trayectoria_privada: [{ id: 0, persona_id: 0, cargo: "", fecha: "", fecha_final: "" }],
    imagenes: [{ id: 0, persona_id: 0, imagen: "" }],
    contactos: [
        {
            id: 0, persona_id: 0, dato_contacto_id: 0, cuenta: "",
            datos_contacto: {
                id: 0, nombre: "", tipo: "", datos_contacto_imagen: [
                    { id: 0, datos_contacto_id: 0, imagen: "" }
                ]
            }
        }
    ]
}

const ConstDataDetalle = {
    filterCuatrienio: { value: -1, label: "" },
    dataSelectCuatrienio: [{ value: -1, label: "" }],
    congresistaDetalleSelected: Object.assign({}, ConstData.congresista.detalle[0])
}

// SSR
export async function getServerSideProps({ query }) {
    let data = await CongresistasDataService.get(query.props[CongresistasURLParameters.id]).then((response) => {
        return response.data;
    }).catch(e => {
        console.error(e)
    })

    let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(data.congresista.detalle);
    let DetalleSelected = data.congresista.detalle.find((x) => { return x.cuatrienio_id === SelectedCuatrienio.value });

    return {
        props: { data, URLNombre: query.props[CongresistasURLParameters.nombre], URLId: query.props[CongresistasURLParameters.id], DSCuatrienio, SelectedCuatrienio, DetalleSelected }
    }
}

// PETICIONES
const getComboCuatrienio = async (detalle = ConstData.congresista.detalle) => {
    let combo = [];
    let selected = {};

    let year = new Date().getFullYear();
    detalle.forEach((i) => {
        combo.push({ value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` });
        if (year >= i.cuatrienio.fechaInicio && year <= i.cuatrienio.fechaFin) {
            selected = { value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` };
        }
    });
    if (typeof selected.value === "undefined")
        selected = combo[0];

    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};
// EXPORT
const PerfilCongresistas = ({ data = ConstData, URLNombre, URLId, DSCuatrienio = ConstDataDetalle.dataSelectCuatrienio, SelectedCuatrienio = ConstDataDetalle.filterCuatrienio, DetalleSelected = ConstDataDetalle.congresistaDetalleSelected }) => {
    const [subloader, setSubloader] = useState(PageData.subloader);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [localDetalleSelected, setDetalleSelected] = useState(DetalleSelected);
    useEffect(() => {
        setSubloader(false)
    }, []);

    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        setSelectedCuatrienio(selectCuatrienio);
        let detalle = data.congresista.detalle.find((x) => { return x.cuatrienio_id === selectCuatrienio.value });
        setDetalleSelected(detalle)
        setSubloader(false)
    };
    return (
        <>
            <Head>
                <title>{data.nombres} {data.apellidos} | Perfil congresista | Congreso Visible </title>
                <meta name="description" content={auth.filterStringHTML(data.perfil_educativo) || data.nombres + " " + data.apellidos + " " + "| Perfil congresista | Congreso Visible"} />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={data.nombres + " " + data.apellidos + " " + "| Perfil congresista | Congreso Visible"} />
                <meta property="og:description" content={auth.filterStringHTML(data.perfil_educativo) || data.nombres + " " + data.apellidos + " " + "| Perfil congresista | Congreso Visible"} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/congresistas/perfil/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/congresistas/perfil/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta name="twitter:title" content={data.nombres + " " + data.apellidos + " " + "| Perfil congresista | Congreso Visible"} />
                <meta name="twitter:description" content={auth.filterStringHTML(data.perfil_educativo) || data.nombres + " " + data.apellidos + " " + "| Perfil congresista | Congreso Visible"} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/congresistas/perfil/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`}/>
            </Head>
            <section className="profileSection no-overflow">
                <div className="background gradient">
                    <ul className="bg-bubbles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div className="profileContainer">
                    <div className="profilePhoto">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="verticalProfileTabsContainer">
                            <ul>
                                <li className="active">
                                    <a href={`/congresistas/perfil/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-user"></i></div>
                                        <div className="desc"><p>Perfil</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/autorias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-gavel"></i></div>
                                        <div className="desc"><p>Autorías</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/ponencias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="far fa-hand-point-up"></i></div>
                                        <div className="desc"><p>Ponencias</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/citaciones/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-exclamation-circle"></i></div>
                                        <div className="desc"><p>Control político</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/conflictos-de-interes/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-hand-holding-usd"></i></div>
                                        <div className="desc"><p>Conflictos de interés</p></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="PerfilFilter">
                            <Select
                                divClass=""
                                selectplaceholder="Seleccione"
                                selectValue={filterCuatrienio}
                                selectIsSearchable={true}
                                selectoptions={dataSelectCuatrienio}
                                selectOnchange={handlerFilterCuatrienio}
                                selectclassNamePrefix="selectReact__value-container"
                                spanClass="error"
                                spanError=""
                            ></Select>
                        </div>
                        <div className="photo">
                            <img src={typeof data.imagenes[2] !== "undefined" ? auth.pathApi() + data.imagenes[2].imagen : Constantes.NoImagen} alt={`${data.nombres} ${data.apellidos}`} />
                        </div>
                        <div className="name">
                            <h1>{data.nombres || ''} {data.apellidos || ''}</h1>
                            {data.congresista.activo === 0 ? <i style={{ color: "#e44e44" }} className="fas fa-arrow-alt-circle-down"></i> : ""}
                        </div>
                        <div className="job">
                            <p>
                                <img src={typeof localDetalleSelected?.partido?.partido_imagen[0] !== "undefined"
                                    ? auth.pathApi() + localDetalleSelected?.partido?.partido_imagen[0].imagen
                                    : Constantes.NoImagenPicture} alt={localDetalleSelected.partido.nombre} />
                                {localDetalleSelected.partido?.nombre || 'Sin partido'}
                            </p>
                        </div>
                        <div className="littleProfileCard">
                            <div className="icon"><i className="fas fa-walking"></i></div>
                            <div className="vertical-text">
                                <small>Circunscripción</small>
                                <p>{localDetalleSelected.circunscripcion?.nombre || "Sin circunscripción"} en {localDetalleSelected.departamento?.nombre || "..."}</p>
                            </div>
                        </div>
                        <div className="corporacion">
                            <p>{localDetalleSelected.corporacion?.nombre || "Sin corporación"}</p>
                        </div>
                        <div className="contact">
                            <div className="social-links text-center">
                                {
                                    data?.contactos.length !== 0 ?
                                        data?.contactos.map((x, i) => {
                                            if (x.datos_contacto !== null) {
                                                if (x.datos_contacto.tipo === 2) {
                                                    let href = x.cuenta.includes("http") ? x.cuenta : `https://${x.cuenta}`
                                                    return (
                                                        <a key={i} target="_blank" href={href} rel="noreferrer">
                                                            <img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombres}-${data.apellidos}-${x.datos_contacto.nombre}`} />
                                                        </a>
                                                    )
                                                }
                                            }
                                        })
                                        :
                                        <p>No hay redes sociales asociadas</p>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="verticalTabsContainer">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="PerfilTabs">
                            <ul>
                                <li onClick={(e) => { changeTab(e) }} className="active" data-ref="1">
                                    <div className="icon"><i className="fa fa-user"></i></div>
                                    <div className="desc"><p>Acerca de</p></div>
                                </li>
                                <li onClick={(e) => { changeTab(e) }} data-ref="2">
                                    <div className="icon"><i className="fas fa-chart-line"></i></div>
                                    <div className="desc"><p>Trayectoria</p></div>
                                </li>
                                <li onClick={(e) => { changeTab(e) }} data-ref="3">
                                    <div className="icon"><i className="fas fa-search"></i></div>
                                    <div className="desc"><p>Investigaciones</p></div>
                                </li>
                                <li onClick={(e) => { changeTab(e) }} data-ref="4">
                                    <div className="icon"><i className="fas fa-male"></i></div>
                                    <div className="desc"><p>Reemplazo</p></div>
                                </li>
                            </ul>
                        </div>
                        <div className="verticalTab active" data-ref="1">
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h2>Perfil</h2></div>
                                    <hr />
                                    <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: data.perfil_educativo || "Sin descripción académica" }}>
                                    </div>
                                </div>
                            </div>
                            <div className="info three-columns">
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Grado de estudio</small>
                                        <p>{data.grado_estudio?.nombre || "Sin grado de estudio"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Profesión</small>
                                        <p>{data.profesion?.nombre || "Sin profesión"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="far fa-clock"></i></div>
                                    <div className="vertical-text">
                                        <small>Cuatrienio</small>
                                        <p>{localDetalleSelected.cuatrienio?.nombre || "Sin cuatrienio"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-user"></i></div>
                                    <div className="vertical-text">
                                        <small>Fecha de nacimiento</small>
                                        <p>{new Date(data?.fechaNacimiento + "T00:00:00").toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'}</p>
                                    </div>
                                </div>
                                {
                                    data?.fecha_fallecimiento !== null ?
                                        <div className="littleProfileCard">
                                            <div className="icon"><i className="fas fa-user"></i></div>
                                            <div className="vertical-text">
                                                <small>Fecha de fallecimiento</small>
                                                <p>{new Date(data?.fecha_fallecimiento + "T00:00:00").toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'}</p>
                                            </div>
                                        </div>
                                        : ""
                                }

                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-user"></i></div>
                                    <div className="vertical-text">
                                        <small>Edad</small>
                                        <p>{auth.getAge(data.fechaNacimiento + "T00:00:00") || "Sin edad asignada"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-globe-americas"></i></div>
                                    <div className="vertical-text">
                                        <small>Lugar de nacimiento</small>
                                        <p>{data.lugar_nacimiento?.nombre || "Sin lugar asignado"}</p>
                                    </div>
                                </div>
                                <div className="littleProfileCard">
                                    <div className="icon"><img src={auth.pathApi() + data.genero?.genero_imagen[0]?.imagen} alt={`${data.nombres}-${data.apellidos}-${data.genero.nombre}`} /></div>
                                    <div className="vertical-text">
                                        <small>Género</small>
                                        <p>{data.genero?.nombre || "Sin género asignado"}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h3>Datos de contacto</h3></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info three-columns">
                                {
                                    data?.contactos.map((x, i) => {
                                        if (x.datos_contacto !== null) {
                                            if (x.datos_contacto?.tipo === 1 && x.dato_contacto_id !== 7) { // no se imprime cédula
                                                return (
                                                    <div key={i} className="littleProfileCard">
                                                        <div className="icon"><img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombres}-${data.apellidos}-${x.datos_contacto.nombre}`} /></div>
                                                        <div className="vertical-text">
                                                            <small>{x.datos_contacto.nombre}</small>
                                                            <p>{x.cuenta}</p>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        }
                                    })
                                }
                            </div>
                        </div>
                        <div className="verticalTab" data-ref="2">
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h4>Trayectoria pública</h4></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info three-columns">
                                {
                                    data.persona_trayectoria_publica.length > 0 ?
                                        data?.persona_trayectoria_publica.map((x, i) => {
                                            return (
                                                <div key={i} className="littleProfileCard wpadding relative">
                                                    <div className="icon">
                                                        {
                                                            x.partido !== null ?
                                                                <img src={auth.pathApi() + x.partido.partido_imagen[0]?.imagen} alt={x.partido.nombre} />
                                                                :
                                                                <img src={Constantes.NoImagenPicture} alt="Sin imagen" />
                                                        }
                                                    </div>
                                                    <div className="description">
                                                        <p><strong>Partido:</strong> {x.partido?.nombre || "S/N"}</p>
                                                        <p><strong>Cargo:</strong> {x.cargo || 'Sin cargo asignado'}</p>
                                                        <p><strong>Fecha de inicio:</strong> {auth.coloquialDate(x.fecha) || 'Sin fecha'}</p>
                                                        <p><strong>Fecha de finalización:</strong> {auth.coloquialDate(x.fecha_final) || 'Sin fecha'}</p>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                        <p>Sin trayectoria asignada</p>
                                }
                            </div>

                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h4>Trayectoria privada</h4></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info three-columns">
                                {
                                    data.persona_trayectoria_privada?.length > 0 ?
                                        data.persona_trayectoria_privada.map((x, i) => {
                                            return (
                                                <div key={i} className="littleProfileCard wpadding relative">
                                                    <div className="description">
                                                        <p><strong>Cargo:</strong> {x.cargo || 'Sin cargo asignado'}</p>
                                                        <p><strong>Fecha de inicio:</strong> {auth.coloquialDate(x.fecha) || 'Sin fecha'}</p>
                                                        <p><strong>Fecha de finalización:</strong> {auth.coloquialDate(x.fecha_final) || 'Sin fecha'}</p>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                        <p>Sin trayectoria asignada</p>
                                }
                            </div>
                        </div>
                        <div className="verticalTab" data-ref="3">
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h4>Investigaciones</h4></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info two-columns">
                                {
                                    data.congresista.investigaciones.length > 0 ?
                                        data.congresista.investigaciones.map((x, i) => {
                                            return (
                                                <div key={i} className="littleProfileCard wpadding relative">
                                                    <div className="icon"><i className="fas fa-search"></i></div>
                                                    <div className="description">
                                                        <p><strong>Tipo de investigación:</strong> {x.tipo_investigacion !== null ? x.tipo_investigacion.nombre : ""}</p>
                                                        <p><strong>Detalles:</strong></p>
                                                        <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: x.descripcion || "Sin descripción" }}></div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        :
                                        <p>Sin investigaciones</p>
                                }
                            </div>
                        </div>
                        <div className="verticalTab" data-ref="4">
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h5>Reemplazo</h5></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="info one-columns">
                                {
                                    data.congresista.reemplazo !== null && typeof data.congresista.reemplazo !== "undefined" ?
                                        <div className="littleProfileCard wpadding relative">
                                            <div className="icon"><i className="fas fa-user"></i></div>
                                            <div className="description text-center">
                                                <p><strong>Nombre:</strong> {data.congresista.reemplazo.persona !== null ? data.congresista.reemplazo.persona.nombres + " " + data.congresista.reemplazo.persona.apellidos : ""}</p>
                                                <p><strong>Periodo:</strong> {data.congresista.reemplazo !== null ? data.congresista.reemplazo?.fecha_inicio + " - " + data.congresista.reemplazo?.fecha_fin : "S/F"}</p>
                                                <a style={{ margin: "auto" }} rel="noreferrer" target="_blank" href={`/congresistas/perfil/${auth.filterStringForURL(`${data.nombres} ${data.apellidos}`)}/${URLId}`} className="linkButton text-center"><i className="fas fa-chevron-right"></i></a>
                                            </div>
                                        </div>
                                        :
                                        <p>Sin reemplazo</p>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

function changeTab(e) {
    let tabs = document.querySelectorAll(".verticalTabsContainer ul li");
    tabs.forEach(y => {
        y.classList.remove("active");
    })
    tabs.forEach(z => {
        if (document.querySelector(`.verticalTabsContainer .verticalTab[data-ref="${z.getAttribute("data-ref")}"]`)) {
            document.querySelector(`.verticalTabsContainer .verticalTab[data-ref="${z.getAttribute("data-ref")}"]`).classList.remove("active")
        }
    })
    e.currentTarget.classList.add("active");
    if (document.querySelector(`.verticalTabsContainer .verticalTab[data-ref="${e.currentTarget.getAttribute("data-ref")}"]`)) {
        document.querySelector(`.verticalTabsContainer .verticalTab[data-ref="${e.currentTarget.getAttribute("data-ref")}"]`).classList.add("active")
    }
}

export default PerfilCongresistas;
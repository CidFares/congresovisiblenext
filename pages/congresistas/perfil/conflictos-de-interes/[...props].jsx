import Head from 'next/head';
import Link from "next/link";
import { CongresistasURLParameters, Constantes, URLBase } from "../../../../Constants/Constantes"
import CongresistasDataService from "../../../../Services/Catalogo/Congresistas.Service";
import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Select from "../../../../Components/Select";
import AccordionCheckbox from "../../../../Components/AccordionCheckbox";
import MoneyInput from "../../../../Components/MoneyInput";
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import AuthLogin from "../../../../Utils/AuthLogin";
const auth = new AuthLogin();

const PageData = {
    subloader: true,
    accordionConflictos: {
        infoGeneral: false,
        juntasAsociaciones: false,
        infoCercanias: false,
        ingresos: false,
        acreencias: false,
        bienes: false,
        documentos: false
    }
}
const ConstData = {
    id: 0,
    nombres: "",
    apellidos: "",
    fechaNacimiento: "",
    municipio_id_nacimiento: 0,
    profesion_id: 0,
    genero_id: 0,
    fecha_fallecimiento: null,
    perfil_educativo: "",
    grado_estudio_id: 0,
    activo: 0,
    congresista: {
        id: 0,
        persona_id: 0,
        urlHojaVida: null,
        reemplazado: 0,
        activo: 1,
        reemplazo: null,
        cargo: null,
        detalle: [
            {
                id: 0,
                congresista_id: 0,
                corporacion_id: 0,
                cuatrienio_id: 0,
                partido_id: 0,
                curul_id: 0,
                circunscripcion_id: 0,
                departamento_id_mayor_votacion: 0,
                activo: 0,
                departamento: {
                    id: 0,
                    region_id: 0,
                    nombre: "",
                    activo: 0
                },
                corporacion: {
                    id: 0,
                    nombre: "",
                    descripcion: "",
                    activo: 0
                },
                cuatrienio: {
                    id: 0,
                    nombre: "",
                    fechaInicio: 0,
                    fechaFin: 0,
                    activo: 0
                },
                partido: {
                    id: 0,
                    nombre: "",
                    resenaHistorica: "",
                    lineamientos: "",
                    lugar: "",
                    fechaDeCreacion: "",
                    estatutos: "",
                    color: "",
                    partidoActivo: 0,
                    activo: 0,
                    partido_imagen: [
                        {
                            id: 0,
                            partido_id: 0,
                            imagen: "",
                            activo: 0
                        }
                    ]
                },
                circunscripcion: {
                    id: 0,
                    nombre: "",
                    departamento_id: 0,
                    activo: 0
                },

            }
        ]
    },
    imagenes: [{ id: 0, persona_id: 0, imagen: "" }],
    contactos: [
        {
            id: 0, persona_id: 0, dato_contacto_id: 0, cuenta: "",
            datos_contacto: {
                id: 0, nombre: "", tipo: "", datos_contacto_imagen: [
                    { id: 0, datos_contacto_id: 0, imagen: "" }
                ]
            }
        }
    ]
}

const ConstConflictoInteres = {
    cf_info_general: {
        id: 0,
        congresista_detalle_id: 0,
        conflictos_declarados: "",
        actividad_economica: "",
        activo: 0,
        usercreated: null,
        usermodifed: null,
        created_at: null,
        updated_at: null
    },
    cf_juntas_asociaciones: [
        {
            id: 0,
            congresista_detalle_id: 0,
            nombre_entidad: "",
            cf_organo_id: 0,
            cf_calidad_miembro_id: 0,
            pais_id: 0,
            activo: 0,
            usercreated: null,
            usermodifed: null,
            created_at: null,
            updated_at: null,
            calidad_miembro: {
                id: 0,
                Nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: ""
            },
            organo: {
                id: 0,
                Nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: ""
            },
            pais: {
                id: 0,
                iso: "",
                nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: null
            }
        }
    ],
    cf_acreencias: [
        {
            id: 352,
            congresista_detalle_id: 2388,
            cf_acreencia_cpt: "",
            saldo: "",
            activo: 0,
            usercreated: null,
            usermodifed: null,
            created_at: null,
            updated_at: null
        }
    ],
    cf_bienes: [
        {
            id: 0,
            congresista_detalle_id: 0,
            cf_tipo_bien: "",
            pais_id: 0,
            cf_departamento_id: 0,
            valor: "",
            activo: 0,
            usercreated: null,
            usermodifed: null,
            created_at: null,
            updated_at: null,
            pais: {
                id: 0,
                iso: "",
                nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: null
            },
            departamento: {
                id: 0,
                pais_id: 0,
                nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: ""
            }
        }
    ],
    cf_documentos: [
        {
            id: 0,
            congresista_detalle_id: 0,
            cf_tipo_documento_id: 0,
            year: 0,
            url_documento: "",
            nombre: "",
            activo: 0,
            usercreated: null,
            usermodifed: null,
            created_at: null,
            updated_at: null,
            tipo_documento: {
                id: 0,
                nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: null
            }
        }
    ],
    cf_info_cercanias: [
        {
            id: 0,
            congresista_detalle_id: 0,
            nombre: "",
            cf_parentesco_id: 0,
            conflicto_interes: "",
            activo: 0,
            usercreated: null,
            usermodifed: null,
            created_at: null,
            updated_at: null,
            parentesco: {
                id: 0,
                Nombre: "",
                activo: 0,
                usercreated: "",
                usermodifed: null,
                created_at: "",
                updated_at: ""
            }
        }
    ],
    cf_ingresos: {
        id: 0,
        congresista_detalle_id: 0,
        salario_anual: "",
        interes_cesantias_anual: "",
        gasto_representacion_anual: "",
        arriendo_anual: "",
        honorario_anual: "",
        otros_anual: "",
        total_anual: "",
        activo: 0,
        usercreated: null,
        usermodifed: null,
        created_at: null,
        updated_at: null
    }
};

const ConstDataDetalle = {
    filterCuatrienio: { value: -1, label: "" },
    dataSelectCuatrienio: [{ value: -1, label: "" }],
    congresistaDetalleSelected: Object.assign({}, ConstData.congresista.detalle[0])
}

// SSR
export async function getServerSideProps({ query }) {
    let data = await CongresistasDataService.getBasico(query.props[CongresistasURLParameters.id]).then((response) => {
        return response.data;
    }).catch(e => {
        console.error(e)
    })

    let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(data.congresista.detalle);
    let DetalleSelected = data.congresista.detalle.find((x) => { return x.cuatrienio_id === SelectedCuatrienio.value });

    let ConflictosIntereses = await getConflictoInteresByIdCongresista(data.congresista.id, DetalleSelected.cuatrienio_id);

    return {
        props: { data, URLNombre: query.props[CongresistasURLParameters.nombre], URLId: query.props[CongresistasURLParameters.id], DSCuatrienio, SelectedCuatrienio, DetalleSelected, ConflictosIntereses }
    }
}

// PETICIONES
const getComboCuatrienio = async (detalle = ConstData.congresista.detalle) => {
    let combo = [];
    let selected = {};

    let year = new Date().getFullYear();
    detalle.forEach((i) => {
        combo.push({ value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` });
        if (year >= i.cuatrienio.fechaInicio && year <= i.cuatrienio.fechaFin) {
            selected = { value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` };
        }
    });
    if (typeof selected.value === "undefined")
        selected = combo[0];

    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};
const getConflictoInteresByIdCongresista = async (id, idCuatrienio) => {
    let DataConflictos = null;

    await CongresistasDataService.getConflictoInteresByIdCongresista(id, idCuatrienio).then((response) => {
        DataConflictos = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    return DataConflictos;
};

// EXPORT
const PerfilCongresistaConflictoIntereses = ({ data = ConstData, URLNombre, URLId, DSCuatrienio = ConstDataDetalle.dataSelectCuatrienio, SelectedCuatrienio = ConstDataDetalle.filterCuatrienio, DetalleSelected = ConstDataDetalle.congresistaDetalleSelected, ConflictosIntereses = ConstConflictoInteres }) => {
    const [subloader, setSubloader] = useState(PageData.subloader);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [localDetalleSelected, setDetalleSelected] = useState(DetalleSelected);
    const [conflictos, setConflictos] = useState(ConflictosIntereses);
    const [ACCInfoGeneral, setACCInfoGeneral] = useState(PageData.accordionConflictos.infoGeneral);
    const [ACCJuntasAsociaciones, setACCJuntasAsociaciones] = useState(PageData.accordionConflictos.juntasAsociaciones);
    const [ACCInfoCercanias, setACCInfoCercanias] = useState(PageData.accordionConflictos.infoCercanias);
    const [ACCIngresos, setACCIngresos] = useState(PageData.accordionConflictos.ingresos);
    const [ACCAcreencias, setACCAcreencias] = useState(PageData.accordionConflictos.acreencias);
    const [ACCBienes, setACCBienes] = useState(PageData.accordionConflictos.bienes);
    const [ACCDocumentos, setACCDocumentos] = useState(PageData.accordionConflictos.documentos);

    useEffect(() => {
        setSubloader(false)
    }, []);

    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        setSelectedCuatrienio(selectCuatrienio);
        let detalle = data.congresista.detalle.find((x) => { return x.cuatrienio_id === selectCuatrienio.value });
        setDetalleSelected(detalle)
        let DataConflictos = await getConflictoInteresByIdCongresista(data.congresista.id, selectCuatrienio.value);
        setConflictos(DataConflictos);
        setSubloader(false)
    };

    const handlerCheckboxInfoGeneral = async (value) => {
        setACCInfoGeneral(value);
    }
    const handlerCheckboxJuntasAsociaciones = async (value) => {
        setACCJuntasAsociaciones(value);
    }
    const handlerCheckboxInfoCercanias = async (value) => {
        setACCInfoCercanias(value);
    }
    const handlerCheckboxIngresos = async (value) => {
        setACCIngresos(value);
    }
    const handlerCheckboxAcreencias = async (value) => {
        setACCAcreencias(value);
    }
    const handlerCheckboxBienes = async (value) => {
        setACCBienes(value);
    }
    const handlerCheckboxDocumentos = async (value) => {
        setACCDocumentos(value);
    }
    const getYearDocumentosSoporte = () => {
        let years = [];
        let documentos = conflictos?.cf_documentos;
        if (documentos) {
            let y = 0;
            documentos.forEach((x, i) => {
                if (i === 0) {
                    y = x.year
                    years.push(x.year)
                }
                else if (y !== x.year) {
                    years.push(x.year);
                    y = x.year
                }
            })

            return years;
        }
    }

    return (
        <>
            <Head>
                <title>{data.nombres} {data.apellidos} | Conflictos de interés</title>
                <meta name="description" content={data.nombres + " " + data.apellidos + " " + "| Conflictos de interés"} />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={data.nombres + " " + data.apellidos + " " + "| Conflictos de interés"} />
                <meta property="og:description" content={data.nombres + " " + data.apellidos + " " + "| Conflictos de interés"} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/congresistas/perfil/conflictos-de-interes/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/congresistas/perfil/conflictos-de-interes/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta name="twitter:title" content={data.nombres + " " + data.apellidos + " " + "| Conflictos de interés"} />
                <meta name="twitter:description" content={data.nombres + " " + data.apellidos + " " + "| Conflictos de interés"} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/congresistas/perfil/conflictos-de-interes/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`}/>
            </Head>
            <section className="profileSection no-overflow">
                <div className="background gradient">
                    <ul className="bg-bubbles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div className="profileContainer">
                    <div className="profilePhoto">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="verticalProfileTabsContainer">
                            <ul>
                                <li>
                                    <a href={`/congresistas/perfil/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-user"></i></div>
                                        <div className="desc"><p>Perfil</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/autorias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-gavel"></i></div>
                                        <div className="desc"><p>Autorías</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/ponencias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="far fa-hand-point-up"></i></div>
                                        <div className="desc"><p>Ponencias</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/citaciones/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-exclamation-circle"></i></div>
                                        <div className="desc"><p>Control político</p></div>
                                    </a>
                                </li>
                                <li className="active">
                                    <a href={`/congresistas/perfil/conflictos-de-interes/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-hand-holding-usd"></i></div>
                                        <div className="desc"><p>Conflictos de interés</p></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="PerfilFilter">
                            <Select
                                divClass=""
                                selectplaceholder="Seleccione"
                                selectValue={filterCuatrienio}
                                selectIsSearchable={true}
                                selectoptions={dataSelectCuatrienio}
                                selectOnchange={handlerFilterCuatrienio}
                                selectclassNamePrefix="selectReact__value-container"
                                spanClass="error"
                                spanError=""
                            ></Select>
                        </div>
                        <div className="photo">
                            <img src={typeof data.imagenes[2] !== "undefined" ? auth.pathApi() + data.imagenes[2].imagen : Constantes.NoImagen} alt={`${data.nombres} ${data.apellidos}`} />
                        </div>
                        <div className="name">
                            <h1>{data.nombres || ''} {data.apellidos || ''}</h1>
                            {data.congresista.activo === 0 ? <i style={{ color: "#e44e44" }} className="fas fa-arrow-alt-circle-down"></i> : ""}
                        </div>
                        <div className="job">
                            <p>
                                <img src={typeof localDetalleSelected?.partido?.partido_imagen[0] !== "undefined"
                                    ? auth.pathApi() + localDetalleSelected?.partido?.partido_imagen[0].imagen
                                    : Constantes.NoImagenPicture} alt={localDetalleSelected.partido.nombre} />
                                {localDetalleSelected.partido?.nombre || 'Sin partido'}
                            </p>
                        </div>
                        <div className="littleProfileCard">
                            <div className="icon"><i className="fas fa-walking"></i></div>
                            <div className="vertical-text">
                                <small>Circunscripción</small>
                                <p>{localDetalleSelected.circunscripcion?.nombre || "Sin circunscripción"} en {localDetalleSelected.departamento.nombre || "..."}</p>
                            </div>
                        </div>
                        <div className="corporacion">
                            <p>{localDetalleSelected.corporacion?.nombre || "Sin corporación"}</p>
                        </div>
                        <div className="contact">
                            <div className="social-links text-center">
                                {
                                    data?.contactos.length !== 0 ?
                                        data?.contactos.map((x, i) => {
                                            if (x.datos_contacto !== null) {
                                                if (x.datos_contacto.tipo === 2) {
                                                    let href = x.cuenta.includes("http") ? x.cuenta : `https://${x.cuenta}`
                                                    return (
                                                        <a key={i} target="_blank" href={href} rel="noreferrer">
                                                            <img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombres}-${data.apellidos}-${x.datos_contacto.nombre}`} />
                                                        </a>
                                                    )
                                                }
                                            }
                                        })
                                        :
                                        <p>No hay redes sociales asociadas</p>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="verticalTabsContainer">
                        <div className="verticalTab active">
                            <div className={`subloader ${subloader ? "active" : ""}`}></div>
                            <div className="info">
                                <div className="littleSection two-columns">
                                    <div className="title"><h5>Conflictos de interés</h5></div>
                                    <div className="PerfilFilter">
                                        <Select
                                            divClass=""
                                            selectplaceholder="Seleccione"
                                            selectValue={filterCuatrienio}
                                            selectIsSearchable={true}
                                            selectoptions={dataSelectCuatrienio}
                                            selectOnchange={handlerFilterCuatrienio}
                                            selectclassNamePrefix="selectReact__value-container"
                                            spanClass="error"
                                            spanError=""
                                        ></Select>
                                    </div>

                                </div>
                                <hr />
                            </div>
                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxInfoGeneral} label={"Información general"} open={ACCInfoGeneral}>
                                {
                                    conflictos?.cf_info_general !== null ?
                                        <>
                                            <div className="cfItemDescription">
                                                <label htmlFor=""><i className="fas fa-caret-right"></i> Conflictos declarados</label>
                                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: conflictos?.cf_info_general.conflictos_declarados || "Sin conflictos declarados" }}></div>
                                            </div>
                                            <div className="cfItemDescription">
                                                <label htmlFor=""><i className="fas fa-caret-right"></i>  Actividad económica privada </label>
                                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: conflictos?.cf_info_general.actividad_economica || "Sin actividad económica privada" }}></div>
                                            </div>
                                        </>
                                        :
                                        <p>Sin conflictos declarados</p>
                                }
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxJuntasAsociaciones} label={"Participación en Juntas Directivas, Consejos, Corporaciones, Sociedades y/o Asociaciones"} open={ACCJuntasAsociaciones}>
                                <div className="one-columns">
                                    {
                                        conflictos?.cf_juntas_asociaciones.length > 0 ?
                                            conflictos?.cf_juntas_asociaciones.map((item, i) => {
                                                return (
                                                    <div className="listadoItem type-2" key={i}>
                                                        <div className="itemHeader">
                                                            <h4>{item.nombre_entidad}</h4>
                                                        </div>
                                                        <div className="itemFooter">
                                                            <div className="with-pdb">
                                                                <p>{item.organo.Nombre || 'Sin nombre'}</p>
                                                                <div className="sub">
                                                                    <p>Órgano</p>
                                                                </div>
                                                            </div>
                                                            <div className="with-pdb" style={{ minWidth: "190px" }}>
                                                                <p>{item.calidad_miembro?.Nombre || 'Sin nombre'}</p>
                                                                <div className="sub">
                                                                    <p>Calidad de miembro</p>
                                                                </div>
                                                            </div>
                                                            <div className="with-pdb">
                                                                <p>{item.pais.nombre || 'Sin nombre'}</p>
                                                                <div className="sub">
                                                                    <p>País</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                            :
                                            <p>Sin información añadida</p>
                                    }
                                </div>
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxInfoCercanias} label={"Información de cónyuge, compañero(a) permanente y/o parientes"} open={ACCInfoCercanias}>
                                <div className="two-columns">
                                    {
                                        conflictos?.cf_info_cercanias.length > 0 ?
                                            conflictos?.cf_info_cercanias.map((item, i) => {
                                                return (
                                                    <div className="listadoItem type-2" key={i}>
                                                        <div className="itemHeader">
                                                            <h4>{item.nombre}</h4>
                                                        </div>
                                                        <div className="itemBody">
                                                            <p style={{ whiteSpace: "pre-line" }}><strong>Posible conflicto de interés:</strong> {item.conflicto_interes}</p>
                                                        </div>
                                                        <div className="itemFooter">
                                                            <div className="with-pdb">
                                                                <p>{item.parentesco.Nombre || 'Sin parentesco'}</p>
                                                                <div className="sub">
                                                                    <p>Parentesco</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                            :
                                            <p>Sin información de parientes, cónyuge o compañero(a) permanente</p>
                                    }
                                </div>
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxIngresos} label={"Información patrimonial"} open={ACCIngresos}>
                                {
                                    conflictos?.cf_ingresos !== null ?
                                        <div className="three-columns justify-center">
                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.salario_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Salario anual</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.interes_cesantias_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Intereses cesantías anuales</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.gasto_representacion_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Gastos de representación anuales</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.arriendo_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Arriendos anuales</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.honorario_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Honorarios anuales</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.otros_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Otros ingresos anuales</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="listadoItem type-2">
                                                <div className="itemHeader">
                                                    <h4 className="text-center">
                                                        <MoneyInput showAddon={false} value={conflictos?.cf_ingresos.total_anual || 'No reporta'} justTextForReadOnly={true} />
                                                    </h4>
                                                </div>
                                                <div className="itemFooter">
                                                    <div>
                                                        <p>Total de ingresos anuales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <p>Sin ingresos declarados</p>
                                }
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxAcreencias} label={"Acreencias"} open={ACCAcreencias}>
                                <div className="three-columns">
                                    {
                                        conflictos?.cf_acreencias.length > 0 ?
                                            conflictos?.cf_acreencias.map((x, i) => {
                                                return (
                                                    <div className="listadoItem type-2" key={i}>
                                                        <div className="itemHeader">
                                                            <h4 className="text-center"><MoneyInput showAddon={false} value={x.saldo || 'Sin saldo'} justTextForReadOnly={true} /></h4>
                                                        </div>
                                                        <div className="itemFooter">
                                                            <div>
                                                                <p>{x.cf_acreencia_cpt}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                            :
                                            <p>Sin acreencias declaradas</p>
                                    }
                                </div>
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxBienes} label={"Bienes patrimoniales"} open={ACCBienes}>
                                <div className="two-columns">
                                    {
                                        conflictos?.cf_bienes.length > 0 ?
                                            conflictos?.cf_bienes.map((x, i) => {
                                                return (
                                                    <div className="listadoItem type-2" key={i}>
                                                        <div className="itemHeader">
                                                            <h4>{x.cf_tipo_bien || 'Sin tipo'}</h4>
                                                        </div>
                                                        <div className="itemFooter">
                                                            <div className="with-pdb">
                                                                <MoneyInput showAddon={false} value={x.valor || 'Sin valor'} justTextForReadOnly={true} />
                                                                <div className="sub">
                                                                    <p>Valor</p>
                                                                </div>
                                                            </div>
                                                            <div className="with-pdb">
                                                                <p>{`${x.pais.nombre}, ${x.departamento.nombre}` || 'Sin valor'}</p>
                                                                <div className="sub">
                                                                    <p>Ubicación</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                            :
                                            <p>Sin bienes declarados</p>
                                    }
                                </div>
                            </AccordionCheckbox>

                            <AccordionCheckbox handlerCheckboxSelected={handlerCheckboxDocumentos} label={"Documentos de soporte"} open={ACCDocumentos}>
                                <div className="one-columns">
                                    {
                                        getYearDocumentosSoporte()?.length > 0 ?
                                            getYearDocumentosSoporte().map((year, i) => {
                                                let documentos = conflictos?.cf_documentos.filter((e) => { return e.year === year })
                                                return (
                                                    <div className="listadoItem type-2" key={i}>
                                                        <div className="itemHeader">
                                                            <h4>{year}</h4>
                                                        </div>
                                                        <div className="itemFooter no-out">
                                                            {
                                                                documentos.map((doc, j) => {
                                                                    return (
                                                                        <a key={j} rel="noreferrer" className="button" target="_blank" href={`${auth.pathApi()}${doc.url_documento}`}><i style={{ marginRight: "7px" }} className="fas fa-download"></i> {doc.tipo_documento.nombre}</a>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                            :
                                            <p>Sin documentos añadidos</p>
                                    }
                                </div>
                            </AccordionCheckbox>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}


export default PerfilCongresistaConflictoIntereses;
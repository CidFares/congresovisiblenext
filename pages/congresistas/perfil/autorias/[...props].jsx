import Head from 'next/head';
import Link from "next/link";
import { CongresistasURLParameters, Constantes, URLBase } from "../../../../Constants/Constantes"
import CongresistasDataService from "../../../../Services/Catalogo/Congresistas.Service";
import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Select from "../../../../Components/Select";
import PerfilCongresistaSubList from "../../../../Components/CongresoVisible/PerfilCongresistaSubList";
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import AuthLogin from "../../../../Utils/AuthLogin";
const auth = new AuthLogin();

const PageData = {
    subloader: true
}
const ConstData = {
    id: 0,
    nombres: "",
    apellidos: "",
    fechaNacimiento: "",
    municipio_id_nacimiento: 0,
    profesion_id: 0,
    genero_id: 0,
    fecha_fallecimiento: null,
    perfil_educativo: "",
    grado_estudio_id: 0,
    activo: 0,
    congresista: {
        id: 0,
        persona_id: 0,
        urlHojaVida: null,
        reemplazado: 0,
        activo: 1,
        reemplazo: null,
        cargo: null,
        detalle: [
            {
                id: 0,
                congresista_id: 0,
                corporacion_id: 0,
                cuatrienio_id: 0,
                partido_id: 0,
                curul_id: 0,
                circunscripcion_id: 0,
                departamento_id_mayor_votacion: 0,
                activo: 0,
                departamento: {
                    id: 0,
                    region_id: 0,
                    nombre: "",
                    activo: 0
                },
                corporacion: {
                    id: 0,
                    nombre: "",
                    descripcion: "",
                    activo: 0
                },
                cuatrienio: {
                    id: 0,
                    nombre: "",
                    fechaInicio: 0,
                    fechaFin: 0,
                    activo: 0
                },
                partido: {
                    id: 0,
                    nombre: "",
                    resenaHistorica: "",
                    lineamientos: "",
                    lugar: "",
                    fechaDeCreacion: "",
                    estatutos: "",
                    color: "",
                    partidoActivo: 0,
                    activo: 0,
                    partido_imagen: [
                        {
                            id: 0,
                            partido_id: 0,
                            imagen: "",
                            activo: 0
                        }
                    ]
                },
                circunscripcion: {
                    id: 0,
                    nombre: "",
                    departamento_id: 0,
                    activo: 0
                },

            }
        ]
    },
    imagenes: [{ id: 0, persona_id: 0, imagen: "" }],
    contactos: [
        {
            id: 0, persona_id: 0, dato_contacto_id: 0, cuenta: "",
            datos_contacto: {
                id: 0, nombre: "", tipo: "", datos_contacto_imagen: [
                    { id: 0, datos_contacto_id: 0, imagen: "" }
                ]
            }
        }
    ]
}

const ConstAutorias = {
    data: [],
    propiedades:
    {
        id: 'proyecto.id',
        title: 'proyecto.titulo',
        description:
            [
                { title: "Tema principal", text: "proyecto.tema_principal.nombre" },
                { title: "Tema secundario", text: "proyecto.tema_secundario.nombre" },
                { title: "Tipo", text: "proyecto.tipo_proyecto_ley.nombre" },
                { title: "Iniciativa", text: "proyecto.iniciativa.nombre" },
            ]
    },
    totalRows: 0,
    search: "",
    page: 1,
    rows: 32
};

const ConstDataDetalle = {
    filterCuatrienio: { value: -1, label: "" },
    dataSelectCuatrienio: [{ value: -1, label: "" }],
    congresistaDetalleSelected: Object.assign({}, ConstData.congresista.detalle[0])
}

// SSR
export async function getServerSideProps({ query }) {
    let data = await CongresistasDataService.getBasico(query.props[CongresistasURLParameters.id]).then((response) => {
        return response.data;
    }).catch(e => {
        console.error(e)
    })

    let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(data.congresista.detalle);
    let DetalleSelected = data.congresista.detalle.find((x) => { return x.cuatrienio_id === SelectedCuatrienio.value });

    let { DataAutorias, TotalAutorias } = await getAutoriasByIdCongresista(data.congresista.id, ConstAutorias.search, ConstAutorias.page, ConstAutorias.rows)
    let ListAutorias = Object.assign({}, ConstAutorias);
    ListAutorias.data = DataAutorias;
    ListAutorias.totalRows = TotalAutorias;
    return {
        props: { data, URLNombre: query.props[CongresistasURLParameters.nombre], URLId: query.props[CongresistasURLParameters.id], DSCuatrienio, SelectedCuatrienio, DetalleSelected, ListAutorias }
    }
}

// PETICIONES
const getComboCuatrienio = async (detalle = ConstData.congresista.detalle) => {
    let combo = [];
    let selected = {};

    let year = new Date().getFullYear();
    detalle.forEach((i) => {
        combo.push({ value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` });
        if (year >= i.cuatrienio.fechaInicio && year <= i.cuatrienio.fechaFin) {
            selected = { value: i.cuatrienio_id, label: `En ${i.cuatrienio.nombre} de ${i.corporacion.nombre}` };
        }
    });
    if (typeof selected.value === "undefined")
        selected = combo[0];

    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};
const getAutoriasByIdCongresista = async (id, search, page, rows) => {
    let DataAutorias = [];
    let TotalAutorias = 0;

    await CongresistasDataService.getAutoriasByIdCongresista(id, search, page, rows).then((response) => {
        DataAutorias = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    await CongresistasDataService.totalrecordsAutorias(id, search).then((response) => {
        TotalAutorias = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    return { DataAutorias, TotalAutorias };
};

// EXPORT
const PerfilCongresistasAutorias = ({ data = ConstData, URLNombre, URLId, DSCuatrienio = ConstDataDetalle.dataSelectCuatrienio, SelectedCuatrienio = ConstDataDetalle.filterCuatrienio, DetalleSelected = ConstDataDetalle.congresistaDetalleSelected, ListAutorias = ConstAutorias }) => {
    const [subloader, setSubloader] = useState(PageData.subloader);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [localDetalleSelected, setDetalleSelected] = useState(DetalleSelected);
    const [autorias, setAutorias] = useState(ListAutorias.data);
    const [totalRows, setTotalRows] = useState(ListAutorias.totalRows);
    const [searchAutoria, setSearch] = useState(ListAutorias.search);
    const [pageAutoria, setPageAutoria] = useState(ListAutorias.page);
    const [rowsAutoria, setRowsAutoria] = useState(ListAutorias.rows);

    useEffect(() => {
        setSubloader(false)
    }, []);

    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        setSelectedCuatrienio(selectCuatrienio);
        let detalle = data.congresista.detalle.find((x) => { return x.cuatrienio_id === selectCuatrienio.value });
        setDetalleSelected(detalle)
        setSubloader(false)
    };

    const handlerPaginationAutorias = async (page, rows, search = "") => {

        setTimeout(async () => {
            setSubloader(true);
            setPageAutoria(page);
            setRowsAutoria(rows);
            setSearch(search);
            let { DataAutorias, TotalAutorias } = await getAutoriasByIdCongresista(data.congresista.id, search, page, rows);
            setAutorias(DataAutorias);
            setTotalRows(TotalAutorias);
            setSubloader(false);
        }, 1000);

    }

    return (
        <>
            <Head>
                <title>{data.nombres} {data.apellidos} | Autorías </title>
                <meta name="description" content={data.nombres + " " + data.apellidos + " " + "| Autorías"} />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={data.nombres + " " + data.apellidos + " " + "| Autorías"} />
                <meta property="og:description" content={data.nombres + " " + data.apellidos + " " + "| Autorías"} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/congresistas/perfil/autorias/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/congresistas/perfil/autorias/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`} />
                <meta name="twitter:title" content={data.nombres + " " + data.apellidos + " " + "| Autorías"} />
                <meta name="twitter:description" content={data.nombres + " " + data.apellidos + " " + "| Autorías"} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/congresistas/perfil/autorias/${auth.filterStringForURL(data.nombres + " " + data.apellidos)}/${data.id}`}/>
            </Head>
            <section className="profileSection no-overflow">
                <div className="background gradient">
                    <ul className="bg-bubbles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
                <div className="profileContainer">
                    <div className="profilePhoto">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="verticalProfileTabsContainer">
                            <ul>
                                <li>
                                    <a href={`/congresistas/perfil/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-user"></i></div>
                                        <div className="desc"><p>Perfil</p></div>
                                    </a>
                                </li>
                                <li className="active">
                                    <a href={`/congresistas/perfil/autorias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fa fa-gavel"></i></div>
                                        <div className="desc"><p>Autorías</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/ponencias/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="far fa-hand-point-up"></i></div>
                                        <div className="desc"><p>Ponencias</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/citaciones/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-exclamation-circle"></i></div>
                                        <div className="desc"><p>Control político</p></div>
                                    </a>
                                </li>
                                <li>
                                    <a href={`/congresistas/perfil/conflictos-de-interes/${URLNombre}/${URLId}`} onClick={() => { setSubloader(true) }}>
                                        <div className="icon"><i className="fas fa-hand-holding-usd"></i></div>
                                        <div className="desc"><p>Conflictos de interés</p></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="PerfilFilter">
                            <Select
                                divClass=""
                                selectplaceholder="Seleccione"
                                selectValue={filterCuatrienio}
                                selectIsSearchable={true}
                                selectoptions={dataSelectCuatrienio}
                                selectOnchange={handlerFilterCuatrienio}
                                selectclassNamePrefix="selectReact__value-container"
                                spanClass="error"
                                spanError=""
                            ></Select>
                        </div>
                        <div className="photo">
                            <img src={typeof data.imagenes[2] !== "undefined" ? auth.pathApi() + data.imagenes[2].imagen : Constantes.NoImagen} alt={`${data.nombres} ${data.apellidos}`} />
                        </div>
                        <div className="name">
                            <h1>{data.nombres || ''} {data.apellidos || ''}</h1>
                            {data.congresista.activo === 0 ? <i style={{ color: "#e44e44" }} className="fas fa-arrow-alt-circle-down"></i> : ""}
                        </div>
                        <div className="job">
                            <p>
                                <img src={typeof localDetalleSelected?.partido?.partido_imagen[0] !== "undefined"
                                    ? auth.pathApi() + localDetalleSelected?.partido?.partido_imagen[0].imagen
                                    : Constantes.NoImagenPicture} alt={localDetalleSelected.partido?.nombre} />
                                {localDetalleSelected.partido?.nombre || 'Sin partido'}
                            </p>
                        </div>
                        <div className="littleProfileCard">
                            <div className="icon"><i className="fas fa-walking"></i></div>
                            <div className="vertical-text">
                                <small>Circunscripción</small>
                                <p>{localDetalleSelected.circunscripcion?.nombre || "Sin circunscripción"} en {localDetalleSelected.departamento?.nombre || "..."}</p>
                            </div>
                        </div>
                        <div className="corporacion">
                            <p>{localDetalleSelected.corporacion?.nombre || "Sin corporación"}</p>
                        </div>
                        <div className="contact">
                            <div className="social-links text-center">
                                {
                                    data?.contactos.length !== 0 ?
                                        data?.contactos.map((x, i) => {
                                            if (x.datos_contacto !== null) {
                                                if (x.datos_contacto.tipo === 2) {
                                                    let href = x.cuenta.includes("http") ? x.cuenta : `https://${x.cuenta}`
                                                    return (
                                                        <a key={i} target="_blank" href={href} rel="noreferrer">
                                                            <img src={auth.pathApi() + x.datos_contacto.datos_contacto_imagen[0]?.imagen} alt={`${data.nombres}-${data.apellidos}-${x.datos_contacto.nombre}`} />
                                                        </a>
                                                    )
                                                }
                                            }
                                        })
                                        :
                                        <p>No hay redes sociales asociadas</p>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="verticalTabsContainer">
                        <div className="verticalTab active">
                            <div className={`subloader ${subloader ? "active" : ""}`}></div>
                            <div className="info one-columns">
                                <div className="littleSection">
                                    <div className="title"><h2>Autorías</h2></div>
                                    <hr />
                                </div>
                            </div>
                            <div className="miembrosContainer">
                                <div className="buscador pd-25">
                                    <div className="input-group">
                                        <input type="text" value={searchAutoria}
                                            onChange={async (e) => {
                                                setSearch(e.target.value)
                                            }}
                                            onKeyUp={async (e) => {
                                                if (e.key === "Enter") {
                                                    await handlerPaginationAutorias(1, rowsAutoria, e.target.value)
                                                }
                                            }}
                                            placeholder="Escriba para buscar" className="form-control" />

                                        <span className="input-group-text"><button onClick={async () => { await handlerPaginationAutorias(1, rowsAutoria, searchAutoria) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                    </div>
                                </div>
                                <div className="miembros container-fluid">
                                    <PerfilCongresistaSubList data={autorias} propiedades={ListAutorias.propiedades} link={`/proyectos-de-ley/`} params={["proyecto_ley_id"]} handler={handlerPaginationAutorias} pageExtends={pageAutoria} pageSize={rowsAutoria} totalRows={totalRows} esProyectoLey={true} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}


export default PerfilCongresistasAutorias;
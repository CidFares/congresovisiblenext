import Head from "next/head";
import { Constantes, URLBase } from "../../Constants/Constantes";
import CongresistasDataService from "../../Services/Catalogo/Congresistas.Service";
import SquareCards from "../../Components/CongresoVisible/SquareCards";
import Select from "../../Components/Select";
import CIDPagination from "../../Components/CIDPagination";
import Link from "next/link";
import { useState, useEffect } from "react";
import AuthLogin from "../../Utils/AuthLogin";
const auth = new AuthLogin();

const defaultTipoComision = { value: -1, label: "Elegir tipo de comisión" };
const defaultComision = { value: -1, label: "Ver todas" };

const ConstGeneral = {
    subloader: true,
    filterCorporacion: { value: 1, label: "Cámara de Representantes" },
    filterCuatrienio: { value: 8, label: "2018 - 2022" },
    dataSelectCorporacion: [{ value: 1, label: "Cámara de Representantes" }],
    dataSelectCuatrienio: [{ value: 8, label: "2018 - 2022" }],
    filterActive: { value: -1, label: "Ver todos" },
    filterPartido: { value: -1, label: "Ver todos" },
    filterDepartamento: { value: -1, label: "Ver todos" },
    filterGradoEstudio: { value: -1, label: "Ver todos" },
    filterProfesion: { value: -1, label: "Ver todas" },
    filterGenero: { value: -1, label: "Ver todos" },
    filterTipoComision: { value: -1, label: "Elegir tipo de comisión" },
    filterComision: { value: -1, label: "Ver todas" },
    filterCircunscripcion: { value: -1, label: "Ver todas" },
    filterGrupoEdad: { value: -1, label: "Ver todos" },
    dataSelectPartido: [{ value: -1, label: "Ver todos" }],
    dataSelectDepartamento: [{ value: -1, label: "Ver todos" }],
    dataSelectGradoEstudio: [{ value: -1, label: "Ver todos" }],
    dataSelectProfesion: [{ value: -1, label: "Ver todas" }],
    dataSelectGenero: [{ value: -1, label: "Ver todos" }],
    dataSelectCircunscripcion: [{ value: -1, label: "Ver todas" }],
    dataSelectGrupoEdad: [{ value: -1, label: "Ver todos" }],
    dataSelectTipoComisiones: [{ value: -1, label: "Elegir tipo de comisión" }],
    dataSelectComisiones: [{ value: -1, label: "Ver todas" }],
    dataSelectActive: [
        { value: -1, label: "Ver todos" },
        { value: 1, label: "Activo" },
        { value: 0, label: "Inactivo" }
    ],
    listData: [],
    page: 1,
    rows: 24,
    search: "",
    totalRows: 0,
}

// SSR
export async function getServerSideProps({ query }) {
    let { DSCoroporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
    let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(Number(query.cuatrienio));
    let { listData, totalRows } = await getAll(
        query.active || ConstGeneral.filterActive.value
        , query.corporacion || SelectedCorporacion.value
        , query.cuatrienio || SelectedCuatrienio.value
        , query.partido || ConstGeneral.filterPartido.value
        , query.gradoEstudio || ConstGeneral.filterGradoEstudio.value
        , query.genero || ConstGeneral.filterGenero.value
        , query.circunscripcion || ConstGeneral.filterCircunscripcion.value
        , query.grupoEdad || ConstGeneral.filterGrupoEdad.value
        , query.comision || ConstGeneral.filterComision.value
        , query.departamento || ConstGeneral.filterDepartamento.value
        , query.profesion || ConstGeneral.filterProfesion.value
        , query.page || 1
        , query.rows || ConstGeneral.rows
        , query.search || ConstGeneral.search
        );
    return {
        props: { DSCoroporacion, SelectedCorporacion, DSCuatrienio, SelectedCuatrienio, ListaInicial: listData, TotalInicial: totalRows, query },
    };
}

// PETICIONES

const getComboCuatrienio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboCuatrienio().then((response) => {
        let year = new Date().getFullYear();
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }else{
                if (year >= i.fechaInicio && year <= i.fechaFin) {
                    selected = { value: i.id, label: i.nombre };
                }
            }
        });
        if (!selected)
            selected = combo[combo.length - 1];
    });
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
};
const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboCorporacion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        if(!selected)
            selected = combo[0];
    });
    return { DSCoroporacion: combo, SelectedCorporacion: selected };
};
const getComboDepartamento = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboDepartamento().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
        if(!selected)
            selected = combo[0];
    }
    );
    return { DSDepartamento: combo, SelectedDepartamento: selected};
};
const getComboGradoEstudio = async (ID = null) => {
    let combo = [];
    await CongresistasDataService.getComboGradoEstudio().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
    }
    );
    return combo;
};

const getComboProfesionFilter = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboProfesionFilter({ activo: 1 }).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todas" });
        if(!selected)
            selected = combo[0];
    }
    );
    return { DSProfesion: combo, SelectedProfesion: selected };
};

const getComboCircunscripcion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboCircunscripcion().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todas" });
        if(!selected)
            selected = combo[0];
    }
    );
    return { DSCircunscripcion: combo, SelectedCircunscripcion: selected };
};

const getComboGenero = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboGenero().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
        if(!selected)
            selected = combo[0];
    });
    return { DSGenero: combo, SelectedGenero: selected };
};
const getComboPartido = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboPartido().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
        if(!selected)
            selected = combo[0];
    });
    return { DSPartido: combo, SelectedPartido: selected };
};
const getComboGruposEdad = async (ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboGruposEdad().then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
        if(!selected)
            selected = combo[0];
    });
    return { DSGrupoEdad: combo, SelectedGrupoEdad: selected };
};
const getComboTipoComision = async (idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboTipoComision(idCorporacion).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todos" });
        if(!selected)
            selected = combo[0];
    });
    return { DSTipoComision: combo, SelectedTipoComision: selected };
};
const getComboComisiones = async (idCorporacion, idTipoComision, ID = null) => {
    let combo = [];
    let selected = null;
    await CongresistasDataService.getComboComisiones(idCorporacion, idTipoComision).then((response) => {
        response.data.forEach((i) => {
            combo.push({ value: i.id, label: i.nombre });
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        });
        combo.unshift({ value: -1, label: "Ver todas" });
        if(!selected)
            selected = combo[0];
    }
    );
    return { DSComision: combo, SelectedComision: selected };
};
const getAll = async (idFilterActive, corporacion, cuatrienio, partido, gradoEstudio, genero, circunscripcion, grupoEdad, comision, departamento, profesion, page, rows, search) => {
    let listData = [];
    let totalRows = 0;
    await CongresistasDataService.getAll(idFilterActive, corporacion, cuatrienio, partido, gradoEstudio, genero, circunscripcion, grupoEdad, comision, departamento, profesion, search, page, rows).then((response) => {
        listData = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    await CongresistasDataService.getTotalRecords(idFilterActive, corporacion, cuatrienio, partido, gradoEstudio, genero, circunscripcion, grupoEdad, comision, departamento, profesion, search).then((response) => {
        totalRows = response.data;
    })
        .catch((e) => {
            console.error(e);
        });

    return { listData, totalRows }
};
// EXPORT
const Congresistas = ({ DSCoroporacion = ConstGeneral.dataSelectCorporacion, SelectedCorporacion = ConstGeneral.filterCorporacion, DSCuatrienio = ConstGeneral.dataSelectCuatrienio, SelectedCuatrienio = ConstGeneral.filterCuatrienio, ListaInicial = ConstGeneral.listData, TotalInicial = ConstGeneral.totalRows, query }) => {
    const [subloader, setSubloader] = useState(ConstGeneral.subloader);
    const [filterCorporacion, setSelectedCorporacion] = useState(SelectedCorporacion);
    const [filterCuatrienio, setSelectedCuatrienio] = useState(SelectedCuatrienio);
    const [filterActive, setSelectedActive] = useState(ConstGeneral.filterActive);
    const [filterCircunscripcion, setSelectedCircunscripcion] = useState(ConstGeneral.filterCircunscripcion);
    const [filterComision, setSelectedComision] = useState(ConstGeneral.filterComision);
    const [filterDepartamento, setSelectedDepartamento] = useState(ConstGeneral.filterDepartamento);
    const [filterGenero, setSelectedGenero] = useState(ConstGeneral.filterGenero);
    const [filterGradoEstudio, setSelectedGradoEstudio] = useState(ConstGeneral.filterGradoEstudio);
    const [filterGrupoEdad, setSelectedGrupoEdad] = useState(ConstGeneral.filterGrupoEdad);
    const [filterPartido, setSelectedPartido] = useState(ConstGeneral.filterPartido);
    const [filterProfesion, setSelectedProfesion] = useState(ConstGeneral.filterProfesion);
    const [filterTipoComision, setSelectedTipoComision] = useState(ConstGeneral.filterTipoComision);
    const [dataSelectCorporacion, setDSCoroporacion] = useState(DSCoroporacion);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(DSCuatrienio);
    const [dataSelectActive, setDSActive] = useState(ConstGeneral.dataSelectActive);
    const [dataSelectCircunscripcion, setDSCircunscripcion] = useState(ConstGeneral.dataSelectCircunscripcion);
    const [dataSelectComisiones, setDSComisiones] = useState(ConstGeneral.dataSelectComisiones);
    const [dataSelectDepartamento, setDSDepartamento] = useState(ConstGeneral.dataSelectDepartamento);
    const [dataSelectGenero, setDSGenero] = useState(ConstGeneral.dataSelectGenero);
    const [dataSelectGradoEstudio, setDSGradoEstudio] = useState(ConstGeneral.dataSelectGradoEstudio);
    const [dataSelectGrupoEdad, setDSGrupoEdad] = useState(ConstGeneral.dataSelectGrupoEdad);
    const [dataSelectPartido, setDSPartido] = useState(ConstGeneral.dataSelectPartido);
    const [dataSelectProfesion, setDSProfesion] = useState(ConstGeneral.dataSelectProfesion);
    const [dataSelectTipoComisiones, setDSTipoComisiones] = useState(ConstGeneral.dataSelectTipoComisiones);
    const [page, setPage] = useState(ConstGeneral.page);
    const [rows, setRows] = useState(ConstGeneral.rows);
    const [listData, setListData] = useState(ListaInicial);
    const [totalRows, setTotalRows] = useState(TotalInicial);
    const [search, setSearch] = useState(query.search || ConstGeneral.search);

    useEffect(async () => {
        setDSCoroporacion(DSCoroporacion);
        setDSCuatrienio(DSCuatrienio);
        setSelectedCorporacion(SelectedCorporacion);
        setSelectedCuatrienio(SelectedCuatrienio);
        setSubloader(false)

        if(query.tipoComision){
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(filterCorporacion.value, Number(query.tipoComision));
            setDSTipoComisiones(DSTipoComision); setSelectedTipoComision(SelectedTipoComision);
        }
        if(query.comision){
            let { DSComision, SelectedComision } = await getComboComisiones(filterCorporacion.value, Number(query.tipoComision), Number(query.comision));
            setDSComisiones(DSComision); setSelectedComision(SelectedComision);
        }
        if(query.departamento){
            let { DSDepartamento, SelectedDepartamento } = await getComboDepartamento(Number(query.departamento));
            setDSDepartamento(DSDepartamento); setSelectedDepartamento(SelectedDepartamento);
        }
        if(query.partido){
            let { DSPartido, SelectedPartido } = await getComboPartido(Number(query.partido))
            setDSPartido(DSPartido); setSelectedPartido(SelectedPartido);
        }
        if(query.genero){
            let { DSGenero, SelectedGenero } = await getComboGenero(Number(query.genero));
            setDSGenero(DSGenero); setSelectedGenero(SelectedGenero);
        }
        if(query.circunscripcion){
            let { DSCircunscripcion, SelectedCircunscripcion } = await getComboCircunscripcion(Number(query.circunscripcion))
            setDSCircunscripcion(DSCircunscripcion); setSelectedCircunscripcion(SelectedCircunscripcion);
        }
        if(query.profesion){
            let { DSProfesion, SelectedProfesion } = await getComboProfesionFilter(Number(query.profesion))
            setDSProfesion(DSProfesion); setSelectedProfesion(SelectedProfesion);
        }
        if(query.grupoEdad){
            let { DSGrupoEdad, SelectedGrupoEdad } = await getComboGruposEdad(Number(queyr.grupoEdad));
            setDSGrupoEdad(DSGrupoEdad); setSelectedGrupoEdad(SelectedGrupoEdad);
        }
    }, []);

    // HANDLERS

    const handlerFilterCorporacion = async (selectCorporacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', selectCorporacion.value, q)
        window.location = window.location.pathname + q
    };
    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('cuatrienio', selectCuatrienio.value, q)
        window.location = window.location.pathname + q
    };
    const handlerFilterTipoComision = async (tipoComisionSelected) => {
        setSelectedTipoComision(tipoComisionSelected)
        setSelectedComision(Object.assign({}, defaultComision))
        setDSComisiones([]);
        let { DSComision } = await getComboComisiones(filterCorporacion.value, tipoComisionSelected.value);
        setDSComisiones(DSComision);
    };
    const handlerFilterComision = async (comisionSelected) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoComision', filterTipoComision.value, q)
        q = auth.replaceQueryParam('comision', comisionSelected.value, q)
        window.location = window.location.pathname + q
    };
    const handlerFilterDepartamento = async (departamentoSelected) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('departamento', departamentoSelected.value, q)
        window.location = window.location.pathname + q
    };
    const handlerPartido = async (selectPartido) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('partido', selectPartido.value, q)
        window.location = window.location.pathname + q
    };
    const handlerGenero = async (selectGenero) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('genero', selectGenero.value, q)
        window.location = window.location.pathname + q
    };
    const handlerCircunscripcion = async (selectCircunscripcion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('circunscripcion', selectCircunscripcion.value, q)
        window.location = window.location.pathname + q
    };
    const handlerGradoEstudio = async (selectGradoEstudio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('gradoEstudio', selectGradoEstudio.value, q)
        window.location = window.location.pathname + q
    };
    const handlerProfesion = async (selectProfesion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('profesion', selectProfesion.value, q)
        window.location = window.location.pathname + q
    };
    const handlerGrupoEdad = async (selectGrupoEdad) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('grupoEdad', selectGrupoEdad.value, q)
        window.location = window.location.pathname + q
    };

    const handlerPagination = async (pageA, rowsA, searchA = "") => {
        setSubloader(true)
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
        // setSubloader(false)
    };

    return (
        <div>
            <Head>
                <title>Conoce a todos los congresistas {filterCuatrienio.label} de Colombia | Congreso Visible</title>
                <meta name="description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta name="keywords" content="Congreso Colombia, Democracia, Colombia, Senado, Cámara de Representantes, Legislación, Congresistas, Partidos Políticos, Proyectos de Ley, Órdenes del día, Agenda Legislativa, Citaciones, Debates de Control Político, Comisiones, Actividad Legislativa, Transparencia, Actos Legislativos, Leyes,Elecciones, Bancadas, Enmiendas Constitucionales, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`Conoce a todos los congresistas ${filterCuatrienio.label} de Colombia | Congreso Visible`} />
                <meta property="og:description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/congresistas`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/congresistas`} />
                <meta name="twitter:title" content={`Conoce a todos los congresistas ${filterCuatrienio.label} de Colombia | Congreso Visible`}/>
                <meta name="twitter:description" content="En Congreso Visible podrás encontrar toda la información sobre los congresistas colombianos, como su trayectoria y a qué partido político pertenecen." />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href="https://congresovisible.uniandes.edu.co/congresistas/"/>
            </Head>
            <div className="topPanel">
                <div className="pageTitle">
                    <h1>Congresistas</h1>
                </div>
            </div>
            <div className="listadoPageContainer">
                <div className="container-fluid">
                    <div className="filtros">
                        <div className="two-columns j-lr-margin">
                            <div className="item">
                                <label htmlFor="">Corporación</label>
                                <Select
                                    divClass=""
                                    selectplaceholder="Seleccione"
                                    selectValue={filterCorporacion}
                                    selectIsSearchable={true}
                                    selectoptions={dataSelectCorporacion}
                                    selectOnchange={handlerFilterCorporacion}
                                    selectclassNamePrefix="selectReact__value-container"
                                    spanClass="error"
                                    spanError=""
                                ></Select>
                            </div>
                            <div className="item">
                                <label htmlFor="">Cuatrienio</label>
                                <Select
                                    divClass=""
                                    selectplaceholder="Seleccione"
                                    selectValue={filterCuatrienio}
                                    selectIsSearchable={true}
                                    selectoptions={dataSelectCuatrienio}
                                    selectOnchange={handlerFilterCuatrienio}
                                    selectclassNamePrefix="selectReact__value-container"
                                    spanClass="error"
                                    spanError=""
                                ></Select>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="relative">
                        <div className={`subloader ${subloader ? "active" : ""}`}></div>
                        <div className="centerTabs min-height-85" style={{ paddingTop: "25px" }}>
                            <ul>
                                <li className="active"><h2><a href="/congresistas" onClick={()=>{setSubloader(true)}}>Lista</a></h2></li>
                                <li><h2><a href="/congresistas/curules" onClick={()=>{setSubloader(true)}}>Curules</a></h2></li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className="contentTab active">
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-lg-3 col-md-12">
                                            <div className="filtros-vertical">
                                                <h3>
                                                    <i className="fa fa-filter"></i>{" "}
                                                    Filtros de información
                                                </h3>
                                                <div className="one-columns" style={{ overflowY: "inherit" }}>
                                                    <div className="item">
                                                        <label htmlFor="">Partido</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectPartido.length <= 1) {
                                                                    let { DSPartido } = await getComboPartido();
                                                                    setDSPartido(DSPartido)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterPartido}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectPartido}
                                                            selectOnchange={handlerPartido}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    {/* <div className="item none">
                                                        <label htmlFor="">Grado de estudio</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectGradoEstudio.length <= 1) {
                                                                    let combo = await getComboGradoEstudio();
                                                                    setDSGradoEstudio(combo)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterGradoEstudio}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectGradoEstudio}
                                                            selectOnchange={handlerGradoEstudio}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div> */}
                                                    <div className="item">
                                                        <label htmlFor="">Profesión</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectProfesion.length <= 1) {
                                                                    let { DSProfesion } = await getComboProfesionFilter();
                                                                    setDSProfesion(DSProfesion)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterProfesion}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectProfesion}
                                                            selectOnchange={handlerProfesion}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Género</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectGenero.length <= 1) {
                                                                    let { DSGenero } = await getComboGenero();
                                                                    setDSGenero(DSGenero)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterGenero}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectGenero}
                                                            selectOnchange={handlerGenero}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Circunscripción</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectCircunscripcion.length <= 1) {
                                                                    let { DSCircunscripcion } = await getComboCircunscripcion();
                                                                    setDSCircunscripcion(DSCircunscripcion)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterCircunscripcion}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectCircunscripcion}
                                                            selectOnchange={handlerCircunscripcion}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Grupo de edad</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectGrupoEdad.length <= 1) {
                                                                    let { DSGrupoEdad } = await getComboGruposEdad();
                                                                    setDSGrupoEdad(DSGrupoEdad)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterGrupoEdad}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectGrupoEdad}
                                                            selectOnchange={handlerGrupoEdad}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Tipo de comisión</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectTipoComisiones.length <= 1) {
                                                                    let { DSTipoComision } = await getComboTipoComision(filterCorporacion.value);
                                                                    setDSTipoComisiones(DSTipoComision)
                                                                }
                                                            }}
                                                            divClass=""
                                                            noOptionsMessage="Debe seleccionar una corporación"
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterTipoComision}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectTipoComisiones}
                                                            selectOnchange={handlerFilterTipoComision}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Comisión</label>
                                                        <Select
                                                            noOptionsMessage="Debe seleccionar un tipo de comisión"
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterComision}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectComisiones}
                                                            selectOnchange={handlerFilterComision}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                    <div className="item">
                                                        <label htmlFor="">Departamento de mayor votación</label>
                                                        <Select
                                                            handlerOnClick={async () => {
                                                                if (dataSelectDepartamento.length <= 1) {
                                                                    let { DSDepartamento } = await getComboDepartamento();
                                                                    setDSDepartamento(DSDepartamento)
                                                                }
                                                            }}
                                                            divClass=""
                                                            selectplaceholder="Seleccione"
                                                            selectValue={filterDepartamento}
                                                            selectIsSearchable={true}
                                                            selectoptions={dataSelectDepartamento}
                                                            selectOnchange={handlerFilterDepartamento}
                                                            selectclassNamePrefix="selectReact__value-container"
                                                            spanClass="error"
                                                            spanError=""
                                                        ></Select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-9 col-md-12">
                                            <div className="container-fluid">
                                                <h3>
                                                    <i className="fas fa-users"></i>{" "}
                                                    Listado de{" "}
                                                    {
                                                        filterCorporacion.label
                                                    }
                                                </h3>
                                                <div className="row">
                                                    <div className="buscador pd-25">
                                                        <div className="input-group">
                                                            <input type="text" value={search} onChange={async (e) => {
                                                                let search = search;
                                                                search = e.target.value;
                                                                setSearch(search)
                                                            }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPagination(page, rows, e.target.value);
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar"
                                                                className="form-control"
                                                            />

                                                            <span className="input-group-text">
                                                                <button
                                                                    onClick={async () => {
                                                                        await handlerPagination(page, rows, search);
                                                                    }}
                                                                    type="button"
                                                                    className="btn btn-primary">
                                                                    <i className="fa fa-search"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <SquareCards
                                                        onClickLinks={()=>{setSubloader(true)}}
                                                        data={listData}
                                                        handler={handlerPagination}
                                                        pageExtends={query.page || page}
                                                        pageSize={query.rows || rows}
                                                        totalRows={totalRows}
                                                        defaultImage={Constantes.NoImagen}
                                                        pathImgOrigen={auth.pathApi()}
                                                    />
                                                    <CIDPagination totalPages={totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/congresistas"} hrefQuery={{
                                                        active: query.active || filterActive.value,
                                                        corporacion: query.corporacion || filterCorporacion.value,
                                                        cuatrienio: query.cuatrienio || filterCuatrienio.value,
                                                        partido: query.partido || filterPartido.value,
                                                        tipoComision: query.tipoComision || filterTipoComision.value,
                                                        // gradoEstudio: query.gradoEstudio || filterGradoEstudio.value,
                                                        genero: query.genero || filterGenero.value,
                                                        circunscripcion: query.circunscripcion || filterCircunscripcion.value,
                                                        grupoEdad: query.grupoEdad || filterGrupoEdad.value,
                                                        comision: query.comision || filterComision.value,
                                                        departamento: query.departamento || filterDepartamento.value,
                                                        profesion: query.profesion || filterProfesion.value
                                                    }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Congresistas;

import Link from 'next/link'
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import dynamic from "next/dynamic";
import Head from 'next/head';
import 'suneditor/dist/css/suneditor.min.css';
const SunEditor = dynamic(() => import("suneditor-react"), {
    ssr: false,
});
import { useState, useEffect } from "react";
import { Constantes, EleccionURLParameters } from '../../Constants/Constantes'
import EleccionDataService from "../../Services/Elecciones/Elecciones.Service";
import InputPercent from "../../Components/InputPercent";
import AuthLogin from "../../Utils/AuthLogin";
import ValidForm from "../../Utils/ValidForm";
const validForm = new ValidForm();
const auth = new AuthLogin();
const DataCandidatosConst = {
    id: null,
    eleccion_id: null,
    congresista_id: null,
    comision_cargo_congresista_id: null,
    nombreCargo: null,
    nombre: null,
    activo: null,
};

const EstudiosModalConst = {
    profesion: "",
    grado_estudio: "",
    perfil_educativo: ""
};
const CongresistaDetalleModalConst = {
    congresista_trayectoria_publica: null,
    persona_trayectoria_privada:null
};

const DetalleEleccionConst={
    id: 0,
    tipo_comision_id: 0,
    comision_id: 0,
    corporacion_id: 0,
    cuatrienio_id: 0,
    congresista_id: 0,
    comision_miembro_id: 0,
    comision_cargo_congresista_id: 0,
    titulo: "",
    infoGeneral: "",
    fechaDeEleccion: '',
    resultadoVotacion: '',
    imagen: null,
    candidato: [],
    user: "",
    subloader: true,
    url: "",
    datosContactoDetalle: [],
    funcionarioDetalle: {},
    candidatosDetalle: [],
    imagesResized: [],
    candidatos: [DataCandidatosConst],
    estudiosModal: EstudiosModalConst,
    congresistaDetalle: [CongresistaDetalleModalConst],
    funcionario_actual:null
    
}
export async function getServerSideProps({ query }) {
    let DetalleEleccionData=DetalleEleccionConst;
    //DetalleEleccionData.id=query.props[EleccionURLParameters.id];
    DetalleEleccionData=await getByID(query.props[EleccionURLParameters.id]);
   
    return {
        props: { DetalleEleccionData }
    }
}

const getByID = async (id) => {
    let DetalleEleccion=DetalleEleccionConst
    await EleccionDataService.get(id)
        .then((response) => {                                
            DetalleEleccion = response.data[0];
            
            DetalleEleccion.funcionarioDetalle = response.data[0].funcionario_actual.congresista_elecciones.persona_elecciones;
            DetalleEleccion.funcionarioDetalle.nombres = DetalleEleccion.funcionarioDetalle.nombres + " " + DetalleEleccion.funcionarioDetalle.apellidos;
            DetalleEleccion.candidatosDetalle = DetalleEleccion.candidato;
            DetalleEleccion.candidatosDetalle.map((item, i) => {
                item.congresista.persona_elecciones.nombres = item.congresista.persona_elecciones.nombres + " " + item.congresista.persona_elecciones.apellidos;
            });
                      
        })
        .catch((e) => {           
            console.log(e);
        });
        return DetalleEleccion;
};

export default function DetalleEleccion({DetalleEleccionData=DetalleEleccionConst}){
    const [subloader,setSubloader]=useState(DetalleEleccionData.subloader);
    const [url,setUrl]=useState(DetalleEleccionData.url);
    const [datosContactoDetalle,setDatosContacto]=useState(DetalleEleccionData.datosContactoDetalle);
    const [funcionarioDetalle,setFuncionario]=useState(DetalleEleccionData.funcionarioDetalle);
    const [candidatosDetalle,setCandidatosDetalle]=useState(DetalleEleccionData.candidatosDetalle);
    const [imagesResized,setImgResized]=useState(DetalleEleccionData.imagesResized);
    const [candidatos,setCandidatos]=useState(DetalleEleccionData.candidatos);
    const [estudiosModal,setEstudiosModal]=useState(DetalleEleccionConst.estudiosModal);
    const [congresistaDetalle,setCongresista]=useState(DetalleEleccionConst.congresistaDetalle);

    useEffect(() => {
        let ctrl=getByID(DetalleEleccionData.id);
    }, []);

   const renderImagenMiembro = (idCandidato) => {
        let candidato = candidatosDetalle;
        let elemento = candidato.find((x) => x.congresista_id == idCandidato);
        if (elemento != undefined) {
            let itemImagen = elemento.congresista.persona_elecciones.imagenes[2];
            if (itemImagen != undefined) {
                return itemImagen.imagen;
            }
        }
    };

  

   const handlerEstudio = (tipo, id) => {
       setSubloader(true);
        let estudios = estudiosModal;
        if (tipo === 1) {
            if (funcionarioDetalle.profesion !== null)
                estudios.profesion = funcionarioDetalle.profesion.nombre;
            if (funcionarioDetalle.grado_estudio !== null)
                estudios.grado_estudio = funcionarioDetalle.grado_estudio.nombre;
            if (funcionarioDetalle.perfil_educativo !== null)
                estudios.perfil_educativo = funcionarioDetalle.perfil_educativo;
        }
        else {
            let candidato = candidatosDetalle.find((x) => x.congresista_id === id);
           
            if (candidato.congresista.persona_elecciones.profesion !== null)
                estudios.profesion = candidato.congresista.persona_elecciones.profesion.nombre;
            if (candidato.congresista.persona_elecciones.grado_estudio !== null)
                estudios.grado_estudio = candidato.congresista.persona_elecciones.grado_estudio.nombre;
            if (candidato.congresista.persona_elecciones.perfil_educativo !== null)
                estudios.perfil_educativo = candidato.congresista.persona_elecciones.perfil_educativo;

        }
        setEstudiosModal(estudios);
        setSubloader(false);
        return estudios;

       
       
    };

   const handlerCargos =  (tipo, id) => {
       setSubloader(true);
        let congresistaDetalle = [];
        if (tipo === 1) {
            if (typeof DetalleEleccionData.funcionario_actual.congresista_elecciones !== undefined)
                congresistaDetalle = DetalleEleccionData.funcionario_actual.congresista_elecciones.persona_elecciones;
            else
                return congresistaDetalle;
        }
        else {
            let candidato = candidatosDetalle.find((x) => x.congresista_id === id);
            if (candidato) {
                if (typeof candidato.congresista !== undefined)
                    congresistaDetalle = candidato.congresista.persona_elecciones;
                else
                    return congresistaDetalle;
            }
        }
       setCongresista(congresistaDetalle);
       setSubloader(false);
    };

    return (
            <>
            <Head>
                <title>Elecciones | {DetalleEleccionData.titulo}</title>
                <meta name="description" content={auth.filterStringHTML(DetalleEleccionData.infoGeneral).slice(0, 165)} />
                <meta name="keywords" content="elecciones, cuatrienio, congresista, Congreso Colombia, Colombia, Open Data, Parliamentary Monitoring" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={DetalleEleccionData.titulo} />
                <meta property="og:description" content={auth.filterStringHTML(DetalleEleccionData.infoGeneral).slice(0, 165)} />
                <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
                <meta property="og:image:width" content="828" />
                <meta property="og:image:height" content="450" />
                <meta property="og:url" content={`${URLBase}/funcion-electoral/${auth.filterStringForURL(DetalleEleccionData.titulo)}/${DetalleEleccionData.id}`} />
                <meta property="og:site_name" content="Congreso Visible" />
                <meta name="twitter:url" content={`${URLBase}/funcion-electoral/${auth.filterStringForURL(DetalleEleccionData.titulo)}/${DetalleEleccionData.id}`} />
                <meta name="twitter:title" content={DetalleEleccionData.titulo}/>
                <meta name="twitter:description" content={auth.filterStringHTML(DetalleEleccionData.infoGeneral).slice(0, 165)} />
                <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
                <link rel="canonical" href={`https://congresovisible.uniandes.edu.co/funcion-electoral/${auth.filterStringForURL(DetalleEleccionData.titulo)}/${DetalleEleccionData.id}/`}/>
            </Head>
              <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${"https://www.elheraldo.co/sites/default/files/styles/width_860/public/articulo/2018/11/01/salarios_2.jpg?itok=bTPnZO68"}')` }}>
                    <div className="CVBannerCentralInfo">
                        <div className="CVBanerIcon"><i className="fas fa-vote-yea"></i></div>
                        <div className="CVBannerTitle">
                            <h3>Detalles de la función electoral</h3>
                        </div>
                    </div>
                </section>
                <section gtdtarget="1" className="DetalleComisionSection text-justify no-full-height">
                    <div className={`subloader ${subloader ? "active" : ""}`}></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                                <h2>Información general</h2>
                                <hr />
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: DetalleEleccionData.infoGeneral || "Sin información añadida" }}></div>
                            </div>
                            <div className="col-md-4 datosVotaciones">
                                <div className="dateTitle">
                                    <p>{DetalleEleccionData.fechaDeEleccion}</p>
                                </div>
                                <div className="dato">
                                    <InputPercent noShowValue={true} value="100" color={"var(--circle-chart-color-two)"} fontColor="#000" fontSize="2.6" />
                                    <div className="award"><i className="fas fa-trophy"></i></div>
                                    <div className="result">{!subloader ? DetalleEleccionData.resultadoVotacion : ""}</div>
                                </div>
                                <h4 className="text-center">{!subloader ? DetalleEleccionData.funcionario_actual.congresista_elecciones.persona_elecciones.nombres || "" : ""}</h4>
                            </div>
                        </div>
                        <div className="row">
                            <h2>Elegido</h2>
                            <div className="col-lg-12 col-md-12">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="one-columns">
                                                <div className="listadoItem type-2">
                                                    <div className="itemBody">
                                                        <div className="itemSection">
                                                            <div className="list-avatar">
                                                                <ul>
                                                                    <li><img src={!subloader ? (typeof DetalleEleccionData.funcionario_actual.congresista_elecciones.persona_elecciones.imagenes[2] !== "undefined" ?
                                                                        auth.pathApi() + DetalleEleccionData.funcionario_actual.congresista_elecciones.persona_elecciones.imagenes[2].imagen 
                                                                        : Constantes.NoImagen) : "" || ''} alt="" /> {!subloader ? DetalleEleccionData.funcionario_actual.congresista_elecciones.persona_elecciones.nombres || "" : ""}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="itemFooter">
                                                        <div className="with-pdb">
                                                        <p>{!subloader ? DetalleEleccionData.funcionario_actual.comision_cargo_congresista.nombre || "Cargo no disponible" : 'Cargo no disponible'}</p>
                                                            <div className="sub">
                                                                <p>Cargo</p>
                                                            </div>
                                                        </div>
                                                        <div
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#estudios"
                                                            onClick={() => { handlerEstudio(1, 0) }}
                                                            className="button">
                                                            <p>Estudios</p>
                                                        </div>
                                                        <div
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#cargos"
                                                            onClick={() => { handlerCargos(1, DetalleEleccionData.id) }}
                                                            className="button">
                                                            <p>Cargos</p>
                                                        </div>
                                                        {
                                                           !subloader ? (DetalleEleccionData.funcionario_actual.congresista_elecciones.urlHojaVida !== null ?
                                                                <div className="button">
                                                                    <a rel="noreferrer" target="_blank" href={auth.pathApi() + DetalleEleccionData.funcionario_actual.congresista_elecciones.urlHojaVida} >
                                                                        Hoja de vida
                                                                    </a>
                                                                </div>
                                                                :
                                                                <div>
                                                                    <p>Sin hoja</p>
                                                                </div>)
                                                                : ""
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <h2>Candidatos</h2>
                            <div className="col-lg-12 col-md-12">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="two-columns">
                                                {
                                                    DetalleEleccionData.candidato.map((item, i) => {
                                                        let path = renderImagenMiembro(item.congresista_id) != null
                                                            ? auth.pathApi() +
                                                            renderImagenMiembro(
                                                                item.congresista_id
                                                            ) ||
                                                            ""
                                                            : Constantes.NoImagen
                                                        return (
                                                            <div className="listadoItem type-2" key={i}>
                                                                <div className="itemBody">
                                                                    <div className="itemSection">
                                                                        <div className="list-avatar">
                                                                            <ul>
                                                                                <li><figure><img src={path} alt={item.congresista.persona_elecciones?.nombres || "Sin nombre"} /></figure> {item.congresista?.persona_elecciones.nombres || "Sin nombre"}</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="itemFooter">
                                                                    <div className="with-pdb">
                                                                        <p>{item.comision_cargo_congresista.nombre || 'Cargo no disponible'}</p>
                                                                        <div className="sub">
                                                                            <p>Cargo</p>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#estudios"
                                                                        onClick={() => { handlerEstudio(2, item.congresista_id) }}
                                                                        className="button">
                                                                        <p>Estudios</p>
                                                                    </div>
                                                                    <div
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#cargos"
                                                                        onClick={() => { handlerCargos(2, item.congresista_id) }}
                                                                        className="button">
                                                                        <p>Cargos</p>
                                                                    </div>
                                                                    {
                                                                        (item.congresista.urlHojaVida !== null ?
                                                                            <div className="button">
                                                                                <a rel="noreferrer" target="_blank" href={auth.pathApi() + item.congresista.urlHojaVida} >
                                                                                    Hoja de vida
                                                                                </a>
                                                                            </div>
                                                                            :
                                                                            <div>
                                                                                <p>Sin hoja</p>
                                                                            </div>)
                                                                    }
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="modal fade" id="estudios" tabIndex="-1" aria-labelledby="estudios" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="estudios"><i className="fas fa-graduation-cap"></i> Descripción académica</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="TextoFormateado" dangerouslySetInnerHTML={{ __html: estudiosModal.perfil_educativo !== "" ?
                                        estudiosModal.perfil_educativo : "Sin descripción académica" }}></div>
                                <div className="littleProfileCard">
                                    <div className="icon"><i className="fas fa-graduation-cap"></i></div>
                                    <div className="vertical-text">
                                        <small>Grado de estudio</small>
                                        <p>{estudiosModal.grado_estudio !== "" ? estudiosModal.grado_estudio : "Sin grado de estudio"}</p>
                                        <small>Profesión</small>
                                        <p>{estudiosModal.profesion !== "" ? estudiosModal.profesion : "Sin profesión"}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="cargos" tabIndex="-1" aria-labelledby="cargos" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="cargos"><i className="fas fa-user-tie"></i> Cargos</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="verticalTab" data-ref="2">
                                    <div className="info one-columns">
                                        <div className="littleSection">
                                            <div className="title"><h5>Trayectoria pública</h5></div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div className="info two-columns">
                                        {
                                            congresistaDetalle.persona_trayectoria_publica !== null && typeof congresistaDetalle.persona_trayectoria_publica !== "undefined" && congresistaDetalle.persona_trayectoria_publica.length > 0 ?
                                                congresistaDetalle.persona_trayectoria_publica.map((x, i) => {
                                                    return (
                                                        <div key={i} className="littleProfileCard wpadding relative">
                                                            <div className="icon">
                                                                {
                                                                    x.partido !== null ?
                                                                        <img src={typeof x.partido.partido_imagen[0] !== 'undefined' && x.partido.partido_imagen.length > 0 ? auth.pathApi() + x.partido.partido_imagen[0].imagen : Constantes.NoImagenPicture} alt="" />
                                                                        :
                                                                        <img src={Constantes.NoImagenPicture} alt="" />
                                                                }
                                                            </div>
                                                            <div className="description">
                                                                <p><strong>Partido:</strong> {x.partido !== null ? x.partido.nombre : "Sin partido"}</p>
                                                                <p><strong>Cargo:</strong> {x.cargo || 'Sin cargo'}</p>
                                                                <p><strong>Fecha de inicio:</strong> {x.fecha || 'Sin fecha'}</p>
                                                                <p><strong>Fecha de finalización:</strong> {x.fecha_final || 'Sin fecha'}</p>
                                                            </div>
                                                            {/* <div className={`aplicaPartido ${x.aplica === 0 ? "no" : "si"}`}>
                                                        <p><i className={`fas fa-${x.aplica === 0 ? "times" : "check"}`}></i> {x.aplica === 0 ? "No aplica cargo" : "Aplica cargo"}</p>
                                                    </div> */}
                                                        </div>
                                                    )
                                                })
                                                :
                                                <p>Sin trayectoria asignada</p>
                                        }
                                    </div>

                                    <div className="info one-columns">
                                        <div className="littleSection">
                                            <div className="title"><h5>Trayectoria privada</h5></div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div className="info two-columns">
                                        {
                                            congresistaDetalle.persona_trayectoria_privada !== null && typeof congresistaDetalle.persona_trayectoria_privada !== "undefined" && congresistaDetalle.persona_trayectoria_privada.length > 0 ?
                                                congresistaDetalle.persona_trayectoria_privada.map((x, i) => {
                                                    return (
                                                        <div key={i} className="littleProfileCard wpadding relative">
                                                            <div className="description">
                                                                <p><strong>Cargo:</strong> {x.cargo || 'Sin cargo'}</p>
                                                                <p><strong>Fecha de inicio:</strong> {x.fecha || 'Sin fecha'}</p>
                                                                <p><strong>Fecha de finalización:</strong> {x.fecha_final || 'Sin fecha'}</p>
                                                            </div>
                                                            {/* <div className={`aplicaPartido ${x.aplica === 0 ? "no" : "si"}`}>
                                                        <p><i className={`fas fa-${x.aplica === 0 ? "times" : "check"}`}></i> {x.aplica === 0 ? "No aplica cargo" : "Aplica cargo"}</p>
                                                    </div> */}
                                                        </div>
                                                    )
                                                })
                                                :
                                                <p>Sin trayectoria asignada</p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </>
    )
    
}
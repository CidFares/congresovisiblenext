import Head from 'next/head'
import Select from '../../Components/Select';
import Link from 'next/link'
import AuthLogin from "../../Utils/AuthLogin";
import { useState, useEffect } from "react";
import { Constantes,URLBase } from "../../Constants/Constantes.js";
import ActividadesLegislativasDataService from "../../Services/ActividadesLegislativas/ActividadesLegislativas.Service";
import infoSitioDataService from "../../Services/General/informacionSitio.Service";
import ActLegislativaEleccionList from "../../Components/CongresoVisible/ActLegislativaEleccionList";
import CIDPagination from "../../Components/CIDPagination";
const auth = new AuthLogin();

const PageConst = { imgPrincipal: null, subloader: false }

const FuncionElectConst = {
    filterTipoCorporacion: { value: -1, label: "Ver cámara y senado" },
    dataSelectTipoCorporacion: [],
    filterTipoComision: { value: -1, label: "Elija tipo de comisión" },
    dataSelectTipoComision: [],
    filterComision: { value: -1, label: "Ver todas" },
    dataSelectComision: [],
    filterCuatrienio: { value: -1, label: "Ver todos" },
    dataSelectCuatrienio: [],
    listElecciones: {
        data: [],
        totalRows: 0,
        search: "",
        page: 1,
        rows: 8
    }
}

export async function getServerSideProps({ query }) {

    let PageData = PageConst;
    let FuncionElectoralData = FuncionElectConst;
    await infoSitioDataService.getInformacionSitioHome()
        .then((response) => {
            PageData.imgPrincipal = response.data[0].imgPrincipal || Constantes.NoImagenPicture;
        })
        .catch((e) => {
            console.error(e);
        });
    FuncionElectoralData.listElecciones = await getAllElecciones(
        1
        , query.corporacion || FuncionElectoralData.filterTipoCorporacion.value
        , query.cuatrienio || FuncionElectoralData.filterCuatrienio.value
        , query.comision || FuncionElectoralData.filterComision.value
        , query.search || FuncionElectoralData.listElecciones.search
        , query.page || FuncionElectoralData.listElecciones.page
        , query.rows || FuncionElectoralData.listElecciones.rows
    );
    return {
        props: { PageData, FuncionElectoralData, query }
    }
}
const getComboCuatrienio = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCuatrienio().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todos" })
        if (!selected)
            selected = combo[0]

    })
    return { DSCuatrienio: combo, SelectedCuatrienio: selected };
}
const getComboCorporacion = async (ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboCorporacion().then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver Cámara y Senado" })
        if (!selected)
            selected = combo[0]
    })
    return { DSCorporacion: combo, SelectedCorporacion: selected };
}
const getComboTipoComision = async (idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboTipoComision(idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Elija tipo de comisión" })
        if (!selected)
            selected = combo[0]
    })
    return { DSTipoComision: combo, SelectedTipoComision: selected };
}
const getComboComisiones = async (idTipoComision, idCorporacion, ID = null) => {
    let combo = [];
    let selected = null;
    await ActividadesLegislativasDataService.getComboComisiones(idTipoComision, idCorporacion).then(response => {
        response.data.forEach(i => {
            combo.push({ value: i.id, label: i.nombre })
            if (i.id === ID) {
                selected = { value: i.id, label: i.nombre };
            }
        })
        combo.unshift({ value: -1, label: "Ver todas" })
        if (!selected)
            selected = combo[0]
    })
    return { DSComision: combo, SelectedComision: selected };
}
const getAllElecciones = async (idFilterActive, corporacion, cuatrienio, comision, search, page, rows) => {
    let listElecciones = FuncionElectConst.listElecciones;
    await ActividadesLegislativasDataService.getAllElecciones(
        idFilterActive,
        corporacion, cuatrienio, comision,
        search, page, rows
    )
        .then((response) => {
            listElecciones.data = response.data;
        })
        .catch((e) => {
            console.error(e);
        });
    await ActividadesLegislativasDataService.getTotalRecordsElecciones(
        idFilterActive,
        corporacion, cuatrienio, comision,
        search
    )
        .then((response) => {
            listElecciones.totalRows = response.data;
        })
        .catch((e) => {
            console.error(e);
        });

    return listElecciones;
}
export default function FuncionElectoral({ PageData = PageConst, FuncionElectoralData = FuncionElectConst, query }) {
    const [imgPrincipal, setImg] = useState(PageConst.imgPrincipal);
    const [subloader, setSubloader] = useState(PageConst.subloader);
    const [filterCuatrienio, setFilterCuatrienio] = useState(FuncionElectoralData.filterCuatrienio);
    const [dataSelectCuatrienio, setDSCuatrienio] = useState(FuncionElectoralData.dataSelectCuatrienio);
    const [filterTipoCorporacion, setFilterTipoCorporacion] = useState(FuncionElectoralData.filterTipoCorporacion);
    const [dataSelectTipoCorporacion, setDSCorporacion] = useState(FuncionElectoralData.dataSelectTipoCorporacion);
    const [filterTipoComision, setFilterTComision] = useState(FuncionElectoralData.filterTipoComision);
    const [dataSelectTipoComision, setDSTComision] = useState(FuncionElectoralData.dataSelectTipoComision);
    const [filterComision, setFilterComision] = useState(FuncionElectoralData.filterComision);
    const [dataSelectComision, setDSComision] = useState(FuncionElectoralData.dataSelectComision);
    const [listElecciones, setlistElecciones] = useState(FuncionElectoralData.listElecciones);
    const [search, setSearch] = useState(query.search || FuncionElectoralData.listElecciones.search);
    const [rows, setRows] = useState(query.rows || FuncionElectoralData.listElecciones.rows);
    const [page, setPage] = useState(query.page || FuncionElectoralData.listElecciones.page);

    useEffect(async () => {
        if (query.corporacion) {
            let { DSCorporacion, SelectedCorporacion } = await getComboCorporacion(Number(query.corporacion));
            setDSCorporacion(DSCorporacion); setFilterTipoCorporacion(SelectedCorporacion);
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(SelectedCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if (query.cuatrienio) {
            let { DSCuatrienio, SelectedCuatrienio } = await getComboCuatrienio(Number(query.cuatrienio));
            setDSCuatrienio(DSCuatrienio); setFilterCuatrienio(SelectedCuatrienio);
        }
        if (query.tipoComision) {
            let { DSTipoComision, SelectedTipoComision } = await getComboTipoComision(filterTipoCorporacion.value, Number(query.tipoComision));
            setDSTComision(DSTipoComision); setFilterTComision(SelectedTipoComision);
        }
        if (query.comision) {
            let { DSComision, SelectedComision } = await getComboComisiones(Number(query.tipoComision), Number(query.corporacion), Number(query.comision));
            setDSComision(DSComision); setFilterComision(SelectedComision);
        }
    }, []);


    const handlerFilterCuatrienio = async (selectCuatrienio) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('cuatrienio', selectCuatrienio.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoCorporacion = async (selectTipoCorporacion) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('corporacion', selectTipoCorporacion.value, q)
        window.location = window.location.pathname + q
    }
    const handlerFilterTipoComision = async (selectTipoComision) => {
        setFilterTComision(selectTipoComision)
        setFilterComision(Object.assign({}, FuncionElectConst.filterComision))
        setDSComision([]);
        let { DSComision } = await getComboComisiones(selectTipoComision.value, filterTipoCorporacion.value);
        setDSComision(DSComision);
    }
    const handlerFilterComision = async (selectComision) => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('tipoComision', filterTipoComision.value, q)
        q = auth.replaceQueryParam('comision', selectComision.value, q)
        window.location = window.location.pathname + q
    }

    const handlerPaginationElecciones = async (page, rowsA, searchA = "") => {
        setSubloader(true);
        let q = location.search;
        q = auth.replaceQueryParam('page', 1, q)
        q = auth.replaceQueryParam('rows', rowsA, q)
        q = auth.replaceQueryParam('search', searchA, q)
        window.location = window.location.pathname + q
    }

    return (
        <>
            <Head>
            <title>Función electoral | Congreso Visible</title>
            <meta name="description" content="Aquí podrás encontrar todo lo relacionado a la función electoral" />
            <meta name="keywords" content="Elecciones, congresistas, proyectos de ley, congreso" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="Función electoral | Congreso Visible" />
            <meta property="og:description" content="Aquí podrás encontrar todos lo relacionado a la función electoral" />
            <meta property="og:image" content="https://congresovisible.uniandes.edu.co/img/congreso-logo-footer.svg" />
            <meta property="og:image:width" content="828" />
            <meta property="og:image:height" content="450" />
            <meta property="og:url" content={`${URLBase}/funcion-electoral`} />
            <meta property="og:site_name" content="Congreso Visible" />
            <meta name="twitter:url" content={`${URLBase}/funcion-electoral`} />
            <meta name="twitter:title" content="Función electoral | Congreso Visible"/>
            <meta name="twitter:description" content="Aquí podrás encontrar todos lo relacionado a la función electoral" />
            <meta name="twitter:image" content={`${URLBase}/favicon.png`} />
            <link rel="canonical" href="https://congresovisible.uniandes.edu.co/funcion-electoral/"/>

            </Head>

            <section className="CVBannerMenuContainer no-full-height bg-blue" style={{ backgroundImage: `url('${auth.pathApi() + PageData.imgPrincipal}')` }}>
                <div className="CVBannerCentralInfo">
                    <div className="CVBanerIcon"><i className="fas fa-file-contract"></i></div>
                    <div className="CVBannerTitle text-center">
                        <h1>Función electoral</h1>
                    </div>
                </div>
            </section>
            <main>
                <div className="listadoPageContainer">
                    <div className="container-fluid">
                        <div className="centerTabs lg min-height-85">
                            <ul>
                                <li>
                                    <h2>
                                        <a href="/orden-del-dia">Agenda legislativa</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/votaciones">Votaciones</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/citaciones">Control político</a>
                                    </h2>
                                </li>
                                <li className="active">
                                    <h2>
                                        <a href="/funcion-electoral">Función electoral</a>
                                    </h2>
                                </li>
                                <li>
                                    <h2>
                                        <a href="/partidos">Partidos</a>
                                    </h2>
                                </li>
                            </ul>
                        </div>
                        <div className="contentForCenterTabs">
                            <div className={`subloader ${subloader ? "active" : ""}`} />
                            <div className="contentTab active">
                                <div className="listadoPageContainer">
                                    <div className="container-fluid">
                                        <div className="listadoWPhotoContainer">
                                            <div className="row">
                                                <div className="col-lg-3 col-md-12">
                                                    <div className="filtros-vertical evenColors">
                                                        <h3><i className="fa fa-filter"></i> Filtros de información</h3>
                                                        <div className="one-columns">
                                                            <div className="item">
                                                                <label htmlFor="">Cuatrienio</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectCuatrienio.length <= 1) {
                                                                            let { DSCuatrienio } = await getComboCuatrienio();
                                                                            setDSCuatrienio(DSCuatrienio)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterCuatrienio}
                                                                    selectoptions={dataSelectCuatrienio}
                                                                    selectOnchange={handlerFilterCuatrienio}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de corporación</label>
                                                                <Select
                                                                    handlerOnClick={async () => {
                                                                        if (dataSelectTipoCorporacion.length <= 1) {
                                                                            let { DSCorporacion } = await getComboCorporacion();
                                                                            setDSCorporacion(DSCorporacion)
                                                                        }
                                                                    }}
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoCorporacion}
                                                                    selectoptions={dataSelectTipoCorporacion}
                                                                    selectOnchange={handlerFilterTipoCorporacion}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Tipo de comisión</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterTipoComision}
                                                                    selectoptions={dataSelectTipoComision}
                                                                    selectOnchange={handlerFilterTipoComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un tipo de comisión"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                            <div className="item">
                                                                <label htmlFor="">Comisión</label>
                                                                <Select
                                                                    divClass=""
                                                                    selectplaceholder="Seleccione"
                                                                    selectValue={filterComision}
                                                                    selectoptions={dataSelectComision}
                                                                    selectOnchange={handlerFilterComision}
                                                                    selectIsSearchable={true}
                                                                    selectclassNamePrefix="selectReact__value-container"
                                                                    noOptionsMessage="Debe elegir un tipo de comisión"
                                                                    spanClass=""
                                                                    spanError="" >
                                                                </Select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-9 col-md-12">
                                                    <div className="buscador">
                                                        <div className="input-group">
                                                            <input type="text" value={search}
                                                                onChange={async (e) => {
                                                                    setSearch(e.target.value)
                                                                }}
                                                                onKeyUp={async (e) => {
                                                                    if (e.key === "Enter") {
                                                                        await handlerPaginationElecciones(listElecciones.page, listElecciones.rows, e.target.value)
                                                                    }
                                                                }}
                                                                placeholder="Escriba para buscar" className="form-control" />

                                                            <span className="input-group-text"><button onClick={async () => { await handlerPaginationElecciones(listElecciones.page, listElecciones.rows, listElecciones.search) }} type="button" className="btn btn-primary"><i className="fa fa-search"></i></button></span>
                                                        </div>
                                                    </div>
                                                    <ActLegislativaEleccionList data={listElecciones.data} handler={handlerPaginationElecciones} defaultImage={Constantes.NoImagen} pageExtends={page} pageSize={rows} totalRows={listElecciones.totalRows} pathImgOrigen={auth.pathApi()} />
                                                    <CIDPagination totalPages={listElecciones.totalRows} totalRows={query.rows || rows} searchBy={query.search || search} currentPage={query.page || page} hrefPath={"/funcion-electoral"} hrefQuery={{
                                                        corporacion: query.corporacion || filterTipoCorporacion.value,
                                                        cuatrienio: query.cuatrienio || filterCuatrienio.value,
                                                        tipoComision: query.tipoComision || filterTipoComision.value,
                                                        comision: query.comision || filterComision.value,
                                                    }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

        </>
    )
}

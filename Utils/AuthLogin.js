﻿export default class AuthLogin {
    handleAuthentication = (resultLogin) => {
        localStorage.setItem('environment', resultLogin.environment);
        localStorage.setItem('homeAccountId', resultLogin.homeAccountId);
        localStorage.setItem('idTokenClaims', resultLogin.idTokenClaims);
        localStorage.setItem('localAccountId', resultLogin.localAccountId);
        localStorage.setItem('name', resultLogin.name);
        localStorage.setItem('tenantId', resultLogin.tenantId);
        localStorage.setItem('username', resultLogin.username);
    }

    logout = () => {
        localStorage.removeItem('environment');
        localStorage.removeItem('homeAccountId');
        localStorage.removeItem('idTokenClaims');
        localStorage.removeItem('localAccountId');
        localStorage.removeItem('name');
        localStorage.removeItem('expireSesion');
        localStorage.removeItem('tenantId');
        localStorage.removeItem('username');
    }

    isAuthenticated = () => {
        if (this.expireSesion() === null) {
            return false;
        }
        
        return new Date().getTime() < new Date(this.expireSesion()).getTime();
    }

    environment = () => {
        return localStorage.getItem("environment");
    }
    homeAccountId = () => {
        return localStorage.getItem("homeAccountId");
    }
    idTokenClaims = () => {
        return localStorage.getItem("idTokenClaims");
    }
    localAccountId = () => {
        return localStorage.getItem("localAccountId");
    }
    name = () => {
        return localStorage.getItem("name");
    }
    expireSesion = () => {
        return localStorage.getItem("expireSesion");
    }
    tenantId = () => {
        return localStorage.getItem("tenantId");
    }
    username = () => {
        return localStorage.getItem("username");
    }
    keyJwt = () => {
        // let contador = 0;
        // let token = null;
        // while (!token && contador <= 1000) {
        //     token = localStorage.getItem("keyJwt") || "";
        //     if (token) {
        //         contador = 1000;
        //     } else {
        //         contador++;
        //     }
        // }
        
        return "";
    }
    expireSesion = () => {
        let contador = 0;
        let expireSesion = null;
        while (!expireSesion && contador <= 1000) {
            expireSesion = localStorage.getItem("username");;
            if (expireSesion) {
                contador = 1000;
            } else {
                contador++;
            }
        }
        return expireSesion;
    }

    pathApi = () => {
        //return " https://localhost:44305/";
        // return "http://localhost:8000/uploads/";
        // return "https://cvcliente.cidfares.com/aservice/uploads/";
        // return "https://camstoop.com/aservice/uploads/";
        return "https://apicongresovisible.uniandes.edu.co/uploads/";
        // return "https://congresovisible.uniandes.edu.co/aservice/uploads/"
    }

    setUsuer = (value) => {
        localStorage.setItem("user", value);
    }
    setEmail = (value) => {
        localStorage.setItem("email", value);
    }
    getAge = (dateString) => {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    filterStringForURL = (str) => {
        if(str !== null)
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^a-zA-Z0-9 ]/g, '').replace(/\s/g,'-').toLowerCase();
        return ""
    }
    filterStringHTML = (str) => {
        if(str !== null)
            return str.replace(/<[^>]*>/g, "");
        return ""
    }
    coloquialDate = (strDate, hour = false) => {
        if(hour)
            return new Date(strDate).toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'
        if(strDate){
            if(strDate.includes("T"))
            return new Date(strDate).toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'
        else
            return new Date(strDate + "T00:00:00").toLocaleString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' }) || 'S/F'
        }
    }
    shortDate = (strDate, hour = false) => {
        if(hour)
            return new Date(strDate).toLocaleString('es-ES', { year: 'numeric', month: '2-digit', day: 'numeric' }) || 'S/F'
        if(strDate.includes("T"))
            return new Date(strDate).toLocaleString('es-ES', { year: 'numeric', month: '2-digit', day: 'numeric' }) || 'S/F'
        else
            return new Date(strDate + "T00:00:00").toLocaleString('es-ES', { year: 'numeric', month: '2-digit', day: 'numeric' }) || 'S/F'
    }

    replaceQueryParam = (param, newval, search) => {
        if(search.includes(`${param}=`)){
            var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
            var query = search.replace(regex, "$1").replace(/&$/, '');
        
            return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '') + "&";
        }else{
            if(search.includes("?"))
                return `${search}${param}=${newval}&`;
            else
                return `?${search}${param}=${newval}&`;
        }
    }
    getKeywords = (words = []) => {
        let str = ``;
        words.forEach((x, i) => {
            if(i === words.length - 1)
                str += x
            else
                str += x + ","
        })
        return str;
    }
}
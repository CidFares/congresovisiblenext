// IMPORTANTE CAMBIAR ANTES DE SUBIR A PRODUCCIÓN PARA META ETIQUETAS
// export const URLBase = "http://localhost:3000";
// export const URLBase = "https://cv2.cidfares.com";
// export const URLBase = "https://cvcliente.cidfares.com";
export const URLBase = "https://congresovisible.uniandes.edu.co";

export const CongresistasURLParameters = {
    nombre: 0,
    id: 1
}
export const ProyectoLeyURLParameters = {
    nombre: 0,
    id: 1
}
export const CitacionesURLParameters = {
    nombre: 0,
    id: 1
}
export const OrdenDiaURLParameters = {
    nombre: 0,
    id: 1
}
export const PartidosURLParameters = {
    nombre: 0,
    id: 1
}
export const EleccionURLParameters = {
    nombre: 0,
    id: 1
}
export const TypeCombos = {
    EquipoOpiniones: 1,
    CongresistasEnOpinionesCongresista: 2,
    EquiposBalancesInformes: 3,
    ConceptosInformes: 4
};
export const DeviceResolutions = {
    DesktopFullHD: 1920,
    DesktopHD: 1280,
    TabletPrimnary: 1024,
    TabletSecondary: 748,
    PhonePrimary: 480,
    PhoneSecondary: 300
};
Object.freeze(TypeCombos);
export const Constantes = {
    NoImagen: "assets/images/users/no-image.jpg",
    NoImagenPicture: "assets/images/default_large.png",
    defaultResolutions: [16,32,68,120]
}
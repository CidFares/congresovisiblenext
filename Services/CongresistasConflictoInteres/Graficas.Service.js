import http from "../../Http/Http-processcommon";
const datos = "datosCongresistasConflictoInteres";

class GraficasDataService {
    async getTotalCongresistasConflictoInteresParientes(idCorporacion, idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasConflictoInteresParientes?corporacion_id=${idCorporacion}&cuatrienio_id=${idCuatrienio}`);
    }

    async getTotalProyectoLeyConflictoInteres(idCorporacion, idCuatrienio) {
        return await http.get(`/${datos}/totalProyectoLeyConflictoInteres?corporacion_id=${idCorporacion}&cuatrienio_id=${idCuatrienio}`);
    }

    async getTotalCongresistasConflictoInteresCamara(idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasConflictoInteresCamara?cuatrienio_id=${idCuatrienio}`);
    }

    async getTotalCongresistasConflictoInteresSenado(idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasConflictoInteresSenado?cuatrienio_id=${idCuatrienio}`);
    }

    async getTotalCongresistasReportaronCampaniasCamara(idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasReportaronCampaniasCamara?cuatrienio_id=${idCuatrienio}`);
    }
    async getTotalCongresistasReportaronCampaniasSenado(idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasReportaronCampaniasSenado?cuatrienio_id=${idCuatrienio}`);
    }
    async getTotalCongresistasParticipanJuntas(idCorporacion, idCuatrienio) {
        return await http.get(`/${datos}/totalCongresistasParticipanJuntas?corporacion_id=${idCorporacion}&cuatrienio_id=${idCuatrienio}`);
    }
    async getPorcentajeIngresosPublicosPrivados(idCorporacion, idCuatrienio) {
        return await http.get(`/${datos}/porcentajeIngresosPublicosPrivados?corporacion_id=${idCorporacion}&cuatrienio_id=${idCuatrienio}`);
    }
}

export default new GraficasDataService();

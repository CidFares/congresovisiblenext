import apibase from "../../Http/Http-apibase";
const NombreDelModulo = "utils";

class UtilsDataService {
    async getComboDatosContacto() {
        return await apibase.get(`/${NombreDelModulo}/getComboDatosContacto`);
    }

    async getComboComisionTipoCongresista() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboComisionTipoCongresista`
        );
    }

    async getComboCorporacion() {
        return await apibase.get(`/${NombreDelModulo}/getComboCorporacion`);
    }

    async getComboTipoComision(idCorporacion) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboTipoComision?idcorporacion=${idCorporacion}`
        );
    }

    async getComboCuatrienio() {
        return await apibase.get(`/${NombreDelModulo}/getComboCuatrienio`);
    }

    async getComboCargoCongresista() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboCargoCongresista`
        );
    }

    async getComboCargoMiembrosCongresista() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboCargoMiembrosCongresista`
        );
    }

    async getComboCargoMesaDirectivaCongresista() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboCargoMesaDirectivaCongresista`
        );
    }

    async getComboCongresistasComision(corporacion, cuatrienio) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboCongresistasComision?corporacion=${corporacion}&cuatrienio=${cuatrienio}`
        );
    }

    async getComboSecretariosComision() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboSecretariosComision`
        );
    }
    async getComboCongresistas() {
        return await apibase.get(`/${NombreDelModulo}/getComboCongresistas`);
    }

    async getComboComisionMiembro(data) {
        return await apibase.post(`/utils/getComboComisionMiembro`, data);
    }
    async getComboPartidoPorCongresistaEnComision(idcomision, mesa) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboPartidoPorCongresistaEnComision?idcomision=${idcomision}&mesa=${mesa}`
        );
    }
    async getComboPartidoPorCongresistaEnProyecto(proyecto) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboPartidoPorCongresistaEnProyecto?proyecto=${proyecto}`
        );
    }

    async getComboNivelBlog() {
        return await apibase.get(`/${NombreDelModulo}/getComboNivelBlog`);
    }

    async getComboTemaBlog() {
        return await apibase.get(`/${NombreDelModulo}/getComboTemaBlog`);
    }

    async getComboLegislatura(cuatrienio) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboLegislatura?cuatrienio=${cuatrienio}`
        );
    }

    async getComboTipoProyecto() {
        return await apibase.get(`/${NombreDelModulo}/getComboTipoProyecto`);
    }

    async getComboEstadoProyecto() {
        return await apibase.get(`/${NombreDelModulo}/getComboEstadoProyecto`);
    }

    async getComboCargoIntegrante() {
        return await apibase.get(`/${NombreDelModulo}/getComboCargoIntegrante`);
    }
    async getComboComisiones(idCorporacion, idTipoComision) {
        return await apibase.get(
            `/${NombreDelModulo}/getComboComisiones?idcorporacion=${idCorporacion}&idtipocomision=${idTipoComision}`
        );
    }
    async getComboTipoComisionFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTipoComisionFilter`,
            data
        );
    }
    async getComboPartido() {
        return await apibase.get(`/${NombreDelModulo}/getComboPartido`);
    }
    async getCongresistasFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCongresistasFilter`,
            data
        );
    }
    async getComboEstadoControlPolitico() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboEstadoControlPolitico`
        );
    }
    async getComboEquipo() {
        return await apibase.get(`/${NombreDelModulo}/getComboEquipoCV`);
    }
    async getComboTipoPublicaicon() {
        return await apibase.get(`/${NombreDelModulo}/getComboTipoPublicacion`);
    }
    async getAutoresOtrosFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getAutoresOtrosFilter`,
            data
        );
    }

    async getComboComisionesFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboComisionesFilter`,
            data
        );
    }

    async getComboTipoPublicacionProyectoLeyFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTipoPublicacionProyectoLeyFilter`,
            data
        );
    }

    async getComboTipoCitacion() {
        return await apibase.get(`/${NombreDelModulo}/getComboTipoCitacion`);
    }

    async getComboIniciativaFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboIniciativaFilter`,
            data
        );
    }

    async getComboTipoFechaProyectoLeyFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTipoFechaProyectoLeyFilter`,
            data
        );
    }

    async getComboTemaFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTemaFilter`,
            data
        );
    }
    async getComboTemaControlPoliticoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTemaControlPoliticoFilter`,
            data
        );
    }
    async getComboTipoMultimedia() {
        return await apibase.get(`/${NombreDelModulo}/getComboTipoMultimedia`);
    }
    async getComboGlosarioLegislativo() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboGlosarioLegislativo`
        );
    }
    async getComboTipoActividadAgenda() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboTipoActividadAgenda`
        );
    }
    async getComboLegislaturaFilter(data) {
        return await apibase.post(`/${NombreDelModulo}/getComboLegislaturaFilter`, data);
    }
    async getComboTipoUsuarioFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTipoUsuarioFilter`,
            data
        );
    }
    async getComboTemaProyectoLeyFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTemaProyectoLeyFilter`,
            data
        );
    }
    async getComboTipoSucursalFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboTipoSucursalFilter`,
            data
        );
    }
    async getComboOpinionCongresistaAnno() {
        return await apibase.get(
            `/${NombreDelModulo}/getComboOpinionCongresistaAnno`
        );
    }
    async getComboBlogNdAnno() {
        return await apibase.get(`/${NombreDelModulo}/getComboBlogNdAnno`);
    }

    async getComboMunicipioFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboMunicipioFilter`,
            data
        );
    }
    async getComboProfesionFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboProfesionFilter`,
            data
        );
    }
    async getComboGeneroFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboGeneroFilter`,
            data
        );
    }
    async getComboGradoEstudioFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboGradoEstudioFilter`,
            data
        );
    }
    async getComboPartidoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboPartidoFilter`,
            data
        );
    }
    async getComboDepartamentoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboDepartamentoFilter`,
            data
        );
    }
    async getComboAlcanceFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComboAlcanceFilter`,
            data
        );
    }
    async getProyectoLeyFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getProyectoLeyFilter`,
            data
        );
    }
    async getProyectoLeyFilterTotalRecords(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getProyectoLeyFilterTotalRecords`,
            data
        );
    }
    async getComisionFilterPagination(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComisionFilterPagination`,
            data
        );
    }
    async getComisionFilterPaginationTotalRecord(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getComisionFilterPaginationTotalRecord`,
            data
        );
    }
    async getPonenteFilterPagination(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getPonenteFilterPagination`,
            data
        );
    }
    async getPonenteFilterPaginationTotalRecord(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getPonenteFilterPaginationTotalRecord`,
            data
        );
    }
    async getAutorFilterPagination(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getAutorFilterPagination`,
            data
        );
    }
    async getAutorFilterPaginationTotalRecord(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getAutorFilterPaginationTotalRecord`,
            data
        );
    }
    async getPaisFilter(data) {
        return await apibase.post(`/${NombreDelModulo}/getPaisFilter`, data);
    }
    async getCfCalidadMiembroFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfCalidadMiembroFilter`,
            data
        );
    }
    async getCfOrganoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfOrganoFilter`,
            data
        );
    }
    async getCfParentescoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfParentescoFilter`,
            data
        );
    }
    async getCfAcreenciasCptFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfAcreenciasCptFilter`,
            data
        );
    }
    async getCfTipoBienFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfTipoBienFilter`,
            data
        );
    }
    async getCfDepartamentoFilter(data) {
        return await apibase.post(
            `/${NombreDelModulo}/getCfDepartamentoFilter`,
            data
        );
    }
}

export default new UtilsDataService();

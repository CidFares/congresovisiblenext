import http from "../../Http/Http-processcommon";
import http2 from "../../Http/Http-processcommon2";
import apibase from "../../Http/Http-apibase";
const NombreDelModulo = "partidos";
const Utils = "utils";

class PartidosDataService {
    async get(id) {
        return await http.get(`/${NombreDelModulo}/${id}`);
    }
}

export default new PartidosDataService();

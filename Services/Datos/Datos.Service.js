import http from "../../Http/Http-processcommon";
const datos = "datos";

class DatosDataService {
    async getDatosInicio() {
        return await http.get(`/${datos}/datos_inicio`);
    }
    async getCamaraOrigenIniciativas(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/camara_origen_iniciativas?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getCongresoHoyCongresistasMasCitaciones(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/congresistas_mas_citaciones?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getCongresoHoyPartidosMasCitaciones(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/partidos_mas_citaciones?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getHistoricoTotalCitacionesPorCuatrienio() {
        return await http.get(`/${datos}/total_citaciones_por_cuatrienio`);
    }
    async getCamaraTemasRecurrentesPorPartido(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/camara_temas_recurrentes_por_partido?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getCamaraPartidosMayorNumeroAutoriasProyectoLey(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/camara_partidos_mayor_numero_autorias_proyecto_ley?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getCamaraRepresentantesMayorNumeroAutoriasProyectoLey(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/camara_representantes_mayor_numero_autorias_proyecto_ley?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getCamaraAsientosPorPartidos(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/camara_asientos_por_partidos?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getSenadoAsientosPorPartidos(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/senado_asientos_por_partidos?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getEdadMedianaPorPartidos(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/edad_mediana_por_partidos?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getCamaraPiramidePoblacional(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/camara_piramide_poblacional?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getSenadoPiramidePoblacional(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/senado_piramide_poblacional?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getTotalProyectoDeLeyPorGenero(cuatrienio_id) {
        return await http.get(`/${datos}/total_proyecto_de_ley_por_genero?cuatrienio_id=${cuatrienio_id}`);
    }
    async getTotalProyectoDeLeySancionadosPorAño() {
        return await http.get(`/${datos}/total_proyecto_de_ley_sancionados_por_anio`);
    }
    async getCantidadTemasTodosCuatrienios() {
        return await http.get(`/${datos}/cantidad_temas_todos_cuatrienios`);
    }
    async getCantidadTemasUnCuatrienio(cuatrienio_id) {
        return await http.get(`/${datos}/cantidad_temas_un_cuatrienio?cuatrienio_id=${cuatrienio_id}`);
    }
    async getTiempoSancionProyectosDeLey() {
        return await http.get(`/${datos}/tiempo_sancion_proyectos_de_ley`);
    }
    async getProporcionDeMujeres() {
        return await http.get(`/${datos}/proporcion_de_mujeres`);
    }
    async getProporcionDeMujeresPorCuatrienio(cuatrienio_id) {
        return await http.get(`/${datos}/proporcion_de_mujeres_por_cuatrienio?cuatrienio_id=${cuatrienio_id}`);
    }
    async getMinimoPromedioMaximoCuatrienio(cuatrienio_id) {
        return await http.get(`/${datos}/minimo_promedio_maximo_cuatrienio?cuatrienio_id=${cuatrienio_id}`);
    }
    async getNumeroIniciativasGubernamentales() {
        return await http.get(`/${datos}/numero_iniciativas_gubernamentales`);
    }
    async getTop5PartidosCongresistasMayorEdad(cuatrienio_id) {
        return await http.get(`/${datos}/top_5_partidos_congresistas_mayor_edad?cuatrienio_id=${cuatrienio_id}`);
    }
    async getTop5PartidosCongresistasMasJovenes(cuatrienio_id) {
        return await http.get(`/${datos}/top_5_partidos_congresistas_mas_jovenes?cuatrienio_id=${cuatrienio_id}`);
    }
    async getPartidosMayorRepresentacionCongresoPorCuatrienio() {
        return await http.get(`/${datos}/partidos_mayor_representacion_congreso_por_cuatrienio`);
    }
    async getNumeroIniciativasGubernamentalesSancionadasComoLey() {
        return await http.get(`/${datos}/numero_iniciativas_gubernamentales_sancionadas_como_ley`);
    }
    async getDistribucionEdadCongresoTodosCuatrienios() {
        return await http.get(`/${datos}/distribucion_edad_congreso_todos_cuatrienios`);
    }
    async getTop10TemasProyectosLey(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/top_10_temas_proyectos_ley?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getEstadoProyectosLey(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/estado_proyectos_ley?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxProyectosLey(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_proyecto_de_ley?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxAudiendiasPublicasCitadas(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_audiencias_publicas_citadas?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxDebatesDeControlPolitico(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_debates_de_control_politico?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxSentenciasEmitidas(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_sentencias_emitidas?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxObjecionesEmitidas(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_objeciones_emitidas?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getValueBoxProyectosRadicados(cuatrienio_id, legislatura_id, corporacion_id) {
        return await http.get(`/${datos}/value_box_proyectos_radicados?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}&corporacion_id=${corporacion_id}`);
    }
    async getVotacionesPorTemaCamara(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/votaciones_por_tema_camara?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getVotacionesPorTemaSenado(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/votaciones_por_tema_senado?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getVotacionesPorIniciativaCamara(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/votaciones_por_iniciativa_camara?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getVotacionesPorIniciativaSenado(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/votaciones_por_iniciativa_senado?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getTipoVotacionesCamara(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/tipo_votaciones_camara?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }
    async getTipoVotacionesSenado(cuatrienio_id, legislatura_id) {
        return await http.get(`/${datos}/tipo_votaciones_senado?cuatrienio_id=${cuatrienio_id}&legislatura_id=${legislatura_id}`);
    }

    // Descartados
    async getSenadoOrigenIniciativas(data) {
        return await http.get(`/${datos}/senado_origen_iniciativas`);
    }
    async getSenadoTemasRecurrentesPorPartido(data) {
        return await http.get(`/${datos}/senado_temas_recurrentes_por_partido`);
    }
    async getSenadoPartidosMayorNumeroAutoriasProyectoLey(data) {
        return await http.get(`/${datos}/senado_partidos_mayor_numero_autorias_proyecto_ley`);
    }
    async getSenadoRepresentantesMayorNumeroAutoriasProyectoLey(data) {
        return await http.get(`/${datos}/senado_representantes_mayor_numero_autorias_proyecto_ley`);
    }
}

export default new DatosDataService();

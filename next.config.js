module.exports = {
    trailingSlash: true,
    reactStrictMode: true,
    generateEtags: false,
    images: {
        domains: ["cvcliente.cidfares.com"],
    },
};

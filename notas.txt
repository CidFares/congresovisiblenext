elementos instalados

- Para combinar diferentes clases
npm install classnames

- Para peticiones get/post/put, etc
npm install axios

-para slider de inicio 
npm install react-awesome-slider

- para highcharts
npm install highcharts --save
npm install highcharts-react-official

- ternaria en loops
{arreglo.length > 0 && (
<>
	elemento
</>
)} 


- constantes

const {address, nombre, neighbourhood} = props.coffeeStore // con esto se mapean los datos que se reciben
y se usan como variables.
En vez de: <p>{props.data.nombre}</p>
se usa: <p>{nombre}</p>
*Poner después del router.isFallback



Cambios a considerar para el SEO
1.- Paginación. Debemos tratar de que queden como enlaces.
2.- Meta descriptions. Como usamos el Suneditor, es preciso ver cómo obtener solo el texto, para así asignar una pequeña cantidad del texto a la meta description.

-- producción, modificar package.json en scripts

 "scripts": {
    "dev": "node server.js",
    "build": "next build",
    "start": "NODE_ENV=production node server.js",
    "lint": "next lint",
    "export": "npm run build && next export"
  },
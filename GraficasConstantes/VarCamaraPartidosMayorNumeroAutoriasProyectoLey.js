// CamaraPartidosMayorNumeroAutoriasProyectoLey

class VarCamaraPartidosMayorNumeroAutoriasProyectoLey {
    default_camara() {
        return {
            partidos_mayor_numero_autorias_proyecto_ley: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        type: "lollipop",
                        inverted: true,
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
        
                    legend: {
                        enabled: false
                    },
        
                    title: {
                        text: "Partidos con mayor número de autorías de proyectos de ley, en la Cámara de Representantes"
                    },
                    subtitle: {
                        text: ""
                    },
                    tooltip: {
                        shared: true,
                        pointFormat: "<br>{point.y}",
                    },

                    plotOptions: {
                        series: {
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },  
                        }  
                    },
        
                    xAxis: {
                        type: "category",
                        allowDecimals: false,
                        labels: {
                            step: 1
                        },
                    },
        
                    yAxis: {
                        title: {
                            text: "Total"
                        },
                    },
                    credits: {
                        text: ""
                    },
                    series: {
                        data: []
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCamaraPartidosMayorNumeroAutoriasProyectoLey();

//  End CamaraPartidosMayorNumeroAutoriasProyectoLey
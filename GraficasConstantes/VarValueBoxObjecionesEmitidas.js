// CamaraCongresoHoyAsientosPorPartido

class VarValueBoxObjecionesEmitidas {
    value_box() {
        return {
            objeciones_emitidas: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        backgroundColor: "#FB8500",
                        style: {
                            fontFamily: "Helvetica"
                        },
                        height: "250px",
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    title: {
                        text: "Objeciones emitidas",
                        align: "left",
                        style: {
                            color: "#FFF"
                        }
                    },
                    yAxis: {
                        title: {
                            text: "n",
                            enabled: false
                        },
                        lineWidth: 0,
                        type: "linear",
                        gridLineWidth: 0,
                        labels: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            label: {
                                enabled: false
                            },
                            marker: {
                                radius: 0
                            },
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            turboThreshold: 0,
                            showInLegend: false,
                            lineWidth: 8
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        }
                    },
                    series: [],
                    xAxis: {
                        type: "category",
                        title: {
                            text: "mes",
                            enabled: false
                        },
                        lineWidth: 0,
                        labels: {
                            enabled: false
                        },
                        tickWidth: 0
                    },
                    subtitle: {
                        text: "",
                        style: {
                            fontSize: "100px",
                            color: "#FFF"
                        },
                        align: "left"
                    },
                    colors: ["#FFF"],
                    tooltip: {
                        formatter: function() {
                            let  date = new Date(this.point.mes);
                            let  customDate = date.toLocaleString('default', { month: 'long',  year: "numeric" });
                            return  customDate +'<br/>' +
                                this.point.n + ' proyectos de ley.';
                        }
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarValueBoxObjecionesEmitidas();

//  End CamaraCongresoHoyAsientosPorPartido
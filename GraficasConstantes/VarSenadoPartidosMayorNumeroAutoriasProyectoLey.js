// SenadoPartidosMayorNumeroAutoriasProyectoLey

class VarSenadoPartidosMayorNumeroAutoriasProyectoLey {
    default_senado() {
        return {
            partidos_mayor_numero_autorias_proyecto_ley: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                    tipo_proyecto_ley: {
                        item: {
                            value: "",
                            label: "Seleccione un tipo de proyecto",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        type: "lollipop",
                        inverted: true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    plotOptions: {
                        series:{
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                        }
                    },
                    legend: {
                        enabled: false
                    },
        
                    title: {
                        "text": "Partidos con mayor número de autorías de proyectos de ley, en la Senado de Representantes"
                    },
        
                    tooltip: {
                        shared: true
                    },
        
                    xAxis: {
                        type: "category",
                        allowDecimals: false,
                        labels: {
                            step: 1
                        },
                    },
        
                    yAxis: {
                        title: {
                            "text": "Total"
                        },
                    },
        
                    series: [
                        {
                            data: []
                        }
                    ],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarSenadoPartidosMayorNumeroAutoriasProyectoLey();

//  End SenadoPartidosMayorNumeroAutoriasProyectoLey
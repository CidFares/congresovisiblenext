// CongresoHoyTotalProyectoDeLeyPorGenero

class VarCongresoHoyTotalProyectoDeLeyPorGenero {
    default_congreso_hoy() {
        return {
            edad_mediana_por_partido: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Total de Proyectos de Ley presentados por género"
                    },
                    "yAxis": {
                        "title": {
                            "text": "1",
                            "enabled": false
                        },
                        "type": "linear",
                        "lineWidth": 0,
                        "labels": {
                            "enabled": false
                        },
                        "gridLineWidth": 0
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bubble": {
                            "colorByPoint": false,
                            "dataLabels": {
                                "enabled": true,
                                "style": {
                                    "fontSize": "20px"
                                }
                            },
                            "maxSize": "50%",
                            "minSize": "20%",
                            "marker": {
                                "fillOpacity": 0.91,
                                "lineWidth": 0
                            }
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "genero",
                            "enabled": false
                        },
                        "lineWidth": 0,
                        "labels": {
                            "enabled": false
                        }
                    },
                    "tooltip": {
                        "pointFormat": "{point.pct}%"
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCongresoHoyTotalProyectoDeLeyPorGenero();

//  End CongresoHoyTotalProyectoDeLeyPorGenero
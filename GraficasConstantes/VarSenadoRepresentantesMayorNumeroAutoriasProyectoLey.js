// SenadoRepresentantesMayorNumeroAutoriasProyectoLey

class VarSenadoRepresentantesMayorNumeroAutoriasProyectoLey {
    default_senado() {
        return {
            representantes_mayor_numero_autorias_proyecto_ley: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                    tipo_proyecto_ley: {
                        item: {
                            value: "",
                            label: "Seleccione un tipo de proyecto",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "inverted": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Representantes con mayor número de autorías de proyectos de ley"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Total"
                        },
                        "type": "linear",
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "lollipop"
                    }
                    ],
                    "xAxis": {
                        "type": "category",
                        allowDecimals: false,
                        labels: {
                            step: 1
                        },
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }]
                    }
                },
            },
        };
    };
}

export default new VarSenadoRepresentantesMayorNumeroAutoriasProyectoLey();

//  End SenadoRepresentantesMayorNumeroAutoriasProyectoLey
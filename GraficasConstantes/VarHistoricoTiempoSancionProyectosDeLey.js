// HistoricoTiempoSancionProyectosDeLey

class VarHistoricoTiempoSancionProyectosDeLey {
    default_historico() {
        return {
            tiempo_sancion_proyectos_de_ley: {
                grafica: {
                    "chart": {
                        "reflow": true,
                        "inverted": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Tiempo de sanción de Proyectos de Ley"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Días"
                        },
                        "tickAmount": 6,
                        "min": 0
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        }
                    },
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "Cuatrienio"
                        }
                    },
                    "series": [{
                        "name": "Proyecto de Ley",
                        "data": [{
                            "name": "1998-2002",
                            "low": 12,
                            "q1": 301,
                            "median": 466.5,
                            "q3": 655.5,
                            "high": 1181
                        }, {
                            "name": "2002-2006",
                            "low": 25,
                            "q1": 346.5,
                            "median": 497,
                            "q3": 669,
                            "high": 1144
                        }, {
                            "name": "2006-2010",
                            "low": 84,
                            "q1": 376,
                            "median": 509,
                            "q3": 677,
                            "high": 1078
                        }, {
                            "name": "2010-2014",
                            "low": 24,
                            "q1": 271.5,
                            "median": 427.5,
                            "q3": 590,
                            "high": 1038
                        }, {
                            "name": "2014-2018",
                            "low": 17,
                            "q1": 353,
                            "median": 524,
                            "q3": 669,
                            "high": 1079
                        }, {
                            "name": "2018-2022",
                            "low": 57,
                            "q1": 248.5,
                            "median": 469,
                            "q3": 643.5,
                            "high": 756
                        }
                        ],
                        "id": "Proyecto de Ley",
                        "type": "boxplot"
                    }, {
                        "name": "Proyecto de ley Estatutaria",
                        "data": [{
                            "name": "1998-2002",
                            "low": 377,
                            "q1": 450,
                            "median": 583,
                            "q3": 652,
                            "high": 782
                        }, {
                            "name": "2002-2006",
                            "low": 353,
                            "q1": 428,
                            "median": 583,
                            "q3": 705.5,
                            "high": 716
                        }, {
                            "name": "2006-2010",
                            "low": 719,
                            "q1": 719,
                            "median": 818,
                            "q3": 917,
                            "high": 917
                        }, {
                            "name": "2010-2014",
                            "low": 270,
                            "q1": 514.5,
                            "median": 699,
                            "q3": 831,
                            "high": 1070
                        }, {
                            "name": "2014-2018",
                            "low": 348,
                            "q1": 523,
                            "median": 627,
                            "q3": 674,
                            "high": 674
                        }
                        ],
                        "id": "Proyecto de ley Estatutaria",
                        "type": "boxplot"
                    }
                    ],
                    "tooltip": {
                        "shared": false,
                        "pointFormat": "<span style=\"color:{point.color}\">●<\/span> <b> {series.name}<\/b><br/>Máximo: {point.high}<br/>3º cuantil: {point.q3}<br/>Mediana: {point.median} <br/>1º cuantil: {point.q1}<br/>Mínimo: {point.low}<br/>"
                    },
                    "colors": ["#17789E", "#F78031"],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoTiempoSancionProyectosDeLey();

//  End HistoricoTiempoSancionProyectosDeLey
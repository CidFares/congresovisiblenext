// CamaraRepresentantesMayorNumeroAutoriasProyectoLey

class VarCamaraRepresentantesMayorNumeroAutoriasProyectoLey {
    default_camara() {
        return {
            representantes_mayor_numero_autorias_proyecto_ley: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        inverted: true,
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    title: {
                        text: "Representantes con mayor número de autorías de proyectos de ley"
                    },
                    subtitle: {
                        text: ""
                    },
                    yAxis: {
                        title: {
                            text: "Total"
                        },
                        type: "linear",
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            label: {
                                enabled: false
                            },
                            turboThreshold: 0,
                            showInLegend: false
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        }
                    },
                    series: [{
                        group: "group",
                        data: [],
                        type: "lollipop"
                    }
                    ],
                    xAxis: {
                        type: "category",
                        allowDecimals: false,
                        labels: {
                            step: 1
                        },
                    },
                    tooltip: {
                        pointFormat: " {point.y}",
                        headerFormat: ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }]
                    }
                },
            },
        };
    };
}

export default new VarCamaraRepresentantesMayorNumeroAutoriasProyectoLey();

//  End CamaraRepresentantesMayorNumeroAutoriasProyectoLey
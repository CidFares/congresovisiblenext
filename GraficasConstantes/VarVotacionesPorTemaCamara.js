// VarVotacionesPorTemaCamara

class VarVotacionesPorTemaCamara {
    votacion() {
        return {
            votacion_por_tema_camara: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    }
                },
                grafica: {
                    chart: {
                        type: 'bar',
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        },
                        zoomType: 'x'
                    },
                    colors: ["rgb(201, 201, 201)", "rgb(165, 95, 116)", "rgb(41, 82, 74)"],
                    title: {
                        text: 'Stacked bar chart'
                    },
                    xAxis: {
                        categories: []
                    },
                    "credits": {
                        "enabled": false
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        labels: {
                            format: "{value:,.0f}%"
                        },
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    tooltip: {
                        headerFormat: "<b>{point.key}</b><br>",
                        pointFormat: "{series.name}: {point.y}% <br>",
                        shared: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                        }
                    },
                    series: [],
                },
            },
        };
    };
}

export default new VarVotacionesPorTemaCamara();

//  End VarVotacionesPorTemaCamara
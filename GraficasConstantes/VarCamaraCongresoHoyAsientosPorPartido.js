// CamaraCongresoHoyAsientosPorPartido

class VarCamaraCongresoHoyAsientosPorPartido {
    default_congreso_hoy() {
        return {
            asientos_por_partido: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Curules por partido"
                    },
                    "yAxis": {
                        "title": {
                            "text": "n"
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "label": {
                                "enabled": false
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "colorByPoint": false,
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "item"
                    }
                    ],
                    "xAxis": {
                        "type": null,
                        "title": {
                            "text": []
                        },
                        "categories": null
                    },
                    "subtitle": {
                        "text": "Cámara de Representantes"
                    },
                    "tooltip": {
                        "pointFormat": "<b>: {point.n}</b>"
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCamaraCongresoHoyAsientosPorPartido();

//  End CamaraCongresoHoyAsientosPorPartido
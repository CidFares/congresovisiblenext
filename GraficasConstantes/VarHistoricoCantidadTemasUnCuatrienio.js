// HistoricoCantidadTemasUnCuatrienio

class VarHistoricoCantidadTemasUnCuatrienio {
    default_historico() {
        return {
            cantidad_temas_un_cuatrienio: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    }
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": ""
                    },
                    "yAxis": {
                        "title": {
                            "text": []
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "treemap"
                    }
                    ],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "tema"
                        },
                        "categories": null
                    },
                    "tooltip": {
                        "pointFormat": "{point.name}: <b>{point.n}<\/b> <b> ({point.porc})%<\/b>"
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoCantidadTemasUnCuatrienio();

//  End HistoricoCantidadTemasUnCuatrienio
// HistoricoNumeroIniciativasGubernamentales

class VarHistoricoNumeroIniciativasGubernamentales {
    default_historico() {
        return {
            numero_iniciativas_gubernamentales: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        type: 'bar',
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Número de iniciativas gubernamentales"
                    },
                    "yAxis": {
                        "title": {
                            "text": ""
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 10
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": ""
                        }
                    },
                    "tooltip": {
                        "pointFormat": "{point.n}",
                        "headerFormat": ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoNumeroIniciativasGubernamentales();

//  End HistoricoNumeroIniciativasGubernamentales
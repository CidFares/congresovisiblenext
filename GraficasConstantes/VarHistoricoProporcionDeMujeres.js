// HistoricoProporcionDeMujeres

class VarHistoricoProporcionDeMujeres {
    default_historico() {
        return {
            proporcion_de_mujeres: {
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Proporción de mujeres"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Porcentaje"
                        },
                        "type": "linear",
                        "min": 0
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "area": {
                            "stacking": true
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "title": {
                            "text": "Fecha cuatrienios"
                        },
                        "crosshair": true,
                        "labels": {
                            "step": 2
                        },
                        categories: []
                    },
                    "subtitle": {
                        "text": "Historico"
                    },
                    "tooltip": {
                        "pointFormat": "{series.name}: {point.pct} %<br>",
                        "shared": true
                    },
                    "colors": ["#0B3B5D", "#17789E"],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoProporcionDeMujeres();

//  End HistoricoProporcionDeMujeres
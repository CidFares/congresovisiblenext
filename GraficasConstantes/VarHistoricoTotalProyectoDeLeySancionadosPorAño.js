// HistoricoTotalProyectoDeLeySancionadosPorAño

class VarHistoricoTotalProyectoDeLeySancionadosPorAño {
    default_historico() {
        return {
            total_proyecto_de_ley_sancionados_por_año: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Total de Proyectos de Ley sancionados por año"
                    },
                    "yAxis": {
                        "title": {
                            "text": ""
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 5,
                            "colorByPoint": false
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "bar"
                    }
                    ],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": ""
                        }
                    },
                    "tooltip": {
                        "pointFormat": "{point.n}",
                        "headerFormat": ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoTotalProyectoDeLeySancionadosPorAño();

//  End HistoricoTotalProyectoDeLeySancionadosPorAño
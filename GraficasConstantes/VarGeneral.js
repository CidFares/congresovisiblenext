
class VarGeneral {
    default_theme() {
        return {
            "colors": ["#062647", "#0B3B5D", "#0D4568", "#0F4F73", "#136489", "#17789E", "#3B90B5", "#5EA8CC", "#5EA8CC", "#82C0E3", "#56A2C7", "#94CCEF", "#A5D8FA", "#C1E3FA", "#DCEEFA", "#E3F2FE"],
            "chart": {
                "style": {
                    "fontFamily": "Helvetica",
                    "fontSize": "20px"
                }
            },
            "title": {
                "style": {
                    "color": "#17789E",
                    "fontSize": "22px",
                    "fontWeight": "bold"
                }
            },
            "subtitle": {
                "style": {
                    "color": "#666666"
                }
            },
            "legend": {
                "itemStyle": {
                    "color": "black"
                },
                "itemHoverStyle": {
                    "color": "gray"
                }
            },
            "tooltip": {
                "borderWidth": 0,
                "shadow": false,
                "headerFormat": "<span style=\"font-size: 20px\"><b>{point.key}<\/span><br/><\/b>",
                "shape": "square",
                "style": {
                    "fontSize": "16px"
                }
            },
            "yAxis": {
                "lineWidth": 3,
                "title": {
                    "style": {
                        "fontSize": "16px"
                    }
                },
                "tickAmount": 5,
                "labels": {
                    "style": {
                        "fontSize": "15px"
                    }
                }
            },
            "xAxis": {
                "lineWidth": 0,
                "title": {
                    "style": {
                        "fontSize": "16px"
                    }
                },
                "labels": {
                    "style": {
                        "fontSize": "18px"
                    }
                }
            },
            "plotOptions": {
                "bar": {
                    "borderRadius": 5
                },
                "scatter": {
                    "marker": {
                        "radius": 8
                    }
                },
                "treemap": {
                    "borderRadius": 5,
                    "colorByPoint": true,
                    "dataLabels": {
                        "style": {
                            "fontFamily": "Helvetica",
                            "fontSize": "16px",
                            "fontWeight": "normal"
                        }
                    }
                },
                "errorbar": {
                    "maxPointWidth": 20
                }
            }
        }
    }
}

export default new VarGeneral();
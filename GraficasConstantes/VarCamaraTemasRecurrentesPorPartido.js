// CamaraTemasRecurrentesPorPartido

class VarCamaraTemasRecurrentesPorPartido {
    default_camara() {
        return {
            temas_recurrentes_por_partido: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    title: {
                        text: "Temas recurrentes por partido político en la Cámara de Representantes"
                    },
                    subtitle: {
                        text: ""
                    },
                    yAxis: {
                        title: {
                            text: []
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            label: {
                                enabled: false
                            },
                            turboThreshold: 0,
                            showInLegend: false
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        }
                    },
                    series: [
                        {
                            name:"Partido",
                            group: "group",
                            data: [],
                            type: "sankey"
                        }
                    ],
                    xAxis: {
                        type: null,
                        title: {
                            text: []
                        },
                        categories: null
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCamaraTemasRecurrentesPorPartido();

//  End CamaraTemasRecurrentesPorPartido
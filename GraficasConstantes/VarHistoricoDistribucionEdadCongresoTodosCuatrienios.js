// HistoricoDistribucionEdadCongresoTodosCuatrienios

class VarHistoricoDistribucionEdadCongresoTodosCuatrienios {
    default_historico() {
        return {
            distribucion_edad_congreso_todos_cuatrienios: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Distribución de edad en el congreso por cuatrienio"
                    },
                    "yAxis": {
                        "title": {
                            "text": null
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        }
                    },
                    tooltip: {
                        headerFormat: '<b> {point.key} años</b><br>',
                        valueDecimals: 2
                    },
                    "series": [],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
    colors(){
        return ['#66CCFF', '#17789E', '#6AEEB0', '#379E80'];
    }
}

export default new VarHistoricoDistribucionEdadCongresoTodosCuatrienios();

//  End HistoricoDistribucionEdadCongresoTodosCuatrienios
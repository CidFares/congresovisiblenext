// VarCongresoHoyTop10TemasProyectosLey

class VarCongresoHoyTop10TemasProyectosLey {
    default_congreso_hoy() {
        return {
            top_10_temas_proyectos_ley: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    title: {
                        text: ""
                    },
                    subtitle: {
                        text: ""
                    },
                    yAxis: {
                        title: {
                            text: []
                        },
                        type: "linear"
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            label: {
                                enabled: false
                            },
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            turboThreshold: 0,
                            showInLegend: false
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        }
                    },
                    series: [{
                        group: "group",
                        data: [],
                        type: "treemap"
                    }
                    ],
                    xAxis: {
                        type: "category",
                        title: {
                            text: "tema"
                        },
                        categories: null
                    },
                    tooltip: {
                        pointFormat: "{point.name} <br> <b>{point.n}<\/b> <b> ({point.porc}%)<\/b>"
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCongresoHoyTop10TemasProyectosLey();

//  End VarCongresoHoyTop10TemasProyectosLey
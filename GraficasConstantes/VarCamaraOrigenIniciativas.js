// CamaraOrigenIniciativas

class VarCamaraOrigenIniciativas {
    default_camara() {
        return {
            origen_iniciativa: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        reflow: true,
                        style: {
                            fontFamily: "var(--font-lato)"
                        }
                    },
                    title: {
                        text: "Origen de las iniciativas en el Cámara"
                    },
                    subtitle: {
                        text: ""
                    },
                    yAxis: {
                        title: {
                            text: "Total"
                        },
                        type: "linear"
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    boost: {
                        enabled: false
                    },
                    plotOptions: {
                        
                        series: {
                            dataLabels: {
                                style: {
                                    fontFamily: "var(--font-lato)"
                                }
                            },
                            label: {
                                enabled: false
                            },
                            turboThreshold: 0,
                            showInLegend: false
                        },
                        treemap: {
                            layoutAlgorithm: "squarified"
                        },
                        scatter: {
                            marker: {
                                symbol: "circle"
                            }
                        },
                        bar: {
                            borderRadius: 5,
                            colorByPoint: true
                        }
                    },
                    series: [
                        {
                            name: "Iniciativas",
                            type: "bar",
                            group: "group",
                            data: [],
                        },
                    ],
                    xAxis: {
                        type: "category",
                        title: {
                            text: "Iniciativa"
                        }
                    },
                    tooltip: {
                        pointFormat: "<b>{point.n}<\/b>",
                        headerFormat: ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCamaraOrigenIniciativas();

//  End CamaraOrigenIniciativas
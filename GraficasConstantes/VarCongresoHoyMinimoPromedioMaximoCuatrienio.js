// CongresoHoyMinimoPromedioMaximoCuatrienio

class VarCongresoHoyMinimoPromedioMaximoCuatrienio {
    default_congreso_hoy() {
        return {
            minimo_promedio_maximo_cuatrienio: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "inverted": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Cuatrienios "
                    },
                    "yAxis": {
                        "title": {
                            "text": "Mínimo, promedio y máximo de períodos consecutivos"
                        },
                        "tickAmount": 4,
                        "softMax": 3,
                        "type": "linear",
                        "min": 0
                    },
                    "credits": {
                        "enabled": true,
                        text: 'Nota: se toma en cuenta la cantidad de cuatrienios que los congresistas han tenido un curul.',
                        style: {
                            fontSize: "14px",
                            color: "#333"
                        }
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "errorbar": {
                            "showInLegend": false,
                            "tooltip": {
                                "enabled": false
                            }
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": ""
                        }
                    },
                    "tooltip": {
                        "pointFormat": "Media: {point.media2}<br>Mínimo: {point.minimo}<br>Máximo: {point.maxi}"
                    },
                    "colors": ["#0B3B5D", "#17789E"],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCongresoHoyMinimoPromedioMaximoCuatrienio();

//  End CongresoHoyMinimoPromedioMaximoCuatrienio
// HistoricoTop5PartidosCongresistasMayorEdad

class VarHistoricoTop5PartidosCongresistasMayorEdad {
    default_congreso_hoy() {
        return {
            top_5_partidos_congresistas_mayor_edad: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "type": "bar",
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Top 10 partidos con congresistas de mayor edad"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Edad"
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 5,
                            "colorByPoint": true
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": " "
                        }
                    },
                    "tooltip": {
                        "shared": true,
                        "pointFormat": "<br/>Mediana de edad:  <b>{point.y}<\/b>",
                        valueDecimals: 0
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoTop5PartidosCongresistasMayorEdad();

//  End HistoricoTop5PartidosCongresistasMayorEdad
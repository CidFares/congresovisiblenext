// SenadoCongresoHoyPiramidePoblacional

class VarSenadoCongresoHoyPiramidePoblacional {
    default_congreso_hoy() {
        return {
            piramide_poblacional: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "type": "bar",
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Pirámide poblacional"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Total de congresistas"
                        },
                        "tickAmount": 7,
                        "softMin": -200,
                        "softMax": 100,
                        "type": "linear",
                        "reversedStacks": false,
                        "labels": {
                            "formatter":
                                function(){
                                return Math.abs(this.value);
                            }
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "label": {
                                "enabled": false
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "stacking": true
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "Rango de edad"
                        }
                    },
                    "subtitle": {
                        "text": "Senado"
                    },
                    "tooltip": {
                        shared: true,
                        useHTML: true,
                        "pointFormat": "{series.name}: <b>{point.n} <\/b><br/>",
                        "headerFormat": "<span style=\"font-size: 20px\"><b>Rango: {point.key} años<\/span><br/><\/b>"
                    },
                    "colors": ["#0B3B5D", "#17789E"],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarSenadoCongresoHoyPiramidePoblacional();

//  End SenadoCongresoHoyPiramidePoblacional
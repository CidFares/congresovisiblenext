// HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey

class VarHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey {
    default_historico() {
        return {
            numero_iniciativas_gubernamentales_sancionadas_como_ley: {
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Número de iniciativas gubernamentales sancionadas como ley"
                    },
                    "yAxis": {
                        "title": {
                            "text": ""
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 5,
                            "colorByPoint": false
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "bar"
                    }
                    ],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": ""
                        }
                    },
                    "tooltip": {
                        "pointFormat": "{point.n}",
                        "headerFormat": ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoNumeroIniciativasGubernamentalesSancionadasComoLey();

//  End HistoricoNumeroIniciativasGubernamentalesSancionadasComoLey
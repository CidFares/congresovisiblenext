// HistoricoCantidadTemasTodosCuatrienios

class VarHistoricoCantidadTemasTodosCuatrienios {
    default_historico() {
        return {
            cantidad_temas_todos_cuatrienios: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Distribución de temas de los proyectos de ley sancionados por cuatrienio"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Porcentaje"
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": true
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "area": {
                            "stacking": true
                        }
                    },
                    "series": [],
                    "xAxis": {
                        "title": {
                            "text": ""
                        },
                        "crosshair": true,
                        categories: []
                    },
                    "tooltip": {
                        "pointFormat": "<br>{point.tema}: <b>{point.n} <\/b> <b> ({point.porc}%)<\/b><br>",
                        "shared": true
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoCantidadTemasTodosCuatrienios();

//  End HistoricoCantidadTemasTodosCuatrienios
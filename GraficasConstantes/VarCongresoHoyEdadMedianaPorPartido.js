// CongresoHoyEdadMedianaPorPartido

class VarCongresoHoyEdadMedianaPorPartido {
    default_congreso_hoy() {
        return {
            edad_mediana_por_partido: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    chart: {
                        type: "lollipop",
                        inverted: true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
        
                    legend: {
                        enabled: false
                    },
        
                    title: {
                        "text": "Edad mediana por partido"
                    },
        
                    tooltip: {
                        shared: true,
                        valueSuffix: '',
                        formatter:function(){
                            return this.points[0].point.y+' años';
                        }
                    },
        
                    xAxis: {
                        type: "category",
                        allowDecimals: false,
                        labels: {
                            step: 1
                        },
                        title:{
                            text: "Partido"
                        }
                    },

                    plotOptions: {
                        series: {
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                        }
                    },
        
                    yAxis: {
                        title: {
                            "text": "Edad"
                        },
                    },
        
                    series: [
                        {
                            data: []
                        }
                    ],
                    credits: {
                        text: ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarCongresoHoyEdadMedianaPorPartido();

//  End CongresoHoyEdadMedianaPorPartido
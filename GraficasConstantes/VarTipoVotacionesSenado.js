// TipoVotacionesSenado

class VarTipoVotacionesSenado {
    votacion() {
        return {
            tipo_votaciones_senado: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    colors: ["rgb(6, 38, 71)"],
                    "title": {
                        "text": "Tipo de votaciones"
                    },
                    "subtitle": {
                        text: "Cámara de Representantes"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Total"
                        },
                        "type": "linear",
                        max: 100,
                        min: 0,
                        labels: {
                            format: "{value:,.0f}%"
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 5,
                            "colorByPoint": true
                        }
                    },
                    series: [
                        {
                            name: "Iniciativas",
                            "type": "bar",
                            "group": "group",
                            data: [],
                        },
                    ],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "Tipo"
                        }
                    },
                    "tooltip": {
                        "pointFormat": " <b> {point.n}%<\/b>",
                        "headerFormat": ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    }
}

export default new VarTipoVotacionesSenado();

//  End TipoVotacionesSenado
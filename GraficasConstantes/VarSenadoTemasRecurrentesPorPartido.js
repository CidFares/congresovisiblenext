// SenadoTemasRecurrentesPorPartido

class VarSenadoTemasRecurrentesPorPartido {
    default_senado() {
        return {
            temas_recurrentes_por_partido: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                    tipo_proyecto_ley: {
                        item: {
                            value: "",
                            label: "Seleccione un tipo de proyecto",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Temas recurrentes por partido político en el Senado"
                    },
                    "yAxis": {
                        "title": {
                            "text": []
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": [{
                        name:"Partido",
                        "group": "group",
                        "data": [],
                        "type": "sankey"
                    }
                    ],
                    "xAxis": {
                        "type": null,
                        "title": {
                            "text": []
                        },
                        "categories": null,
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarSenadoTemasRecurrentesPorPartido();

//  End SenadoTemasRecurrentesPorPartido
// HistoricoProporcionDeMujeresPorCuatrienio

class VarHistoricoProporcionDeMujeresPorCuatrienio {
    default_historico() {
        return {
            proporcion_de_mujeres_por_cuatrienio: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    }
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "type": "treemap",
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Proporción de mujeres por partido"
                    },
                    "yAxis": {
                        "title": {
                            "text": null
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        }
                    },
                    "series": [{
                        "data": [],
                        "allowDrillToNode": true,
                        "levelIsConstant": true,
                        "textOverflow": "clip",
                        "dataLabels": {
                            "color": "white"
                        },
                        "levels": [{
                            "level": 1,
                            "borderWidth": 1,
                            "dataLabels": {
                                "enabled": false,
                                "verticalAlign": "top",
                                "align": "left",
                                "style": {
                                    "fontSize": "12px",
                                    "textOutline": false
                                }
                            }
                        }, {
                            "level": 2,
                            "borderWidth": 0,
                            "dataLabels": {
                                "enabled": false
                            }
                        }
                        ]
                    }
                    ],
                    "colors": ["transparent"],
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };

    colors(){
        return ["#4B0055", "#4B0059", "#4A075C", "#491160", "#481964", "#471F68", "#4A075C",
        "#45256B", "#432A6F", "#402F72", "#3D3476", "#3A3979", "#353E7C", "#30437F", "#294882",
        "#204C85", "#125187", "#00568A", "#005A8C", "#005F8E", "#006390", "#006791", "#006C93",
        "#007094", "#007495", "#007896", "#007D97", "#008197", "#008598", "#008998", "#008C98",
        "#009097", "#009497", "#009896", "#009B95", "#009F94", "#00A293", "#00A691", "#00A98F",
        "#00AC8D", "#00B08B", "#00B389", "#00B686", "#00B983", "#00BC80", "#00BE7D", "#00C179",
        "#00C476", "#21C672", "#3AC96D", "#4CCB69", "#5BCE64", "#68D060", "#74D25B", "#80D456",
        "#8BD650", "#96D84B", "#A1D946", "#ABDB40", "#B5DC3B", "#BEDE36", "#C8DF32", "#D1E02E",
        "#DBE12C", "#E4E22B", "#ECE32C", "#F5E32F", "#FDE333"
        ];
    }
}

export default new VarHistoricoProporcionDeMujeresPorCuatrienio();

//  End HistoricoProporcionDeMujeresPorCuatrienio
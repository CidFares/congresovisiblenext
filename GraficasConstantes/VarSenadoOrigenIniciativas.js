// SenadoOrigenIniciativas

class VarSenadoOrigenIniciativas {
    // default_theme() {
    //     return {
    //         "colors": ["#062647", "#0B3B5D", "#0D4568", "#0F4F73", "#136489", "#17789E", "#3B90B5", "#5EA8CC", "#5EA8CC", "#82C0E3", "#56A2C7", "#94CCEF", "#A5D8FA", "#C1E3FA", "#DCEEFA", "#E3F2FE"],
    //         "chart": {
    //             "style": {
    //                 "fontFamily": "Bell MT",
    //                 "fontSize": "20px"
    //             }
    //         },
    //         "title": {
    //             "style": {
    //                 "color": "#17789E",
    //                 "fontSize": "22px",
    //                 "fontWeight": "bold"
    //             }
    //         },
    //         "subtitle": {
    //             "style": {
    //                 "color": "#666666"
    //             }
    //         },
    //         "legend": {
    //             "itemStyle": {
    //                 "color": "black"
    //             },
    //             "itemHoverStyle": {
    //                 "color": "gray"
    //             }
    //         },
    //         "tooltip": {
    //             "borderWidth": 0,
    //             "shadow": false,
    //             "headerFormat": "<span style=\"font-size: 20px\"><b>{point.key}<\/span><br/><\/b>",
    //             "shape": "square",
    //             "style": {
    //                 "fontSize": "16px"
    //             }
    //         },
    //         "yAxis": {
    //             "lineWidth": 3,
    //             "title": {
    //                 "style": {
    //                     "fontSize": "16px"
    //                 }
    //             },
    //             "tickAmount": 5,
    //             "labels": {
    //                 "style": {
    //                     "fontSize": "15px"
    //                 }
    //             }
    //         },
    //         "xAxis": {
    //             "lineWidth": 0,
    //             "title": {
    //                 "style": {
    //                     "fontSize": "16px"
    //                 }
    //             },
    //             "labels": {
    //                 "style": {
    //                     "fontSize": "18px"
    //                 }
    //             }
    //         },
    //         "plotOptions": {
    //             "bar": {
    //                 "borderRadius": 5
    //             },
    //             "scatter": {
    //                 "marker": {
    //                     "radius": 8
    //                 }
    //             },
    //             "treemap": {
    //                 "borderRadius": 5,
    //                 "colorByPoint": true,
    //                 "dataLabels": {
    //                     "style": {
    //                         "fontFamily": "Bell MT",
    //                         "fontSize": "16px"
    //                     }
    //                 }
    //             },
    //             "errorbar": {
    //                 "maxPointWidth": 20
    //             }
    //         }
    //     }
    // }

    default_senado() {
        return {
            origen_iniciativa: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Seleccione una legislatura",
                        },
                        data: [],
                        error: "",
                    },
                    tipo_proyecto_ley: {
                        item: {
                            value: "",
                            label: "Seleccione un tipo de proyecto",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Origen de las iniciativas en el Senado"
                    },
                    "yAxis": {
                        "title": {
                            "text": "Total"
                        },
                        "type": "linear"
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        },
                        "bar": {
                            "borderRadius": 5,
                            "colorByPoint": true
                        }
                    },
                    series: [
                        {
                            name: "Iniciativas",
                            "type": "bar",
                            "group": "group",
                            data: [],
                        },
                    ],
                    "xAxis": {
                        "type": "category",
                        "title": {
                            "text": "Iniciativa"
                        }
                    },
                    "tooltip": {
                        "pointFormat": "<b>{point.n}<\/b>"
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    }
}

export default new VarSenadoOrigenIniciativas();

//  End SenadoOrigenIniciativas
// ValueBoxAudienciasPublicasCitadas

class ValueBoxAudienciasPublicasCitadas {
    value_box() {
        return {
            audiencias_publicas_citadas: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Seleccione un cuatrienio",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    corporacion: {
                        item: {
                            value: "",
                            label: "Ver ambas",
                        },
                        data: [],
                        error: "",
                    },
                },
                grafica: {
                    "chart": {
                        "reflow": true,
                        "backgroundColor": "#023047",
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        },
                        "height": "250px"
                    },
                    "title": {
                        "text": "Audiencias públicas citadas",
                        "align": "left",
                        "style": {
                            "color": "#FFF"
                        }
                    },
                    "yAxis": {
                        "title": {
                            "text": "n",
                            "enabled": false
                        },
                        lineWidth: 0,
                        "type": "linear",
                        "gridLineWidth": 0,
                        "labels": {
                            "enabled": false
                        }
                    },
                    "credits": {
                        "enabled": false
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "turboThreshold": 0,
                            "showInLegend": false,
                            "lineWidth": 8
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": {
                        "label": {
                            "enabled": false
                        },
                        "dataLabels": {
                            "style": {
                                "fontFamily": "var(--font-lato)"
                            }
                        },
                        "marker": {
                            "radius": 0
                        },
                        "turboThreshold": 0,
                        "showInLegend": false,
                        "lineWidth": 8
                    },
                    "xAxis": {
                        "title": {
                            "text": "mes",
                            "enabled": false
                        },
                        "lineWidth": 0,
                        "labels": {
                            "enabled": false
                        },
                        "tickWidth": 0
                    },
                    "subtitle": {
                        "text": "",
                        "style": {
                            "fontSize": "100px",
                            "color": "#FFF"
                        },
                        "align": "left"
                    },
                    "colors": ["#FFF"],
                    tooltip: {
                        formatter: function() {
                            let  date = new Date(this.point.mes);
                            let  customDate = date.toLocaleString('default', { month: 'long',  year: "numeric" });
                            return  customDate +'<br/>' +
                                this.point.n + ' audiencias.';
                        }
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new ValueBoxAudienciasPublicasCitadas();

//  End ValueBoxAudienciasPublicasCitadas
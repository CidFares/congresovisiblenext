// VarVotacionesPorIniciativaSenado

class VarVotacionesPorIniciativaSenado {
    votacion() {
        return {
            votacion_por_iniciativa_senado: {
                combo: {
                    cuatrienio: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    },
                    legislatura: {
                        item: {
                            value: "",
                            label: "Ver todas",
                        },
                        data: [],
                        error: "",
                    }
                },
                grafica: {
                    chart: {
                        type: 'bar',
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        },
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Stacked bar chart'
                    },
                    xAxis: {
                        categories: []
                    },
                    tooltip: {
                        headerFormat: "<b>{point.key}</b><br>",
                        pointFormat: "{series.name}: {point.y}% <br>",
                        shared: true
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        labels: {
                            format: "{value:,.0f}%"
                        },
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    "credits": {
                        "enabled": false
                    },
                    colors: ["rgb(201, 201, 201)", "rgb(165, 95, 116)", "rgb(41, 82, 74)"],
                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                        }
                    },
                    series: [],
                },
            },
        };
    };
}

export default new VarVotacionesPorIniciativaSenado();

//  End VarVotacionesPorIniciativaSenado
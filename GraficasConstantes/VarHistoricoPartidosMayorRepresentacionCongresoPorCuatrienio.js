// HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio

class VarHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio {
    default_historico() {
        return {
            partidos_mayor_representacion_congreso_por_cuatrienio: {
                grafica: {
                    "chart": {
                        "reflow": true,
                        "style": {
                            "fontFamily": "var(--font-lato)"
                        }
                    },
                    "title": {
                        "text": "Partidos con mayor representación en el Congreso, por cuatrienio"
                    },
                    "yAxis": {
                        "title": {
                            "text": []
                        }
                    },
                    "credits": {
                        "enabled": true,
                        text: 'Nota: se toman en cuenta los partidos con una representación nominal mayor a la del promedio.',
                        style: {
                            fontSize: "14px",
                            color: "#333"
                        }
                    },
                    "exporting": {
                        "enabled": false
                    },
                    "boost": {
                        "enabled": false
                    },
                    "plotOptions": {
                        "series": {
                            "label": {
                                "enabled": false
                            },
                            "dataLabels": {
                                "style": {
                                    "fontFamily": "var(--font-lato)"
                                }
                            },
                            "turboThreshold": 0,
                            "showInLegend": false,
                            "dataLabels": {
                                "style": {
                                    "fontSize": "18px"
                                }
                            }
                        },
                        "treemap": {
                            "layoutAlgorithm": "squarified"
                        },
                        "scatter": {
                            "marker": {
                                "symbol": "circle"
                            }
                        }
                    },
                    "series": [{
                        "group": "group",
                        "data": [],
                        "type": "sankey"
                    }
                    ],
                    "xAxis": {
                        "type": null,
                        "title": {
                            "text": []
                        },
                        "categories": null
                    },
                    "tooltip": {
                        "headerFormat": ""
                    },
                    lang: {
                        noData: "Sin datos",
                    },
                    noData: {
                        style: {
                            fontWeight: "bold",
                            fontSize: "15px",
                            color: "#303030",
                        },
                    },
                },
            },
        };
    };
}

export default new VarHistoricoPartidosMayorRepresentacionCongresoPorCuatrienio();

//  End HistoricoPartidosMayorRepresentacionCongresoPorCuatrienio
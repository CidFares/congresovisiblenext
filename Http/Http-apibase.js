import axios from "axios";
import AuthLogin from "../Utils/AuthLogin";
const auth = new AuthLogin();
const FormData = require('form-data');
let instance = axios.create({
    //  baseURL: "http://localhost:8000/api",
    // baseURL: "https://cvcliente.cidfares.com/aservice/api",
    // baseURL: 'https://camstoop.com/aservice/api',
    baseURL: 'https://apicongresovisible.uniandes.edu.co/api',
    // baseURL: 'https://congresovisible.uniandes.edu.co/aservice/api',
    headers: {
        Authorization: "Bearer " + auth.keyJwt(),
        "Content-Type": "multipart/form-data",
    },
});

// Request interceptor for API calls
instance.interceptors.request.use(
    async (config) => {
        if (config.data) {
            let data = ToFormData(config.data);
            config.data = data;
        }
        return config;
    },
    (error) => {
        Promise.reject(error);
    }
);

function ToFormData(data) {
    if (data instanceof FormData) return data;
    let formData = new FormData();
    Object.keys(data).forEach((key) => {
        if (Array.isArray(data[key])) {
            let array = data[key];
            for (let i = 0; i < array.length; i++) {
                let obj = array[i];
                for (var objKey in obj) {
                    if (obj[objKey] === null) {
                        obj[objKey] = "";
                    }
                    formData.append(`${key}[${i}][${objKey}]`, obj[objKey]);
                }
            }
        } else if (typeof data[key] === "object") {
            if (data[key] === null) {
                data[key] = "";
                formData.append(key, data[key]);
            } else {
                let obj = data[key];

                for (var objKey in obj) {
                    if (obj[objKey] === null) {
                        obj[objKey] = "";
                    }
                    formData.append(`${key}[${objKey}]`, obj[objKey]);
                }
            }
        } else {
            if (data[key] === null) {
                data[key] = "";
            }
            formData.append(key, data[key]);
        }
    });

    return formData;
}

export { instance as default };
